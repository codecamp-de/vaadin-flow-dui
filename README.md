
# Vaadin Flow UI Toolkit 

![Latest Release](https://img.shields.io/maven-central/v/de.codecamp.vaadin/vaadin-flow-dui)
[![Latest Snapshot](https://img.shields.io/nexus/s/de.codecamp.vaadin/vaadin-flow-dui?server=https%3A%2F%2Foss.sonatype.org)](https://codecamp.de)
---

This UI toolkit for Vaadin Flow consists of two major components that you can use individually or together complementing each other:

- [Declarative UI](#declarative-ui-for-vaadin-flow):  Declarative (HTML-style) UIs for Vaadin Flow.  
- [Fluent API](#fluent-api-for-vaadin-flow): Fluent API for Vaadin Flow components.


## Declarative UI for Vaadin Flow

The goal is to externalize the component trees of views and have them as much more readable HTML instead of cluttering up the Java code. The Java code itself then only references the components it actually needs to access.


### How Does It Work?

A component or view based on DUI consists of two parts:
 * an HTML-like template file () containing the component tree. The syntax is largely identical to that of Vaadin's Web Components.
 * a Java class, usually extending `TemplateComposite`, receiving components mapped from the template once created.

Per default, the HTML file is expected beside the Java class - same name, but `.dui` or `.html` extension - in the classpath. The HTML is parsed *on the server-side*. Components are created from HTML elements using `ComponentFactory` implementations. And `ComponentPostProcessor` implementations are used to apply mapping and other logic to multiple Component types, e.g. for the shared attributes of [mixin interfaces](https://vaadin.com/docs/latest/flow/creating-components/mixins). The standard Vaadin components work out of the box, but it's easy to add your own elements.

> See further down for some [pros and cons of this add-on vs Lit-based templates](#dui-vs-lit).


### Getting Started

To get started add the following dependency to your project:

```xml
<dependency>
  <groupId>de.codecamp.vaadin</groupId>
  <artifactId>vaadin-flow-dui</artifactId>
  <version>5.1.0</version>
</dependency>
```

Whenever the Vaadin application is not running in production mode, this add-on will automatically register a view for the route path `component-declarations`. This view displays all components and attributes currently declared via the registered factories and post-processors. This replaces the static documentation that was previously available here as it is much more likely to be up-to-date and will also include your own factories and post-processors.


### Basic Example

This is a basic example for a DUI-based view that also demonstrates how components can be mapped from and into the template. 

`@Mapped` is used to mark fields where components from the template will be mapped into. `@Slotted` is used to mark fields that provide components to insert into predefined `<slot>`s in your template. Both assume the ID/name to be the same as the name of their corresponding field. But both annotations do allow you to explicitly specify an ID/name when necessary.

```java
// src/main/java/my/package/DemoView.java

@Route("demo")
public class DemoView
  extends TemplateComposite
{

  /**
   * Created in the template and mapped to this field.
   */
  @Mapped
  private Button shuffleButton;

  /**
   * Created here and mapped into the template.
   */
  @Slotted
  private Grid<Locale> localeGrid = new Grid<>(Locale.class);


  public DemoView()
  {
  }


  @Override
  protected void contentInitialized()
  {
    localeGrid.removeAllColumns();
    localeGrid.addColumn(Locale::toLanguageTag).setHeader("Language Tag");
    localeGrid.addColumn("displayLanguage");
    localeGrid.addColumn("displayCountry");
    localeGrid.addColumn("displayVariant");
    localeGrid.setDetailsVisibleOnClick(true);
    localeGrid.setItemDetailsRenderer(new ComponentRenderer<>(locale -> {
      LocaleDetails details = new LocaleDetails();
      details.setLocale(locale);
      return details;
    }));


    shuffleButton.addClickListener(e -> shuffleLocales());

    localeGrid.setItems(Locale.getAvailableLocales());
  }

  private void shuffleLocales()
  {
    List<Locale> locales = Arrays.asList(Locale.getAvailableLocales());
    Collections.shuffle(locales);
    localeGrid.setItems(locales);
  }


  public static class LocaleDetails
    extends FragmentComposite
  {

    @Mapped
    private TextField info;


    public void setLocale(Locale locale)
    {
      info.setValue(locale.getDisplayLanguage(locale) + " / " + locale.getDisplayCountry(locale)
          + " / " + locale.getDisplayVariant(locale));
    }

  }

}
```

```html
<!-- src/main/resources/my/package/DemoView.html -->

<vaadin-vertical-layout size-full>
  <slot name="localeGrid"></slot>
  <vaadin-button id="shuffleButton" theme="primary">Shuffle</vaadin-button>
</vaadin-vertical-layout>

<fragment id="LocaleDetails">
  <vaadin-horizontal-layout width-full>
    <vaadin-text-field id="info" readonly width-full>
    </vaadin-text-field>
  </vaadin-horizontal-layout>
</fragment>
```


### Template Sources

Per default, the template files are expected besides their respective Java class in the classpath. The following filename extensions are looked for: `.dui`, `.html`, `.ts`, `.js`.

But those extensions don't imply actually different formats. Especially `.ts` and `.js` would just allow JavaScript-based Lit templates to be picked up which might allow you to still make use of Vaadin Designer, as DUI attempts to stay as close to the Web Component API of Vaadin's components as possible and reasonable. But this isn't really tested and would probably require you to avoid any of the custom attributes and components that DUI provides.

Other than from the classpath, templates can also be loaded from any other source by providing additional `TemplateResolver` implementations. 
In Spring Boot applications they'll be picked up automatically if they're declared as beans. In other environments they can be registered manually:

```
TemplateEngine.getAdditionalResolvers().add(...)
```


### Adding Custom Elements and Attributes

Implementing new factories (`ComponentFactory`) and post processors (`ComponentPostProcessor`) should for the most part be painless. Take a look at the source code to see plenty of examples.

In Spring Boot applications it's enough to simply declare them as beans to have them automatically be picked up. In other environments they can be registered manually:
```
TemplateEngine.getAdditionalFactories().add(...)
TemplateEngine.getAdditionalPostProcessors().add(...)
```

Annotate your factories and post-processors with `@DuiComponent` to describe them as best as possible. These declarations are all collected at runtime and shown in a special view that is automatically registered whenever the Vaadin application does not run in production mode. This view is registered for the route path `component-declarations`.


### DUI vs Lit

There is some overlap with [Lit-based templates](https://vaadin.com/docs/latest/create-ui/templates), but the goals and approach are different.

 * DUI has none of the [limitations Lit templates have](https://vaadin.com/docs/latest/flow/templates/limitations). It's completely interpreted and converted to a Component-tree on the server-side. So one can expect exactly the same behavior as using the Java API directly.
 * DUI likely has a bigger memory overhead on the server-side, because every element is represented there, whether you need to actually access it or not. But it doesn't have any overhead compared to purely using Vaadin's Java API to build your UI.
 * DUI does not support JavaScript or CSS, and is not ideal when you intend to also make considerable use of actual HTML elements (as they will all be represented by a Component). The focus is purely on the composition of Components to build a UI. Lit is likely the better option when creating new or heavily customized Components.
 * While the HTML-like syntax of DUI for each Component is mostly compatible with their respective Web Component API (as it is also used in Lit templates), it adds a few conveniences that you might already know from Vaadin 8 but which aren't available in Lit templates. E.g. adding `size-full` is more convenient than using the equivalent CSS.
 * DUI is extensible. Add your own custom elements (that can be anything, not just Web Components) and attributes (even to existing Components).



## Fluent API for Vaadin Flow

This is a fluent-style API for Vaadin's Components. It's implemented as wrappers, so it can be used to create new components or configure existing ones.


### Getting Started

To get started add the following dependency to your project:

```xml
<dependency>
  <groupId>de.codecamp.vaadin</groupId>
  <artifactId>vaadin-flow-fluent</artifactId>
  <version>5.1.0</version>
</dependency>
```

The easiest way to use the fluent API is via the static factory methods of `de.codecamp.vaadin.fluent.Fluent`.

```java
import static de.codecamp.vaadin.fluent.Fluent.*;
```

Ideally you would configure your IDE to automatically import them. So e.g. in Eclipse's preferences add that type to the list in Java / Editor / Content Assist / Favorites.


### The Basics

This is a basic example of what using the fluent API can look like:

```java
// create a new button; assign it for later use
Button button = button().text("Label").primary().onClick(event ->
{
  notification().text("Clicked.").positionMiddle().open();
}).get();
```

```java
// configure an existing button
fluent(someButton).text("Label").primary();
```

The wrapped `Component` can be accessed directly using `get()` at the end of the chain e.g. to assign it to a field for later use. It's generally better to store the component itself instead of its fluent wrapper.

For short-term access to the wrapped component without breaking the `apply(...)` methods can be used:

```java
fluentButton().text("Delete").apply(c ->
{
  ...
}).onClick(...
```

### Reusable Configurers

The `apply(...)` methods can also be used to apply reusable configurer functions:

```java
class CommonButtons
{
  public static void deleteButton(Button button)
  {
    button.setText("Delete");
    button.addThemeVariants(ButtonVariant.LUMO_ERROR);
    // OR
    fluent(button).text("Delete").error();
  }
}

...

fluentButton().apply(CommonButtons::deleteButton).onClick(...
```


### Hierarchical Layouts

The fluent API can also make creating component hierarchies and layouting them much more convenient. Every `FluentComponent` can be added to a container via the overloaded method `addTo(...)`, where the optional second parameter lets you directly change the layout configuration for the component.

So instead of:

```java
VerticalLayout layout = ...

Button button = new Button();
layout.add(button);
layout.setAlignSelf(Alignment.CENTER, button);
```
you can just write

```java
VerticalLayout layout = ...

button().addTo(layout, l -> l.alignCenter());
```

A fuller example could look like this:

```java
VerticalLayout content = verticalLayout().sizeFull().padding().spacing().get();

button().icon(VaadinIcon.REFRESH).text("Refresh")
  .onClick(event -> {
    ...
  })
  .addTo(content, l -> l.alignEnd());

Grid<MyItem> grid = grid(MyItem.class).addTo(content, l -> l.sizeFull()).get();
grid.addColumn(...
```

### The Fluent API Design in General

Here's a few points on how the fluent API was designed in general:

- For most Vaadin components (and mix-in interfaces) there is an equivalent fluent type prefixed with `Fluent`. So e.g. there's a `FluentButton` for `Button`.
- For setters the `set` has been omitted from the fluent equivalent. E.g.: `setId(...)` -> `id(...)`.
- For event listener registration methods the `add` has been replaced with `on` and `Listener` has been omitted. E.g.: `addClickListener(...)` -> `onClick(...)`. If you require the `Registration` you need to register the listener the old fashioned way directly on the `Component`.
- The Lumo-related theme variants are a simple method call in the fluent API:
    - Where the variants are mutually exclusive the other variants are transparently removed. E.g.: `ButtonVariant#LUMO_PRIMARY` -> `primary()` removes `LUMO_TERTIARY` and `LUMO_TERTIARY_INLINE`.  
There'a also always a gap because the default doesn't have its own variant and is defined by the absence of all others. E.g. for buttons there's `LUMO_PRIMARY`, `LUMO_TERTIARY` and `LUMO_TERTIARY_INLINE`, but no `LUMO_SECONDARY`. So for the fluent API the gap is closed by introducing an artificial variant. E.g. `secondary()` will not add a theme variant but only remove all other related ones.
    - For singleton variants there's one fluent method to activate them and one with boolean parameter to toggle them. E.g.: `ButtonVariant#LUMO_ICON` -> `icon()` and `icon(boolean)`.
- Methods with an enum parameter will (usually) be expanded for the fluent API. So there will be one additional method per enum element.

