package de.codecamp.vaadin.components.fluent;


import static de.codecamp.vaadin.fluent.Fluent.fluent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import de.codecamp.vaadin.components.ButtonBar;
import de.codecamp.vaadin.fluent.visandint.FluentButton;
import java.util.function.Consumer;


public class FluentButtonBar
{

  /**
   * @return the new fluent button
   * @see ButtonBar#addButtonToStart()
   */
  public FluentButton buttonStart()
  {
    return fluent(get().addButtonToStart());
  }

  /**
   * @param buttonConfigurer
   *          the configurer to apply to the button
   * @return this
   * @see ButtonBar#addButtonToStart()
   */
  public FluentButtonBar buttonStart(Consumer<? super FluentButton> buttonConfigurer)
  {
    buttonConfigurer.accept(buttonStart());
    return this;
  }

  /**
   * @param buttonConfigurers
   *          the configurers to apply to the button
   * @return this
   * @see ButtonBar#addButtonToStart()
   */
  @SafeVarargs
  public final FluentButtonBar buttonStart(
      Consumer<? super FluentButton>... buttonConfigurers)
  {
    FluentButton fluentButton = buttonStart();
    for (Consumer<? super FluentButton> configurer : buttonConfigurers)
      configurer.accept(fluentButton);
    return this;
  }

  /**
   * @return this
   * @see ButtonBar#addToStart(Component)
   */
  public FluentButtonBar componentStart(Component component)
  {
    get().addToStart(component);
    return this;
  }

  /**
   * @return this
   * @see ButtonBar#addSpacerToStart()
   */
  public FluentButtonBar spacerStart()
  {
    get().addSpacerToStart();
    return this;
  }


  /**
   * @return the new fluent button
   * @see ButtonBar#addButtonToEnd()
   */
  public FluentButton buttonEnd()
  {
    return fluent(get().addButtonToEnd());
  }

  /**
   * @param buttonConfigurer
   *          the configurer to apply to the button
   * @return this
   * @see ButtonBar#addButtonToEnd()
   */
  public FluentButtonBar buttonEnd(Consumer<? super FluentButton> buttonConfigurer)
  {
    buttonConfigurer.accept(buttonEnd());
    return this;
  }

  /**
   * @param buttonConfigurers
   *          the configurers to apply to the button
   * @return this
   * @see ButtonBar#addButtonToEnd()
   */
  @SafeVarargs
  public final FluentButtonBar buttonEnd(
      Consumer<? super FluentButton>... buttonConfigurers)
  {
    FluentButton fluentButton = buttonEnd();
    for (Consumer<? super FluentButton> configurer : buttonConfigurers)
      configurer.accept(fluentButton);
    return this;
  }

  /**
   * @return this
   * @see ButtonBar#addToEnd(Component)
   */
  public FluentButtonBar componentEnd(Component component)
  {
    get().addToEnd(component);
    return this;
  }

  /**
   * @return this
   * @see ButtonBar#addSpacerToEnd()
   */
  public FluentButtonBar spacerEnd()
  {
    get().addSpacerToEnd();
    return this;
  }

}
