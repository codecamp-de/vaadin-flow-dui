package de.codecamp.vaadin.components.fluent;


import de.codecamp.vaadin.components.GridLayout;
import de.codecamp.vaadin.components.Notice;
import de.codecamp.vaadin.components.NoticeVariant;


public class FluentCc
{

  /**
   * Returns a fluent wrapper configuring a new new pre-configured {@link Notice} instance.
   * 
   * @return a new fluent wrapper with a new component instance
   * @see Notice
   * @see NoticeVariant#LUMO_SUCCESS
   */
  public static FluentNotice noticeStatusSuccess()
  {
    return new FluentNotice(Notice.createSuccess());
  }

  /**
   * Returns a fluent wrapper configuring a new new pre-configured {@link Notice} instance.
   * 
   * @return a new fluent wrapper with a new component instance
   * @see Notice
   * @see NoticeVariant#LUMO_INFO
   */
  public static FluentNotice noticeStatusInfo()
  {
    return new FluentNotice(Notice.createInfo());
  }

  /**
   * Returns a fluent wrapper configuring a new new pre-configured {@link Notice} instance.
   * 
   * @return a new fluent wrapper with a new component instance
   * @see Notice
   * @see NoticeVariant#LUMO_WARNING
   */
  public static FluentNotice noticeStatusWarning()
  {
    return new FluentNotice(Notice.createWarning());
  }

  /**
   * Returns a fluent wrapper configuring a new new pre-configured {@link Notice} instance.
   * 
   * @return a new fluent wrapper with a new component instance
   * @see Notice
   * @see NoticeVariant#LUMO_ERROR
   */
  public static FluentNotice noticeStatusError()
  {
    return new FluentNotice(Notice.createError());
  }


  public static FluentGridLayout gridLayout()
  {
    return new FluentGridLayout();
  }

  public static FluentGridLayout fluent(GridLayout component)
  {
    return new FluentGridLayout(component);
  }

}
