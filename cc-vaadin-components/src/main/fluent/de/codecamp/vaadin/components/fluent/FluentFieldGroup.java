package de.codecamp.vaadin.components.fluent;


import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.fluent.FluentHasSingleComponentContainerExtension;


public class FluentFieldGroup
  implements
    FluentHasSingleComponentContainerExtension<FieldGroup, FluentFieldGroup, FluentFieldGroupLayoutConfig>
{

  @Override
  public FluentFieldGroupLayoutConfig getLayoutConfigFor(Component... components)
  {
    return new FluentFieldGroupLayoutConfig(get(), components);
  }

}
