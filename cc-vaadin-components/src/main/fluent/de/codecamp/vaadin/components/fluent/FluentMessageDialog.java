package de.codecamp.vaadin.components.fluent;


import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.dialog.Dialog.DialogCloseActionEvent;
import com.vaadin.flow.component.dialog.Dialog.OpenedChangeEvent;
import de.codecamp.vaadin.components.MessageDialog;
import de.codecamp.vaadin.components.MessageDialog.ColorTheme;
import java.util.function.Consumer;


public class FluentMessageDialog
{

  /**
   * @return the new fluent button
   * @see MessageDialog#addButton()
   */
  public FluentMessageDialogButton button()
  {
    return new FluentMessageDialogButton(get().addButton(), get());
  }

  /**
   * @param buttonConfigurer
   *          the configurer to apply to the button
   * @return this
   * @see MessageDialog#addButton()
   */
  public FluentMessageDialog button(Consumer<? super FluentMessageDialogButton> buttonConfigurer)
  {
    buttonConfigurer.accept(button());
    return this;
  }

  /**
   * @param buttonConfigurers
   *          the configurers to apply to the button
   * @return this
   * @see MessageDialog#addButton()
   */
  @SafeVarargs
  public final FluentMessageDialog button(
      Consumer<? super FluentMessageDialogButton>... buttonConfigurers)
  {
    FluentMessageDialogButton fluentButton = button();
    for (Consumer<? super FluentMessageDialogButton> configurer : buttonConfigurers)
      configurer.accept(fluentButton);
    return this;
  }

  /**
   * @return this
   * @see MessageDialog#addSpacer()
   */
  public FluentMessageDialog spacer()
  {
    get().addSpacer();
    return this;
  }

  /**
   * @return the new fluent button
   * @see MessageDialog#addButtonToSecondary()
   */
  public FluentMessageDialogButton buttonSecondary()
  {
    return new FluentMessageDialogButton(get().addButtonToSecondary(), get());
  }

  /**
   * @param buttonConfigurer
   *          the configurer to apply to the button
   * @return this
   * @see MessageDialog#addButtonToSecondary()
   */
  public FluentMessageDialog buttonSecondary(
      Consumer<? super FluentMessageDialogButton> buttonConfigurer)
  {
    buttonConfigurer.accept(buttonSecondary());
    return this;
  }

  /**
   * @param buttonConfigurers
   *          the configurers to apply to the button
   * @return this
   * @see MessageDialog#addButtonToSecondary()
   */
  @SafeVarargs
  public final FluentMessageDialog buttonSecondary(
      Consumer<? super FluentMessageDialogButton>... buttonConfigurers)
  {
    FluentMessageDialogButton fluentButton = buttonSecondary();
    for (Consumer<? super FluentMessageDialogButton> configurer : buttonConfigurers)
      configurer.accept(fluentButton);
    return this;
  }

  /**
   * @return this
   * @see MessageDialog#addSpacerToSecondary()
   */
  public FluentMessageDialog spacerSecondary()
  {
    get().addSpacerToSecondary();
    return this;
  }


  /**
   * @return this
   * @see MessageDialog#open()
   */
  public FluentMessageDialog open()
  {
    get().open();
    return this;
  }

  /**
   * @return this
   * @see MessageDialog#close()
   */
  public FluentMessageDialog close()
  {
    get().close();
    return this;
  }


  /**
   * @return this
   * @see MessageDialog#setColorTheme(ColorTheme)
   * @see ColorTheme#STANDARD
   */
  public FluentMessageDialog standard()
  {
    get().setColorTheme(ColorTheme.STANDARD);
    return this;
  }

  /**
   * @return this
   * @see MessageDialog#setColorTheme(ColorTheme)
   * @see ColorTheme#PRIMARY
   */
  public FluentMessageDialog primary()
  {
    get().setColorTheme(ColorTheme.PRIMARY);
    return this;
  }

  /**
   * @return this
   * @see MessageDialog#setColorTheme(ColorTheme)
   * @see ColorTheme#SUCCESS
   */
  public FluentMessageDialog success()
  {
    get().setColorTheme(ColorTheme.SUCCESS);
    return this;
  }

  /**
   * @return this
   * @see MessageDialog#setColorTheme(ColorTheme)
   * @see ColorTheme#WARNING
   */
  public FluentMessageDialog warning()
  {
    get().setColorTheme(ColorTheme.WARNING);
    return this;
  }

  /**
   * @return this
   * @see MessageDialog#setColorTheme(ColorTheme)
   * @see ColorTheme#ERROR
   */
  public FluentMessageDialog error()
  {
    get().setColorTheme(ColorTheme.ERROR);
    return this;
  }

}
