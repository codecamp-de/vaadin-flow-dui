package de.codecamp.vaadin.components.fluent;


import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.fluent.FluentHasSingleComponentContainerExtension;


public class FluentPanel
  implements
    FluentHasSingleComponentContainerExtension<Panel, FluentPanel, FluentPanelLayoutConfig>
{

  @Override
  public FluentPanelLayoutConfig getLayoutConfigFor(Component... components)
  {
    return new FluentPanelLayoutConfig(get(), components);
  }

}
