package de.codecamp.vaadin.components.fluent;


import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.fluent.FluentHasSingleComponentContainerExtension;


public class FluentScrollPane
  implements
    FluentHasSingleComponentContainerExtension<ScrollPane, FluentScrollPane, FluentScrollPaneLayoutConfig>
{

  @Override
  public FluentScrollPaneLayoutConfig getLayoutConfigFor(Component... components)
  {
    return new FluentScrollPaneLayoutConfig(get(), components);
  }

}
