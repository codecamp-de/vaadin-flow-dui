package de.codecamp.vaadin.components;


import static java.util.stream.Collectors.joining;

import com.vaadin.flow.component.ClickNotifier;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasEnabled;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.HasTheme;
import com.vaadin.flow.dom.Element;
import de.codecamp.vaadin.base.css.CssGridProperties;
import de.codecamp.vaadin.base.css.CssValue;
import java.util.stream.Stream;


/**
 * A base-class for layouts that are based on CSS grids.
 *
 * @param <L>
 *          the type of the layout
 */
public abstract class AbstractGridLayout<L extends AbstractGridLayout<L>>
  extends
    Component
  implements
    HasEnabled,
    ClickNotifier<L>,
    HasSize,
    HasTheme
{

  protected AbstractGridLayout()
  {
    setInline(false);
  }

  protected AbstractGridLayout(Element element)
  {
    super(element);
    setInline(false);
  }


  protected void setInline(boolean inline)
  {
    getStyle().set(CssGridProperties.display,
        inline ? CssGridProperties._display_inlineGrid : CssGridProperties._display_grid);
  }

  protected void setAutoFlow(GridAutoFlow gridAutoFlow)
  {
    getStyle().set(CssGridProperties.gridAutoFlow, CssValue.toValue(gridAutoFlow));
  }


  protected void setTemplateColumns(String gridTemplateColumns)
  {
    getStyle().set(CssGridProperties.gridTemplateColumns, gridTemplateColumns);
  }

  protected void setTemplateRows(String gridTemplateRows)
  {
    getStyle().set(CssGridProperties.gridTemplateRows, gridTemplateRows);
  }

  protected void setTemplateAreas(String... rows)
  {
    getStyle().set(CssGridProperties.gridTemplateAreas,
        Stream.of(rows).map(row -> "'" + row + "'").collect(joining("\n")));
  }

  protected void setAutoColumns(String trackSizes)
  {
    getStyle().set(CssGridProperties.gridAutoColumns, trackSizes);
  }

  protected void setAutoRows(String trackSizes)
  {
    getStyle().set(CssGridProperties.gridAutoRows, trackSizes);
  }


  protected void setColumnGap(String columnGap)
  {
    getStyle().set(CssGridProperties.columnGap, columnGap);
  }

  protected void setRowGap(String rowGap)
  {
    getStyle().set(CssGridProperties.rowGap, rowGap);
  }

  protected void setGaps(String gaps)
  {
    setRowGap(gaps);
    setColumnGap(gaps);
  }


  protected void setJustifyItems(GridItemAlignment justifyItems)
  {
    getStyle().set(CssGridProperties.justifyItems, CssValue.toValue(justifyItems));
  }

  protected void setAlignItems(GridItemAlignment alignItems)
  {
    getStyle().set(CssGridProperties.alignItems, CssValue.toValue(alignItems));
  }

  protected void setJustifyContent(GridContentAlignment justifyContent)
  {
    getStyle().set(CssGridProperties.justifyContent, CssValue.toValue(justifyContent));
  }

  protected void setAlignContent(GridContentAlignment alignContent)
  {
    getStyle().set(CssGridProperties.alignContent, CssValue.toValue(alignContent));
  }


  protected void setItemColumnAndRow(Component component, Object columnLineStart,
      Object columnLineEnd, Object rowLineStart, Object rowLineEnd)
  {
    setItemColumn(component, toString(columnLineStart), toString(columnLineEnd));
    setItemRow(component, toString(rowLineStart), toString(rowLineEnd));
  }

  protected void setItemCoordinates(Component childComponent, int column, int row)
  {
    setItemColumnStart(childComponent, toString(column + 1));
    setItemRowStart(childComponent, toString(row + 1));
  }

  protected void setItemCoordinates(Component childComponent, int column, int row, int columnSpan,
      int rowSpan)
  {
    setItemCoordinates(childComponent, column, row);
    setItemColumnAndRowSpan(childComponent, columnSpan, rowSpan);
  }

  protected void setItemColumnStart(Component childComponent, String line)
  {
    childComponent.getElement().getStyle().set(CssGridProperties.gridColumnStart, line);
  }

  protected void setItemColumnEnd(Component childComponent, String line)
  {
    childComponent.getElement().getStyle().set(CssGridProperties.gridColumnEnd, line);
  }

  protected void setItemColumn(Component childComponent, String lineStart, String lineEnd)
  {
    setItemColumnStart(childComponent, lineStart);
    setItemColumnEnd(childComponent, lineEnd);
  }

  protected void setItemRowStart(Component childComponent, String line)
  {
    childComponent.getElement().getStyle().set(CssGridProperties.gridRowStart, line);
  }

  protected void setItemRowEnd(Component childComponent, String line)
  {
    childComponent.getElement().getStyle().set(CssGridProperties.gridRowEnd, line);
  }

  protected void setItemRow(Component childComponent, String lineStart, String lineEnd)
  {
    setItemRowStart(childComponent, lineStart);
    setItemRowEnd(childComponent, lineEnd);
  }

  protected void setItemColumnSpan(Component childComponent, int columnSpan)
  {
    setItemColumnEnd(childComponent, columnSpan > 1 ? "span " + columnSpan : null);
  }

  protected void setItemRowSpan(Component childComponent, int rowSpan)
  {
    setItemRowEnd(childComponent, rowSpan > 1 ? "span " + rowSpan : null);
  }

  protected void setItemColumnAndRowSpan(Component childComponent, int columnSpan, int rowSpan)
  {
    setItemColumnSpan(childComponent, columnSpan);
    setItemRowSpan(childComponent, rowSpan);
  }

  protected void setItemArea(Component childComponent, String area)
  {
    childComponent.getElement().getStyle().set(CssGridProperties.gridArea, area);
  }


  protected void setItemJustify(Component childComponent, GridItemAlignment justifySelf)
  {
    childComponent.getElement().getStyle().set(CssGridProperties.justifySelf,
        CssValue.toValue(justifySelf));
  }

  protected void setItemAlign(Component childComponent, GridItemAlignment alignSelf)
  {
    childComponent.getElement().getStyle().set(CssGridProperties.alignSelf,
        CssValue.toValue(alignSelf));
  }


  private static String toString(Object value)
  {
    return value == null ? null : value.toString();
  }

}
