package de.codecamp.vaadin.components;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasOrderedComponents;
import com.vaadin.flow.component.HasText;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.shared.HasPrefix;
import com.vaadin.flow.component.shared.HasSuffix;
import com.vaadin.flow.component.shared.HasThemeVariant;
import com.vaadin.flow.component.shared.HasTooltip;


@Tag(Badge.TAG)
@JsModule("./components/cc-badge.ts")
public class Badge
  extends
    Component
  implements
    HasOrderedComponents,
    HasPrefix,
    HasSuffix,
    HasText,
    HasThemeVariant<BadgeVariant>,
    HasTooltip
{

  public static final String TAG = "cc-badge";


  public Badge()
  {
  }

  public Badge(String text)
  {
    setText(text);
  }

  public Badge(Component icon, String text)
  {
    setPrefixComponent(icon);
    setText(text);
  }

  public Badge(Component... components)
  {
    add(components);
  }

}
