package de.codecamp.vaadin.components;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasEnabled;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.shared.SlotUtils;
import java.util.Locale;
import java.util.stream.Stream;


/**
 * The button bar provides a layout for buttons and similar components for forms and dialogs where
 * they can be placed at either end of the bar. {@link #addSpacerToEnd() Spacers} can be added to
 * visually separate certain (groups of) buttons.
 */
@Tag(ButtonBar.TAG)
@JsModule("./components/cc-button-bar.ts")
public class ButtonBar
  extends
    Component
  implements
    HasEnabled,
    HasSize
{

  public static final String TAG = "cc-button-bar";

  public static final String SLOT_START = "start";

  public static final String SLOT_END = "end";

  private static final String PROPERTY_ORIENTATION = "orientation";


  public ButtonBar()
  {
  }


  public Orientation getOrientation()
  {
    String propertyValue = getElement().getProperty(PROPERTY_ORIENTATION);
    return Stream.of(Orientation.values())
        .filter(e -> e.name().toLowerCase(Locale.ENGLISH).equals(propertyValue)).findFirst()
        .orElse(Orientation.HORIZONTAL);
  }

  public void setOrientation(Orientation orientation)
  {
    getElement().setProperty(PROPERTY_ORIENTATION,
        (orientation == null || orientation == Orientation.HORIZONTAL) ? null
            : orientation.name().toLowerCase(Locale.ENGLISH));
  }


  /**
   * Adds the given component to the (end of the) start area of the button bar.
   *
   * @param <C>
   *          the component type
   * @param component
   *          the component to add
   * @return the component
   */
  public <C extends Component> C addToStart(C component)
  {
    SlotUtils.addToSlot(this, SLOT_START, component);
    return component;
  }

  /**
   * Adds a new button to the (end of the) start area of the button bar.
   *
   * @return a new button
   */
  public Button addButtonToStart()
  {
    return addToStart(new Button());
  }

  /**
   * Adds a spacer to the (end of the) start area of the button bar. Further components will be
   * added to the next button group.
   */
  public void addSpacerToStart()
  {
    SlotUtils.addToSlot(this, SLOT_START, new Span());
  }


  /**
   * Adds the given component to the (end of the) end area of the button bar.
   *
   * @param <C>
   *          the component type
   * @param component
   *          the component to add
   * @return the component
   */
  public <C extends Component> C addToEnd(C component)
  {
    SlotUtils.addToSlot(this, SLOT_END, component);
    return component;
  }

  /**
   * Adds a new button to the (end of the) end area of the button bar.
   *
   * @return a new button
   */
  public Button addButtonToEnd()
  {
    return addToEnd(new Button());
  }

  /**
   * Adds a spacer to the (end of the) end area of the button bar. Further components will be added
   * to the next button group.
   */
  public void addSpacerToEnd()
  {
    SlotUtils.addToSlot(this, SLOT_END, new Span());
  }


  public enum Orientation
  {
    VERTICAL,
    HORIZONTAL
  }

}
