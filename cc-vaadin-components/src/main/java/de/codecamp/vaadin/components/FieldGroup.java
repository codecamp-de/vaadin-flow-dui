package de.codecamp.vaadin.components;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.shared.HasThemeVariant;
import com.vaadin.flow.component.shared.HasTooltip;
import com.vaadin.flow.component.shared.SlotUtils;
import de.codecamp.vaadin.base.HasSingleComponent;
import de.codecamp.vaadin.components.fluent.FluentFieldGroupLayoutConfig;
import de.codecamp.vaadin.fluent.FluentLayoutSupport;
import de.codecamp.vaadin.fluent.FluentLayoutSupportSingle;


@Tag(FieldGroup.TAG)
@JsModule("./components/cc-field-group.ts")
public class FieldGroup
  extends
    Component
  implements
    FluentLayoutSupport<FluentFieldGroupLayoutConfig>,
    FluentLayoutSupportSingle<FluentFieldGroupLayoutConfig>,
    HasSingleComponent,
    HasSize,
    HasThemeVariant<FieldGroupVariant>,
    HasTooltip
{

  public static final String TAG = "cc-field-group";

  public static final String SLOT_HEADER = "header";


  private Component content;


  public FieldGroup()
  {
  }

  public FieldGroup(String header, Component content)
  {
    setHeaderText(header);
    setContent(content);
  }

  public FieldGroup(Component header, Component content)
  {
    setHeader(header);
    setContent(content);
  }


  public Component getHeader()
  {
    return SlotUtils.getChildInSlot(this, SLOT_HEADER);
  }

  public void setHeader(Component header)
  {
    SlotUtils.setSlot(this, SLOT_HEADER, header);
  }

  public String getHeaderText()
  {
    Component header = getHeader();
    return header == null ? null : header.getElement().getText();
  }

  public void setHeaderText(String header)
  {
    setHeader(header == null ? null : new Span(header));
  }


  @Override
  public Component getContent()
  {
    return content;
  }

  @Override
  public void setContent(Component content)
  {
    if (this.content != null)
      this.content.getElement().removeFromParent();

    this.content = content;

    if (content != null)
      this.getElement().appendChild(content.getElement());
  }


  @Override
  public FluentFieldGroupLayoutConfig fluentAdd(Component component)
  {
    return fluentAdd(component, false);
  }

  @Override
  public FluentFieldGroupLayoutConfig fluentAdd(Component component, boolean replace)
  {
    if (!replace && getContent() != null)
      throw new IllegalStateException("The container already has a content component.");

    setContent(component);
    return new FluentFieldGroupLayoutConfig(this, component);
  }

}
