package de.codecamp.vaadin.components;


import com.vaadin.flow.component.shared.ThemeVariant;


public enum FieldGroupVariant
  implements
    ThemeVariant
{

  /**
   * This built-in padding is a little smaller at the top when a header is set. This may look a bit
   * better than margin or padding on a nested layout.
   */
  LUMO_PADDING("padding"),

  LUMO_SMALL("small");


  private final String variant;


  FieldGroupVariant(String variant)
  {
    this.variant = variant;
  }


  @Override
  public String getVariantName()
  {
    return variant;
  }

}
