package de.codecamp.vaadin.components;


import de.codecamp.vaadin.base.css.CssValue;


public enum GridAutoFlow
  implements
    CssValue
{

  ROW("row"),
  COLUMN("column"),
  ROW_DENSE("row dense"),
  COLUMN_DENSE("column dense");


  private final String value;


  GridAutoFlow(String value)
  {
    this.value = value;
  }


  @Override
  public String getValue()
  {
    return value;
  }


  public static GridAutoFlow fromCss(String value, GridAutoFlow defaultEnumConstant)
  {
    return CssValue.fromValue(GridAutoFlow.class, value, defaultEnumConstant);
  }

  public static GridAutoFlow fromCss(String value)
  {
    return CssValue.fromValue(GridAutoFlow.class, value);
  }

}
