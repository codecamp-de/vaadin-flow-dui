package de.codecamp.vaadin.components;


import de.codecamp.vaadin.base.css.CssValue;


public enum GridContentAlignment
  implements
    CssValue
{

  START("start"),
  END("end"),
  CENTER("center"),
  STRETCH("stretch"),
  SPACE_AROUND("space-around"),
  SPACE_BETWEEN("space-between"),
  SPACE_EVENLY("space-evenly");


  private final String value;


  GridContentAlignment(String value)
  {
    this.value = value;
  }

  @Override
  public String getValue()
  {
    return value;
  }


  public static GridContentAlignment fromCss(String value, GridContentAlignment defaultEnumConstant)
  {
    return CssValue.fromValue(GridContentAlignment.class, value, defaultEnumConstant);
  }

  public static GridContentAlignment fromCss(String value)
  {
    return CssValue.fromValue(GridContentAlignment.class, value);
  }

}
