package de.codecamp.vaadin.components;


import de.codecamp.vaadin.base.css.CssValue;


public enum GridItemAlignment
  implements
    CssValue
{

  AUTO("auto"),
  START("start"),
  END("end"),
  CENTER("center"),
  STRETCH("stretch"),
  BASELINE("baseline"),
  LAST_BASELINE("last baseline");


  private final String value;


  GridItemAlignment(String value)
  {
    this.value = value;
  }

  @Override
  public String getValue()
  {
    return value;
  }


  public static GridItemAlignment fromCss(String value, GridItemAlignment defaultEnumConstant)
  {
    return CssValue.fromValue(GridItemAlignment.class, value, defaultEnumConstant);
  }

  public static GridItemAlignment fromCss(String value)
  {
    return CssValue.fromValue(GridItemAlignment.class, value);
  }

}
