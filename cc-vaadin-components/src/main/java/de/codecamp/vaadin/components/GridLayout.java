package de.codecamp.vaadin.components;


import static java.util.stream.Collectors.joining;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasOrderedComponents;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.orderedlayout.ThemableLayout;
import de.codecamp.vaadin.components.fluent.FluentGridLayoutConfig;
import de.codecamp.vaadin.fluent.FluentLayoutSupport;
import de.codecamp.vaadin.fluent.FluentLayoutSupportIndexed;
import java.util.stream.Stream;


@Tag(GridLayout.TAG)
@JsModule("./components/cc-grid-layout.ts")
public class GridLayout
  extends
    AbstractGridLayout<GridLayout>
  implements
    HasOrderedComponents,
    ThemableLayout,
    FluentLayoutSupport<FluentGridLayoutConfig>,
    FluentLayoutSupportIndexed<FluentGridLayoutConfig>
{

  public static final String TAG = "cc-grid-layout";


  public GridLayout()
  {
  }

  public GridLayout(int columns)
  {
    this();
    setColumns(columns);
  }

  public GridLayout(String columns)
  {
    this();
    setColumns(columns);
  }

  public GridLayout(String columnWidths, String rowHeights, String... areasPerRow)
  {
    this();
    setColumns(columnWidths);
    setRows(rowHeights);
    setAreas(areasPerRow);
  }


  @Override
  public void setAutoFlow(GridAutoFlow autoFlow)
  {
    super.setAutoFlow(autoFlow);
  }

  public void setColumns(int columnCount)
  {
    setTemplateColumns("repeat(" + columnCount + ", auto)");
  }

  public void setColumns(String columns)
  {
    setTemplateColumns(columns);
  }

  @Override
  public void setAutoColumns(String columns)
  {
    setAutoColumns(columns);
  }

  public void setRows(int rowCount)
  {
    super.setTemplateRows("repeat(" + rowCount + ", auto)");
  }

  public void setRows(String rows)
  {
    super.setTemplateRows(rows);
  }

  @Override
  public void setAutoRows(String rows)
  {
    setAutoRows(rows);
  }

  public void setAreas(String... areasPerRow)
  {
    super.setTemplateAreas(Stream.of(areasPerRow).collect(joining(" ", "'", "'")));
  }


  public void setColumnsAlignment(GridContentAlignment alignment)
  {
    super.setJustifyContent(alignment);
  }

  public void setRowsAlignment(GridContentAlignment alignment)
  {
    super.setAlignContent(alignment);
  }

  public void setDefaultHorizontalItemAlignment(GridItemAlignment alignment)
  {
    super.setJustifyItems(alignment);
  }

  public void setDefaultVerticalItemAlignment(GridItemAlignment alignment)
  {
    super.setAlignItems(alignment);
  }


  public void addTo(Component childComponent, int x, int y)
  {
    addTo(childComponent, x, y, -1, -1);
  }

  public void addTo(Component childComponent, int x, int y, int colSpan, int rowSpan)
  {
    add(childComponent);
    setItemCoordinates(childComponent, x, y);
    setItemColumnAndRowSpan(childComponent, colSpan, rowSpan);
  }

  public void addTo(Component childComponent, String area)
  {
    getElement().appendChild(childComponent.getElement());
    setItemArea(childComponent, area);
  }


  public void setItemLocation(Component childComponent, int x, int y)
  {
    super.setItemCoordinates(childComponent, x, y);
  }

  @Override
  public void setItemColumnSpan(Component childComponent, int colSpan)
  {
    super.setItemColumnSpan(childComponent, colSpan);
  }

  @Override
  public void setItemRowSpan(Component childComponent, int rowSpan)
  {
    super.setItemRowSpan(childComponent, rowSpan);
  }

  @Override
  public void setItemArea(Component childComponent, String area)
  {
    super.setItemArea(childComponent, area);
  }

  public void setHorizontalItemAlignment(Component childComponent, GridItemAlignment alignment)
  {
    super.setItemJustify(childComponent, alignment);
  }

  public void setVerticalItemAlignment(Component childComponent, GridItemAlignment alignment)
  {
    super.setItemAlign(childComponent, alignment);
  }


  @Override
  public FluentGridLayoutConfig fluentAdd(Component component)
  {
    add(component);
    return new FluentGridLayoutConfig(this, component);
  }

  @Override
  public FluentGridLayoutConfig fluentAdd(Component component, int index)
  {
    addComponentAtIndex(index, component);
    return new FluentGridLayoutConfig(this, component);
  }


  public enum Schlott
  {
    SLOT1,
    SLOT2
  }

}
