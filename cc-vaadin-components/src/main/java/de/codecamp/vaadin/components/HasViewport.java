package de.codecamp.vaadin.components;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.shared.Registration;
import elemental.json.Json;
import elemental.json.JsonObject;


public interface HasViewport<C extends Component & HasViewport<C>>
  extends
    HasElement
{

  @SuppressWarnings({"unchecked", "rawtypes"})
  default Registration addScrollListener(ComponentEventListener<ScrollEvent<C>> listener)
  {
    return ComponentUtil.addListener((Component) this, ScrollEvent.class,
        (ComponentEventListener) listener);
  }

  default void scrollTo(Integer xOffset, Integer yOffset)
  {
    scrollTo(xOffset, yOffset, null);
  }

  default void scrollTo(Integer xOffset, Integer yOffset, Boolean smooth)
  {
    JsonObject options = Json.createObject();
    if (yOffset != null)
      options.put("top", yOffset);
    if (xOffset != null)
      options.put("left", xOffset);
    if (smooth != null)
      options.put("behavior", toBehaviorString(smooth));
    getElement().executeJs("this.scrollTo($0)", options);
  }


  default void scrollBy(Integer xDelta, Integer yDelta)
  {
    scrollBy(xDelta, yDelta, null);
  }

  default void scrollBy(Integer xDelta, Integer yDelta, Boolean smooth)
  {
    JsonObject options = Json.createObject();
    if (yDelta != null)
      options.put("top", yDelta);
    if (xDelta != null)
      options.put("left", xDelta);
    if (smooth != null)
      options.put("behavior", toBehaviorString(smooth));
    getElement().executeJs("this.scrollBy($0)", options);
  }


  default void scrollToLeft()
  {
    scrollToLeft(null);
  }

  default void scrollToLeft(Boolean smooth)
  {
    getElement().executeJs("this.scrollToLeft($0)", toBehaviorString(smooth));
  }


  default void scrollToRight()
  {
    scrollToRight(null);
  }

  default void scrollToRight(Boolean smooth)
  {
    getElement().executeJs("this.scrollToRight($0)", toBehaviorString(smooth));
  }


  default void scrollToTop()
  {
    scrollToTop(null);
  }

  default void scrollToTop(Boolean smooth)
  {
    getElement().executeJs("this.scrollToTop($0)", toBehaviorString(smooth));
  }


  default void scrollToBottom()
  {
    scrollToBottom(null);
  }

  default void scrollToBottom(Boolean smooth)
  {
    getElement().executeJs("this.scrollToBottom($0)", toBehaviorString(smooth));
  }


  private static String toBehaviorString(Boolean smooth)
  {
    if (smooth == null)
      return null;
    return smooth ? "smooth" : "instant";
  }

}
