package de.codecamp.vaadin.components;


import com.vaadin.flow.component.ClickNotifier;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasEnabled;
import com.vaadin.flow.component.HasOrderedComponents;
import com.vaadin.flow.component.HasText;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.shared.HasPrefix;
import com.vaadin.flow.component.shared.HasSuffix;
import com.vaadin.flow.component.shared.HasThemeVariant;
import com.vaadin.flow.component.shared.HasTooltip;
import de.codecamp.vaadin.components.util.HasTextUtils;


@Tag(Label.TAG)
@JsModule("./components/cc-label.ts")
public class Label
  extends
    Component
  implements
    ClickNotifier<Label>,
    HasEnabled,
    HasOrderedComponents,
    HasPrefix,
    HasSuffix,
    HasText,
    HasThemeVariant<LabelVariant>,
    HasTooltip
{

  public static final String TAG = "cc-label";


  public Label()
  {
  }

  public Label(String text)
  {
    setText(text);
  }

  public Label(Component icon, String text)
  {
    setPrefixComponent(icon);
    setText(text);
  }

  public Label(Component... components)
  {
    add(components);
  }


  @Override
  public void setText(String text)
  {
    HasTextUtils.setText(this, text);
  }

}
