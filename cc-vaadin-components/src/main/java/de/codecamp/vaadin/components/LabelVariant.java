package de.codecamp.vaadin.components;


import com.vaadin.flow.component.shared.ThemeVariant;


public enum LabelVariant
  implements
    ThemeVariant
{

  LUMO_BOLD("bold"),

  LUMO_SMALL("small"),
  LUMO_LARGE("large"),

  LUMO_PRIMARY("primary"),
  LUMO_SUCCESS("success"),
  LUMO_WARNING("warning"),
  LUMO_ERROR("error");


  private final String variant;


  LabelVariant(String variant)
  {
    this.variant = variant;
  }


  @Override
  public String getVariantName()
  {
    return variant;
  }

}
