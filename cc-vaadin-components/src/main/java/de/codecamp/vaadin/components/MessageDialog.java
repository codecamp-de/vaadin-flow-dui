package de.codecamp.vaadin.components;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.FlexLayout.FlexDirection;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.dom.ElementFactory;
import de.codecamp.vaadin.base.css.CssProperties;
import de.codecamp.vaadin.base.lumo.LumoColor;
import de.codecamp.vaadin.base.lumo.LumoFontSize;
import de.codecamp.vaadin.base.lumo.LumoSize;
import de.codecamp.vaadin.base.lumo.LumoSpace;


/**
 * A dialog component providing a convenient base structure that is suitable for most message,
 * confirmation, error, question etc. dialogs.
 */
@CssImport(themeFor = "vaadin-dialog-overlay", value = "./styles/message-dialog.css")
public class MessageDialog
  extends
    Dialog
{

  /**
   * The default max width of message dialogs.
   */
  public static final String DEFAULT_MAX_WIDTH = "40rem";


  private Icon icon;

  private final Div titleContainer;

  private final Div iconContainer;

  private final Div messageContainer;

  private final ButtonBar buttonBar;

  private ColorTheme colorTheme = ColorTheme.STANDARD;


  /**
   * Creates a new empty message dialog.
   */
  public MessageDialog()
  {
    setMaxWidth(DEFAULT_MAX_WIDTH);

    setCloseOnEsc(false);
    setCloseOnOutsideClick(false);

    updateColors();

    addThemeName("message-dialog");


    // content
    FlexLayout content = new FlexLayout();
    add(content);

    iconContainer = new Div();
    content.add(iconContainer);

    FlexLayout textContent = new FlexLayout();
    textContent.setFlexDirection(FlexDirection.COLUMN);
    content.add(textContent);
    content.setFlexGrow(1, textContent);

    titleContainer = new Div();
    titleContainer.getStyle() //
        .set(CssProperties.fontSize, LumoFontSize.XL.var()) //
        .set(CssProperties.fontWeight, "bold") //
        .set(CssProperties.marginBottom, LumoSpace.M.var());
    titleContainer.setVisible(false);
    textContent.add(titleContainer);

    messageContainer = new Div();
    textContent.add(messageContainer);

    // footer
    buttonBar = new ButtonBar();
    buttonBar.setWidthFull();
    getFooter().add(buttonBar);
  }


  /**
   * Sets the icon of the message dialog.
   *
   * @param icon
   *          the new icon or null
   */
  public void setIcon(Icon icon)
  {
    iconContainer.removeAll();

    this.icon = icon;

    if (icon != null)
    {
      icon.getElement().getStyle() //
          .set(CssProperties.width, LumoSize.XL.var()) //
          .set(CssProperties.height, LumoSize.XL.var());

      Div iconFrame = new Div(icon);
      iconFrame.addClassName("icon-frame");
      iconFrame.getElement().getStyle() //
          .set(CssProperties.marginRight, LumoSpace.L.var()) //
          .set(CssProperties.padding, LumoSpace.M.var());

      iconFrame.getStyle() //
          .set(CssProperties.borderRadius, "50%");
      iconContainer.add(iconFrame);
      updateColors();
    }
  }

  /**
   * Not supported. Use {@link #setTitle(String)} instead.
   */
  @Override
  public void setHeaderTitle(String title)
  {
    throw new UnsupportedOperationException("Not supported. Use setTitle(...) instead.");
  }

  /**
   * Sets the title of the message dialog.
   *
   * @param titleText
   *          the title
   */
  public void setTitle(String titleText)
  {
    titleContainer.setText(titleText);
    titleContainer.setVisible(titleText != null && !titleText.isEmpty());
  }

  /**
   * Sets the message of the message dialog. Newlines will be handled appropriately to actually
   * create line breaks; HTML tags are escaped and not interpreted.
   *
   * @param messageText
   *          the message
   */
  public void setMessage(String messageText)
  {
    if (messageText == null)
    {
      setMessage();
      return;
    }

    Div messageDiv = new Div();
    boolean first = true;
    for (String token : messageText.split("\\r?\\n"))
    {
      if (first)
        first = false;
      else
        messageDiv.getElement().appendChild(ElementFactory.createBr());

      messageDiv.getElement().appendChild(Element.createText(token));
    }
    setMessage(messageDiv);
  }

  /**
   * Sets the message of the message dialog. HTML is actually intepreted as HTML and not escaped,
   * unlike with {@link #setMessage(String)}. <em>So be careful with user data.</em>
   *
   * @param messageText
   *          the message
   */
  public void setMessageAsHtml(String messageText)
  {
    if (messageText == null)
    {
      setMessage();
      return;
    }

    setMessage(new Html("<span>" + messageText + "</span>"));
  }

  /**
   * Sets the given components as message.
   *
   * @param messageComponents
   *          the message components
   */
  public void setMessage(Component... messageComponents)
  {
    messageContainer.removeAll();
    if (messageComponents != null && messageComponents.length > 0)
      messageContainer.add(messageComponents);
  }

  /**
   * Sets the color theme of the message dialog.
   *
   * @param colorTheme
   *          the color theme
   */
  public void setColorTheme(ColorTheme colorTheme)
  {
    this.colorTheme = colorTheme != null ? colorTheme : ColorTheme.STANDARD;
    updateColors();
  }


  private void updateColors()
  {
    /*
     * shows the header part, so it can be colored: setHeaderTitle(" ");
     *
     * hides the header part: setHeaderTitle(null);
     */

    removeThemeNames("primary", "success", "warning", "error");
    switch (colorTheme)
    {
      case PRIMARY:
        super.setHeaderTitle(" ");
        addThemeName("primary");

        if (icon != null)
        {
          icon.getParent().get().getElement().getStyle() //
              .set(CssProperties.backgroundColor, LumoColor.primaryColor.var());
          icon.setColor(LumoColor.primaryContrastColor.var());
        }
        break;

      case SUCCESS:
        super.setHeaderTitle(" ");
        addThemeName("success");

        if (icon != null)
        {
          icon.getParent().get().getElement().getStyle() //
              .set(CssProperties.backgroundColor, LumoColor.successColor.var());
          icon.setColor(LumoColor.successContrastColor.var());
        }
        break;

      case WARNING:
        super.setHeaderTitle(" ");
        addThemeName("warning");

        if (icon != null)
        {
          icon.getParent().get().getElement().getStyle() //
              .set(CssProperties.backgroundColor, LumoColor.warningColor.var());
          icon.setColor(LumoColor.warningContrastColor.var());
        }
        break;

      case ERROR:
        super.setHeaderTitle(" ");
        addThemeName("error");

        if (icon != null)
        {
          icon.getParent().get().getElement().getStyle() //
              .set(CssProperties.backgroundColor, LumoColor.errorColor.var());
          icon.setColor(LumoColor.errorContrastColor.var());
        }
        break;

      case STANDARD:
      default:
        super.setHeaderTitle(null);

        if (icon != null)
        {
          icon.getParent().get().getElement().getStyle() //
              .set(CssProperties.backgroundColor, LumoColor.contrast.var());
          icon.setColor(LumoColor.baseColor.var());
        }
        break;
    }
  }


  /**
   * Returns the button bar.
   *
   * @return the button bar
   */
  public ButtonBar getButtonBar()
  {
    return buttonBar;
  }

  /**
   * Adds a button to the primary button area to the right within the button bar.
   *
   * @return the new blank button
   * @see #getButtonBar()
   * @see ButtonBar#addButtonToEnd()
   */
  public Button addButton()
  {
    return getButtonBar().addButtonToEnd();
  }

  /**
   * Adds a spacer to the primary button area to the right within the button bar.
   *
   * @see #getButtonBar()
   * @see ButtonBar#addSpacerToEnd()
   */
  public void addSpacer()
  {
    getButtonBar().addSpacerToEnd();
  }

  /**
   * Adds a button to the secondary button area to the left within the button bar.
   *
   * @return the new blank button
   * @see #getButtonBar()
   * @see ButtonBar#addButtonToStart()
   */
  public Button addButtonToSecondary()
  {
    return getButtonBar().addButtonToStart();
  }

  /**
   * Adds a spacer to the secondary button area to the left within the button bar.
   *
   * @see #getButtonBar()
   * @see ButtonBar#addSpacerToStart()
   */
  public void addSpacerToSecondary()
  {
    getButtonBar().addSpacerToStart();
  }


  /**
   * The available color themes of the {@link MessageDialog}.
   */
  public enum ColorTheme
  {
    STANDARD,
    PRIMARY,
    SUCCESS,
    WARNING,
    ERROR
  }

}
