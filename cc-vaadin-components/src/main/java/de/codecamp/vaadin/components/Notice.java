package de.codecamp.vaadin.components;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasOrderedComponents;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.shared.HasPrefix;
import com.vaadin.flow.component.shared.HasSuffix;
import com.vaadin.flow.component.shared.HasThemeVariant;
import com.vaadin.flow.component.shared.SlotUtils;
import de.codecamp.vaadin.base.lumo.LumoIconSize;


@Tag(Notice.TAG)
@JsModule("./components/cc-notice.ts")
public class Notice
  extends
    Component
  implements
    HasOrderedComponents,
    HasPrefix,
    HasSuffix,
    HasThemeVariant<NoticeVariant>
{

  public static final String TAG = "cc-notice";

  public static final String SLOT_HEADER = "header";


  public Notice()
  {
  }

  public Notice(String text)
  {
    add(text);
  }

  public Notice(Component icon, String text)
  {
    setPrefixComponent(icon);
    add(text);
  }

  public Notice(Component... components)
  {
    add(components);
  }


  public static Notice createSuccess()
  {
    return createInfo(VaadinIcon.CHECK_CIRCLE_O, NoticeVariant.LUMO_SUCCESS);
  }

  public static Notice createInfo()
  {
    return createInfo(VaadinIcon.INFO_CIRCLE_O, NoticeVariant.LUMO_INFO);
  }

  public static Notice createWarning()
  {
    return createInfo(VaadinIcon.EXCLAMATION_CIRCLE_O, NoticeVariant.LUMO_WARNING);
  }

  public static Notice createError()
  {
    return createInfo(VaadinIcon.CLOSE_CIRCLE_O, NoticeVariant.LUMO_ERROR);
  }

  private static Notice createInfo(VaadinIcon icon, NoticeVariant theme)
  {
    Notice notice = new Notice();
    Icon iconComponent = icon.create();
    iconComponent.setSize(LumoIconSize.S.var());
    notice.setPrefixComponent(iconComponent);
    notice.addThemeVariants(theme, NoticeVariant.LUMO_FILLED, NoticeVariant.LUMO_EDGE);
    return notice;
  }


  public Component getHeader()
  {
    return SlotUtils.getChildInSlot(this, SLOT_HEADER);
  }

  public void setHeader(Component header)
  {
    SlotUtils.setSlot(this, SLOT_HEADER, header);
  }

  public String getHeaderText()
  {
    Component header = getHeader();
    return header == null ? null : header.getElement().getText();
  }

  public void setHeaderText(String header)
  {
    setHeader(header == null ? null : new Span(header));
  }

}
