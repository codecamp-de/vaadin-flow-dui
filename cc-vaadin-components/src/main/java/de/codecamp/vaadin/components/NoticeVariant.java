package de.codecamp.vaadin.components;


import com.vaadin.flow.component.shared.ThemeVariant;


public enum NoticeVariant
  implements
    ThemeVariant
{

  LUMO_FILLED("filled"),

  LUMO_EDGE("edge"),

  LUMO_BORDER("border"),

  LUMO_ELEVATED("elevated"),

  LUMO_SUCCESS("success"),
  LUMO_INFO("info"),
  LUMO_WARNING("warning"),
  LUMO_ERROR("error"),
  LUMO_CONTRAST("contrast");


  private final String variant;


  NoticeVariant(String variant)
  {
    this.variant = variant;
  }


  @Override
  public String getVariantName()
  {
    return variant;
  }

}
