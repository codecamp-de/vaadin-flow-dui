package de.codecamp.vaadin.components;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.Scroller.ScrollDirection;
import com.vaadin.flow.component.shared.HasThemeVariant;
import com.vaadin.flow.component.shared.HasTooltip;
import com.vaadin.flow.component.shared.SlotUtils;
import de.codecamp.vaadin.base.HasSingleComponent;
import de.codecamp.vaadin.components.fluent.FluentPanelLayoutConfig;
import de.codecamp.vaadin.fluent.FluentLayoutSupport;
import de.codecamp.vaadin.fluent.FluentLayoutSupportSingle;
import java.util.Locale;
import java.util.stream.Stream;


@Tag(Panel.TAG)
@JsModule("./components/cc-panel.ts")
public class Panel
  extends
    Component
  implements
    FluentLayoutSupport<FluentPanelLayoutConfig>,
    FluentLayoutSupportSingle<FluentPanelLayoutConfig>,
    HasSingleComponent,
    HasSize,
    HasThemeVariant<PanelVariant>,
    HasTooltip,
    HasViewport<Panel>
{

  public static final String TAG = "cc-panel";

  public static final String SLOT_HEADER = "header";

  private static final String SCROLL_DIRECTION_PROPERTY = "scrollDirection";


  private Component content;


  public Panel()
  {
  }

  public Panel(String header, Component content)
  {
    setHeaderText(header);
    setContent(content);
  }

  public Panel(Component header, Component content)
  {
    setHeader(header);
    setContent(content);
  }


  public Component getHeader()
  {
    return SlotUtils.getChildInSlot(this, SLOT_HEADER);
  }

  public void setHeader(Component header)
  {
    SlotUtils.setSlot(this, SLOT_HEADER, header);
  }

  public String getHeaderText()
  {
    Component header = getHeader();
    return header == null ? null : header.getElement().getText();
  }

  public void setHeaderText(String header)
  {
    setHeader(header == null ? null : new Span(header));
  }


  @Override
  public Component getContent()
  {
    return content;
  }

  @Override
  public void setContent(Component content)
  {
    if (this.content != null)
      this.content.getElement().removeFromParent();

    this.content = content;

    if (content != null)
      this.getElement().appendChild(content.getElement());
  }

  public ScrollDirection getScrollDirection()
  {
    String propertyValue = getElement().getProperty(SCROLL_DIRECTION_PROPERTY);
    return Stream.of(ScrollDirection.values())
        .filter(e -> e.name().toLowerCase(Locale.ENGLISH).equals(propertyValue)).findFirst()
        .orElse(ScrollDirection.NONE);
  }

  public void setScrollDirection(ScrollDirection scrollDirection)
  {
    getElement().setProperty(SCROLL_DIRECTION_PROPERTY,
        (scrollDirection == null || scrollDirection == ScrollDirection.NONE) ? null
            : scrollDirection.name().toLowerCase(Locale.ENGLISH));
  }


  @Override
  public FluentPanelLayoutConfig fluentAdd(Component component)
  {
    return fluentAdd(component, false);
  }

  @Override
  public FluentPanelLayoutConfig fluentAdd(Component component, boolean replace)
  {
    if (!replace && getContent() != null)
      throw new IllegalStateException("The container already has a content component.");

    setContent(component);
    return new FluentPanelLayoutConfig(this, component);
  }

}
