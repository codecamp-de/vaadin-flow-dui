package de.codecamp.vaadin.components;


import com.vaadin.flow.component.shared.ThemeVariant;


public enum PanelVariant
  implements
    ThemeVariant
{

  LUMO_PADDING("padding"),

  LUMO_CARD("card"),

  LUMO_LARGE("large");


  private final String variant;


  PanelVariant(String variant)
  {
    this.variant = variant;
  }


  @Override
  public String getVariantName()
  {
    return variant;
  }

}
