package de.codecamp.vaadin.components;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.DebounceSettings;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;
import com.vaadin.flow.dom.DebouncePhase;


@DomEvent(
    value = "scroll",
    debounce = @DebounceSettings(timeout = 250, phases = DebouncePhase.INTERMEDIATE))
public class ScrollEvent<C extends Component & HasViewport<C>>
  extends
    ComponentEvent<C>
{

  private final int offsetX;

  private final int offsetY;


  public ScrollEvent(C source, boolean fromClient,
      @EventData("event.detail.scrollLeft") int offsetX,
      @EventData("event.detail.scrollTop") int offsetY)
  {
    super(source, fromClient);
    this.offsetX = offsetX;
    this.offsetY = offsetY;
  }


  public int getOffsetX()
  {
    return offsetX;
  }

  public int getOffsetY()
  {
    return offsetY;
  }

}
