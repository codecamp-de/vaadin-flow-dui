package de.codecamp.vaadin.components;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.HasTheme;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.orderedlayout.Scroller.ScrollDirection;
import de.codecamp.vaadin.base.HasSingleComponent;
import de.codecamp.vaadin.components.fluent.FluentScrollPaneLayoutConfig;
import de.codecamp.vaadin.fluent.FluentLayoutSupport;
import de.codecamp.vaadin.fluent.FluentLayoutSupportSingle;
import java.util.Locale;
import java.util.stream.Stream;


@Tag(ScrollPane.TAG)
@JsModule("./components/cc-scroll-pane.ts")
public class ScrollPane
  extends
    Component
  implements
    FluentLayoutSupport<FluentScrollPaneLayoutConfig>,
    FluentLayoutSupportSingle<FluentScrollPaneLayoutConfig>,
    HasSingleComponent,
    HasSize,
    HasTheme,
    HasViewport<ScrollPane>
{

  public static final String TAG = "cc-scroll-pane";

  private static final String SCROLL_DIRECTION_PROPERTY = "scrollDirection";

  private static final String FIT_CONTENT_PROPERTY = "fitContent";


  private Component content;


  public ScrollPane()
  {
  }

  public ScrollPane(Component content)
  {
    setContent(content);
  }


  @Override
  public Component getContent()
  {
    return content;
  }

  @Override
  public void setContent(Component content)
  {
    if (this.content != null)
      this.content.getElement().removeFromParent();

    this.content = content;

    if (content != null)
      this.getElement().appendChild(content.getElement());
  }

  public ScrollDirection getScrollDirection()
  {
    String propertyValue = getElement().getProperty(SCROLL_DIRECTION_PROPERTY);
    return Stream.of(ScrollDirection.values())
        .filter(e -> e.name().toLowerCase(Locale.ENGLISH).equals(propertyValue)).findFirst()
        .orElse(ScrollDirection.NONE);
  }

  public void setScrollDirection(ScrollDirection scrollDirection)
  {
    getElement().setProperty(SCROLL_DIRECTION_PROPERTY,
        (scrollDirection == null || scrollDirection == ScrollDirection.NONE) ? null
            : scrollDirection.name().toLowerCase(Locale.ENGLISH));
  }

  public FitContentMode getFitContent()
  {
    String propertyValue = getElement().getProperty(FIT_CONTENT_PROPERTY);
    return Stream.of(FitContentMode.values())
        .filter(e -> e.name().toLowerCase(Locale.ENGLISH).equals(propertyValue)).findFirst()
        .orElse(FitContentMode.NONE);
  }

  public void setFitContent(FitContentMode fitContent)
  {
    getElement().setProperty(FIT_CONTENT_PROPERTY,
        (fitContent == null || fitContent == FitContentMode.NONE) ? null
            : fitContent.name().toLowerCase(Locale.ENGLISH));
  }


  @Override
  public FluentScrollPaneLayoutConfig fluentAdd(Component component)
  {
    return fluentAdd(component, false);
  }

  @Override
  public FluentScrollPaneLayoutConfig fluentAdd(Component component, boolean replace)
  {
    if (!replace && getContent() != null)
      throw new IllegalStateException("The container already has a content component.");

    setContent(component);
    return new FluentScrollPaneLayoutConfig(this, component);
  }


  public enum FitContentMode
  {
    NONE,
    WIDTH,
    HEIGHT,
    BOTH
  }

}
