package de.codecamp.vaadin.components.autoconfigure;


import de.codecamp.vaadin.components.dui.BadgeFactory;
import de.codecamp.vaadin.components.dui.ButtonBarFactory;
import de.codecamp.vaadin.components.dui.FieldGroupFactory;
import de.codecamp.vaadin.components.dui.GridLayoutFactory;
import de.codecamp.vaadin.components.dui.LabelFactory;
import de.codecamp.vaadin.components.dui.PanelFactory;
import de.codecamp.vaadin.components.dui.ScrollPaneFactory;
import de.codecamp.vaadin.flowdui.autoconfigure.TemplateProcessorRegistrar;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class CodecampComponentsAutoConfiguration
{

  @Bean
  TemplateProcessorRegistrar vaadinCodecampComponentsRegistrar()
  {
    return registry ->
    {
      registry.addFactoryOrPostProcessor(new BadgeFactory());
      registry.addFactoryOrPostProcessor(new ButtonBarFactory());
      registry.addFactoryOrPostProcessor(new FieldGroupFactory());
      registry.addFactoryOrPostProcessor(new GridLayoutFactory());
      registry.addFactoryOrPostProcessor(new LabelFactory());
      registry.addFactoryOrPostProcessor(new PanelFactory());
      registry.addFactoryOrPostProcessor(new ScrollPaneFactory());
    };
  }

}
