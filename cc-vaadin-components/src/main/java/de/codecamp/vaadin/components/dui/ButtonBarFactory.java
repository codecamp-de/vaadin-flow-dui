package de.codecamp.vaadin.components.dui;


import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.components.ButtonBar;
import de.codecamp.vaadin.components.ButtonBar.Orientation;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = ButtonBarFactory.TAG_BUTTON_BAR,
    componentType = ButtonBar.class,
    category = Constants.CATEGORY)
public class ButtonBarFactory
  implements
    ComponentFactory
{

  public static final String TAG_BUTTON_BAR = "cc-button-bar";

  public static final String ATTR_ORIENTATION = "orientation";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_BUTTON_BAR:
      {
        ButtonBar component = new ButtonBar();
        context.mapAttribute(ATTR_ORIENTATION).asEnum(Orientation.class)
            .to(component::setOrientation);

        context.readChildren(component,
            "ButtonBar cannot be populated using a template. Use its Java API instead.");

        return component;
      }
    }

    return null;
  }

}
