package de.codecamp.vaadin.components.dui;


final class Constants
{

  static final String CATEGORY = "codecamp.de";


  private Constants()
  {
    // utility class
  }

}
