package de.codecamp.vaadin.components.dui;


import static de.codecamp.vaadin.components.FieldGroup.SLOT_HEADER;
import static de.codecamp.vaadin.components.FieldGroup.TAG;
import static de.codecamp.vaadin.components.dui.FieldGroupFactory.ATTR_HEADER;
import static de.codecamp.vaadin.components.dui.FieldGroupFactory.ATTR_PADDING;

import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.components.FieldGroup;
import de.codecamp.vaadin.components.FieldGroupVariant;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG,
    componentType = FieldGroup.class,
    category = Constants.CATEGORY,
    attributes = { //
        @DuiAttribute(name = ATTR_HEADER, type = String.class, custom = true), //
        @DuiAttribute(name = ATTR_PADDING, type = Boolean.class, custom = true) //
    },
    slots = {SLOT_HEADER})
public class FieldGroupFactory
  implements
    ComponentFactory
{

  public static final String ATTR_HEADER = "header";

  public static final String ATTR_PADDING = "padding";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG:
      {
        FieldGroup component = new FieldGroup();
        context.mapAttribute(ATTR_HEADER).asString().to(component::setHeaderText);
        context.mapAttribute(ATTR_PADDING).asBoolean().to(value -> component
            .setThemeName(FieldGroupVariant.LUMO_PADDING.getVariantName(), value));

        // content component handled by HasSingleComponentPostProcessor
        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_HEADER:
              if (component.getHeader() != null)
                throw context.fail("FieldGroup only supports a single header component.");
              component.setHeader(context.readComponentForSlot(childElement));
              return true;
          }
          return false;
        }, null);

        return component;
      }
    }

    return null;
  }

}
