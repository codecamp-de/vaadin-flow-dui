package de.codecamp.vaadin.components.dui;


import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.base.util.SizeUtils;
import de.codecamp.vaadin.components.GridContentAlignment;
import de.codecamp.vaadin.components.GridItemAlignment;
import de.codecamp.vaadin.components.GridLayout;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import java.util.Set;


public class GridLayoutFactory
  implements
    ComponentFactory
{

  public static final String TAG_GRID_LAYOUT = "cc-grid-layout";

  public static final String ATTR_COLUMNS = "columns";

  public static final String ATTR_ALIGN_GRID = "align-grid";

  public static final String ATTR_ALIGN_COLOMNS = "align-columns";

  public static final String ATTR_ALIGN_ROWS = "align-rows";

  public static final String ATTR_DEFAULT_ALIGN = "default-align";

  public static final String ATTR_DEFAULT_ALIGN_H = "default-align-h";

  public static final String ATTR_DEFAULT_ALIGN_V = "default-align-v";


  public static final String CATTR_WIDTH_FULL = SizeUtils.ATTR_WIDTH_FULL;

  public static final String CATTR_HEIGHT_FULL = SizeUtils.ATTR_HEIGHT_FULL;

  public static final String CATTR_SIZE_FULL = SizeUtils.ATTR_SIZE_FULL;

  public static final String CATTR_COL_SPAN = "col-span";

  public static final String CATTR_ROW_SPAN = "row-span";

  public static final String CATTR_AREA = "area";

  public static final String CATTR_ALIGN = "align";

  public static final String CATTR_ALIGN_H = "align-h";

  public static final String CATTR_ALIGN_V = "align-v";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_GRID_LAYOUT:
      {
        GridLayout component = new GridLayout();

        context.mapAttribute(ATTR_COLUMNS).asInteger().to(component::setColumns);

        context.mapAttribute(ATTR_ALIGN_GRID).asConverted(this::parseCombinedContentAlignment)
            .to(aligns ->
            {
              component.setColumnsAlignment(aligns[0]);
              component.setRowsAlignment(aligns[1]);
            });
        context.mapAttribute(ATTR_ALIGN_COLOMNS).asEnum(GridContentAlignment::fromCss)
            .to(component::setColumnsAlignment);
        context.mapAttribute(ATTR_ALIGN_ROWS).asEnum(GridContentAlignment::fromCss)
            .to(component::setRowsAlignment);

        context.mapAttribute(ATTR_DEFAULT_ALIGN).asConverted(this::parseCombinedItemAlignment)
            .to(aligns ->
            {
              component.setDefaultHorizontalItemAlignment(aligns[0]);
              component.setDefaultVerticalItemAlignment(aligns[1]);
            });
        context.mapAttribute(ATTR_DEFAULT_ALIGN_H, Set.of("default-h-align"))
            .asEnum(GridItemAlignment::fromCss).to(component::setDefaultHorizontalItemAlignment);
        context.mapAttribute(ATTR_DEFAULT_ALIGN_V, Set.of("default-v-align"))
            .asEnum(GridItemAlignment::fromCss).to(component::setDefaultVerticalItemAlignment);

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_DEFAULT:

              context.readComponent(childElement, (childComponent, childContext) ->
              {
                component.add(childComponent);

                childContext.mapAttribute(CATTR_COL_SPAN, Set.of("colspan")).asInteger()
                    .to(colspan -> component.setItemColumnSpan(childComponent, colspan));
                childContext.mapAttribute(CATTR_ROW_SPAN, Set.of("rowspan")).asInteger()
                    .to(rowspan -> component.setItemRowSpan(childComponent, rowspan));

                childContext.mapAttribute(CATTR_AREA).asString()
                    .to(area -> component.setItemArea(childComponent, area));

                childContext.mapAttribute(CATTR_SIZE_FULL).asBoolean().to(v ->
                {
                  component.setHorizontalItemAlignment(childComponent, GridItemAlignment.STRETCH);
                  SizeUtils.unlockHeight(childComponent);
                  component.setVerticalItemAlignment(childComponent, GridItemAlignment.STRETCH);
                });
                childContext.mapAttribute(CATTR_WIDTH_FULL).asBoolean().to(v ->
                {
                  component.setHorizontalItemAlignment(childComponent, GridItemAlignment.STRETCH);
                });
                childContext.mapAttribute(CATTR_HEIGHT_FULL).asBoolean().to(v ->
                {
                  SizeUtils.unlockHeight(childComponent);
                  component.setVerticalItemAlignment(childComponent, GridItemAlignment.STRETCH);
                });

                childContext.mapAttribute(CATTR_ALIGN).asConverted(this::parseCombinedItemAlignment)
                    .to(aligns ->
                    {
                      component.setHorizontalItemAlignment(childComponent, aligns[0]);
                      component.setVerticalItemAlignment(childComponent, aligns[1]);
                    });
                childContext.mapAttribute(CATTR_ALIGN_H, Set.of("h-align"))
                    .asEnum(GridItemAlignment::fromCss).to(horizontalAlign -> component
                        .setHorizontalItemAlignment(childComponent, horizontalAlign));
                childContext.mapAttribute(CATTR_ALIGN_V, Set.of("v-align"))
                    .asEnum(GridItemAlignment::fromCss).to(verticalAlign -> component
                        .setVerticalItemAlignment(childComponent, verticalAlign));
              });

              return true;
          }
          return false;
        }, null);

        return component;
      }
    }

    return null;
  }

  private GridContentAlignment[] parseCombinedContentAlignment(String value)
  {
    String[] tokens = value.trim().split("\\s+", 2);
    if (tokens.length == 2)
    {
      return new GridContentAlignment[] {GridContentAlignment.fromCss(tokens[0].trim()),
          GridContentAlignment.fromCss(tokens[1].trim())};
    }
    else
    {
      GridContentAlignment alignment = GridContentAlignment.fromCss(tokens[0].trim());
      return new GridContentAlignment[] {alignment, alignment};
    }
  }

  private GridItemAlignment[] parseCombinedItemAlignment(String value)
  {
    String[] tokens = value.trim().split("\\s+", 2);
    if (tokens.length == 2)
    {
      return new GridItemAlignment[] {GridItemAlignment.fromCss(tokens[0].trim()),
          GridItemAlignment.fromCss(tokens[1].trim())};
    }
    else
    {
      GridItemAlignment alignment = GridItemAlignment.fromCss(tokens[0].trim());
      return new GridItemAlignment[] {alignment, alignment};
    }
  }

}
