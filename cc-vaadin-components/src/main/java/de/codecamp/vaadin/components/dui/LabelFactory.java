package de.codecamp.vaadin.components.dui;


import static de.codecamp.vaadin.components.Label.TAG;
import static de.codecamp.vaadin.flowdui.declare.DuiComponent.SLOT_DEFAULT;

import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.components.Label;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG,
    componentType = Label.class,
    category = Constants.CATEGORY,
    slots = {SLOT_DEFAULT})
public class LabelFactory
  implements
    ComponentFactory
{

  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG:
      {
        Label component = new Label();

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_DEFAULT:
              component.add(context.readComponent(childElement));
              return true;
          }
          return false;
        }, textNode ->
        {
          component.add(textNode.text());
        });

        return component;
      }
    }

    return null;
  }

}
