package de.codecamp.vaadin.components.dui;


import static de.codecamp.vaadin.components.Panel.SLOT_HEADER;
import static de.codecamp.vaadin.components.Panel.TAG;
import static de.codecamp.vaadin.components.dui.PanelFactory.ATTR_HEADER;
import static de.codecamp.vaadin.components.dui.PanelFactory.ATTR_PADDING;
import static de.codecamp.vaadin.components.dui.PanelFactory.ATTR_SCROLL_DIRECTION;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.Scroller;
import de.codecamp.vaadin.components.Panel;
import de.codecamp.vaadin.components.PanelVariant;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG,
    componentType = Panel.class,
    category = Constants.CATEGORY,
    attributes = { //
        @DuiAttribute(name = ATTR_HEADER, type = String.class, custom = true), //
        @DuiAttribute(name = ATTR_PADDING, type = Boolean.class, custom = true), //
        @DuiAttribute(
            name = ATTR_SCROLL_DIRECTION,
            type = String.class,
            description = "Allowed values: 'both', 'horizontal', 'vertical' or 'none'") //


    },
    slots = {SLOT_HEADER})
public class PanelFactory
  implements
    ComponentFactory
{

  public static final String ATTR_HEADER = "header";

  public static final String ATTR_PADDING = "padding";

  public static final String ATTR_SCROLL_DIRECTION = "scroll-direction";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG:
      {
        Panel component = new Panel();
        context.mapAttribute(ATTR_HEADER).asString().to(component::setHeaderText);
        context.mapAttribute(ATTR_PADDING).asBoolean()
            .to(value -> component.setThemeName(PanelVariant.LUMO_PADDING.getVariantName(), value));
        context.mapAttribute(ATTR_SCROLL_DIRECTION).asEnum(Scroller.ScrollDirection.class)
            .to(component::setScrollDirection);

        // content component handled by HasSingleComponentPostProcessor
        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_HEADER:
              if (component.getHeader() != null)
                throw context.fail("Panel only supports a single header component.");
              component.setHeader(context.readComponentForSlot(childElement));
              return true;
          }
          return false;
        }, null);

        return component;
      }
    }

    return null;
  }

}
