package de.codecamp.vaadin.components.dui;


import static de.codecamp.vaadin.components.ScrollPane.TAG;
import static de.codecamp.vaadin.components.dui.ScrollPaneFactory.ATTR_SCROLL_DIRECTION;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.Scroller;
import de.codecamp.vaadin.components.ScrollPane;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG,
    componentType = ScrollPane.class,
    description = "Can only have a single child component as content.",
    category = Constants.CATEGORY,
    attributes = { //
        @DuiAttribute(
            name = ATTR_SCROLL_DIRECTION,
            type = String.class,
            description = "Allowed values: 'both' (default), 'horizontal', 'vertical' or 'none'") //
    })
public class ScrollPaneFactory
  implements
    ComponentFactory
{

  public static final String ATTR_SCROLL_DIRECTION = "scroll-direction";

  public static final String ATTR_FIT_CONTENT = "fit-content";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG:
      {
        ScrollPane component = new ScrollPane();
        context.mapAttribute(ATTR_SCROLL_DIRECTION).asEnum(Scroller.ScrollDirection.class)
            .to(component::setScrollDirection);
        context.mapAttribute(ATTR_FIT_CONTENT).asEnum(ScrollPane.FitContentMode.class)
            .to(component::setFitContent);

        // content component handled by HasSingleComponentPostProcessor
        context.readChildren(component, null, null);

        return component;
      }
    }

    return null;
  }

}
