package de.codecamp.vaadin.components.fluent;


import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.components.FieldGroup;
import de.codecamp.vaadin.fluent.FluentHasSingleComponentLayoutConfig;


public class FluentFieldGroupLayoutConfig
  extends
    FluentHasSingleComponentLayoutConfig<FieldGroup, FluentFieldGroupLayoutConfig>
{

  public FluentFieldGroupLayoutConfig(FieldGroup container, Component... components)
  {
    super(container, components);
  }

}
