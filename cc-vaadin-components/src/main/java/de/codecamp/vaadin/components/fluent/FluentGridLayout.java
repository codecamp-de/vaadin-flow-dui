package de.codecamp.vaadin.components.fluent;


import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.components.GridAutoFlow;
import de.codecamp.vaadin.components.GridContentAlignment;
import de.codecamp.vaadin.components.GridItemAlignment;
import de.codecamp.vaadin.components.GridLayout;
import de.codecamp.vaadin.fluent.FluentClickNotifier;
import de.codecamp.vaadin.fluent.FluentComponent;
import de.codecamp.vaadin.fluent.FluentHasOrderedComponentsContainerExtension;
import de.codecamp.vaadin.fluent.FluentHasSize;
import de.codecamp.vaadin.fluent.FluentHasTheme;
import de.codecamp.vaadin.fluent.layouts.FluentThemableLayout;


public class FluentGridLayout
  extends
    FluentComponent<GridLayout, FluentGridLayout>
  implements
    FluentClickNotifier<GridLayout, FluentGridLayout>,
    FluentHasOrderedComponentsContainerExtension<GridLayout, FluentGridLayout, FluentGridLayoutConfig>,
    FluentHasSize<GridLayout, FluentGridLayout>,
    FluentHasTheme<GridLayout, FluentGridLayout>,
    FluentThemableLayout<GridLayout, FluentGridLayout>
{

  public FluentGridLayout()
  {
    super(new GridLayout());
  }

  public FluentGridLayout(GridLayout component)
  {
    super(component);
  }


  @Override
  public FluentGridLayoutConfig getLayoutConfigFor(Component... components)
  {
    return new FluentGridLayoutConfig(get(), components);
  }


  public FluentGridLayout autoFlow(GridAutoFlow autoFlow)
  {
    get().setAutoFlow(autoFlow);
    return this;
  }

  public FluentGridLayout columns(int columnCount)
  {
    get().setColumns(columnCount);
    return this;
  }

  public FluentGridLayout columns(String columns)
  {
    get().setColumns(columns);
    return this;
  }

  public FluentGridLayout autoColumns(String columns)
  {
    get().setAutoColumns(columns);
    return this;
  }

  public FluentGridLayout rows(int rowCount)
  {
    get().setRows(rowCount);
    return this;
  }

  public FluentGridLayout rows(String rows)
  {
    get().setRows(rows);
    return this;
  }

  public FluentGridLayout autoRows(String rows)
  {
    get().setAutoRows(rows);
    return this;
  }

  public FluentGridLayout areas(String... areasPerRow)
  {
    get().setAreas(areasPerRow);
    return this;
  }

  public FluentGridLayout alignColumns(GridContentAlignment alignment)
  {
    get().setColumnsAlignment(alignment);
    return this;
  }

  public FluentGridLayout alignColumnsStart()
  {
    return alignColumns(GridContentAlignment.START);
  }

  public FluentGridLayout alignColumnsEnd()
  {
    return alignColumns(GridContentAlignment.END);
  }

  public FluentGridLayout alignColumnsCenter()
  {
    return alignColumns(GridContentAlignment.CENTER);
  }

  public FluentGridLayout alignColumnsStretch()
  {
    return alignColumns(GridContentAlignment.STRETCH);
  }

  public FluentGridLayout alignColumnsSpaceAround()
  {
    return alignColumns(GridContentAlignment.SPACE_AROUND);
  }

  public FluentGridLayout alignColumnsSpaceBetween()
  {
    return alignColumns(GridContentAlignment.SPACE_BETWEEN);
  }

  public FluentGridLayout alignColumnsSpaceEvenly()
  {
    return alignColumns(GridContentAlignment.SPACE_EVENLY);
  }


  public FluentGridLayout alignRows(GridContentAlignment alignment)
  {
    get().setRowsAlignment(alignment);
    return this;
  }

  public FluentGridLayout alignRowsStart()
  {
    return alignRows(GridContentAlignment.START);
  }

  public FluentGridLayout alignRowsEnd()
  {
    return alignRows(GridContentAlignment.END);
  }

  public FluentGridLayout alignRowsCenter()
  {
    return alignRows(GridContentAlignment.CENTER);
  }

  public FluentGridLayout alignRowsStretch()
  {
    return alignRows(GridContentAlignment.STRETCH);
  }

  public FluentGridLayout alignRowsSpaceAround()
  {
    return alignRows(GridContentAlignment.SPACE_AROUND);
  }

  public FluentGridLayout alignRowsSpaceBetween()
  {
    return alignRows(GridContentAlignment.SPACE_BETWEEN);
  }

  public FluentGridLayout alignRowsSpaceEvenly()
  {
    return alignRows(GridContentAlignment.SPACE_EVENLY);
  }


  public FluentGridLayout alignGrid(GridContentAlignment columnsAndRowsAlignment)
  {
    get().setColumnsAlignment(columnsAndRowsAlignment);
    get().setRowsAlignment(columnsAndRowsAlignment);
    return this;
  }

  public FluentGridLayout alignGrid(GridContentAlignment columnsAlignment,
      GridContentAlignment rowsAlignment)
  {
    get().setColumnsAlignment(columnsAlignment);
    get().setRowsAlignment(rowsAlignment);
    return this;
  }


  public FluentGridLayout defaultHorizontalItemAlignment(GridItemAlignment alignment)
  {
    get().setDefaultHorizontalItemAlignment(alignment);
    return this;
  }

  public FluentGridLayout defaultHorizontalItemAlignmentAuto()
  {
    return defaultHorizontalItemAlignment(GridItemAlignment.AUTO);
  }

  public FluentGridLayout defaultHorizontalItemAlignmentStart()
  {
    return defaultHorizontalItemAlignment(GridItemAlignment.START);
  }

  public FluentGridLayout defaultHorizontalItemAlignmentEnd()
  {
    return defaultHorizontalItemAlignment(GridItemAlignment.END);
  }

  public FluentGridLayout defaultHorizontalItemAlignmentCenter()
  {
    return defaultHorizontalItemAlignment(GridItemAlignment.CENTER);
  }

  public FluentGridLayout defaultHorizontalItemAlignmentStretch()
  {
    return defaultHorizontalItemAlignment(GridItemAlignment.STRETCH);
  }


  public FluentGridLayout defaultVerticalItemAlignment(GridItemAlignment alignment)
  {
    get().setDefaultVerticalItemAlignment(alignment);
    return this;
  }

  public FluentGridLayout defaultVerticalItemAlignmentAuto()
  {
    return defaultVerticalItemAlignment(GridItemAlignment.AUTO);
  }

  public FluentGridLayout defaultVerticalItemAlignmentStart()
  {
    return defaultVerticalItemAlignment(GridItemAlignment.START);
  }

  public FluentGridLayout defaultVerticalItemAlignmentEnd()
  {
    return defaultVerticalItemAlignment(GridItemAlignment.END);
  }

  public FluentGridLayout defaultVerticalItemAlignmentCenter()
  {
    return defaultVerticalItemAlignment(GridItemAlignment.CENTER);
  }

  public FluentGridLayout defaultVerticalItemAlignmentStretch()
  {
    return defaultVerticalItemAlignment(GridItemAlignment.STRETCH);
  }

  public FluentGridLayout defaultVerticalItemAlignmentBaseline()
  {
    return defaultVerticalItemAlignment(GridItemAlignment.BASELINE);
  }

  public FluentGridLayout defaultVerticalItemAlignmentLastBaseline()
  {
    return defaultVerticalItemAlignment(GridItemAlignment.LAST_BASELINE);
  }


  public FluentGridLayout defaultItemAlignment(GridItemAlignment alignment)
  {
    get().setDefaultHorizontalItemAlignment(alignment);
    get().setDefaultVerticalItemAlignment(alignment);
    return this;
  }

  public FluentGridLayout defaultItemAlignment(GridItemAlignment horizontalAlignment,
      GridItemAlignment verticalAlignment)
  {
    get().setDefaultHorizontalItemAlignment(horizontalAlignment);
    get().setDefaultVerticalItemAlignment(verticalAlignment);
    return this;
  }

}
