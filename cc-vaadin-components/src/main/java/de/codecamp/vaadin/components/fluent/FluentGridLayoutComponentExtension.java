package de.codecamp.vaadin.components.fluent;


import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.components.GridLayout;
import de.codecamp.vaadin.fluent.FluentComponent;
import de.codecamp.vaadin.fluent.FluentComponentExtension;
import java.util.function.Consumer;


@SuppressWarnings("unchecked")
public interface FluentGridLayoutComponentExtension<C extends Component, F extends FluentComponent<C, F>>
  extends
    FluentComponentExtension<C, F>
{

  default F addTo(GridLayout container)
  {
    return addTo(container, (Consumer<FluentGridLayoutConfig>) null);
  }

  default F addTo(GridLayout container, Consumer<FluentGridLayoutConfig> layoutConfigurer)
  {
    container.add(get());
    if (layoutConfigurer != null)
      layoutConfigurer.accept(new FluentGridLayoutConfig(container, get()));
    return (F) this;
  }


  default F addToAt(GridLayout container, int index)
  {
    return addToAt(container, index, (Consumer<FluentGridLayoutConfig>) null);
  }

  default F addToAt(GridLayout container, int index,
      Consumer<FluentGridLayoutConfig> layoutConfigurer)
  {
    container.addComponentAtIndex(index, get());
    if (layoutConfigurer != null)
      layoutConfigurer.accept(new FluentGridLayoutConfig(container, get()));
    return (F) this;
  }


  default F replace(GridLayout container, Component oldComponent)
  {
    return replace(container, oldComponent, (Consumer<FluentGridLayoutConfig>) null);
  }

  default F replace(GridLayout container, Component oldComponent,
      Consumer<FluentGridLayoutConfig> layoutConfigurer)
  {
    container.replace(oldComponent, get());
    if (layoutConfigurer != null)
      layoutConfigurer.accept(new FluentGridLayoutConfig(container, get()));
    return (F) this;
  }

}
