package de.codecamp.vaadin.components.fluent;


import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.base.util.SizeUtils;
import de.codecamp.vaadin.components.GridItemAlignment;
import de.codecamp.vaadin.components.GridLayout;
import de.codecamp.vaadin.fluent.FluentLayoutConfig;


public class FluentGridLayoutConfig
  extends
    FluentLayoutConfig<GridLayout, FluentGridLayoutConfig>
{

  public FluentGridLayoutConfig(GridLayout container, Component... components)
  {
    super(container, components);
  }


  public FluentGridLayoutConfig location(int x, int y)
  {
    for (Component component : getComponentsAsArray())
      getContainer().setItemLocation(component, x, y);
    return this;
  }

  public FluentGridLayoutConfig colSpan(int colSpan)
  {
    for (Component component : getComponentsAsArray())
      getContainer().setItemColumnSpan(component, colSpan);
    return this;
  }

  public FluentGridLayoutConfig rowSpan(int rowSpan)
  {
    for (Component component : getComponentsAsArray())
      getContainer().setItemRowSpan(component, rowSpan);
    return this;
  }

  public FluentGridLayoutConfig span(int colSpan, int rowSpan)
  {
    colSpan(colSpan);
    rowSpan(rowSpan);
    return this;
  }

  public FluentGridLayoutConfig area(String area)
  {
    for (Component component : getComponentsAsArray())
      getContainer().setItemArea(component, area);
    return this;
  }


  public FluentGridLayoutConfig alignHorizontal(GridItemAlignment alignment)
  {
    for (Component component : getComponentsAsArray())
      getContainer().setHorizontalItemAlignment(component, alignment);
    return this;
  }

  public FluentGridLayoutConfig alignHorizontalAuto()
  {
    return alignHorizontal(GridItemAlignment.AUTO);
  }

  public FluentGridLayoutConfig alignHorizontalStart()
  {
    return alignHorizontal(GridItemAlignment.START);
  }

  public FluentGridLayoutConfig alignHorizontalEnd()
  {
    return alignHorizontal(GridItemAlignment.END);
  }

  public FluentGridLayoutConfig alignHorizontalCenter()
  {
    return alignHorizontal(GridItemAlignment.CENTER);
  }

  public FluentGridLayoutConfig alignHorizontalStretch()
  {
    return alignHorizontal(GridItemAlignment.STRETCH);
  }


  public FluentGridLayoutConfig alignVertical(GridItemAlignment alignment)
  {
    for (Component component : getComponentsAsArray())
      getContainer().setVerticalItemAlignment(component, alignment);
    return this;
  }

  public FluentGridLayoutConfig alignVerticalAuto()
  {
    return alignVertical(GridItemAlignment.AUTO);
  }

  public FluentGridLayoutConfig alignVerticalStart()
  {
    return alignVertical(GridItemAlignment.START);
  }

  public FluentGridLayoutConfig alignVerticalEnd()
  {
    return alignVertical(GridItemAlignment.END);
  }

  public FluentGridLayoutConfig alignVerticalCenter()
  {
    return alignVertical(GridItemAlignment.CENTER);
  }

  public FluentGridLayoutConfig alignVerticalStretch()
  {
    return alignVertical(GridItemAlignment.STRETCH);
  }

  public FluentGridLayoutConfig alignVerticalBaseline()
  {
    return alignVertical(GridItemAlignment.BASELINE);
  }

  public FluentGridLayoutConfig alignVerticalLastBaseline()
  {
    return alignVertical(GridItemAlignment.LAST_BASELINE);
  }


  public FluentGridLayoutConfig align(GridItemAlignment align)
  {
    alignHorizontal(align);
    alignVertical(align);
    return this;
  }

  public FluentGridLayoutConfig align(GridItemAlignment halign, GridItemAlignment valign)
  {
    alignHorizontal(halign);
    alignVertical(valign);
    return this;
  }


  public FluentGridLayoutConfig widthFull()
  {
    return alignHorizontalStretch();
  }

  public FluentGridLayoutConfig heightFull()
  {
    for (Component child : getComponentsAsArray())
      SizeUtils.unlockHeight(child);

    return alignVerticalStretch();
  }

  public FluentGridLayoutConfig sizeFull()
  {
    widthFull();
    heightFull();
    return this;
  }

}
