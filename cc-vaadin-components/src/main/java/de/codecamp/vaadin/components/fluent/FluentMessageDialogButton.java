package de.codecamp.vaadin.components.fluent;


import static java.util.Objects.requireNonNull;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.function.SerializablePredicate;
import com.vaadin.flow.function.SerializableRunnable;
import de.codecamp.vaadin.base.util.SerializableBooleanSupplier;
import de.codecamp.vaadin.components.MessageDialog;
import de.codecamp.vaadin.fluent.visandint.FluentButton;
import de.codecamp.vaadin.fluent.visandint.FluentButtonBase;
import java.util.ArrayList;
import java.util.List;


/**
 * An variant of {@link FluentButton} intended for {@link MessageDialog}. Clicking the button will
 * close the dialog unless a listener is registered via the new method
 * {@link FluentMessageDialogButton#onClick(SerializablePredicate)} that returns {@code false}.
 */
public class FluentMessageDialogButton
  extends
    FluentButtonBase<FluentMessageDialogButton>
{

  private final List<Object> listeners = new ArrayList<>(1);


  @SuppressWarnings({"rawtypes", "unchecked"})
  public FluentMessageDialogButton(Button button, MessageDialog dialog)
  {
    super(button);

    get().addClickListener(event ->
    {
      boolean close = true;

      for (Object listener : listeners)
      {
        if (listener instanceof ComponentEventListener)
        {
          ((ComponentEventListener) listener).onComponentEvent(event);
        }
        else if (listener instanceof SerializablePredicate)
        {
          if (!((SerializablePredicate) listener).test(event))
            close = false;
        }
        else if (listener instanceof SerializableRunnable)
        {
          ((SerializableRunnable) listener).run();
        }
        else if (listener instanceof SerializableBooleanSupplier)
        {
          if (!((SerializableBooleanSupplier) listener).getAsBoolean())
            close = false;
        }
      }

      if (close)
        dialog.close();
    });
  }


  @Override
  public FluentMessageDialogButton onClick(ComponentEventListener<ClickEvent<Button>> listener)
  {
    listeners.add(requireNonNull(listener, "listener must not be null"));
    return this;
  }

  public FluentMessageDialogButton onClick(SerializablePredicate<ClickEvent<Button>> listener)
  {
    listeners.add(requireNonNull(listener, "listener must not be null"));
    return this;
  }

  public FluentMessageDialogButton onClick(SerializableRunnable listener)
  {
    listeners.add(requireNonNull(listener, "listener must not be null"));
    return this;
  }

  public FluentMessageDialogButton onClick(SerializableBooleanSupplier listener)
  {
    listeners.add(requireNonNull(listener, "listener must not be null"));
    return this;
  }

}
