package de.codecamp.vaadin.components.fluent;


import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.components.Panel;
import de.codecamp.vaadin.fluent.FluentHasSingleComponentLayoutConfig;


public class FluentPanelLayoutConfig
  extends
    FluentHasSingleComponentLayoutConfig<Panel, FluentPanelLayoutConfig>
{

  public FluentPanelLayoutConfig(Panel container, Component... components)
  {
    super(container, components);
  }

}
