package de.codecamp.vaadin.components.fluent;


import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.components.ScrollPane;
import de.codecamp.vaadin.fluent.FluentHasSingleComponentLayoutConfig;


public class FluentScrollPaneLayoutConfig
  extends
    FluentHasSingleComponentLayoutConfig<ScrollPane, FluentScrollPaneLayoutConfig>
{

  public FluentScrollPaneLayoutConfig(ScrollPane container, Component... components)
  {
    super(container, components);
  }

}
