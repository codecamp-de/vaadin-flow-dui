@FluentMappingSet( //
    staticFactoryClass = "FluentCc")
@FluentMapping( //
    componentType = Badge.class,
    themes = { //
        @ThemeGroup(variant = "LUMO_PILL"), //
        @ThemeGroup(variant = {"LUMO_SMALL", ""}, fluent = {"medium"}),
        @ThemeGroup(variant = {"LUMO_PRIMARY", ""}, fluent = {"secondary"}),
        @ThemeGroup(
            variant = {"", "LUMO_SUCCESS", "LUMO_ERROR", "LUMO_CONTRAST"},
            fluent = {"standard"}), //
    })
@FluentMapping( //
    componentType = ButtonBar.class)
@FluentMapping( //
    componentType = FieldGroup.class,
    mapped = { //
        @FluentMethod(name = "setHeaderText", fluent = "header", withTranslation = true)},
    themes = { //
        @ThemeGroup(variant = "LUMO_PADDING"),
        @ThemeGroup(variant = {"LUMO_SMALL", ""}, fluent = {"medium"}), //
    })
@FluentMapping( //
    componentType = HasViewport.class,
    mapped = { //
        @FluentMethod(name = "scrollTo"), //
        @FluentMethod(name = "scrollBy"), //
        @FluentMethod(name = "scrollToLeft"), //
        @FluentMethod(name = "scrollToRight"), //
        @FluentMethod(name = "scrollToTop"), //
        @FluentMethod(name = "scrollToBottom"), //
    })
@FluentMapping( //
    componentType = Label.class,
    themes = { //
        @ThemeGroup(variant = "LUMO_BOLD"), //
        @ThemeGroup(variant = {"LUMO_SMALL", "", "LUMO_LARGE"}, fluent = {"medium"}),
        @ThemeGroup(
            variant = {"", "LUMO_PRIMARY", "LUMO_SUCCESS", "LUMO_WARNING", "LUMO_ERROR"},
            fluent = {"standard"}), //
    })
@FluentMapping( //
    componentType = MessageDialog.class,
    mapped = { //
        @FluentMethod(name = "setTitle", withTranslation = true),
        @FluentMethod(name = "setMessage", params = {String.class}, withTranslation = true),
        @FluentMethod(name = "setMessageAsHtml", withTranslation = true),
        @FluentMethod(name = "setColorTheme", expandEnum = false), //
    })
@FluentMapping( //
    componentType = Panel.class,
    mapped = { //
        @FluentMethod(name = "setHeaderText", fluent = "header", withTranslation = true)},
    themes = { //
        @ThemeGroup(variant = "LUMO_PADDING"), //
        @ThemeGroup(variant = "LUMO_CARD"), //
        @ThemeGroup(variant = {"", "LUMO_LARGE"}, fluent = {"medium"}), //
    })
@FluentMapping( //
    componentType = ScrollPane.class)
@FluentMapping( //
    componentType = Notice.class,
    mapped = { //
        @FluentMethod(name = "setHeaderText", fluent = "header", withTranslation = true)},
    themes = { //
        @ThemeGroup(variant = "LUMO_FILLED"), //
        @ThemeGroup(variant = "LUMO_EDGE"), //
        @ThemeGroup(variant = "LUMO_BORDER"), //
        @ThemeGroup(variant = "LUMO_ELEVATED"), //
        @ThemeGroup(
            variant = {"", "LUMO_SUCCESS", "LUMO_INFO", "LUMO_WARNING", "LUMO_ERROR",
                "LUMO_CONTRAST"},
            fluent = {"standard"}), //
    })
package de.codecamp.vaadin.components.fluent;


import de.codecamp.vaadin.components.Badge;
import de.codecamp.vaadin.components.ButtonBar;
import de.codecamp.vaadin.components.FieldGroup;
import de.codecamp.vaadin.components.HasViewport;
import de.codecamp.vaadin.components.Label;
import de.codecamp.vaadin.components.MessageDialog;
import de.codecamp.vaadin.components.Notice;
import de.codecamp.vaadin.components.Panel;
import de.codecamp.vaadin.components.ScrollPane;
import de.codecamp.vaadin.fluent.annotations.FluentMapping;
import de.codecamp.vaadin.fluent.annotations.FluentMappingSet;
import de.codecamp.vaadin.fluent.annotations.FluentMethod;
import de.codecamp.vaadin.fluent.annotations.ThemeGroup;
