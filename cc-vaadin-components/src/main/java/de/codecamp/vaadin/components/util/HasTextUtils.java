package de.codecamp.vaadin.components.util;


import static java.util.stream.Collectors.toList;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasText;
import com.vaadin.flow.dom.Element;
import java.util.List;


public final class HasTextUtils
{

  private static final String ATTR_SLOT = "slot";


  private HasTextUtils()
  {
    // utility class
  }


  /**
   * Sets the content of this component to the given text. All text nodes and non-slotted child
   * elements are removed in the process. This is a slot-friendly alternative to the default
   * implementation of {@link HasText#setText(String)}.
   *
   * @param component
   *          the component for which to set the text content
   * @param text
   *          the new text
   */
  public static void setText(Component component, String text)
  {
    Element parentElement = component.getElement();

    // remove all text nodes and element that don't have a "slot" attribute
    List<Element> removeElements = parentElement.getChildren()
        .filter(child -> child.isTextNode() || !child.hasAttribute(ATTR_SLOT)).collect(toList());
    removeElements.forEach(parentElement::removeChild);

    parentElement.appendChild(Element.createText(text));
  }

}
