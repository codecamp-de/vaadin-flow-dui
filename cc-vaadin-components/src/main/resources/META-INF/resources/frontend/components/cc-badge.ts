import { LitElement, html, css } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { classMap } from 'lit/directives/class-map.js';
import { applyTheme } from 'Frontend/generated/theme';
import { PolylitMixin } from '@vaadin/component-base/src/polylit-mixin.js';
import { ControllerMixin } from '@vaadin/component-base/src/controller-mixin.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import { TooltipController } from '@vaadin/component-base/src/tooltip-controller.js';

@customElement('cc-badge')
export class CodecampBadge
  extends ThemableMixin(ControllerMixin(PolylitMixin(LitElement)))
{

  @state()
  private _hasPrefix: boolean = false;

  @state()
  private _hasSuffix: boolean = false;


  static get is() {
    return 'cc-badge';
  }

  protected createRenderRoot() {
    const root = super.createRenderRoot();
    applyTheme(root);
    return root;
  }

  render() {
    var theme: string = 'badge';
    if (this._theme)
      theme += ' ' + this._theme;

    return html`
      <span id="badge-container" theme="${theme}">
        <span part="prefix" class=${classMap({'has-prefix': this._hasPrefix})}>
          <slot name="prefix" @slotchange=${this._handlePrefixSlotChange}></slot>
        </span>
        <span part="content">
          <slot></slot>
        </span>
        <span part="suffix" class=${classMap({'has-suffix': this._hasSuffix})}>
          <slot name="suffix" @slotchange=${this._handleSuffixSlotChange}></slot>
        </span>
      </span>
      <slot name="tooltip"></slot>
    `;
  }

  firstUpdated(changedProperties) {
    super.firstUpdated(changedProperties);

    this._tooltipController = new TooltipController(this);
    this.addController(this._tooltipController);
  }

  static styles = css`
    :host {
      display: inline-block;
    }

    #badge-container {
      display: flex;
      gap: var(--lumo-space-s);
    }

    /* hide this virtual element of Lumo's badge */
    [theme~="badge"]::before {
      display: none;
    }

    [part="prefix"],
    [part="suffix"] {
      flex: none;
    }

    [part="prefix"]:not(.has-prefix),
    [part="suffix"]:not(.has-suffix) {
      display: none;
    }

    :host([hidden]) {
      display: none !important;
    }
  `;


  private _handlePrefixSlotChange(e: UIEvent)
  {
    var slotElement = e.target as HTMLSlotElement;
    this._hasPrefix = slotElement.assignedNodes({flatten: true}).length > 0;
  }

  private _handleSuffixSlotChange(e: UIEvent)
  {
    var slotElement = e.target as HTMLSlotElement;
    this._hasSuffix = slotElement.assignedNodes({flatten: true}).length > 0;
  }

}
