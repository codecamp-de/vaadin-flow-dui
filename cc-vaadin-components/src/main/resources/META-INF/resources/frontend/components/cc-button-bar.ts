import { LitElement, html, css } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { PolylitMixin } from '@vaadin/component-base/src/polylit-mixin.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';

@customElement('cc-button-bar')
export class CodecampButtonBar
  extends ThemableMixin(PolylitMixin(LitElement))
{

  @property({ reflect: true })
  orientation: string = 'horizontal';


  static get is() {
    return 'cc-button-bar';
  }

  render() {
    return html`
      <div part="start-area">
        <slot name="start"></slot>
      </div>
      <div part="end-area">
        <slot name="end"></slot>
      </div>
    `;
  }

  static styles = css`
    :host {
      display: inline-flex;
      vertical-align: bottom;
      justify-content: space-between;
      gap: calc(var(--lumo-space-xl));
    }

    [part="start-area"],
    [part="end-area"] {
      display: flex;
      gap: var(--lumo-space-m);
    }


    :host([hidden]) {
      display: none !important;
    }


    :host([orientation="vertical"]),
    :host([orientation="vertical"]) [part="start-area"],
    :host([orientation="vertical"]) [part="end-area"] {
      flex-direction: column;
    }

    :host([orientation="vertical"]) [part="start-area"],
    :host([orientation="vertical"]) [part="end-area"] {
      gap: var(--lumo-space-s);
    }
  `;

}
