import { LitElement, html, css } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { classMap } from 'lit/directives/class-map.js';
import { PolylitMixin } from '@vaadin/component-base/src/polylit-mixin.js';
import { ControllerMixin } from '@vaadin/component-base/src/controller-mixin.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import { TooltipController } from '@vaadin/component-base/src/tooltip-controller.js';

@customElement('cc-field-group')
export class CodecampFieldGroup
  extends ThemableMixin(ControllerMixin(PolylitMixin(LitElement)))
{

  @state()
  private _hasHeader: boolean = false;


  static get is() {
    return 'cc-field-group';
  }

  render() {
    return html`
      <fieldset class=${classMap({'has-header': this._hasHeader})}>
        <legend part="header">
          <slot name="header" @slotchange=${this._handleHeaderSlotChange}></slot>
          <slot name="tooltip"></slot>
        </legend>
        <div part="content">
          <slot></slot>
        </div>
      </fieldset>
    `;
  }

  firstUpdated(changedProperties) {
    super.firstUpdated(changedProperties);

    this._tooltipController = new TooltipController(this);
    this._tooltipController.setTarget(this.renderRoot.querySelector('[part="header"]'));
    this.addController(this._tooltipController);
  }

  static styles = css`

    :host {
      display: inline-block;
    }

    fieldset {
      box-sizing: border-box;
      width: 100%;
      height: 100%;
      margin: 0;
      border: 1px solid var(--lumo-contrast-10pct);
      border-radius: var(--lumo-border-radius-m);
      padding: 0;
      display: flex;
      flex-direction: column;
    }

    [part="header"] {
      margin-left: var(--lumo-space-m);
      margin-right: var(--lumo-space-m);
      padding-left: var(--lumo-space-s);
      padding-right: var(--lumo-space-s);
      font-weight: 500;
      line-height: normal;
      color: var(--lumo-secondary-text-color);
    }

    fieldset:not(.has-header) [part="header"] {
      display: none;
    }


    [part="content"] {
      flex-grow: 1;
      display: flex;
      flex-direction: column;
      align-items: start;
    }

    slot:not([name])::slotted(*) {
      flex-grow: 1;
    }


    :host([hidden]) {
      display: none !important;
    }


    :host([theme~="padding"]) [part="content"] {
      padding: var(--lumo-space-m);
    }
    :host([theme~="padding"]) fieldset.has-header [part="content"] {
      padding-top: var(--lumo-space-s);
    }

    :host([theme~="small"]) {
      font-size: var(--lumo-font-size-s);
    }
  `;


  private _handleHeaderSlotChange(e: UIEvent)
  {
    var slotElement = e.target as HTMLSlotElement;
    this._hasHeader = slotElement.assignedNodes({flatten: true}).length > 0;
  }

}
