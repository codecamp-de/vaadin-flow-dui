import { LitElement, html, css } from 'lit';
import { customElement } from 'lit/decorators.js';
import { PolylitMixin } from '@vaadin/component-base/src/polylit-mixin.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';

@customElement('cc-grid-layout')
export class CodecampGridLayout
  extends ThemableMixin(PolylitMixin(LitElement))
{

  static get is() {
    return 'cc-grid-layout';
  }

  render() {
    return html`
      <slot></slot>
    `;
  }

  static styles = css`
    :host {
      display: inline-grid;
      vertical-align: bottom;
      box-sizing: border-box;

      justify-items: start;
      align-items: start;
      justify-content: start;
      align-content: start;
    }


    :host([hidden]) {
      display: none !important;
    }


    :host([theme~='margin']) {
      margin: var(--lumo-space-m);
    }
    :host([theme~='padding']) {
      padding: var(--lumo-space-m);
    }

    :host([theme~='spacing-xs']) {
      column-gap: var(--lumo-space-xs);
      row-gap: var(--lumo-space-xs);
    }
    :host([theme~='spacing-s']) {
      column-gap: var(--lumo-space-s);
      row-gap: var(--lumo-space-s);
    }
    :host([theme~='spacing']) {
      column-gap: var(--lumo-space-m);
      row-gap: var(--lumo-space-m);
    }
    :host([theme~='spacing-l']) {
      column-gap: var(--lumo-space-l);
      row-gap: var(--lumo-space-l);
    }
    :host([theme~='spacing-xl']) {
      column-gap: var(--lumo-space-xl);
      row-gap: var(--lumo-space-xl);
    }
  `;

}
