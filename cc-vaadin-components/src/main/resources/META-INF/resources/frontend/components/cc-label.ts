import { LitElement, html, css } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { classMap } from 'lit/directives/class-map.js';
import { PolylitMixin } from '@vaadin/component-base/src/polylit-mixin.js';
import { ControllerMixin } from '@vaadin/component-base/src/controller-mixin.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import { TooltipController } from '@vaadin/component-base/src/tooltip-controller.js';

@customElement('cc-label')
export class CodecampLabel
  extends ThemableMixin(ControllerMixin(PolylitMixin(LitElement)))
{

  @state()
  private _hasPrefix: boolean = false;

  @state()
  private _hasSuffix: boolean = false;


  static get is() {
    return 'cc-label';
  }

  render() {
    return html`
      <span part="prefix" class=${classMap({'has-prefix': this._hasPrefix})}>
        <slot name="prefix" @slotchange=${this._handlePrefixSlotChange}></slot>
      </span>
      <span part="content">
        <slot></slot>
      </span>
      <span part="suffix" class=${classMap({'has-header': this._hasSuffix})}>
        <slot name="suffix" @slotchange=${this._handleSuffixSlotChange}></slot>
      </span>
      <slot name="tooltip"></slot>
    `;
  }
  
  firstUpdated(changedProperties) {
    super.firstUpdated(changedProperties);

    this._tooltipController = new TooltipController(this);
    this.addController(this._tooltipController);
  }

  static styles = css`
    :host {
      display: inline-flex;
      gap: var(--lumo-space-s);
      align-items: center;
    }

    [part="prefix"],
    [part="suffix"] {
      flex: none;
    }


    span[part="prefix"]:not(.has-prefix) {
      display: none;
    }
    span[part="suffix"]:not(.has-suffix) {
      display: none;
    }


    :host([hidden]) {
      display: none !important;
    }


    :host([theme~="small"]) {
      font-size: var(--lumo-font-size-s);
    }
    :host([theme~="large"]) {
      font-size: var(--lumo-font-size-l);
    }


    :host([theme~="bold"]) {
      font-weight: 700;
    }


    :host([theme~="primary"]) {
      color: var(--lumo-primary-text-color);
    }
    :host([theme~="success"]) {
      color: var(--lumo-success-text-color);
    }
    :host([theme~="warning"]) {
      color: var(--lumo-warning-text-color);
    }
    :host([theme~="error"]) {
      color: var(--lumo-error-text-color);
    }


    :host([disabled]) {
      color: var(--lumo-disabled-text-color);
    }
  `;


  private _handlePrefixSlotChange(e: UIEvent)
  {
    var slotElement = e.target as HTMLSlotElement;
    this._hasPrefix = slotElement.assignedNodes({flatten: true}).length > 0;
  }

  private _handleSuffixSlotChange(e: UIEvent)
  {
    var slotElement = e.target as HTMLSlotElement;
    this._hasSuffix = slotElement.assignedNodes({flatten: true}).length > 0;
  }

}
