import { LitElement, html, css } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { classMap } from 'lit/directives/class-map.js';
import { applyTheme } from 'Frontend/generated/theme';
import { PolylitMixin } from '@vaadin/component-base/src/polylit-mixin.js';
import { ControllerMixin } from '@vaadin/component-base/src/controller-mixin.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';

@customElement('cc-notice')
export class CodecampNotice
  extends ThemableMixin(ControllerMixin(PolylitMixin(LitElement)))
{

  @state()
  private _hasPrefix: boolean = false;

  @state()
  private _hasSuffix: boolean = false;


  static get is() {
    return 'cc-notice';
  }

  protected createRenderRoot() {
    const root = super.createRenderRoot();
    applyTheme(root);
    return root;
  }

  render() {
    return html`
      <div part="prefix" class=${classMap({'has-prefix': this._hasPrefix})}>
        <slot name="prefix" @slotchange=${this._handlePrefixSlotChange}></slot>
      </div>
      <div part="main">
        <div part="header">
          <slot name="header"></slot>
        </div>
        <div part="content">
          <slot></slot>
        </div>
      </div>
      <div part="suffix" class=${classMap({'has-suffix': this._hasSuffix})}>
        <slot name="suffix" @slotchange=${this._handleSuffixSlotChange}></slot>
      </div>
    `;
  }

  static styles = css`
    :host {
      box-sizing: border-box;
      display: inline-flex;
      vertical-align: bottom;
      flex-direction: row;
      align-items: baseline;
  
      border-radius: var(--lumo-border-radius-m);
      padding: var(--lumo-space-s) var(--lumo-space-m);

      font-family: var(--lumo-font-family);
    }


    [part="main"] {
      flex-grow: 1;
    }

    [part="prefix"] {
      margin-right: var(--lumo-space-m);
    }
    [part="suffix"] {
      margin-left: var(--lumo-space-m);
    }

    [part="prefix"]:not(.has-prefix),
    [part="suffix"]:not(.has-suffix) {
      display: none;
    }

    [part="header"] {
      font-size: var(--lumo-font-size-m);
      font-weight: 500;
    }
    [part="content"] {
      font-size: var(--lumo-font-size-s);
      color: var(--lumo-secondary-text-color);
    }

    :host([hidden]) {
      display: none !important;
    }


    :host([theme~="border"]) {
      border: 1px solid var(--cc-notice-border-color);
    }
    :host([theme~="edge"]) {
      border-left: var(--lumo-space-xs) solid var(--cc-notice-border-color);
    }


    :host {
      --cc-notice-prefix-color: var(--lumo-header-text-color);
      --cc-notice-background-color: var(--lumo-contrast-10pct);
      --cc-notice-border-color: var(--lumo-contrast-20pct);
    }
    [part="prefix"] {
      color: var(--cc-notice-prefix-color);
    }

    :host([theme~="elevated"]) {
      box-shadow: var(--lumo-box-shadow-xs);
    }
    :host([theme~="filled"]),
    :host(:not([theme~="border"])) {
      background-color: var(--cc-notice-background-color);
    }
    :host([theme~="edge"]),
    :host([theme~="border"]) {
      border-color: var(--cc-notice-border-color);
    }


    :host([theme~="success"]) {
      --cc-notice-prefix-color: var(--lumo-success-text-color);
      --cc-notice-background-color: var(--lumo-success-color-10pct);
      --cc-notice-border-color: var(--lumo-success-color);
    }

    :host([theme~="info"]) {
      --cc-notice-prefix-color: var(--lumo-primary-text-color);
      --cc-notice-background-color: var(--lumo-primary-color-10pct);
      --cc-notice-border-color: var(--lumo-primary-color);
    }

    :host([theme~="warning"]) {
      --cc-notice-prefix-color: var(--lumo-warning-text-color);
      --cc-notice-background-color: var(--lumo-warning-color-10pct);
      --cc-notice-border-color: var(--lumo-warning-color);
    }

    :host([theme~="error"]) {
      --cc-notice-prefix-color: var(--lumo-error-text-color);
      --cc-notice-background-color: var(--lumo-error-color-10pct);
      --cc-notice-border-color: var(--lumo-error-color);
    }

    :host([theme~="contrast"]) {
      --cc-notice-prefix-color: var(--lumo-contrast);
      --cc-notice-background-color: var(--lumo-contrast-10pct);
      --cc-notice-border-color: var(--lumo-contrast);
    }
  `;


  private _handlePrefixSlotChange(e: UIEvent)
  {
    var slotElement = e.target as HTMLSlotElement;
    this._hasPrefix = slotElement.assignedNodes({flatten: true}).length > 0;
  }

  private _handleSuffixSlotChange(e: UIEvent)
  {
    var slotElement = e.target as HTMLSlotElement;
    this._hasSuffix = slotElement.assignedNodes({flatten: true}).length > 0;
  }

}
