import { LitElement, html, css } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { PolylitMixin } from '@vaadin/component-base/src/polylit-mixin.js';
import { ControllerMixin } from '@vaadin/component-base/src/controller-mixin.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import { TooltipController } from '@vaadin/component-base/src/tooltip-controller.js';
import { ViewportMixin } from './cc-viewport-mixin';
import './cc-scroll-pane.ts';

@customElement('cc-panel')
export class CodecampPanel
  extends ViewportMixin(ThemableMixin(ControllerMixin(PolylitMixin(LitElement))))
{

  @property({ attribute: 'scroll-direction', reflect: true })
  scrollDirection: string = 'none';


  static get is() {
    return 'cc-panel';
  }

  render() {
    return html`
      <div part="header">
        <slot name="header"></slot>
        <slot name="tooltip"></slot>
      </div>
      <div part="content">
      ${(this.scrollDirection && this.scrollDirection != 'none') ?
        html`<cc-scroll-pane id="viewport" scroll-direction=${this.scrollDirection} theme=${this._theme}>
          <slot></slot>
        </cc-scroll-pane>` :
        html`<slot></slot>`
      }
      </div>
    `;
  }

  firstUpdated(changedProperties) {
    super.firstUpdated(changedProperties);

    this._tooltipController = new TooltipController(this);
    this._tooltipController.setTarget(this.renderRoot.querySelector('[part="header"]'));
    this.addController(this._tooltipController);
  }

  static styles = css`
    :host {
      box-sizing: border-box;
      display: inline-flex;
      vertical-align: bottom;
      flex-direction: column;

      border-radius: var(--lumo-border-radius-m);
    }

    [part="header"] {
      border-top-left-radius: var(--lumo-border-radius-m);
      border-top-right-radius: var(--lumo-border-radius-m);
      padding: var(--lumo-space-xs) var(--lumo-space-m);
      background-color: var(--cc-panel-border-color);

      font-family: var(--lumo-font-family);
      font-size: var(--lumo-font-size-s);
      font-weight: 500;
      color: var(--cc-panel-header-font-color);
    }

    [part="content"] {
      flex-grow: 1;
      display: flex;
      flex-direction: column;

      border-bottom-left-radius: var(--lumo-border-radius-m);
      border-bottom-right-radius: var(--lumo-border-radius-m);
      border: 1px solid var(--cc-panel-border-color);
      border-top: 0;
    }

    cc-scroll-pane {
      flex-grow: 1;
    }


    :host([hidden]) {
      display: none !important;
    }


    :host {
      --cc-panel-border-color: var(--lumo-contrast-5pct);
      --cc-panel-header-font-color: var(--lumo-secondary-text-color);
    }

    :host([theme~="large"]) [part="header"] {
      padding-top: var(--lumo-space-s);
      padding-bottom: var(--lumo-space-s);
      font-size: var(--lumo-font-size-m);
    }

    :host([theme~="padding"]) [part="content"] {
      padding: var(--lumo-space-m);
    }

    :host([theme~="card"]) {
      box-shadow: var(--lumo-box-shadow-xs);
    }

    :host([disabled]) {
      --cc-panel-border-color: var(--lumo-contrast-10pct);
      --cc-panel-header-font-color: var(--lumo-disabled-text-color);
    }
  `;

}
