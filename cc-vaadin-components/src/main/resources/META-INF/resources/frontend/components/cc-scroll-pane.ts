import { LitElement, html, css } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { PolylitMixin } from '@vaadin/component-base/src/polylit-mixin.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import { ViewportMixin } from './cc-viewport-mixin';

@customElement('cc-scroll-pane')
export class CodecampScrollPane
  extends ViewportMixin(ThemableMixin(PolylitMixin(LitElement)))
{

  @property({attribute: 'scroll-direction', reflect: true})
  scrollDirection: string = 'both';

  @property({attribute: 'fit-content', reflect: true})
  fitContent: string = 'none';


  static get is() {
    return 'cc-scroll-pane';
  }

  render() {
    return html`
      <div id="h-sizer">
        <div id="viewport">
          <slot></slot>
        </div>
      </div>
    `;
  }

  static styles = css`
    :host { /* v-sizer */
      box-sizing: border-box;

      display: inline-flex;
      vertical-align: bottom;
      flex-direction: column;
    }

    #h-sizer {
      flex-grow: 1;
      min-height: var(--lumo-space-m);

      display: flex;
      flex-direction: row;
    }

    #viewport {
      flex-grow: 1;
      min-width: var(--lumo-space-m);
      overflow: hidden;

      display: flex;
      flex-direction: column;
      align-items: start;
    }

    :host(:where([scroll-direction="vertical"],[scroll-direction="both"]):not([fit-content="height"]):not([fit-content="both"])) #h-sizer {
      height: var(--lumo-space-m);
    }
    :host([scroll-direction="vertical"]) #viewport,
    :host([scroll-direction="both"]) #viewport {
      overflow-y: auto;
    }

    :host([scroll-direction="horizontal"]) #h-sizer,
    :host([scroll-direction="both"]) #h-sizer {
    }
    :host([scroll-direction="horizontal"]) #viewport,
    :host([scroll-direction="both"]) #viewport {
      overflow-x: auto;
    }
    :host(:where([scroll-direction="horizontal"],[scroll-direction="both"]):not([fit-content="width"]):not([fit-content="both"])) #viewport {
      width: var(--lumo-space-m);
    }

    :host([scroll-direction="horizontal"]) ::slotted(*),
    :host([scroll-direction="both"]) ::slotted(*) {
      min-width: min-content;
    }


    :host([hidden]) {
      display: none !important;
    }
  `;

}
