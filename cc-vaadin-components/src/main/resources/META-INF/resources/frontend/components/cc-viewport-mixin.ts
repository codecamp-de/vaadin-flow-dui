import { LitElement } from 'lit';
import type { Constructor } from '@open-wc/dedupe-mixin';

export const ViewportMixin = <T extends Constructor<LitElement>>(base: T) => {
  abstract class Viewport extends base {

    protected _viewport: HTMLElement;


    updated(changedProperties: any) {
      super.updated(changedProperties);

      this._viewport = this.shadowRoot.querySelector('#viewport');
      if (this._viewport) {
        this._viewport.addEventListener('scroll', (e: Event) => {
          var event = new CustomEvent('scroll', {
            detail: {
              scrollTop: this._viewport.scrollTop,
              scrollLeft: this._viewport.scrollLeft
            }
          });
          this.dispatchEvent(event);
        });
      }
    }


    scrollTo(...args): void {
      if (!this._viewport)
        return;
      this._viewport.scrollTo(...args);
    }

    scrollBy(...args): void {
      if (!this._viewport)
        return;
      this._viewport.scrollBy(...args);
    }


    scrollToLeft(behavior?: string): void {
      this.#scrollTo(0, undefined, behavior);
    }

    scrollToRight(behavior?: string): void {
      this.#scrollTo(this._viewport.scrollWidth, undefined, behavior);
    }

    scrollToTop(behavior?: string): void {
      this.#scrollTo(undefined, 0, behavior);
    }

    scrollToBottom(behavior?: string): void {
      this.#scrollTo(undefined, this._viewport.scrollHeight, behavior);
    }

    #scrollTo(left?: number, top?: number, behavior?: string): void {
      if (!behavior)
        behavior = 'auto';

      var options = {};
      if (left != null)
        options['left'] = left;
      if (top != null)
        options['top'] = top;
      if (behavior)
        options['behavior'] = behavior;
      this.scrollTo(options);
    }

    #scrollBy(left?: number, top?: number, behavior?: string): void {
      if (!behavior)
        behavior = 'auto';

      var options = {};
      if (left != null)
        options['left'] = left;
      if (top != null)
        options['top'] = top;
      if (behavior)
        options['behavior'] = behavior;
      this.scrollBy(options);
    }

  }
  return Viewport;
};
