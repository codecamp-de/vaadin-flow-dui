<project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"
    child.project.url.inherit.append.path="false">
  <modelVersion>4.0.0</modelVersion>

  <groupId>de.codecamp.vaadin</groupId>
  <artifactId>vaadin-flow-ui-toolkit</artifactId>
  <version>5.1.1-SNAPSHOT</version>
  <packaging>pom</packaging>

  <name>Vaadin Flow UI Toolkit</name>
  <description>A set of UI tools for Vaadin Flow.</description>
  <url>https://gitlab.com/codecamp-de/${gitProjectId}</url>

  <licenses>
    <license>
      <name>Apache License, Version 2.0</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <developers>
    <developer>
      <name>Patrick Schmidt</name>
      <email>patrick@codecamp.de</email>
    </developer>
  </developers>

  <scm child.scm.url.inherit.append.path="false" child.scm.connection.inherit.append.path="false" child.scm.developerConnection.inherit.append.path="false">
    <url>https://gitlab.com/codecamp-de/${gitProjectId}</url>
    <connection>scm:git:https://gitlab.com/codecamp-de/${gitProjectId}.git</connection>
    <developerConnection>scm:git:https://gitlab.com/codecamp-de/${gitProjectId}.git</developerConnection>
  </scm>


  <modules>
    <module>vaadin-base-annotations</module>
    <module>vaadin-base-processor</module>
    <module>vaadin-base</module>
    <module>vaadin-flow-dui</module>
    <module>vaadin-flow-fluent-annotations</module>
    <module>vaadin-flow-fluent-processor</module>
    <module>vaadin-flow-fluent</module>
    <module>cc-vaadin-components</module>
    <module>vaadin-flow-ui-toolkit-bom</module>
  </modules>


  <properties>
    <gitProjectId>vaadin-flow-ui-toolkit</gitProjectId>

    <maven.compiler.release>17</maven.compiler.release>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

    <vaadin.version>24.5.4</vaadin.version>
    <spring-boot.version>3.2.0</spring-boot.version>

    <tiles-maven-plugin.version>2.40</tiles-maven-plugin.version>

    <m2e.apt.activation>jdt_apt</m2e.apt.activation>
  </properties>


  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>com.vaadin</groupId>
        <artifactId>vaadin-bom</artifactId>
        <version>${vaadin.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-dependencies</artifactId>
        <version>${spring-boot.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>


  <build>

    <pluginManagement>
      <plugins>

        <plugin>
          <groupId>io.repaint.maven</groupId>
          <artifactId>tiles-maven-plugin</artifactId>
          <version>${tiles-maven-plugin.version}</version>
          <extensions>true</extensions>
        </plugin>

        <!-- don't install/deploy this POM, but all modules -->
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-install-plugin</artifactId>
          <inherited>false</inherited>
          <configuration>
            <skip>true</skip>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-deploy-plugin</artifactId>
          <inherited>false</inherited>
          <configuration>
            <skip>true</skip>
          </configuration>
        </plugin>

      </plugins>
    </pluginManagement>

  </build>

  <profiles>

    <profile>
      <id>eclipse</id>
      <activation>
        <property>
          <name>eclipse.home.location</name>
        </property>
      </activation>
      <properties>
        <spotless.check.skip>true</spotless.check.skip>
      </properties>
    </profile>
 
  </profiles>

</project>
