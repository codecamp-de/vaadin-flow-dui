package de.codecamp.vaadin.base.annotations;


import com.vaadin.flow.component.Component;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Declares a localizable {@link Component} to generate constants and utility methods for it.
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.CLASS)
@Repeatable(I18nUtils.Container.class)
@Documented
public @interface I18nUtils
{

  /**
   * Returns the localizable {@link Component} type. Requires a getter ({@code getI18n}) and setter
   * ({@code setI18n}) for an i18n bean containing the localizable properties. The type can be
   * omitted when this annotation is placed on the component directly.
   *
   * @return the localizable {@link Component} type
   */
  Class<?> componentType() default Void.class;

  /**
   * Returns additional source code that will be executed when localizing a {@link Component}.
   *
   * @return additional source code that will be executed when localizing a {@link Component}
   */
  String componentLocalize() default "";

  /**
   * Returns additional source code that will be executed when creating a new localized i18n bean.
   *
   * @return additional source code that will be executed when creating a new localized i18n bean
   */
  String i18nInit() default "";

  /**
   * Returns additional source code that will be executed when copying data from a cached i18n bean
   * into a new or existing i18n bean.
   *
   * @return additional source code that will be executed when copying data from a cached i18n bean
   *         into a new or existing i18n bean
   */
  String i18nCopy() default "";


  /**
   * Container for repeated {@link I18nUtils} annotations.
   */
  @Target({ElementType.TYPE})
  @Retention(RetentionPolicy.CLASS)
  @Documented
  @interface Container
  {

    /**
     * Returns the {@link I18nUtils} annotations.
     *
     * @return the {@link I18nUtils} annotations
     */
    I18nUtils[] value();

  }

}
