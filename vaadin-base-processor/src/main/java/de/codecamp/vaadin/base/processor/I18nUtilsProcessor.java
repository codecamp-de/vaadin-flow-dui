package de.codecamp.vaadin.base.processor;


import static de.codecamp.vaadin.base.processor.I18nUtilsProcessor.OPT_DEBUG;
import static de.codecamp.vaadin.base.processor.I18nUtilsProcessor.OPT_TARGET_PACKAGE;
import static javax.lang.model.element.Modifier.FINAL;
import static javax.lang.model.element.Modifier.PRIVATE;
import static javax.lang.model.element.Modifier.PUBLIC;
import static javax.lang.model.element.Modifier.STATIC;
import static org.apache.commons.lang3.StringUtils.toRootUpperCase;

import com.google.auto.service.AutoService;
import com.palantir.javapoet.ClassName;
import com.palantir.javapoet.FieldSpec;
import com.palantir.javapoet.JavaFile;
import com.palantir.javapoet.MethodSpec;
import com.palantir.javapoet.ParameterizedTypeName;
import com.palantir.javapoet.TypeSpec;
import de.codecamp.vaadin.base.annotations.I18nUtils;
import io.toolisticon.aptk.tools.AbstractAnnotationProcessor;
import io.toolisticon.aptk.tools.TypeMirrorWrapper;
import io.toolisticon.aptk.tools.wrapper.ExecutableElementWrapper;
import io.toolisticon.aptk.tools.wrapper.TypeElementWrapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.BiConsumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedOptions;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic.Kind;


@AutoService(Processor.class)
@SupportedOptions({OPT_TARGET_PACKAGE, OPT_DEBUG})
public class I18nUtilsProcessor
  extends
    AbstractAnnotationProcessor
{

  /* package */ static final String OPT_TARGET_PACKAGE = "i18n.targetPackage";

  /* package */ static final String OPT_DEBUG = "i18n.debug";


  private static final String MESSAGE_KEY_CONSTANT_PREFIX = "KEY_";

  private static final String METHOD_CREATE_LOCALIZED_I18N = "createLocalizedI18n";

  private static final String VAR_I18N = "i18n";

  private static final String VAR_CACHED_I18N = "cachedI18n";

  private static final String METHOD_GET_I18N = "getI18n";

  private static final String METHOD_SET_I18N = "setI18n";


  /**
   * Used to log errors with a throwable that would otherwise be lost.
   */
  private static final Logger LOG = Logger.getLogger(I18nUtilsProcessor.class.getName());


  private boolean debug;


  public I18nUtilsProcessor()
  {
  }


  @Override
  public Set<String> getSupportedAnnotationTypes()
  {
    return Set.of(I18nUtils.class.getCanonicalName(), I18nUtils.Container.class.getCanonicalName());
  }


  @Override
  public boolean processAnnotations(Set<? extends TypeElement> annotations,
      RoundEnvironment roundEnv)
  {
    String targetPackage = processingEnv.getOptions().get(OPT_TARGET_PACKAGE);
    debug = Boolean.parseBoolean(processingEnv.getOptions().get(OPT_DEBUG));


    Set<? extends Element> sourceElements =
        ProcessorUtils.getElementsAnnotatedWith(roundEnv, I18nUtils.class);
    if (sourceElements.isEmpty())
      return true;


    for (Element sourceElementTemp : sourceElements)
    {
      TypeElement sourceElement = (TypeElement) sourceElementTemp;

      printNote(String.format("Processing @I18nUtils on '%s'...",
          sourceElement.getQualifiedName().toString()), sourceElement, null);

      try
      {
        List<AnnotationMirror> annotationMirrors =
            LangModelUtils.getAnnotationMirrors(sourceElement, I18nUtils.class);
        for (AnnotationMirror i18nUtilsAt : annotationMirrors)
        {
          I18nUtilsWrapper i18nUtilsAnnot = I18nUtilsWrapper.wrap(sourceElement, i18nUtilsAt);
          TypeElementWrapper componentType;
          if (i18nUtilsAnnot.componentTypeIsDefaultValue())
          {
            componentType = TypeElementWrapper.wrap(sourceElement);
          }
          else
          {
            componentType =
                i18nUtilsAnnot.componentTypeAsTypeMirrorWrapper().getTypeElement().get();
          }

          Optional<ExecutableElementWrapper> getI18nMethodOpt =
              componentType.getMethod(METHOD_GET_I18N);
          if (getI18nMethodOpt.isEmpty())
          {
            printError(
                String.format("Component type '%s' does not have a %s() method.",
                    componentType.getQualifiedName(), METHOD_GET_I18N),
                null, sourceElement, i18nUtilsAt);
            continue;
          }

          TypeElementWrapper i18nType =
              getI18nMethodOpt.get().getReturnType().getTypeElement().get();
          ClassName i18nPoetType = ClassName.get(i18nType.unwrap());


          String i18nUtilsSimpleName = i18nType.getSimpleName().replace("I18N", "I18n") + "Utils";

          TypeSpec.Builder i18nUtilsBuilder = TypeSpec.classBuilder(i18nUtilsSimpleName)
              .addJavadoc("@see $T", i18nType.unwrap()).addModifiers(PUBLIC);

          String constantsPrefix = "I18N_" + toRootUpperCase(componentType.getSimpleName()) + "_";
          String prefixConstantName = constantsPrefix + "PREFIX";
          String cacheConstantName = constantsPrefix + "CACHE";
          String blankConstantName = constantsPrefix + "BLANK";


          List<Property> properties =
              getBeanProperties(i18nType.unwrap(), null, sourceElementTemp, i18nUtilsAt);


          i18nUtilsBuilder.addField(FieldSpec
              .builder(String.class, prefixConstantName, PRIVATE, STATIC, FINAL) //
              .initializer("$T.class.getName() + $S", ClassName.get(componentType.unwrap()), ".")
              .build());

          walkProperties(properties, null, (property, parentProperty) ->
          {
            if (property.hasNestedProperties())
              return;

            i18nUtilsBuilder.addField(FieldSpec
                .builder(String.class, MESSAGE_KEY_CONSTANT_PREFIX + property.getConstantSuffix(),
                    PUBLIC, STATIC, FINAL) //
                .initializer("$L + $S", prefixConstantName, property.getKeySuffix()) //
                .build());
          });

          i18nUtilsBuilder.addField(FieldSpec
              .builder(
                  ParameterizedTypeName.get(ClassName.get(ConcurrentMap.class),
                      ClassName.get(Locale.class), i18nPoetType),
                  cacheConstantName, PRIVATE, STATIC, FINAL) //
              .initializer("new $T<>()", ConcurrentHashMap.class) //
              .build());

          i18nUtilsBuilder
              .addField(FieldSpec.builder(i18nPoetType, blankConstantName, PRIVATE, STATIC, FINAL) //
                  .initializer("new $T()", i18nPoetType) //
                  .build());

          i18nUtilsBuilder.addMethod(
              MethodSpec.constructorBuilder().addModifiers(PRIVATE).addStatement("// utility class") //
                  .build());

          i18nUtilsBuilder.addMethod(
              MethodSpec.methodBuilder("localize").addModifiers(PUBLIC, STATIC).returns(void.class) //
                  .addParameter(ClassName.get(componentType.unwrap()), "component") //
                  .addStatement("localize(component, null)") //
                  .addJavadoc("Localizes the given component.") //
                  .addJavadoc("\n\n") //
                  .addJavadoc("@param component the component to localize") //
                  .build());


          MethodSpec.Builder localizeWithLocaleMethodBuilder =
              MethodSpec.methodBuilder("localize").addModifiers(PUBLIC, STATIC).returns(void.class) //
                  .addParameter(ClassName.get(componentType.unwrap()), "component") //
                  .addParameter(Locale.class, "locale");

          if (!i18nUtilsAnnot.componentLocalizeIsDefaultValue())
          {
            localizeWithLocaleMethodBuilder.addCode(i18nUtilsAnnot.componentLocalize());
          }

          localizeWithLocaleMethodBuilder //
              .addStatement("$T $L = localize(component.$L(), locale)", i18nPoetType, VAR_I18N,
                  METHOD_GET_I18N) //
              .addStatement("if ($L != null) component.$L($L)", VAR_I18N, METHOD_SET_I18N, VAR_I18N) //
              .addJavadoc("Localizes the given component for the given locale.") //
              .addJavadoc("\n") //
              .addJavadoc("\n@param component the component to localize") //
              .addJavadoc("\n@param locale the locale; may be null to use the current locale");

          i18nUtilsBuilder.addMethod(localizeWithLocaleMethodBuilder.build());


          MethodSpec.Builder localizeMethodBuilder = MethodSpec.methodBuilder("localize")
              .addModifiers(PUBLIC, STATIC).returns(i18nPoetType) //
              .addParameter(i18nPoetType, VAR_I18N) //
              .addParameter(Locale.class, "locale")
              .addStatement("if (locale == null) locale = $T.getLocale()",
                  ClassName.bestGuess("de.codecamp.vaadin.base.i18n.TranslationUtils"))
              .addCode("\n") //
              .addStatement("$T $L = $L.computeIfAbsent(locale, $L::$L)", i18nPoetType,
                  VAR_CACHED_I18N, cacheConstantName, i18nUtilsSimpleName,
                  METHOD_CREATE_LOCALIZED_I18N)
              .addStatement("if ($L == $L) return $L", VAR_CACHED_I18N, blankConstantName, VAR_I18N) //
              .addCode("\n") //
              .addCode("\n") //
              .addStatement("if ($L == null) $L = new $T()", VAR_I18N, VAR_I18N, i18nPoetType);

          if (!i18nUtilsAnnot.i18nCopyIsDefaultValue())
          {
            localizeMethodBuilder //
                .addCode("\n") //
                .addCode(i18nUtilsAnnot.i18nCopy());
          }

          if (!properties.isEmpty() && !properties.get(0).hasNestedProperties())
          {
            localizeMethodBuilder //
                .addCode("\n");
          }

          walkProperties(properties, null, (property, parentProperty) ->
          {
            String parentBeanVariableTarget;
            String parentBeanVariableSource;
            if (parentProperty == null)
            {
              parentBeanVariableTarget = VAR_I18N;
              parentBeanVariableSource = VAR_CACHED_I18N;
            }
            else
            {
              parentBeanVariableTarget = VAR_I18N + "_" + parentProperty.getKeySuffix();
              parentBeanVariableSource = VAR_CACHED_I18N + "_" + parentProperty.getKeySuffix();
            }

            String getterName = property.getGetter().getSimpleName().toString();
            String setterName = property.getSetter().getSimpleName().toString();

            if (property.hasNestedProperties())
            {
              TypeElement propertyType =
                  TypeMirrorWrapper.wrap(property.getType()).getTypeElement().get().unwrap();

              String beanVariableTarget = VAR_I18N + "_" + property.getKeySuffix();
              String beanVariableSource = VAR_CACHED_I18N + "_" + property.getKeySuffix();

              localizeMethodBuilder //
                  .addCode("\n") //
                  .addStatement("$T $L = $L.$L()", propertyType, beanVariableSource,
                      parentBeanVariableSource, getterName)
                  .addStatement("$T $L = $L.$L()", propertyType, beanVariableTarget,
                      parentBeanVariableTarget, getterName)
                  .beginControlFlow("if ($L == null)", beanVariableTarget)
                  .addStatement("$L = new $T()", beanVariableTarget, propertyType)
                  .addStatement("$L.$L($L)", parentBeanVariableTarget, setterName,
                      beanVariableTarget)
                  .endControlFlow();
            }
            else
            {
              localizeMethodBuilder //
                  .addStatement("$L.$L($L.$L())", parentBeanVariableTarget, setterName,
                      parentBeanVariableSource, getterName);
            }
          });

          localizeMethodBuilder //
              .addCode("\n") //
              .addStatement("return $L", VAR_I18N) //
              .addJavadoc(
                  "Localizes the given internationalization properties for the given locale.") //
              .addJavadoc("\n") //
              .addJavadoc(
                  "\n@param i18n the internationalization properties to localize; may be {@code null} to create and localize a new instance") //
              .addJavadoc("\n@param locale the locale; may be null to use the current locale") //
              .addJavadoc("\n@return the localized internationalization properties");

          i18nUtilsBuilder.addMethod(localizeMethodBuilder.build());


          MethodSpec.Builder createLocalizedI18nMethodBuilder =
              MethodSpec.methodBuilder(METHOD_CREATE_LOCALIZED_I18N).addModifiers(PRIVATE, STATIC)
                  .returns(i18nPoetType) //
                  .addParameter(Locale.class, "locale") //
                  .addStatement("$T $L = new $T()", i18nPoetType, VAR_I18N, i18nPoetType) //
                  .addCode("\n") //
                  .addStatement("boolean anyMessageSet = false");

          if (!i18nUtilsAnnot.i18nInitIsDefaultValue())
          {
            createLocalizedI18nMethodBuilder //
                .addCode("\n") //
                .addCode(i18nUtilsAnnot.i18nInit());
          }

          if (!properties.isEmpty() && !properties.get(0).hasNestedProperties())
          {
            createLocalizedI18nMethodBuilder //
                .addCode("\n");
          }

          walkProperties(properties, null, (property, parentProperty) ->
          {
            String parentBeanVariable;
            if (parentProperty == null)
            {
              parentBeanVariable = VAR_I18N;
            }
            else
            {
              parentBeanVariable = parentProperty.getKeySuffix();
            }

            if (property.hasNestedProperties())
            {
              TypeElement propertyType =
                  TypeMirrorWrapper.wrap(property.getType()).getTypeElement().get().unwrap();

              createLocalizedI18nMethodBuilder //
                  .addCode("\n") //
                  .addStatement("$T $L = new $T()", propertyType, property.getKeySuffix(),
                      propertyType)
                  .addStatement("$L.$L($L)", parentBeanVariable,
                      property.getSetter().getSimpleName().toString(), property.getKeySuffix());
            }
            else
            {
              createLocalizedI18nMethodBuilder //
                  .addStatement("anyMessageSet |= optionalTranslate(locale, $L, $L::$L)",
                      MESSAGE_KEY_CONSTANT_PREFIX + property.getConstantSuffix(),
                      parentBeanVariable, property.getSetter().getSimpleName().toString());
            }
          });

          createLocalizedI18nMethodBuilder //
              .addCode("\n") //
              .addStatement("return anyMessageSet ? $L : $L", VAR_I18N, blankConstantName);
          i18nUtilsBuilder.addMethod(createLocalizedI18nMethodBuilder.build());


          JavaFile.builder(targetPackage, i18nUtilsBuilder.build())
              .addStaticImport(ClassName.bestGuess("de.codecamp.vaadin.base.i18n.TranslationUtils"),
                  "optionalTranslate")
              .build().writeTo(processingEnv.getFiler());
        }
      }
      catch (IOException | RuntimeException ex)
      {
        printError(String.format("Failed to process @I18nUtils on '%s'...",
            sourceElement.getQualifiedName().toString()), ex, sourceElement);
      }
    }

    return true;
  }

  private List<Property> getBeanProperties(TypeElement beanType, String keyPrefix,
      Element sourceElement, AnnotationMirror sourceAnnotation)
  {
    List<Property> properties = new ArrayList<>();
    LangModelUtils.findBeanProperties(processingEnv, beanType, false).forEach((name, getset) ->
    {
      /* warn about and remove properties without setters */
      if (getset.getRight() == null)
      {
        printDebug(String.format("No setter found for property '%s' on type '%s'.", name,
            beanType.getQualifiedName().toString()), sourceElement, sourceAnnotation);
        return;
      }

      String key = keyPrefix == null ? name : keyPrefix + "_" + name;


      TypeMirrorWrapper returnTypeMirror =
          ExecutableElementWrapper.wrap(getset.getLeft()).getReturnType();

      List<Property> nestedProperties = null;

      // TODO detect arrays
      // TODO detect lists

      if (!returnTypeMirror.isPrimitive() && !returnTypeMirror.getPackage().equals("java.lang")
          && !returnTypeMirror.getPackage().equals("java.util")
          && !returnTypeMirror.getPackage().equals("java.time"))
      {
        TypeElementWrapper propertyType = returnTypeMirror.getTypeElement().get();
        nestedProperties =
            getBeanProperties(propertyType.unwrap(), key, sourceElement, sourceAnnotation);
      }

      properties.add(new Property(name, toRootUpperCase(key), key, returnTypeMirror.unwrap(),
          getset.getLeft(), getset.getRight(), nestedProperties));
    });

    /* normal properties first, then nested properties */
    Collections.sort(properties, Comparator.comparing(Property::hasNestedProperties));

    return properties;
  }

  private void walkProperties(List<Property> properties, Property parentProperty,
      BiConsumer<Property, Property> propertyVisitor)
  {
    for (Property property : properties)
    {
      propertyVisitor.accept(property, parentProperty);

      if (property.hasNestedProperties())
        walkProperties(property.getNestedProperties(), property, propertyVisitor);
    }
  }

  private void printDebug(String msg, @SuppressWarnings("unused") Element sourceElement,
      @SuppressWarnings("unused") AnnotationMirror sourceAnnotation)
  {
    /*
     * sourceElement is ignored currently, because it seems like more noise than actually being
     * useful.
     */
    if (debug)
      processingEnv.getMessager().printMessage(Kind.NOTE, msg, /* sourceElement */ null);
  }

  private void printNote(String msg, Element sourceElement, AnnotationMirror annotation)
  {
    processingEnv.getMessager().printMessage(Kind.NOTE, msg, sourceElement, annotation);
  }

  private void printError(String msg, Throwable throwable, Element sourceElement)
  {
    printError(msg, throwable, sourceElement, null);
  }

  private void printError(String msg, Throwable throwable, Element sourceElement,
      AnnotationMirror sourceAnnotation)
  {
    if (throwable == null)
    {
      processingEnv.getMessager().printMessage(Kind.ERROR, msg, sourceElement, sourceAnnotation);
    }
    else
    {
      String extMsg = String.format("%s See log for full stacktrace.\n -> [%s] %s", msg,
          throwable.getClass().getSimpleName(), throwable.getMessage());
      processingEnv.getMessager().printMessage(Kind.ERROR, extMsg, sourceElement, sourceAnnotation);
      LOG.log(Level.SEVERE, msg, throwable);
    }
  }


  private static class Property
  {

    private final String name;

    private final String constantSuffix;

    private final String keySuffix;

    private final TypeMirror type;

    private final ExecutableElement getter;

    private final ExecutableElement setter;

    private final List<Property> nestedProperties;


    Property(String name, String constantSuffix, String keySuffix, TypeMirror type,
        ExecutableElement getter, ExecutableElement setter, List<Property> nestedProperties)
    {
      this.name = name;
      this.constantSuffix = constantSuffix;
      this.keySuffix = keySuffix;
      this.type = type;
      this.getter = getter;
      this.setter = setter;
      this.nestedProperties = nestedProperties == null ? List.of() : List.copyOf(nestedProperties);
    }


    public String getName()
    {
      return name;
    }

    public String getConstantSuffix()
    {
      return constantSuffix;
    }

    public String getKeySuffix()
    {
      return keySuffix;
    }

    public TypeMirror getType()
    {
      return type;
    }

    public ExecutableElement getGetter()
    {
      return getter;
    }

    public ExecutableElement getSetter()
    {
      return setter;
    }

    public boolean hasNestedProperties()
    {
      return !nestedProperties.isEmpty();
    }

    public List<Property> getNestedProperties()
    {
      return nestedProperties;
    }

  }

}
