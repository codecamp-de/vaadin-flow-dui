package de.codecamp.vaadin.base.processor;


import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static javax.lang.model.util.ElementFilter.methodsIn;

import java.beans.Introspector;
import java.lang.annotation.Annotation;
import java.lang.annotation.Repeatable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeKind;
import org.apache.commons.lang3.tuple.Pair;


public final class LangModelUtils
{

  private static final Set<String> BEAN_PROPERTY_GETTER_PREFIXES = Set.of("get", "is");

  private static final Set<String> BEAN_PROPERTY_SETTER_PREFIXES = Set.of("set");


  private static final Set<String> IGNORED_BEAN_PROPERTIES = Set.of("class");


  private LangModelUtils()
  {
    // utility class
  }


  public static Map<String, Pair<ExecutableElement, ExecutableElement>> findBeanProperties(
      ProcessingEnvironment processingEnv, TypeElement beanTypeElement, boolean onlyVoidSetters)
  {
    Map<String, ExecutableElement> getters = new LinkedHashMap<>();
    Map<String, ExecutableElement> setters = new LinkedHashMap<>();

    for (ExecutableElement methodElement : methodsIn(
        processingEnv.getElementUtils().getAllMembers(beanTypeElement)))
    {
      if (!methodElement.getModifiers().contains(Modifier.PUBLIC))
        continue;
      if (methodElement.getModifiers().contains(Modifier.STATIC))
        continue;

      // getters
      if (methodElement.getReturnType().getKind() != TypeKind.VOID
          && methodElement.getParameters().isEmpty())
      {
        String methodName = methodElement.getSimpleName().toString();

        String propertyName = null;
        for (String prefix : BEAN_PROPERTY_GETTER_PREFIXES)
        {
          if (methodName.startsWith(prefix))
          {
            propertyName = methodName.substring(prefix.length());
            propertyName = Introspector.decapitalize(propertyName);
            break;
          }
        }

        if (propertyName == null)
          continue;
        if (IGNORED_BEAN_PROPERTIES.contains(propertyName))
          continue;

        getters.put(propertyName, methodElement);
      }

      // setters
      if (((onlyVoidSetters && methodElement.getReturnType().getKind() == TypeKind.VOID)
          || !onlyVoidSetters) && methodElement.getParameters().size() == 1)
      {
        if (methodElement.getParameters().size() != 1)
          continue;

        String methodName = methodElement.getSimpleName().toString();

        String propertyName = null;
        for (String prefix : BEAN_PROPERTY_SETTER_PREFIXES)
        {
          if (methodName.startsWith(prefix))
          {
            propertyName = methodName.substring(prefix.length());
            propertyName = Introspector.decapitalize(propertyName);
            break;
          }
        }

        if (propertyName == null)
          continue;
        if (IGNORED_BEAN_PROPERTIES.contains(propertyName))
          continue;

        setters.put(propertyName, methodElement);
      }
    }

    Map<String, Pair<ExecutableElement, ExecutableElement>> properties = new LinkedHashMap<>();
    getters.forEach((name, getter) ->
    {
      properties.put(name, Pair.of(getter, setters.get(name)));
    });
    return properties;
  }


  public static List<AnnotationMirror> getAnnotationMirrors(Element element,
      Class<? extends Annotation> annotationType)
  {
    Class<? extends Annotation> containerAnnotationType = null;
    Repeatable repeatableAt = annotationType.getAnnotation(Repeatable.class);
    if (repeatableAt != null)
    {
      containerAnnotationType = repeatableAt.value();
    }

    List<AnnotationMirror> result = new ArrayList<>();
    for (AnnotationMirror am : element.getAnnotationMirrors())
    {
      if (containerAnnotationType != null
          && am.getAnnotationType().toString().equals(containerAnnotationType.getCanonicalName()))
      {
        result.addAll(getAnnotationValueAsAnnotationMirrors(am, "value"));
      }
      else if (am.getAnnotationType().toString().equals(annotationType.getName()))
      {
        result.add(am);
      }
    }
    return result;
  }


  public static AnnotationValue getAnnotationValue(AnnotationMirror annotationMirror, String key)
  {
    for (Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : annotationMirror
        .getElementValues().entrySet())
    {
      if (entry.getKey().getSimpleName().toString().equals(key))
      {
        AnnotationValue value = entry.getValue();
        if (value == null)
          value = entry.getKey().getDefaultValue();
        return value;
      }
    }
    return null;
  }

  @SuppressWarnings("unchecked")
  public static List<AnnotationValue> getAnnotationValues(AnnotationMirror annotationMirror,
      String key)
  {
    for (Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : annotationMirror
        .getElementValues().entrySet())
    {
      if (entry.getKey().getSimpleName().toString().equals(key))
      {
        AnnotationValue annotationValue = getAnnotationValue(annotationMirror, key);
        return (List<AnnotationValue>) annotationValue.getValue();
      }
    }
    // empty arrays in annotations are not contained among element values
    return emptyList();
  }


  public static List<AnnotationMirror> getAnnotationValueAsAnnotationMirrors(
      AnnotationMirror annotationMirror, String key)
  {
    List<AnnotationValue> values = getAnnotationValues(annotationMirror, key);
    if (values == null)
      return emptyList();

    return values.stream().map(av -> (AnnotationMirror) av.getValue()).collect(toList());
  }

}
