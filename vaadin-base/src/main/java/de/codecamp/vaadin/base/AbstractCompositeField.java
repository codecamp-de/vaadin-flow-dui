package de.codecamp.vaadin.base;


import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.HasEnabled;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.HasValueAndElement;
import com.vaadin.flow.component.internal.AbstractFieldSupport;
import com.vaadin.flow.shared.Registration;


/**
 * The {@link Composite}-based version of {@link com.vaadin.flow.component.AbstractCompositeField}.
 * <p>
 * <em>See {@link Composite} for important information about the initialization.</em>
 *
 * @param <C>
 *          the source type for value change events
 * @param <VALUE>
 *          the value type
 *
 * @see Composite
 */
public abstract class AbstractCompositeField<C extends AbstractCompositeField<C, VALUE>, VALUE>
  extends
    Composite
  implements
    HasValueAndElement<ComponentValueChangeEvent<C, VALUE>, VALUE>,
    HasEnabled
{

  private final AbstractFieldSupport<C, VALUE> fieldSupport;


  /**
   * Constructs a new instance.
   */
  protected AbstractCompositeField()
  {
    this(null);
  }

  /**
   * Constructs a new instance with the given default value.
   *
   * @param defaultValue
   *          the default value
   */
  protected AbstractCompositeField(VALUE defaultValue)
  {
    @SuppressWarnings("unchecked")
    C thisAsS = (C) this;

    fieldSupport = new AbstractFieldSupport<>(thisAsS, defaultValue, this::valueEquals,
        this::setPresentationValue);
  }

  @Override
  public void setValue(VALUE value)
  {
    fieldSupport.setValue(value);
  }

  @Override
  public VALUE getValue()
  {
    return fieldSupport.getValue();
  }

  @Override
  public VALUE getEmptyValue()
  {
    return fieldSupport.getEmptyValue();
  }

  @Override
  public boolean isEmpty()
  {
    return valueEquals(getValue(), getEmptyValue());
  }

  @Override
  public Registration addValueChangeListener(
      HasValue.ValueChangeListener<? super ComponentValueChangeEvent<C, VALUE>> listener)
  {
    return fieldSupport.addValueChangeListener(listener);
  }

  /**
   * @param newPresentationValue
   *          the new presentation value
   * @see com.vaadin.flow.component.AbstractCompositeField#setPresentationValue(Object)
   */
  protected abstract void setPresentationValue(VALUE newPresentationValue);

  /**
   * @param newModelValue
   *          the new model value
   * @param fromClient
   *          whether the value originates from the client
   * @see com.vaadin.flow.component.AbstractCompositeField#setModelValue(Object, boolean)
   */
  protected void setModelValue(VALUE newModelValue, boolean fromClient)
  {
    fieldSupport.setModelValue(newModelValue, fromClient);
  }

  /**
   * @param value1
   *          the first value
   * @param value2
   *          the second value
   * @return whether the two values are equal
   * @see com.vaadin.flow.component.AbstractCompositeField#valueEquals(Object, Object)
   */
  protected boolean valueEquals(VALUE value1, VALUE value2)
  {
    return fieldSupport.valueEquals(value1, value2);
  }

}
