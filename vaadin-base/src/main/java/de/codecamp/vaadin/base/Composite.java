package de.codecamp.vaadin.base;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.dom.Element;
import java.util.stream.Stream;


/**
 * Base class for components that are composed of other components. This is basically a somewhat
 * simplified version of {@link com.vaadin.flow.component.Composite}.
 * <p>
 * The content will be initialized implicitly when it's first accessed ({@link #getContent()} or
 * {@link #getElement()}) or explicitly when {@link #initializeContent()} is called.
 * <p>
 * Here are some general rules or tips that will help to avoid troubles with uninitialized content
 * or initialization order. You should only ignore them if you have a good reason to do so.
 * <ul>
 * <li>{@link #initializeContent()} should only be called in public constructors that will not be
 * used by subclasses. If a composite should be subclass-friendly it's important to provide a
 * (protected) constructor that does not call {@link #initializeContent()}.</li>
 * <li>Methods that may potentially access the content before it's initialized must either call
 * {@link #initializeContent()} beforehand or store state in separate fields that will be used
 * during initialization.</li>
 * </ul>
 */
public abstract class Composite
  extends
    Component
{

  private Component content;

  private transient boolean contentIsBeingCreated = false;

  private boolean contentInitialized = false;


  /**
   * Constructs a new instance.
   */
  protected Composite()
  {
    super(null);
  }


  /**
   * Initializes the content of this composite. Otherwise it will be initialized when it is first
   * {@link #getContent() accessed}. Will do nothing if the content is already initialized.
   */
  protected void initializeContent()
  {
    getContent();
  }

  /**
   * Returns whether the content of this composite has been initialized.
   *
   * @return whether the content of this composite has been initialized
   */
  protected boolean isContentInitialized()
  {
    return contentInitialized;
  }

  /**
   * Returns the content component, initializing it first if necessary. It should generally not be
   * necessary to call this method directly.
   *
   * @return the content component
   *
   * @see #initializeContent()
   * @see #createContent()
   * @see #contentCreated()
   */
  protected Component getContent()
  {
    if (!contentInitialized)
    {
      try
      {
        if (contentIsBeingCreated)
        {
          throw new IllegalStateException("The content is not yet initialized. "
              + "Detected direct or indirect call to 'getContent()' from 'createContent()'.");
        }

        contentIsBeingCreated = true;

        Component newContent = createContent();
        if (newContent == null)
          throw new IllegalStateException("createContent() returned null instead of a component");

        this.content = newContent;

        /*
         * Always replace the composite reference as this will be called from inside out, so the end
         * result is that the element refers to the outermost composite in the probably rare case
         * that multiple composites are nested
         */
        newContent.getElement().getStateProvider().setComponent(newContent.getElement().getNode(),
            this);
      }
      finally
      {
        contentIsBeingCreated = false;
      }

      contentCreated();
      contentInitialized = true;
    }
    return content;
  }

  /**
   * Creates and returns the content of this composite.
   *
   * @return the newly created content; must not be null
   */
  protected abstract Component createContent();

  /**
   * Called right after the content has been created. This method is intended to be overridden. The
   * default implementation does nothing.
   */
  protected void contentCreated()
  {
    // does nothing; hook for subclasses
  }


  @Override
  public Element getElement()
  {
    return getContent().getElement();
  }

  @Override
  public Stream<Component> getChildren()
  {
    return Stream.of(getContent());
  }

}
