package de.codecamp.vaadin.base;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dialog.Dialog;
import java.util.Collection;
import java.util.stream.Stream;


/**
 * A base class for dialogs, similar to {@link Composite}.
 * <p>
 * The content will be initialized implicitly when the dialog is first attached (i.e. opened) or its
 * content accessed (e.g. {@link #getChildren()}, {@link #add(Component...)},
 * {@link #remove(Component...)}, {@link #getHeader()}, {@link #getFooter()}, etc.) or explicitly
 * when {@link #initializeContent()} is called.
 * <p>
 * Here are some general rules or tips that will help to avoid troubles with uninitialized content
 * or initialization order. You should only ignore them if you have a good reason to do so.
 * <ul>
 * <li>{@link #initializeContent()} should only be called in public constructors that will not be
 * used by subclasses. If a dialog should be subclass-friendly it's important to provide a
 * (protected) constructor that does not call {@link #initializeContent()}.</li>
 * <li>Methods that may potentially access the content before it's initialized must either call
 * {@link #initializeContent()} beforehand or store state in separate fields that will be used
 * during initialization.</li>
 * </ul>
 */
public abstract class CompositeDialog
  extends
    Dialog
{

  private boolean contentInitialized = false;

  private transient boolean contentIsBeingCreated = false;


  /**
   * Constructs a new instance.
   */
  protected CompositeDialog()
  {
    addAttachListener(event ->
    {
      initializeContent();
      event.unregisterListener();
    });
  }


  /**
   * Initializes the content of this dialog. This method is called automatically or must be called
   * manually, depending on which constructor was used. Nothing is done if the content is already
   * initialized.
   */
  protected void initializeContent()
  {
    if (!contentInitialized)
    {
      try
      {
        if (contentIsBeingCreated)
        {
          throw new IllegalStateException("The content is not yet initialized. "
              + "Detected direct or indirect call to 'getContent()' from 'createContent()'.");
        }

        contentIsBeingCreated = true;

        Component newContent = createContent();
        if (newContent == null)
          throw new IllegalStateException("createContent() returned null instead of a component");

        add(newContent);
        contentInitialized = true;
      }
      finally
      {
        contentIsBeingCreated = false;
      }

      contentCreated();
    }
  }


  /**
   * Creates and returns the content of this dialog.
   *
   * @return the newly created content; must not be null
   */
  protected abstract Component createContent();

  /**
   * Called right after the content has been created. This method is intended to be overridden. The
   * default implementation does nothing.
   */
  protected void contentCreated()
  {
    // does nothing; hook for subclasses
  }

  /* Component */

  @Override
  public Stream<Component> getChildren()
  {
    initializeContent();
    return super.getChildren();
  }

  /* HasComponents */

  @Override
  public void add(Component... components)
  {
    initializeContent();
    super.add(components);
  }

  @Override
  public void add(Collection<Component> components)
  {
    initializeContent();
    super.add(components);
  }

  @Override
  public void add(String text)
  {
    initializeContent();
    super.add(text);
  }

  @Override
  public void addComponentAsFirst(Component component)
  {
    initializeContent();
    super.addComponentAsFirst(component);
  }

  @Override
  public void remove(Component... components)
  {
    initializeContent();
    super.remove(components);
  }

  @Override
  public void remove(Collection<Component> components)
  {
    initializeContent();
    super.remove(components);
  }

  @Override
  public void removeAll()
  {
    initializeContent();
    super.removeAll();
  }

  /* Dialog */

  @Override
  public DialogHeader getHeader()
  {
    initializeContent();
    return super.getHeader();
  }

  @Override
  public DialogFooter getFooter()
  {
    initializeContent();
    return super.getFooter();
  }

}
