package de.codecamp.vaadin.base;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.HasEnabled;


/**
 * A container component with a singular content component (in the default slot). The container may
 * still use other components for headers or other specialized slots.
 */
public interface HasSingleComponent
  extends
    HasElement,
    HasEnabled
{

  /**
   * Returns the content component of this container or null.
   *
   * @return the content component of this container; may be null
   */
  Component getContent();

  /**
   * Sets the content component of this container or null.
   *
   * @param content
   *          the content component of this container; may be null
   */
  void setContent(Component content);

}
