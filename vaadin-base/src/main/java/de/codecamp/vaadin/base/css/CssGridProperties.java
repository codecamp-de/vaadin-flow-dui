package de.codecamp.vaadin.base.css;


@SuppressWarnings("PMD.FieldNamingConventions")
public final class CssGridProperties
{

  public static final String display = "display";

  public static final String _display_grid = "grid";

  public static final String _display_inlineGrid = "inline-grid";

  public static final String gridTemplateColumns = "grid-template-columns";

  public static final String gridTemplateRows = "grid-template-rows";

  public static final String gridTemplateAreas = "grid-template-areas";

  public static final String gridTemplate = "grid-template";

  public static final String columnGap = "column-gap";

  public static final String rowGap = "row-gap";

  public static final String gap = "gap";

  public static final String justifyItems = "justify-items";

  public static final String alignItems = "align-items";

  public static final String placeItems = "place-items";

  public static final String justifyContent = "justify-content";

  public static final String alignContent = "align-content";

  public static final String placeContent = "place-content";

  public static final String gridAutoColumns = "grid-auto-columns";

  public static final String gridAutoRows = "grid-auto-rows";

  public static final String gridAutoFlow = "grid-auto-flow";

  public static final String grid = "grid";



  public static final String gridColumnStart = "grid-column-start";

  public static final String gridColumnEnd = "grid-column-end";

  public static final String gridRowStart = "grid-row-start";

  public static final String gridRowEnd = "grid-row-end";

  public static final String gridColumn = "grid-column";

  public static final String gridRow = "grid-row";

  public static final String gridArea = "grid-area";

  public static final String justifySelf = "justify-self";

  public static final String alignSelf = "align-self";

  public static final String placeSelf = "place-self";


  private CssGridProperties()
  {
    // utility class
  }

}
