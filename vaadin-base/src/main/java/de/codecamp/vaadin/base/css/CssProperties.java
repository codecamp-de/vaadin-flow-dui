package de.codecamp.vaadin.base.css;


/**
 * Constants for the more commonly useful groups of CSS properties.
 *
 * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS">MDN Web Docs: CSS</a>
 */
@SuppressWarnings("PMD.FieldNamingConventions")
public final class CssProperties
{

  /* Background Properties */

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background">MDN Web Docs</a>
   */
  public static final String background = "background";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background-attachment">MDN Web
   *      Docs</a>
   */
  public static final String backgroundAttachment = "background-attachment";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background-clip">MDN Web
   *      Docs</a>
   */
  public static final String backgroundClip = "background-clip";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background-color">MDN Web
   *      Docs</a>
   */
  public static final String backgroundColor = "background-color";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background-image">MDN Web
   *      Docs</a>
   */
  public static final String backgroundImage = "background-image";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background-origin">MDN Web
   *      Docs</a>
   */
  public static final String backgroundOrigin = "background-origin";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background-position">MDN Web
   *      Docs</a>
   */
  public static final String backgroundPosition = "background-position";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background-repeat">MDN Web
   *      Docs</a>
   */
  public static final String backgroundRepeat = "background-repeat";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background-size">MDN Web
   *      Docs</a>
   */
  public static final String backgroundSize = "background-size";


  /* Border Properties */

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border">MDN Web Docs</a>
   */
  public static final String border = "border";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom">MDN Web Docs</a>
   */
  public static final String borderBottom = "border-bottom";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom-color">MDN Web
   *      Docs</a>
   */
  public static final String borderBottomColor = "border-bottom-color";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom-left-radius">MDN
   *      Web Docs</a>
   */
  public static final String borderBottomLeftRadius = "border-bottom-left-radius";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom-right-radius">MDN
   *      Web Docs</a>
   */
  public static final String borderBottomRightRadius = "border-bottom-right-radius";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom-style">MDN Web
   *      Docs</a>
   */
  public static final String borderBottomStyle = "border-bottom-style";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom-width">MDN Web
   *      Docs</a>
   */
  public static final String borderBottomWidth = "border-bottom-width";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-color">MDN Web Docs</a>
   */
  public static final String borderColor = "border-color";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-image">MDN Web Docs</a>
   */
  public static final String borderImage = "border-image";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-image-outset">MDN Web
   *      Docs</a>
   */
  public static final String borderImageOutset = "border-image-outset";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-image-repeat">MDN Web
   *      Docs</a>
   */
  public static final String borderImageRepeat = "border-image-repeat";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-image-slice">MDN Web
   *      Docs</a>
   */
  public static final String borderImageSlice = "border-image-slice";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-image-source">MDN Web
   *      Docs</a>
   */
  public static final String borderImageSource = "border-image-source";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-image-width">MDN Web
   *      Docs</a>
   */
  public static final String borderImageWidth = "border-image-width";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-left">MDN Web Docs</a>
   */
  public static final String borderLeft = "border-left";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-left-color">MDN Web
   *      Docs</a>
   */
  public static final String borderLeftColor = "border-left-color";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-left-style">MDN Web
   *      Docs</a>
   */
  public static final String borderLeftStyle = "border-left-style";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-left-width">MDN Web
   *      Docs</a>
   */
  public static final String borderLeftWidth = "border-left-width";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-radius">MDN Web Docs</a>
   */
  public static final String borderRadius = "border-radius";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-right">MDN Web Docs</a>
   */
  public static final String borderRight = "border-right";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-right-color">MDN Web
   *      Docs</a>
   */
  public static final String borderRightColor = "border-right-color";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-right-style">MDN Web
   *      Docs</a>
   */
  public static final String borderRightStyle = "border-right-style";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-right-width">MDN Web
   *      Docs</a>
   */
  public static final String borderRightWidth = "border-right-width";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-style">MDN Web Docs</a>
   */
  public static final String borderStyle = "border-style";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-top">MDN Web Docs</a>
   */
  public static final String borderTop = "border-top";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-top-color">MDN Web
   *      Docs</a>
   */
  public static final String borderTopColor = "border-top-color";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-top-left-radius">MDN Web
   *      Docs</a>
   */
  public static final String borderTopLeftRadius = "border-top-left-radius";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-top-right-radius">MDN Web
   *      Docs</a>
   */
  public static final String borderTopRightRadius = "border-top-right-radius";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-top-style">MDN Web
   *      Docs</a>
   */
  public static final String borderTopStyle = "border-top-style";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-top-width">MDN Web
   *      Docs</a>
   */
  public static final String borderTopWidth = "border-top-width";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-width">MDN Web Docs</a>
   */
  public static final String borderWidth = "border-width";


  /* Color Properties */

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/color">MDN Web Docs</a>
   */
  public static final String color = "color";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/opacity">MDN Web Docs</a>
   */
  public static final String opacity = "opacity";


  /* Dimension Properties */

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/height">MDN Web Docs</a>
   */
  public static final String height = "height";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/max-height">MDN Web Docs</a>
   */
  public static final String maxHeight = "max-height";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/max-width">MDN Web Docs</a>
   */
  public static final String maxWidth = "max-width";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/min-height">MDN Web Docs</a>
   */
  public static final String minHeight = "min-height";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/min-width">MDN Web Docs</a>
   */
  public static final String minWidth = "min-width";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/width">MDN Web Docs</a>
   */
  public static final String width = "width";


  /* Font Properties */

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font">MDN Web Docs</a>
   */
  public static final String font = "font";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font-family">MDN Web Docs</a>
   */
  public static final String fontFamily = "font-family";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font-size">MDN Web Docs</a>
   */
  public static final String fontSize = "font-size";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font-size-adjust">MDN Web
   *      Docs</a>
   */
  public static final String fontSizeAdjust = "font-size-adjust";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font-stretch">MDN Web Docs</a>
   */
  public static final String fontStretch = "font-stretch";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font-style">MDN Web Docs</a>
   */
  public static final String fontStyle = "font-style";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font-variant">MDN Web Docs</a>
   */
  public static final String fontVariant = "font-variant";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font-weight">MDN Web Docs</a>
   */
  public static final String fontWeight = "font-weight";


  /* Margin Properties */

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin">MDN Web Docs</a>
   */
  public static final String margin = "margin";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin-bottom">MDN Web Docs</a>
   */
  public static final String marginBottom = "margin-bottom";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin-left">MDN Web Docs</a>
   */
  public static final String marginLeft = "margin-left";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin-right">MDN Web Docs</a>
   */
  public static final String marginRight = "margin-right";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin-top">MDN Web Docs</a>
   */
  public static final String marginTop = "margin-top";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin-inline-start">MDN Web
   *      Docs</a>
   */
  public static final String marginInlineStart = "margin-inline-start";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin-inline-end">MDN Web
   *      Docs</a>
   */
  public static final String marginInlineEnd = "margin-inline-end";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin-block-start">MDN Web
   *      Docs</a>
   */
  public static final String marginBlockStart = "margin-block-start";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin-block-end">MDN Web
   *      Docs</a>
   */
  public static final String marginBlockEnd = "margin-block-end";


  /* Outline Properties */

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/outline">MDN Web Docs</a>
   */
  public static final String outline = "outline";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/outline-color">MDN Web Docs</a>
   */
  public static final String outlineColor = "outline-color";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/outline-offset">MDN Web Docs</a>
   */
  public static final String outlineOffset = "outline-offset";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/outline-style">MDN Web Docs</a>
   */
  public static final String outlineStyle = "outline-style";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/outline-width">MDN Web Docs</a>
   */
  public static final String outlineWidth = "outline-width";


  /* Padding Properties */

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/padding">MDN Web Docs</a>
   */
  public static final String padding = "padding";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/padding-bottom">MDN Web Docs</a>
   */
  public static final String paddingBottom = "padding-bottom";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/padding-left">MDN Web Docs</a>
   */
  public static final String paddingLeft = "padding-left";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/padding-right">MDN Web Docs</a>
   */
  public static final String paddingRight = "padding-right";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/padding-top">MDN Web Docs</a>
   */
  public static final String paddingTop = "padding-top";


  /* Text Properties */

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/direction">MDN Web Docs</a>
   */
  public static final String direction = "direction";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/tab-size">MDN Web Docs</a>
   */
  public static final String tabSize = "tab-size";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-align">MDN Web Docs</a>
   */
  public static final String textAlign = "text-align";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-align-last">MDN Web
   *      Docs</a>
   */
  public static final String textAlignLast = "text-align-last";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-decoration">MDN Web
   *      Docs</a>
   */
  public static final String textDecoration = "text-decoration";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-decoration-color">MDN Web
   *      Docs</a>
   */
  public static final String textDecorationColor = "text-decoration-color";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-decoration-line">MDN Web
   *      Docs</a>
   */
  public static final String textDecorationLine = "text-decoration-line";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-decoration-style">MDN Web
   *      Docs</a>
   */
  public static final String textDecorationStyle = "text-decoration-style";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-indent">MDN Web Docs</a>
   */
  public static final String textIndent = "text-indent";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-justify">MDN Web Docs</a>
   */
  public static final String textJustify = "text-justify";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-overflow">MDN Web Docs</a>
   */
  public static final String textOverflow = "text-overflow";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-shadow">MDN Web Docs</a>
   */
  public static final String textShadow = "text-shadow";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-transform">MDN Web Docs</a>
   */
  public static final String textTransform = "text-transform";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/line-height">MDN Web Docs</a>
   */
  public static final String lineHeight = "line-height";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/vertical-align">MDN Web Docs</a>
   */
  public static final String verticalAlign = "vertical-align";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/letter-spacing">MDN Web Docs</a>
   */
  public static final String letterSpacing = "letter-spacing";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/word-spacing">MDN Web Docs</a>
   */
  public static final String wordSpacing = "word-spacing";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/white-space">MDN Web Docs</a>
   */
  public static final String whiteSpace = "white-space";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/word-break">MDN Web Docs</a>
   */
  public static final String wordBreak = "word-break";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/word-wrap">MDN Web Docs</a>
   */
  public static final String wordWrap = "word-wrap";


  /* Visual formatting Properties */

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/display">MDN Web Docs</a>
   */
  public static final String display = "display";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/position">MDN Web Docs</a>
   */
  public static final String position = "position";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/top">MDN Web Docs</a>
   */
  public static final String top = "top";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/right">MDN Web Docs</a>
   */
  public static final String right = "right";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/bottom">MDN Web Docs</a>
   */
  public static final String bottom = "bottom";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/left">MDN Web Docs</a>
   */
  public static final String left = "left";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/float">MDN Web Docs</a>
   */
  public static final String float_ = "float";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/clear">MDN Web Docs</a>
   */
  public static final String clear = "clear";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/z-index">MDN Web Docs</a>
   */
  public static final String zIndex = "z-index";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/overflow">MDN Web Docs</a>
   */
  public static final String overflow = "overflow";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/overflow-x">MDN Web Docs</a>
   */
  public static final String overflowX = "overflow-x";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/overflow-y">MDN Web Docs</a>
   */
  public static final String overflowY = "overflow-y";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/resize">MDN Web Docs</a>
   */
  public static final String resize = "resize";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/clip">MDN Web Docs</a>
   */
  public static final String clip = "clip";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/visibility">MDN Web Docs</a>
   */
  public static final String visibility = "visibility";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/cursor">MDN Web Docs</a>
   */
  public static final String cursor = "cursor";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/box-shadow">MDN Web Docs</a>
   */
  public static final String boxShadow = "box-shadow";

  /**
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/box-sizing">MDN Web Docs</a>
   */
  public static final String boxSizing = "box-sizing";


  private CssProperties()
  {
    // utility class
  }

}
