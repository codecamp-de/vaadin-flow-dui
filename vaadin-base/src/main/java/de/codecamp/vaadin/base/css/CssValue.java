package de.codecamp.vaadin.base.css;


import java.util.Optional;
import java.util.stream.Stream;


public interface CssValue
{

  /**
   * Returns either a CSS value or an expression that will evaluate to a value.
   *
   * @return either a CSS value or an expression that will evaluate to a value
   */
  String getValue();


  static <E extends Enum<E> & CssValue> Optional<E> fromValueOptional(Class<E> enumType,
      String value)
  {
    return Stream.of(enumType.getEnumConstants()) //
        .filter(v -> v.getValue().equals(value)) //
        .findFirst();
  }

  static <E extends Enum<E> & CssValue> E fromValue(Class<E> enumType, String value,
      E defaultEnumConstant)
  {
    return fromValueOptional(enumType, value).orElse(defaultEnumConstant);
  }

  static <E extends Enum<E> & CssValue> E fromValue(Class<E> enumType, String value)
  {
    return fromValueOptional(enumType, value).orElseThrow(() -> new IllegalArgumentException(
        "Unknown value for '" + enumType.getSimpleName() + "': '" + value + "'"));
  }

  static String toValue(CssValue valueContainer)
  {
    return valueContainer != null ? valueContainer.getValue() : null;
  }

}
