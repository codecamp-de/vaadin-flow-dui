package de.codecamp.vaadin.base.i18n;


import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.avatar.AvatarGroup;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.checkbox.CheckboxGroup;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.combobox.MultiSelectComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.messages.MessageInput;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.sidenav.SideNav;
import com.vaadin.flow.component.textfield.BigDecimalField;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.timepicker.TimePicker;
import com.vaadin.flow.component.upload.Upload;


/**
 * Combines all component-specific localization methods into a single point of access.
 */
public final class ComponentI18n
{

  private ComponentI18n()
  {
    // utility class
  }


  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see AppLayoutI18nUtils#localize
   */
  public static void localize(AppLayout component)
  {
    AppLayoutI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see AvatarGroupI18nUtils#localize
   */
  public static void localize(AvatarGroup component)
  {
    AvatarGroupI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see AvatarI18nUtils#localize
   */
  public static void localize(Avatar component)
  {
    AvatarI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see BigDecimalFieldI18nUtils#localize
   */
  public static void localize(BigDecimalField component)
  {
    BigDecimalFieldI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see CheckboxGroupI18nUtils#localize
   */
  public static void localize(CheckboxGroup<?> component)
  {
    CheckboxGroupI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see CheckboxI18nUtils#localize
   */
  public static void localize(Checkbox component)
  {
    CheckboxI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see ComboBoxI18nUtils#localize
   */
  public static void localize(ComboBox<?> component)
  {
    ComboBoxI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see DatePickerI18nUtils#localize
   */
  public static void localize(DatePicker component)
  {
    DatePickerI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see DateTimePickerI18nUtils#localize
   */
  public static void localize(DateTimePicker component)
  {
    DateTimePickerI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see EmailFieldI18nUtils#localize
   */
  public static void localize(EmailField component)
  {
    EmailFieldI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see IntegerFieldI18nUtils#localize
   */
  public static void localize(IntegerField component)
  {
    IntegerFieldI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see MenuBarI18nUtils#localize
   */
  public static void localize(MenuBar component)
  {
    MenuBarI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see MessageInputI18nUtils#localize
   */
  public static void localize(MessageInput component)
  {
    MessageInputI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see MultiSelectComboBoxI18nUtils#localize
   */
  public static void localize(MultiSelectComboBox<?> component)
  {
    MultiSelectComboBoxI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see NumberFieldI18nUtils#localize
   */
  public static void localize(NumberField component)
  {
    NumberFieldI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see PasswordFieldI18nUtils#localize
   */
  public static void localize(PasswordField component)
  {
    PasswordFieldI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see RadioButtonGroupI18nUtils#localize
   */
  public static void localize(RadioButtonGroup<?> component)
  {
    RadioButtonGroupI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see SelectI18nUtils#localize
   */
  public static void localize(Select<?> component)
  {
    SelectI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see SideNavI18nUtils#localize
   */
  public static void localize(SideNav component)
  {
    SideNavI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see TextAreaI18nUtils#localize
   */
  public static void localize(TextArea component)
  {
    TextAreaI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see TextFieldI18nUtils#localize
   */
  public static void localize(TextField component)
  {
    TextFieldI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see TimePickerI18nUtils#localize
   */
  public static void localize(TimePicker component)
  {
    TimePickerI18nUtils.localize(component);
  }

  /**
   * Localizes the given component.
   *
   * @param component
   *          the component to localize
   * @see UploadI18nUtils#localize
   */
  public static void localize(Upload component)
  {
    UploadI18nUtils.localize(component);
  }

}
