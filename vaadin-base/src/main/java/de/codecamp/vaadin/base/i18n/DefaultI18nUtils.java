package de.codecamp.vaadin.base.i18n;


import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.avatar.AvatarGroup;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.checkbox.CheckboxGroup;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.combobox.MultiSelectComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.messages.MessageInput;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.sidenav.SideNav;
import com.vaadin.flow.component.textfield.BigDecimalField;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.timepicker.TimePicker;
import com.vaadin.flow.component.upload.Upload;
import de.codecamp.vaadin.base.annotations.I18nUtils;


@I18nUtils(componentType = AppLayout.class)
@I18nUtils(componentType = AvatarGroup.class)
@I18nUtils(componentType = Avatar.class)
@I18nUtils(componentType = BigDecimalField.class)
@I18nUtils(componentType = Checkbox.class)
@I18nUtils(componentType = CheckboxGroup.class)
@I18nUtils(componentType = ComboBox.class)
@I18nUtils( //
    componentType = DatePicker.class,
    i18nInit = """
        I18nUtilHelpers.init(i18n, locale);
        anyMessageSet = true;
        """,
    i18nCopy = """
        I18nUtilHelpers.copy(cachedI18n, i18n);
        """)
@I18nUtils( //
    componentType = DateTimePicker.class,
    componentLocalize = """
        I18nUtilHelpers.localize(component, locale);
        """)
@I18nUtils(componentType = EmailField.class)
@I18nUtils(componentType = IntegerField.class)
@I18nUtils(componentType = MenuBar.class)
@I18nUtils(componentType = MessageInput.class)
@I18nUtils(componentType = MultiSelectComboBox.class)
@I18nUtils(componentType = NumberField.class)
@I18nUtils(componentType = PasswordField.class)
@I18nUtils(componentType = RadioButtonGroup.class)
@I18nUtils(componentType = Select.class)
@I18nUtils(componentType = SideNav.class)
@I18nUtils(componentType = TextArea.class)
@I18nUtils(componentType = TextField.class)
@I18nUtils(componentType = TimePicker.class)
@I18nUtils(componentType = Upload.class)
class DefaultI18nUtils
{
// nothing
}
