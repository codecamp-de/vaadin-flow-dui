package de.codecamp.vaadin.base.i18n;


import static java.util.stream.Collectors.toList;

import com.vaadin.flow.component.datepicker.DatePicker.DatePickerI18n;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import java.time.DayOfWeek;
import java.time.Month;
import java.time.format.TextStyle;
import java.time.temporal.WeekFields;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;


class I18nUtilHelpers
{

  private I18nUtilHelpers()
  {
    // utility class
  }


  public static void init(DatePickerI18n i18n, Locale locale)
  {
    /*
     * To determine the first day of the week a country is required; otherwise it will default to
     * Sunday. If the provided locale has no country, pick any available locale for the language
     * that has a country and hope for the best.
     */
    Locale localeWithCountry;
    if (locale.getCountry().isEmpty())
    {
      localeWithCountry = Stream.of(Locale.getAvailableLocales())
          .filter(l -> l.getLanguage().equals(locale.getLanguage()) && !l.getCountry().isEmpty())
          .findFirst().orElse(locale);
    }
    else
    {
      localeWithCountry = locale;
    }
    i18n.setFirstDayOfWeek(WeekFields.of(localeWithCountry).getFirstDayOfWeek().getValue() % 7);

    List<String> monthNames = Stream.of(Month.values())
        .map(m -> m.getDisplayName(TextStyle.FULL_STANDALONE, locale)).collect(toList());
    i18n.setMonthNames(Collections.unmodifiableList(monthNames));

    List<String> weekdays = Stream.of(DayOfWeek.values())
        .map(d -> d.getDisplayName(TextStyle.FULL_STANDALONE, locale)).collect(toList());
    Collections.rotate(weekdays, 1);
    i18n.setWeekdays(Collections.unmodifiableList(weekdays));

    List<String> weekdaysShort = Stream.of(DayOfWeek.values())
        .map(d -> d.getDisplayName(TextStyle.SHORT_STANDALONE, locale)).collect(toList());
    Collections.rotate(weekdaysShort, 1);
    i18n.setWeekdaysShort(Collections.unmodifiableList(weekdaysShort));
  }

  public static void copy(DatePickerI18n source, DatePickerI18n target)
  {
    List<String> dateFormats = source.getDateFormats();
    if (dateFormats == null || dateFormats.isEmpty())
    {
      target.setDateFormats(null);
    }
    else if (dateFormats.size() == 1)
    {
      target.setDateFormats(dateFormats.get(0));
    }
    else
    {
      target.setDateFormats(dateFormats.get(0),
          dateFormats.subList(1, dateFormats.size()).toArray(String[]::new));
    }
  }

  public static void localize(DateTimePicker component, Locale locale)
  {
    DatePickerI18n dpI18n = DatePickerI18nUtils.localize(component.getDatePickerI18n(), locale);
    if (dpI18n != null)
      component.setDatePickerI18n(dpI18n);
  }

}
