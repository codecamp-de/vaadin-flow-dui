package de.codecamp.vaadin.base.i18n;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.i18n.I18NProvider;
import com.vaadin.flow.server.VaadinService;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;
import java.util.function.IntConsumer;


/**
 * Replicates translation-related functionality from {@link Component} to make it usable in other
 * contexts and provides other utilities.
 */
public final class TranslationUtils
{

  private TranslationUtils()
  {
    // utility class
  }


  /**
   * Returns the translation for the current locale and the given key.
   *
   * @param key
   *          translation key
   * @param params
   *          parameters used in translation string
   * @return the translation or an error string
   * @see #getLocale()
   */
  public static String getTranslation(String key, Object... params)
  {
    return getTranslation(getLocale(), key, params);
  }

  /**
   * Returns the translation for the given locale and key.
   *
   * @param locale
   *          locale to use
   * @param key
   *          translation key
   * @param params
   *          parameters used in translation string
   * @return the translation or an error string
   */
  public static String getTranslation(Locale locale, String key, Object... params)
  {
    if (getI18NProvider() == null)
    {
      return "!{" + key + "}!";
    }
    return getI18NProvider().getTranslation(key, locale, params);
  }

  /**
   * Returns the translation for the current locale and the given key.
   *
   * @param key
   *          translation key
   * @param params
   *          parameters used in translation string
   * @return the translation or an error string
   * @see #getLocale()
   */
  public static String getTranslation(Object key, Object... params)
  {
    return getTranslation(getLocale(), key, params);
  }

  /**
   * Returns the translation for the given locale and key.
   *
   * @param locale
   *          locale to use
   * @param key
   *          translation key
   * @param params
   *          parameters used in translation string
   * @return the translation or an error string
   */
  public static String getTranslation(Locale locale, Object key, Object... params)
  {
    if (getI18NProvider() == null)
    {
      return "!{" + key + "}!";
    }
    return getI18NProvider().getTranslation(key, locale, params);
  }

  /**
   * Returns the locale for the current UI.
   *
   * @return the locale for the current UI
   */
  public static Locale getLocale()
  {
    UI currentUi = UI.getCurrent();
    if (currentUi != null)
      return currentUi.getLocale();

    List<Locale> locales = getI18NProvider().getProvidedLocales();
    if (!locales.isEmpty())
      return locales.get(0);

    return Locale.getDefault();
  }

  private static I18NProvider getI18NProvider()
  {
    return VaadinService.getCurrent().getInstantiator().getI18NProvider();
  }

  /**
   * Gets the translation for the specified locale and key and applies it to the given consumer
   * (e.g. a method reference to a setter). If no translation is found, the consumer is not called.
   *
   * @param locale
   *          the locale
   * @param key
   *          the message key
   * @param consumer
   *          the consumer to receive the value
   * @return whether a translation has been found
   */
  public static boolean optionalTranslate(Locale locale, String key, Consumer<String> consumer)
  {
    /*
     * Components like DatePicker, DateTimePicker allow to set an explicit locale that (according to
     * documentation) determines the applied and/or expected format. But the way it's implemented it
     * also overrides the locale that is used for translations which is not obvious and (in my
     * opinion) not desired.
     *
     * E.g. even if the format is set to the same locale for all users (regardless of their personal
     * locale) the translations should still be in the language of the user.
     *
     * For that reason, don't use the Locale-less version of getTranslation(...) and use the default
     * way to explicitly determine the locale instead.
     */

    String translation = getTranslation(locale, key);

    /*
     * When a message is missing, I18NProviders are supposed to return an exception string instead
     * of throwing an exception or returning null. Since it it unknown whether the component is
     * supposed to be translated it's better to leave the defaults in that case. That means
     * "missing message" needs to be detected by examining the returned translation.
     */
    if (translation == null || translation.equals(key)
        || (translation.startsWith("!{") && translation.endsWith("}!"))
        || (translation.startsWith("!") && translation.endsWith("!")))
    {
      return false;
    }

    consumer.accept(translation);
    return true;
  }

  /**
   * Gets the translation for the specified locale and key and applies it to the given consumer
   * (e.g. a method reference to a setter). If no translation is found, the consumer is not called.
   * The value is treated as a comma-separated list of strings.
   *
   * @param locale
   *          the locale
   * @param key
   *          the message key
   * @param consumer
   *          the consumer to receive the value
   * @return whether a translation has been found
   * @see #optionalTranslate(Locale, String, Consumer)
   */
  public static boolean optionalTranslate(Locale locale, String key, StringListConsumer consumer)
  {
    return optionalTranslate(locale, key,
        (String message) -> consumer.accept(List.of(message.split(","))));
  }

  /**
   * Gets the translation for the specified locale and key and applies it to the given consumer
   * (e.g. a method reference to a setter). If no translation is found, the consumer is not called.
   * The value is treated as integer.
   *
   * @param locale
   *          the locale
   * @param key
   *          the message key
   * @param consumer
   *          the consumer to receive the value
   * @return whether a translation has been found
   * @see #optionalTranslate(Locale, String, Consumer)
   */
  public static boolean optionalTranslate(Locale locale, String key, IntConsumer consumer)
  {
    return optionalTranslate(locale, key,
        (String message) -> consumer.accept(Integer.parseInt(message)));
  }

  /**
   * Gets the translation for the specified locale and key and applies it to the given consumer
   * (e.g. a method reference to a setter). If no translation is found, the consumer is not called.
   * The value is treated as {@link LocalDate}.
   *
   * @param locale
   *          the locale
   * @param key
   *          the message key
   * @param consumer
   *          the consumer to receive the value
   * @return whether a translation has been found
   * @see #optionalTranslate(Locale, String, Consumer)
   */
  public static boolean optionalTranslate(Locale locale, String key, LocaleDateConsumer consumer)
  {
    return optionalTranslate(locale, key,
        (String message) -> consumer.accept(LocalDate.parse(message)));
  }


  /**
   * A {@link Consumer} that accepts a list of strings.
   */
  @FunctionalInterface
  public interface StringListConsumer
    extends
      Consumer<List<String>>
  {
    // nothing
  }

  /**
   * A {@link Consumer} that accepts a {@link LocalDate}.
   */
  @FunctionalInterface
  public interface LocaleDateConsumer
    extends
      Consumer<LocalDate>
  {
    // nothing
  }

}
