package de.codecamp.vaadin.base.lumo;


/**
 * Lumo properties related to box shadows.
 *
 * @see <a href="https://vaadin.com/docs/latest/ds/foundation/elevation">Lumo Elevation</a>
 */
public enum LumoBoxShadow
  implements
    LumoProperty
{

  XS("--lumo-box-shadow-xs"),

  S("--lumo-box-shadow-s"),

  M("--lumo-box-shadow-m"),

  L("--lumo-box-shadow-l"),

  XL("--lumo-box-shadow-xl");


  private final String property;


  LumoBoxShadow(String property)
  {
    this.property = property;
  }


  @Override
  public String property()
  {
    return property;
  }

}
