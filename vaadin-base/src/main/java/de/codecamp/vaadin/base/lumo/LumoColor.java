package de.codecamp.vaadin.base.lumo;


/**
 * Lumo properties related to color.
 *
 * @see <a href="https://vaadin.com/docs/latest/ds/foundation/color">Lumo Color</a>
 */
@SuppressWarnings("PMD.FieldNamingConventions")
public enum LumoColor
  implements
    LumoProperty
{

  baseColor("--lumo-base-color"),


  contrast05Pct("--lumo-contrast-5pct"),
  contrast10Pct("--lumo-contrast-10pct"),
  contrast20Pct("--lumo-contrast-20pct"),
  contrast30Pct("--lumo-contrast-30pct"),
  contrast40Pct("--lumo-contrast-40pct"),
  contrast50Pct("--lumo-contrast-50pct"),
  contrast60Pct("--lumo-contrast-60pct"),
  contrast70Pct("--lumo-contrast-70pct"),
  contrast80Pct("--lumo-contrast-80pct"),
  contrast90Pct("--lumo-contrast-90pct"),
  contrast("--lumo-contrast"),


  tint05Pct("--lumo-tint-5pct"),
  tint10Pct("--lumo-tint-10pct"),
  tint20Pct("--lumo-tint-20pct"),
  tint30Pct("--lumo-tint-30pct"),
  tint40Pct("--lumo-tint-40pct"),
  tint50Pct("--lumo-tint-50pct"),
  tint60ct("--lumo-tint-60pct"),
  tint70ct("--lumo-tint-70pct"),
  tint80Pct("--lumo-tint-80pct"),
  tint90Pct("--lumo-tint-90pct"),
  tint("--lumo-tint"),


  shade05Pct("--lumo-shade-5pct"),
  shade10Pct("--lumo-shade-10pct"),
  shade20Pct("--lumo-shade-20pct"),
  shade30Pct("--lumo-shade-30pct"),
  shade40Pct("--lumo-shade-40pct"),
  shade50Pct("--lumo-shade-50pct"),
  shade60Pct("--lumo-shade-60pct"),
  shade70Pct("--lumo-shade-70pct"),
  shade80Pct("--lumo-shade-80pct"),
  shade90Pct("--lumo-shade-90pct"),
  shade("--lumo-shade"),


  primaryColor10Pct("--lumo-primary-color-10pct"),
  primaryColor50Pct("--lumo-primary-color-50pct"),
  primaryColor("--lumo-primary-color"),
  primaryTextColor("--lumo-primary-text-color"),
  primaryContrastColor("--lumo-primary-contrast-color"),


  errorColor10Pct("--lumo-error-color-10pct"),
  errorColor50Pct("--lumo-error-color-50pct"),
  errorColor("--lumo-error-color"),
  errorTextColor("--lumo-error-text-color"),
  errorContrastColor("--lumo-error-contrast-color"),


  warningColor10Pct("--lumo-warning-color-10pct"),
  warningColor("--lumo-warning-color"),
  warningTextColor("--lumo-warning-text-color"),
  warningContrastColor("--lumo-warning-contrast-color"),


  successColor10Pct("--lumo-success-color-10pct"),
  successColor50Pct("--lumo-success-color-50pct"),
  successColor("--lumo-success-color"),
  successTextColor("--lumo-success-text-color"),
  successContrastColor("--lumo-success-contrast-color"),


  headerTextColor("--lumo-header-text-color"),

  bodyTextColor("--lumo-body-text-color"),

  secondaryTextColor("--lumo-secondary-text-color"),

  tertiaryTextColor("--lumo-tertiary-text-color"),

  disabledTextColor("--lumo-disabled-text-color");


  private final String property;


  LumoColor(String property)
  {
    this.property = property;
  }


  @Override
  public String property()
  {
    return property;
  }

}
