package de.codecamp.vaadin.base.lumo;


/**
 * The official CSS variables / custom properties provided by the Lumo theme. Where appropriate
 * there's also enums for certain groups of properties.
 *
 * @see <a href="https://vaadin.com/docs/latest/ds/foundation">Foundation</a>
 * @see LumoBorderRadius
 * @see LumoBoxShadow
 * @see LumoColor
 * @see LumoFontSize
 * @see LumoIconSize
 * @see LumoLineHeight
 * @see LumoSize
 * @see LumoSpace
 */
@SuppressWarnings("PMD.FieldNamingConventions")
public final class LumoProperties
{

  /*
   * Typography
   *
   * https://vaadin.com/docs/latest/ds/foundation/typography
   */

  public static final LumoProperty lumoFontFamily = new LumoPropertyImpl("--lumo-font-family");


  public static final LumoProperty lumoFontSizeXXXL = LumoFontSize.XXXL;

  public static final LumoProperty lumoFontSizeXXL = LumoFontSize.XXL;

  public static final LumoProperty lumoFontSizeXL = LumoFontSize.XL;

  public static final LumoProperty lumoFontSizeL = LumoFontSize.L;

  public static final LumoProperty lumoFontSizeM = LumoFontSize.M;

  public static final LumoProperty lumoFontSizeS = LumoFontSize.S;

  public static final LumoProperty lumoFontSizeXS = LumoFontSize.XS;

  public static final LumoProperty lumoFontSizeXXS = LumoFontSize.XXS;


  public static final LumoProperty lumoLineHeightM = LumoLineHeight.M;

  public static final LumoProperty lumoLineHeightS = LumoLineHeight.S;

  public static final LumoProperty lumoLineHeightXS = LumoLineHeight.XS;


  /*
   * Color
   *
   * https://vaadin.com/docs/latest/ds/foundation/color
   */

  public static final LumoProperty lumoBaseColor = LumoColor.baseColor;


  public static final LumoProperty lumoContrast05Pct = LumoColor.contrast05Pct;

  public static final LumoProperty lumoContrast10Pct = LumoColor.contrast10Pct;

  public static final LumoProperty lumoContrast20Pct = LumoColor.contrast20Pct;

  public static final LumoProperty lumoContrast30Pct = LumoColor.contrast30Pct;

  public static final LumoProperty lumoContrast40Pct = LumoColor.contrast40Pct;

  public static final LumoProperty lumoContrast50Pct = LumoColor.contrast50Pct;

  public static final LumoProperty lumoContrast60Pct = LumoColor.contrast60Pct;

  public static final LumoProperty lumoContrast70Pct = LumoColor.contrast70Pct;

  public static final LumoProperty lumoContrast80Pct = LumoColor.contrast80Pct;

  public static final LumoProperty lumoContrast90Pct = LumoColor.contrast90Pct;

  public static final LumoProperty lumoContrast = LumoColor.contrast;


  public static final LumoProperty lumoTint05Pct = LumoColor.tint05Pct;

  public static final LumoProperty lumoTint10Pct = LumoColor.tint10Pct;

  public static final LumoProperty lumoTint20Pct = LumoColor.tint20Pct;

  public static final LumoProperty lumoTint30Pct = LumoColor.tint30Pct;

  public static final LumoProperty lumoTint40Pct = LumoColor.tint40Pct;

  public static final LumoProperty lumoTint50Pct = LumoColor.tint50Pct;

  public static final LumoProperty lumoTint60ct = LumoColor.tint60ct;

  public static final LumoProperty lumoTint70ct = LumoColor.tint70ct;

  public static final LumoProperty lumoTint80Pct = LumoColor.tint80Pct;

  public static final LumoProperty lumoTint90Pct = LumoColor.tint90Pct;

  public static final LumoProperty lumoTint = LumoColor.tint;


  public static final LumoProperty lumoShade05Pct = LumoColor.shade05Pct;

  public static final LumoProperty lumoShade10Pct = LumoColor.shade10Pct;

  public static final LumoProperty lumoShade20Pct = LumoColor.shade20Pct;

  public static final LumoProperty lumoShade30Pct = LumoColor.shade30Pct;

  public static final LumoProperty lumoShade40Pct = LumoColor.shade40Pct;

  public static final LumoProperty lumoShade50Pct = LumoColor.shade50Pct;

  public static final LumoProperty lumoShade60Pct = LumoColor.shade60Pct;

  public static final LumoProperty lumoShade70Pct = LumoColor.shade70Pct;

  public static final LumoProperty lumoShade80Pct = LumoColor.shade80Pct;

  public static final LumoProperty lumoShade90Pct = LumoColor.shade90Pct;

  public static final LumoProperty lumoShade = LumoColor.shade;


  public static final LumoProperty lumoPrimaryColor10Pct = LumoColor.primaryColor10Pct;

  public static final LumoProperty lumoPrimaryColor50Pct = LumoColor.primaryColor50Pct;

  public static final LumoProperty lumoPrimaryColor = LumoColor.primaryColor;

  public static final LumoProperty lumoPrimaryTextColor = LumoColor.primaryTextColor;

  public static final LumoProperty lumoPrimaryContrastColor = LumoColor.primaryContrastColor;


  public static final LumoProperty lumoErrorColor10Pct = LumoColor.errorColor10Pct;

  public static final LumoProperty lumoErrorColor50Pct = LumoColor.errorColor50Pct;

  public static final LumoProperty lumoErrorColor = LumoColor.errorColor;

  public static final LumoProperty lumoErrorTextColor = LumoColor.errorTextColor;

  public static final LumoProperty lumoErrorContrastColor = LumoColor.errorContrastColor;


  public static final LumoProperty lumoSuccessColor10Pct = LumoColor.successColor10Pct;

  public static final LumoProperty lumoSuccessColor50Pct = LumoColor.successColor50Pct;

  public static final LumoProperty lumoSuccessColor = LumoColor.successColor;

  public static final LumoProperty lumoSuccessTextColor = LumoColor.secondaryTextColor;

  public static final LumoProperty lumoSuccessContrastColor = LumoColor.successContrastColor;


  public static final LumoProperty lumoHeaderTextColor = LumoColor.headerTextColor;

  public static final LumoProperty lumoBodyTextColor = LumoColor.bodyTextColor;

  public static final LumoProperty lumoSecondaryTextColor = LumoColor.secondaryTextColor;

  public static final LumoProperty lumoTertiaryTextColor = LumoColor.tertiaryTextColor;

  public static final LumoProperty lumoDisabledTextColor = LumoColor.disabledTextColor;


  /*
   * Size and Space
   *
   * https://vaadin.com/docs/latest/ds/foundation/size-space
   */

  public static final LumoProperty lumoSizeXS = LumoSize.XS;

  public static final LumoProperty lumoSizeS = LumoSize.S;

  public static final LumoProperty lumoSizeM = LumoSize.M;

  public static final LumoProperty lumoSizeL = LumoSize.L;

  public static final LumoProperty lumoSizeXL = LumoSize.XL;


  public static final LumoProperty lumoSpaceXS = LumoSpace.XS;

  public static final LumoProperty lumoSpaceS = LumoSpace.S;

  public static final LumoProperty lumoSpaceM = LumoSpace.M;

  public static final LumoProperty lumoSpaceL = LumoSpace.L;

  public static final LumoProperty lumoSpaceXL = LumoSpace.XL;


  public static final LumoProperty lumoSpaceWideXS = LumoSpace.WideXS;

  public static final LumoProperty lumoSpaceWideS = LumoSpace.WideS;

  public static final LumoProperty lumoSpaceWideM = LumoSpace.WideM;

  public static final LumoProperty lumoSpaceWideL = LumoSpace.WideL;

  public static final LumoProperty lumoSpaceWideXL = LumoSpace.WideXL;


  public static final LumoProperty lumoSpaceTallXS = LumoSpace.TallXS;

  public static final LumoProperty lumoSpaceTallS = LumoSpace.TallS;

  public static final LumoProperty lumoSpaceTallM = LumoSpace.TallM;

  public static final LumoProperty lumoSpaceTallL = LumoSpace.TallL;

  public static final LumoProperty lumoSpaceTallXL = LumoSpace.TallXL;


  /*
   * Shape
   *
   * https://vaadin.com/docs/latest/ds/foundation/shape
   */

  public static final LumoProperty lumoBorderRadiusS = LumoBorderRadius.S;

  public static final LumoProperty lumoBorderRadiusM = LumoBorderRadius.M;

  public static final LumoProperty lumoBorderRadiusL = LumoBorderRadius.L;


  /*
   * Elevation
   *
   * https://vaadin.com/docs/latest/ds/foundation/elevation
   */

  public static final LumoProperty lumoBoxShadowXS = LumoBoxShadow.XS;

  public static final LumoProperty lumoBoxShadowS = LumoBoxShadow.S;

  public static final LumoProperty lumoBoxShadowM = LumoBoxShadow.M;

  public static final LumoProperty lumoBoxShadowL = LumoBoxShadow.L;

  public static final LumoProperty lumoBoxShadowXL = LumoBoxShadow.XL;


  /*
   * Interaction
   *
   * https://vaadin.com/docs/latest/ds/foundation/interaction
   */

  public static final LumoProperty lumoClickableCursor =
      new LumoPropertyImpl("--lumo-clickable-cursor");


  /*
   * Icons
   *
   * https://vaadin.com/docs/latest/ds/foundation/icons
   */

  public static final LumoProperty lumoIconSizeS = LumoIconSize.S;

  public static final LumoProperty lumoIconSizeM = LumoIconSize.M;

  public static final LumoProperty lumoIconSizeL = LumoIconSize.L;


  /* Misc */

  public static final LumoProperty lumoButtonSize = new LumoPropertyImpl("--lumo-button-size");

  public static final LumoProperty lumoTextFieldSize =
      new LumoPropertyImpl("--lumo-text-field-size");


  private LumoProperties()
  {
    // utility class
  }

}
