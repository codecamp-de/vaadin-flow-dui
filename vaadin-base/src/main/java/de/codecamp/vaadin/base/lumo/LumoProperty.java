package de.codecamp.vaadin.base.lumo;


/**
 * Represents a single Lumo property. Can either return the {@link #property() plain property name}
 * or wrapped in a {@link #var() var(...)} function.
 *
 * @see LumoProperties
 */
public interface LumoProperty
{

  /**
   * Returns the name of the Lumo property.
   *
   * @return the name of the Lumo property
   */
  String property();

  /**
   * Returns the Lumo property name surrouned with var(...) function to access its value. This is an
   * alias for {@link #value()}.
   *
   * @return the Lumo property name surrouned with var(...) function to access its value
   */
  default String var()
  {
    return "var(" + property() + ")";
  }

  /**
   * Returns the Lumo property name surrouned with var(...) function to access its value. This is an
   * alias for {@link #var()}.
   *
   * @return the Lumo property name surrouned with var(...) function to access its value
   */
  default String value()
  {
    return var();
  }

}
