package de.codecamp.vaadin.base.lumo;


class LumoPropertyImpl
  implements
    LumoProperty
{

  private final String name;


  LumoPropertyImpl(String name)
  {
    this.name = name;
  }


  @Override
  public String property()
  {
    return name;
  }

}
