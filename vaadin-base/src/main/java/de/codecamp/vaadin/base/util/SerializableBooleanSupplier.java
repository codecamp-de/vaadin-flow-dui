package de.codecamp.vaadin.base.util;


import java.io.Serializable;
import java.util.function.BooleanSupplier;


@FunctionalInterface
public interface SerializableBooleanSupplier
  extends
    BooleanSupplier,
    Serializable
{
}
