package de.codecamp.vaadin.base.util;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.dom.ElementConstants;
import java.util.List;
import java.util.Set;


public final class SizeUtils
{

  public static final String ATTR_WIDTH_FULL = "width-full";

  public static final String ATTR_HEIGHT_FULL = "height-full";

  public static final String ATTR_SIZE_FULL = "size-full";

  public static final String SIZE_100 = "100%";

  public static final String SIZE_AUTO = "auto";

  public static final String SIZE_UNSET = "unset";


  /**
   * Components where the corresponding Web Component has a specific height set. In certain
   * circumstances that height needs to be "unlocked" when the component should stretch or shrink.
   */
  private static final Set<Class<? extends Component>> UNLOCKABLE_HEIGHT =
      Set.of(Grid.class, Button.class);


  private SizeUtils()
  {
    // utility class
  }


  public static String getWidth(Component component)
  {
    if (component instanceof HasSize)
      return ((HasSize) component).getWidth();
    else
      return component.getElement().getStyle().get(ElementConstants.STYLE_WIDTH);
  }

  public static void setWidth(Component component, String width)
  {
    if (component instanceof HasSize)
      ((HasSize) component).setWidth(width);
    else
      component.getElement().getStyle().set(ElementConstants.STYLE_WIDTH, width);
  }

  public static void setWidthFull(Component component)
  {
    if (component instanceof HasSize)
      ((HasSize) component).setWidthFull();
    else
      component.getElement().getStyle().set(ElementConstants.STYLE_WIDTH, SIZE_100);
  }

  public static void setWidthFullAdaptive(FlexComponent container, List<Component> components,
      boolean horizontal, double weight)
  {
    setWidthFullAdaptive(container, components.toArray(Component[]::new), horizontal, weight);
  }

  public static void setWidthFullAdaptive(FlexComponent container, Component[] components,
      boolean horizontal, double weight)
  {
    if (horizontal)
    {
      container.setFlexGrow(weight, components);
    }
    else
    {
      container.setAlignSelf(Alignment.STRETCH, components);
    }
  }

  public static void setWidthAuto(Component component)
  {
    if (component instanceof HasSize)
      ((HasSize) component).setWidth(SIZE_AUTO);
    else
      component.getElement().getStyle().set(ElementConstants.STYLE_WIDTH, SIZE_AUTO);
  }


  public static String getHeight(Component component)
  {
    if (component instanceof HasSize)
      return ((HasSize) component).getHeight();
    else
      return component.getElement().getStyle().get(ElementConstants.STYLE_HEIGHT);
  }

  public static void setHeight(Component component, String height)
  {
    if (component instanceof HasSize)
      ((HasSize) component).setHeight(height);
    else
      component.getElement().getStyle().set(ElementConstants.STYLE_HEIGHT, height);
  }

  public static void setHeightFull(Component component)
  {
    if (component instanceof HasSize)
      ((HasSize) component).setHeightFull();
    else
      component.getElement().getStyle().set(ElementConstants.STYLE_HEIGHT, SIZE_100);
  }

  public static void setHeightFullAdaptive(FlexComponent container, List<Component> components,
      boolean horizontal, double weight)
  {
    setHeightFullAdaptive(container, components.toArray(Component[]::new), horizontal, weight);
  }

  public static void setHeightFullAdaptive(FlexComponent container, Component[] components,
      boolean horizontal, double weight)
  {
    for (Component child : components)
      unlockHeight(child);

    if (horizontal)
    {
      container.setAlignSelf(Alignment.STRETCH, components);
    }
    else
    {
      container.setFlexGrow(weight, components);
    }
  }

  public static void setHeightAuto(Component component)
  {
    if (component instanceof HasSize)
      ((HasSize) component).setHeight(SIZE_AUTO);
    else
      component.getElement().getStyle().set(ElementConstants.STYLE_HEIGHT, SIZE_AUTO);
  }


  public static void setSize(Component component, String size)
  {
    setWidth(component, size);
    setHeight(component, size);
  }

  public static void setSizeFull(Component component)
  {
    setWidthFull(component);
    setHeightFull(component);
  }

  public static void setSizeAuto(Component component)
  {
    setWidthAuto(component);
    setHeightAuto(component);
  }


  public static String getMinWidth(Component component)
  {
    if (component instanceof HasSize)
      return ((HasSize) component).getMinWidth();
    else
      return component.getElement().getStyle().get(ElementConstants.STYLE_MIN_WIDTH);
  }

  public static void setMinWidth(Component component, String minWidth)
  {
    if (component instanceof HasSize)
      ((HasSize) component).setMinWidth(minWidth);
    else
      component.getElement().getStyle().set(ElementConstants.STYLE_MIN_WIDTH, minWidth);
  }

  public static String getMaxWidth(Component component)
  {
    if (component instanceof HasSize)
      return ((HasSize) component).getMaxWidth();
    else
      return component.getElement().getStyle().get(ElementConstants.STYLE_MAX_WIDTH);
  }

  public static void setMaxWidth(Component component, String maxWidth)
  {
    if (component instanceof HasSize)
      ((HasSize) component).setMaxWidth(maxWidth);
    else
      component.getElement().getStyle().set(ElementConstants.STYLE_MAX_WIDTH, maxWidth);
  }


  public static String getMinHeight(Component component)
  {
    if (component instanceof HasSize)
      return ((HasSize) component).getMinHeight();
    else
      return component.getElement().getStyle().get(ElementConstants.STYLE_MIN_HEIGHT);
  }

  public static void setMinHeight(Component component, String minHeight)
  {
    if (component instanceof HasSize)
      ((HasSize) component).setMinHeight(minHeight);
    else
      component.getElement().getStyle().set(ElementConstants.STYLE_MIN_HEIGHT, minHeight);
  }

  public static String getMaxHeight(Component component)
  {
    if (component instanceof HasSize)
      return ((HasSize) component).getMaxHeight();
    else
      return component.getElement().getStyle().get(ElementConstants.STYLE_MAX_HEIGHT);
  }

  public static void setMaxHeight(Component component, String maxHeight)
  {
    if (component instanceof HasSize)
      ((HasSize) component).setMaxHeight(maxHeight);
    else
      component.getElement().getStyle().set(ElementConstants.STYLE_MAX_HEIGHT, maxHeight);
  }


  public static void setMinSize(Component component, String minSize)
  {
    setMinWidth(component, minSize);
    setMinHeight(component, minSize);
  }

  public static void setMaxSize(Component component, String maxSize)
  {
    setMaxWidth(component, maxSize);
    setMaxHeight(component, maxSize);
  }


  /**
   * Unlocks the height of certain components. This allows <code>stretch</code> to work but also
   * allows them to shrink in vertical layouts.
   *
   * @param component
   *          the component
   */
  public static void unlockHeight(Component component)
  {
    if (getHeight(component) != null || UNLOCKABLE_HEIGHT.contains(component.getClass()))
    {
      setHeight(component, SIZE_UNSET);
    }
  }

}
