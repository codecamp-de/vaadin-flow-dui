package de.codecamp.vaadin.base.util;


import com.vaadin.flow.component.orderedlayout.ThemableLayout;
import com.vaadin.flow.dom.ThemeList;
import java.util.Arrays;


/**
 * Themes related to spacing within a {@link ThemableLayout} that are not easily available otherwise
 * via the Java API.
 */
public enum ThemableLayoutSpacing
{

  XS("spacing-xs"),
  S("spacing-s"),
  M("spacing"),
  L("spacing-l"),
  XL("spacing-xl");


  private final String theme;


  ThemableLayoutSpacing(String theme)
  {
    this.theme = theme;
  }


  public String theme()
  {
    return theme;
  }

  public void applyTo(ThemableLayout themableLayout)
  {
    ThemeList themeList = themableLayout.getThemeList();
    themeList.add(theme());
    Arrays.asList(values()).forEach(s ->
    {
      if (s != this)
        themeList.remove(s.theme());
    });
  }

  public static void removeFrom(ThemableLayout themableLayout)
  {
    Arrays.asList(values()).forEach(s -> themableLayout.getThemeList().remove(s.theme()));
  }

}
