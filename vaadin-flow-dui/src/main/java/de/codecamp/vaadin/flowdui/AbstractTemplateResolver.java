package de.codecamp.vaadin.flowdui;


import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;


public abstract class AbstractTemplateResolver
  implements
    TemplateResolver
{

  /**
   * pattern to extract the html`...` from a Polymer or LitElement template
   */
  public static final Pattern EMBEDDED_TEMPLATE_PATTERN =
      Pattern.compile("html`(.*?)`", Pattern.DOTALL);


  @Override
  public Optional<TemplateDocument> resolveTemplate(ClassLoader classLoader, String templateId)
  {
    return findTemplateStream(classLoader, templateId).map(ts ->
    {
      Document document = readDocumentFromStream(templateId, ts);
      return new TemplateDocument(templateId, document, ts.getSource());
    });
  }

  protected abstract Optional<TemplateStream> findTemplateStream(ClassLoader classLoader,
      String templateId);


  /**
   * Reads the document from the given template stream.
   *
   * @param templateId
   *          the template ID
   * @param templateStream
   *          the template stream to be read
   * @return the document read from the stream
   * @throws TemplateException
   *           if the stream could not be loaded into a document
   */
  public static Document readDocumentFromStream(String templateId, TemplateStream templateStream)
  {
    try (InputStream is = templateStream.getInputStream())
    {
      String docText = IOUtils.toString(is, StandardCharsets.UTF_8);

      /*
       * If the document contains a Polymer or Lit template, it should contain a html`...` block.
       * Extract the first one found as it contains the actual HTML/template that should be parsed
       * here.
       */
      Matcher matcher = EMBEDDED_TEMPLATE_PATTERN.matcher(docText);
      if (matcher.find())
        docText = matcher.group(1);

      return Jsoup.parseBodyFragment(docText);
    }
    catch (IOException ex)
    {
      throw new TemplateException(templateId, templateStream.getSource(),
          "Failed to parse the template resource.", ex);
    }
  }


  protected class TemplateStream
    implements
      Serializable
  {

    private final InputStream inputStream;

    private final String source;


    public TemplateStream(InputStream inputStream, String source)
    {
      this.inputStream = requireNonNull(inputStream, "inputStream must not be null");
      this.source = requireNonNull(source, "source must not be null");
    }


    public InputStream getInputStream()
    {
      return inputStream;
    }

    public String getSource()
    {
      return source;
    }

  }

}
