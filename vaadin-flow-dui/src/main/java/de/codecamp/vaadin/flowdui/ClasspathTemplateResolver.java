package de.codecamp.vaadin.flowdui;


import java.io.InputStream;
import java.util.Optional;


/**
 * A classpath-based {@link TemplateResolver}. Resources will be loaded using the given class
 * loader. The template ID is expected to be the fully qualified name of a class; that name will be
 * used as a resource path with several file extensions (.html, .js) in order to find a template.
 */
public class ClasspathTemplateResolver
  extends
    AbstractTemplateResolver
{

  private static final String[] SUPPORTED_EXTENSIONS = {".dui", ".html", ".ts", ".js"};


  @Override
  protected Optional<TemplateStream> findTemplateStream(ClassLoader classLoader, String templateId)
  {
    String resourcePathBase = templateId.replace(".", "/");

    for (String ext : SUPPORTED_EXTENSIONS)
    {
      String resourcePath = resourcePathBase + ext;
      InputStream inputStream = classLoader.getResourceAsStream(resourcePath); // NOPMD:CloseResource closed by user of TemplateStream
      if (inputStream == null)
        continue;

      return Optional.of(new TemplateStream(inputStream, "classpath:" + resourcePath));
    }

    return Optional.empty();
  }

}
