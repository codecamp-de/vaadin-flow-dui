package de.codecamp.vaadin.flowdui;


import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import org.jsoup.nodes.Element;


/**
 * Component factories are responsible for creating {@link Component components} based on
 * {@link Element HTML elements} parsed from a template.
 * <p>
 * Use {@link DuiComponent} annotations to declare and describe all the
 * {@link DuiComponent#tagName() tags} this factory supports. Generally, without any
 * {@link DuiComponent} annotations, a factory is always considered applicable. When
 * {@link DuiComponent} annotations are present, the factory will only be considered applicable for
 * the declared tags. If a factory should always be applicable for all tags despite having some
 * declarations, an additional {@link DuiComponent} with {@link DuiComponent#TAG_ALL} as
 * {@link DuiComponent#tagName()} (and {@link Component} as {@link DuiComponent#componentType()})
 * can be used.
 *
 * @see DuiComponent
 */
public interface ComponentFactory
{

  /**
   * Used as name of the unnamed default slot of a component.
   */
  String SLOT_DEFAULT = DuiComponent.SLOT_DEFAULT;


  /**
   * Creates a component based on the given {@link ElementParserContext}, if the element is
   * supported by this factory.
   *
   * @param tagName
   *          the tag name
   * @param context
   *          the parse context of the element
   * @return the created component or null if the given element is not recognized
   */
  Component createComponent(String tagName, ElementParserContext context);

}
