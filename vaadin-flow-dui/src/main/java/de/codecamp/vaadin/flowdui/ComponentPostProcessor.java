package de.codecamp.vaadin.flowdui;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasHelper;
import com.vaadin.flow.component.HasSize;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import org.jsoup.nodes.Element;


/**
 * Post-processors can make additional modifications to components created by a factory; this is
 * useful e.g. to handle mix-in interfaces like {@link HasSize}. But they can also be used to handle
 * child {@link Element elements} which is required to implement the shared behavior of e.g.
 * {@link HasHelper}.
 * <p>
 * Use {@link DuiComponent} annotations to declare and describe all the
 * {@link DuiComponent#componentType() components types or mix-in interfaces} this post-processor
 * supports. Generally, without any {@link DuiComponent} annotations, a post-processor is always
 * considered applicable. When {@link DuiComponent} annotations are present, the post-processor will
 * only be considered applicable for the declared {@link DuiComponent#componentType() component
 * types}. If a post-processor should always be applicable for all component types despite having
 * some declarations, an additional {@link DuiComponent} with {@link Object} as
 * {@link DuiComponent#componentType()} can be used.
 *
 * @see DuiComponent
 */
// TODO merge with ComponentProcessor (Serializable)
public interface ComponentPostProcessor
{

  /**
   * Used as name of the unnamed default slot of a component.
   */
  String SLOT_DEFAULT = DuiComponent.SLOT_DEFAULT;


  /**
   * Apply this post processor to the given component.
   *
   * @param component
   *          the component to be post-processed
   * @param context
   *          the parse context of the element
   */
  void postProcessComponent(Component component, ElementParserContext context);


  /**
   * Called to handle a child element of a component element in a template. A child element is only
   * handled once, so this method is not guaranteed to be called on all or any post-processors.
   *
   * @param parentComponent
   *          the parent component
   * @param slotName
   *          the name of the slot the element is intended for;
   *          ({@link ComponentFactory#SLOT_DEFAULT}) for the unnamed default slot
   * @param childElement
   *          the child element
   * @param context
   *          the parse context of the template
   * @return whether the child component has been handled; if {@code true} no further
   *         {@link ComponentPostProcessor} is considered
   */
  // TODO move to separate interface
  default boolean handleChildElement(Component parentComponent, String slotName,
      Element childElement, TemplateParserContext context)
  {
    return false;
  }

}
