package de.codecamp.vaadin.flowdui;


import static java.util.Objects.requireNonNull;

import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.flowdui.TemplateParserContext.AttributeSource;
import de.codecamp.vaadin.flowdui.TemplateParserContext.ChildElementHandler;
import de.codecamp.vaadin.flowdui.TemplateParserContext.ComponentProcessor;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;


public class ElementParserContext
{

  private final TemplateParserContext templateParserContext;

  private final Element element;

  private final Set<String> consumedAttributes;


  /* package */ ElementParserContext(TemplateParserContext templateParserContext, Element element)
  {
    this.templateParserContext = templateParserContext;
    this.element = element;
    this.consumedAttributes = new HashSet<>();
  }


  /**
   * Returns the wrapped {@link TemplateParserContext}. There's usually no reason to access this
   * directly.
   *
   * @return the wrapped {@link TemplateParserContext}
   */
  public TemplateParserContext getTemplateParserContext()
  {
    return templateParserContext;
  }

  /**
   * Returns the element currently being parsed.
   *
   * @return the element currently being parsed
   */
  public Element getElement()
  {
    return element;
  }

  /**
   * Returns the tag name of the element.
   *
   * @return the tag name of the element
   */
  public String getTagName()
  {
    return getElement().tagName();
  }


  /**
   * Returns whether this context contains information about slotted components.
   *
   * @return whether this context contains information about slotted components
   */
  public boolean hasSlottedComponentsInfo()
  {
    return templateParserContext.hasSlottedComponentsInfo();
  }

  /**
   * Returns the component for the specified slot, if available. The slot is
   * {@link #registerAvailableSlot(String) automatically registered}.
   *
   * @param slotName
   *          the slot name
   * @return the optional component for the specified slot; <b>{@code null} if no {@link Slotted}
   *         annotation is actually referencing this slot</b>
   */
  public Optional<Component> getComponentInSlot(String slotName)
  {
    return templateParserContext.getComponentInSlot(slotName);
  }

  /**
   * Registers a slot available in a template. {@link #getComponentInSlot(String)} will already
   * implicitly do this.
   *
   * @param slotName
   *          the slot name
   */
  public void registerAvailableSlot(String slotName)
  {
    templateParserContext.registerAvailableSlot(slotName);
  }


  /**
   * Returns the set of attributes of the {@link #getElement() element} that have so far been
   * consumed to configure the component. Other provided methods will already do that automatically.
   *
   * @return the set of consumed attributes
   */
  public Set<String> getConsumedAttributes()
  {
    return consumedAttributes;
  }


  /**
   * Creates a component based on the given HTML element.
   *
   * @param componentElement
   *          the HTML to create the component from
   * @return the new component
   */
  public Component readComponent(Element componentElement)
  {
    return templateParserContext.readComponent(componentElement);
  }

  /**
   * Creates a component based on the given HTML element.
   * <p>
   * An optional {@link ComponentProcessor} may be provided which is called after the component has
   * been created, but before any {@link ComponentPostProcessor post-processors} are called. This is
   * especially interesting for the factories of layout containers that need to consume attributes
   * that are placed on child components. It allows to consume them before they will be rejected as
   * unconsumed.
   *
   *
   * Escpecially when this method is used to read child components
   *
   * this optional component processor will be called after creating the child component but before
   * calling any other component post-processors; may be null
   *
   * @param componentElement
   *          the HTML to create the component from
   * @param componentProcessor
   *          an optional component processor that will be called after creating the component but
   *          before calling any other component post-processors; may be null
   * @return the new component
   */
  public Component readComponent(Element componentElement, ComponentProcessor componentProcessor)
  {
    return templateParserContext.readComponent(componentElement, componentProcessor);
  }

  /**
   * Creates a component based on the given HTML element. Same as
   * {@link #readComponent(Element, ComponentProcessor)} except that the slot-attribute is
   * automatically marked as consumed.
   *
   * @param componentElement
   *          the element for which to create a component
   * @return the created component
   */
  public Component readComponentForSlot(Element componentElement)
  {
    return templateParserContext.readComponentForSlot(componentElement);
  }

  /**
   * Creates a component based on the given HTML element. Same as
   * {@link #readComponent(Element, ComponentProcessor)} except that the slot-attribute is
   * automatically marked as consumed.
   *
   * @param componentElement
   *          the element for which to create a component
   * @param componentProcessor
   *          an optional component processor that will be called after creating the component but
   *          before calling any other component post-processors; may be null
   * @return the created component
   */
  public Component readComponentForSlot(Element componentElement,
      ComponentProcessor componentProcessor)
  {
    return templateParserContext.readComponentForSlot(componentElement, componentProcessor);
  }


  /**
   * Reads the child nodes of the {@link #getElement() element of this context} one by one. The
   * child {@link Element elements} are passed to the provided {@link ChildElementHandler} and any
   * interested
   * {@link ComponentPostProcessor#handleChildElement(Component, String, Element, TemplateParserContext)
   * post-processors}. Encountered {@link TextNode text nodes} are passed to the
   * {@link TextNodeHandler}.
   *
   * @param parentComponent
   *          the parent component
   * @param childElementHandler
   *          a child element handler; may be null to reject all child elements, post-processors may
   *          still handle them though
   * @param textNodeHandler
   *          a text node handler; may be null to reject all text nodes
   */
  public void readChildren(Component parentComponent, ChildElementHandler childElementHandler,
      TextNodeHandler textNodeHandler)
  {
    templateParserContext.readChildren(parentComponent, element, childElementHandler,
        textNodeHandler);
  }

  /**
   * Reads the child nodes of the given {@link Element element} one by one. The child {@link Element
   * elements} are passed to the provided {@link ChildElementHandler} and any interested
   * {@link ComponentPostProcessor#handleChildElement(Component, String, Element, TemplateParserContext)
   * post-processors}. Encountered {@link TextNode text nodes} are passed to the
   * {@link TextNodeHandler}.
   *
   * @param parentComponent
   *          the parent component
   * @param parentElement
   *          the HTML element of the parent component
   * @param childElementHandler
   *          a child element handler; may be null to reject all child elements, post-processors may
   *          still handle them though
   * @param textNodeHandler
   *          a text node handler; may be null to reject all text nodes
   */
  public void readChildren(Component parentComponent, Element parentElement,
      ChildElementHandler childElementHandler, TextNodeHandler textNodeHandler)
  {
    templateParserContext.readChildren(parentComponent, parentElement, childElementHandler,
        textNodeHandler);
  }

  /**
   * Reads the child nodes of the {@link #getElement() element of this context} one by one. The
   * child {@link Element elements} are passed to any interested
   * {@link ComponentPostProcessor#handleChildElement(Component, String, Element, TemplateParserContext)
   * post-processors}. Encountered {@link TextNode text nodes} are rejected.
   *
   * @param parentComponent
   *          the parent component
   */
  public void readChildren(Component parentComponent)
  {
    templateParserContext.readChildren(parentComponent, element);
  }

  /**
   * Reads the child nodes of the given {@link Element element} one by one. The child {@link Element
   * elements} are passed to any interested
   * {@link ComponentPostProcessor#handleChildElement(Component, String, Element, TemplateParserContext)
   * post-processors}. Encountered {@link TextNode text nodes} are rejected.
   *
   * @param parentComponent
   *          the parent component
   * @param parentElement
   *          the HTML element of the parent component
   */
  public void readChildren(Component parentComponent, Element parentElement)
  {
    templateParserContext.readChildren(parentComponent, parentElement);
  }

  /**
   * Reads the child nodes of the {@link #getElement() element of this context} one by one. The
   * child {@link Element elements} are passed to any interested
   * {@link ComponentPostProcessor#handleChildElement(Component, String, Element, TemplateParserContext)
   * post-processors}. Encountered {@link TextNode text nodes} are rejected. If a child element is
   * unhandled the given error message is displayed.
   *
   * @param parentComponent
   *          the parent component
   * @param unhandledChildMessage
   *          the error message to show when there are unhandled child elements
   */
  public void readChildren(Component parentComponent, String unhandledChildMessage)
  {
    templateParserContext.readChildren(parentComponent, element, unhandledChildMessage);
  }



  /**
   * Returns whether the element has at least one of the specified attributes.
   *
   * @param attributeNames
   *          the attribute names to check
   * @return whether the element has at least one of the specified attributes
   */
  public boolean hasAttribute(String... attributeNames)
  {
    return Stream.of(attributeNames).anyMatch(element::hasAttr);
  }


  /**
   * Starts mapping the given attribute. By default, attributes that are already consumed will be
   * skipped.
   *
   * @param attributeName
   *          the attribute name to be mapped
   * @return the attribute source providing the next step in the mapping
   */
  public AttributeSource mapAttribute(String attributeName)
  {
    return templateParserContext.mapAttribute(element, attributeName, consumedAttributes);
  }

  /**
   * Starts mapping the given attribute names. By default, attributes that are already consumed will
   * be skipped.
   *
   * @param attributeNames
   *          the attribute names to be mapped; one primary name and 0 or more synonyms.
   * @return the attribute source providing the next step in the mapping
   */
  public AttributeSource mapAttribute(List<String> attributeNames)
  {
    requireNonNull(attributeNames, "attributeNames must not be null");
    return templateParserContext.mapAttribute(element, attributeNames, null, consumedAttributes);
  }

  /**
   * Starts mapping the given attribute. By default, attributes that are already consumed will be
   * skipped.
   *
   * @param attributeName
   *          the attribute name to be mapped
   * @param deprecatedAttributeNames
   *          the deprecated or old name(s) for this attribute
   * @return the attribute source providing the next step in the mapping
   */
  public AttributeSource mapAttribute(String attributeName, Set<String> deprecatedAttributeNames)
  {
    requireNonNull(attributeName, "attributeName must not be null");
    return templateParserContext.mapAttribute(element, attributeName, deprecatedAttributeNames,
        consumedAttributes);
  }

  /**
   * Starts mapping the given deprecated attribute. By default, attributes that are already consumed
   * will be skipped. Use this method for deprecated attributes that have no direct replacement
   * (i.e. with the same parameter).
   *
   * @param attributeName
   *          the deprecated attribute name to be mapped
   * @return the attribute source providing the next step in the mapping
   */
  public AttributeSource mapDeprecatedAttribute(String attributeName)
  {
    requireNonNull(attributeName, "attributeName must not be null");
    return templateParserContext.mapAttribute(element, (List<String>) null, Set.of(attributeName),
        consumedAttributes);
  }


  /**
   * Copies the given attribute over to the component's {@link com.vaadin.flow.dom.Element} as it
   * is, unless the attribute is already consumed.
   *
   * @param attributeName
   *          the attribute to copy
   * @param targetComponent
   *          the target component
   */
  public void copyAttributeTo(String attributeName, Component targetComponent)
  {
    templateParserContext.copyAttributeTo(element, attributeName, targetComponent,
        consumedAttributes);
  }

  /**
   * Copy all remaining unconsumed attributes over to the component's {@link Element} as they are.
   *
   * @param targetComponent
   *          the target component
   */
  public void copyAttributesTo(Component targetComponent)
  {
    templateParserContext.copyAttributesTo(element, targetComponent, consumedAttributes);
  }


  /**
   * Fails the current parsing process by throwing a {@link TemplateException}.
   *
   * <p>
   * Despite the return type being {@link TemplateException} this method never actually returns
   * anything as it always throws the {@link TemplateException}. This is essentially to allow the
   * convenient pattern <b>{@code throw context.fail(...);}</b> because the compiler otherwise
   * wouldn't know that code below a call to this method is never executed.
   *
   * @param message
   *          the error message
   * @return nothing, as this method never returns regularly
   * @throws TemplateException
   *           always
   */
  public TemplateException fail(String message)
  {
    return templateParserContext.fail(element, message);
  }

  /**
   * Fails the current parsing process by throwing a {@link TemplateException}.
   *
   * <p>
   * Despite the return type being {@link TemplateException} this method never actually returns
   * anything as it always throws the {@link TemplateException}. This is essentially to allow the
   * convenient pattern <b>{@code throw context.fail(...);}</b> because the compiler otherwise
   * wouldn't know that code below a call to this method is never executed.
   *
   * @param message
   *          the error message
   * @param cause
   *          the cause of the failure
   * @return nothing, as this method never returns regularly
   * @throws TemplateException
   *           always
   */
  public TemplateException fail(String message, Throwable cause)
  {
    return templateParserContext.fail(element, message, cause);
  }

  /**
   * Fails the current parsing process by throwing a {@link TemplateException}.
   *
   * <p>
   * Despite the return type being {@link TemplateException} this method never actually returns
   * anything as it always throws the {@link TemplateException}. This is essentially to allow the
   * convenient pattern <b>{@code throw context.fail(...);}</b> because the compiler otherwise
   * wouldn't know that code below a call to this method is never executed.
   *
   * @param attributeName
   *          the name of the attribute the problem occurred on
   * @param message
   *          the error message
   * @return nothing, as this method never returns regularly
   * @throws TemplateException
   *           always
   */
  public TemplateException fail(String attributeName, String message)
  {
    return templateParserContext.fail(element, attributeName, message);
  }

  /**
   * Fails the current parsing process by throwing a {@link TemplateException}.
   *
   * <p>
   * Despite the return type being {@link TemplateException} this method never actually returns
   * anything as it always throws the {@link TemplateException}. This is essentially to allow the
   * convenient pattern <b>{@code throw context.fail(...);}</b> because the compiler otherwise
   * wouldn't know that code below a call to this method is never executed.
   *
   * @param attributeName
   *          the name of the attribute the problem occurred on
   * @param message
   *          the error message
   * @param cause
   *          the cause of the failure
   * @return nothing, as this method never returns regularly
   * @throws TemplateException
   *           always
   */
  public TemplateException fail(String attributeName, String message, Throwable cause)
  {
    return templateParserContext.fail(element, attributeName, message, cause);
  }

}
