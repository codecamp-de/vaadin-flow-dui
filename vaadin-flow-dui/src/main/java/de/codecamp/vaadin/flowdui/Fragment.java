package de.codecamp.vaadin.flowdui;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Used to annotate fragment hosts (i.e. template hosts of fragments). This annotation is also found
 * on superclasses.
 *
 * @see Template
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Fragment
{

  /**
   * Returns the template ID. If omitted, the ID will be determined from {@link Fragment} instances
   * on superclasses and based on the enclosing class (assuming it's a template host), in that
   * order.
   *
   * @return the template ID
   */
  String templateId() default "";

  /**
   * Returns the fragment ID. If omitted, a default will be determined based on the name of the
   * annotated class.
   *
   * @return the fragment ID
   */
  String id() default "";

}
