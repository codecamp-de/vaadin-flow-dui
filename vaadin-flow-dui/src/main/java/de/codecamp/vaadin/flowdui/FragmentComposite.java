package de.codecamp.vaadin.flowdui;


import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.base.Composite;


/**
 * Abstract base class for composites whose content is created from a template fragment.
 * <p>
 * Use {@link Template} to explicitly specify a template ID. This annotation is also found in
 * superclasses or enclosing classes.<br>
 * Use {@link Fragment} to explicitly specify a fragment ID. This annotation is also found in
 * superclasses.
 * <p>
 * Override {@link #contentCreated()} to execute code right after the content has been initialized.
 * <p>
 * <em>See {@link Composite} for important information about the initialization.</em>
 * <p>
 * Using this class is completely optional and only provides a bit of convenience.
 *
 * @see #contentCreated()
 * @see TemplateEngine#instantiateTemplateFragment(Object)
 * @see Template
 * @see Fragment
 * @see Mapped
 * @see Slotted
 */
public abstract class FragmentComposite
  extends
    Composite
{

  /**
   * Creates a new composite based on a template fragment. Content is initialized lazily or when
   * {@link #initializeContent()} is called.
   */
  protected FragmentComposite()
  {
  }


  @Override
  protected final Component createContent()
  {
    return TemplateEngine.get().instantiateTemplateFragment(this);
  }

}
