package de.codecamp.vaadin.flowdui;


import com.vaadin.flow.component.Component;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.jsoup.nodes.Element;


/**
 * Contains the end result of parsing and processing a template, including the root component and
 * information about the encountered components, slots and fragments.
 */
public class ParsedTemplate
{

  private final String templateId;

  private final String templateSourceDescription;

  private final TemplateReport report;

  private final Component rootComponent;

  /**
   * component ID -> Component
   */
  private final Map<String, Component> idToComponent;

  /**
   * slot name -> TemplateSlot
   */
  private final Map<String, TemplateSlot> nameToSlot;

  /**
   * fragment ID -> Element
   */
  private final Map<String, Element> idToTemplateFragment;


  public ParsedTemplate(String templateId, String templateSourceDescription, TemplateReport report,
      Component rootComponent, Map<String, Component> idToComponent,
      Map<String, TemplateSlot> nameToSlot, Map<String, Element> idToTemplateFragment)
  {
    this.templateId = templateId;
    this.templateSourceDescription = templateSourceDescription;
    this.report = report;
    this.rootComponent = rootComponent;
    this.idToComponent = idToComponent;
    this.nameToSlot = nameToSlot;
    this.idToTemplateFragment = idToTemplateFragment;
  }


  public String getTemplateId()
  {
    return templateId;
  }

  public String getTemplateSourceDescription()
  {
    return templateSourceDescription;
  }

  public Component getRootComponent()
  {
    return rootComponent;
  }

  public TemplateReport getReport()
  {
    return report;
  }

  public Component getComponentById(String id)
  {
    return idToComponent.get(id);
  }

  public Set<String> getSlotNames()
  {
    return new HashSet<>(nameToSlot.keySet());
  }

  public TemplateSlot getSlotByName(String name)
  {
    return nameToSlot.get(name);
  }

  public Element getTemplateFragmentById(String id)
  {
    return idToTemplateFragment.get(id);
  }

}
