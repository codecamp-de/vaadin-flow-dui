package de.codecamp.vaadin.flowdui;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;


@Tag(Style.TAG)
public class Style
  extends
    Component
{

  public static final String TAG = "style";

}
