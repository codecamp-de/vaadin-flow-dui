package de.codecamp.vaadin.flowdui;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Used to annotate template hosts. This annotation is also found on superclasses.
 *
 * @see Fragment
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Template
{

  /**
   * Returns the template ID. If omitted, a default will be determined based on the name of the
   * annotated class.
   *
   * @return the template ID
   */
  String id() default "";


  /**
   * Returns the template content. If empty, the {@link TemplateResolver} mechanism will be used.
   *
   * @return the template content
   */
  String content() default "";

}
