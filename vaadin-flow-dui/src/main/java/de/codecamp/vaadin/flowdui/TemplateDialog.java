package de.codecamp.vaadin.flowdui;


import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.base.CompositeDialog;


/**
 * Abstract base class for dialogs whose content is created from a template.
 * <p>
 * Use {@link Template} to explicitly specify a template ID which different from the default. This
 * annotation is also picked up from superclasses.
 * <p>
 * Override {@link #contentCreated()} to execute code right after the content has been initialized.
 * <p>
 * <em>See {@link CompositeDialog} for important information about the initialization.</em>
 * <p>
 * Using this class is completely optional and only provides a bit of convenience.
 *
 * @see #contentCreated()
 * @see TemplateEngine#instantiateTemplate(Object)
 * @see Template
 * @see Mapped
 * @see Slotted
 */
public abstract class TemplateDialog
  extends
    CompositeDialog
{

  /**
   * Constructs a new instance.
   */
  protected TemplateDialog()
  {
  }


  @Override
  protected final Component createContent()
  {
    return TemplateEngine.get().instantiateTemplate(this);
  }

}
