package de.codecamp.vaadin.flowdui;


import static java.util.Objects.requireNonNull;

import org.jsoup.nodes.Document;


public class TemplateDocument
{

  private final String templateId;

  private final Document document;

  private final String source;


  public TemplateDocument(String templateId, Document document, String source)
  {
    this.templateId = requireNonNull(templateId, "templateId must not be null");
    this.document = requireNonNull(document, "document must not be null");
    this.source = requireNonNull(source, "source must not be null");
  }


  /**
   * Returns the ID of the template.
   *
   * @return the ID of the template
   */
  public String getTemplateId()
  {
    return templateId;
  }

  /**
   * Returns the template as jsoup {@link Document}.
   *
   * @return the template as jsoup {@link Document}
   */
  public Document getDocument()
  {
    return document;
  }

  /**
   * Returns the source of the template document, like a URL or file path. This is used in logs and
   * exception messages to help track down problems.
   *
   * @return the source of the template document
   */
  public String getSource()
  {
    return source;
  }

}
