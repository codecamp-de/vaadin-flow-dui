package de.codecamp.vaadin.flowdui;


import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toUnmodifiableList;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.internal.ReflectTools;
import com.vaadin.flow.server.VaadinService;
import de.codecamp.vaadin.flowdui.autoconfigure.TemplateProcessorRegistrar;
import de.codecamp.vaadin.flowdui.autoconfigure.TemplateProcessorRegistry;
import de.codecamp.vaadin.flowdui.declare.ComponentDec;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import de.codecamp.vaadin.flowdui.factories.CommonComponentPostProcessor;
import de.codecamp.vaadin.flowdui.factories.FocusablePostProcessor;
import de.codecamp.vaadin.flowdui.factories.GenericComponentFactory;
import de.codecamp.vaadin.flowdui.factories.HasComponentsPostProcessor;
import de.codecamp.vaadin.flowdui.factories.HasEnabledPostProcessor;
import de.codecamp.vaadin.flowdui.factories.HasHelperPostProcessor;
import de.codecamp.vaadin.flowdui.factories.HasLabelPostProcessor;
import de.codecamp.vaadin.flowdui.factories.HasPlaceholderPostProcessor;
import de.codecamp.vaadin.flowdui.factories.HasSingleComponentPostProcessor;
import de.codecamp.vaadin.flowdui.factories.HasSizePostProcessor;
import de.codecamp.vaadin.flowdui.factories.HasStylePostProcessor;
import de.codecamp.vaadin.flowdui.factories.HasThemePostProcessor;
import de.codecamp.vaadin.flowdui.factories.HasValidationPostProcessor;
import de.codecamp.vaadin.flowdui.factories.HasValuePostProcessor;
import de.codecamp.vaadin.flowdui.factories.HtmlFactory;
import de.codecamp.vaadin.flowdui.factories.TemplateElementsFactory;
import de.codecamp.vaadin.flowdui.factories.dataentry.CheckboxFactory;
import de.codecamp.vaadin.flowdui.factories.dataentry.ComboBoxFactory;
import de.codecamp.vaadin.flowdui.factories.dataentry.DatePickerFactory;
import de.codecamp.vaadin.flowdui.factories.dataentry.DateTimePickerFactory;
import de.codecamp.vaadin.flowdui.factories.dataentry.ListBoxFactory;
import de.codecamp.vaadin.flowdui.factories.dataentry.MessageInputFactory;
import de.codecamp.vaadin.flowdui.factories.dataentry.RadioButtonFactory;
import de.codecamp.vaadin.flowdui.factories.dataentry.SelectFactory;
import de.codecamp.vaadin.flowdui.factories.dataentry.TextFieldFactory;
import de.codecamp.vaadin.flowdui.factories.dataentry.TimePickerFactory;
import de.codecamp.vaadin.flowdui.factories.dataentry.UploadFactory;
import de.codecamp.vaadin.flowdui.factories.layouts.AppLayoutFactory;
import de.codecamp.vaadin.flowdui.factories.layouts.FormLayoutFactory;
import de.codecamp.vaadin.flowdui.factories.layouts.LoginFactory;
import de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory;
import de.codecamp.vaadin.flowdui.factories.layouts.ScrollerFactory;
import de.codecamp.vaadin.flowdui.factories.layouts.SplitLayoutFactory;
import de.codecamp.vaadin.flowdui.factories.pro.BoardFactory;
import de.codecamp.vaadin.flowdui.factories.pro.ChartFactory;
import de.codecamp.vaadin.flowdui.factories.pro.CookieConsentFactory;
import de.codecamp.vaadin.flowdui.factories.pro.CrudFactory;
import de.codecamp.vaadin.flowdui.factories.pro.GridProFactory;
import de.codecamp.vaadin.flowdui.factories.pro.RichTextEditorFactory;
import de.codecamp.vaadin.flowdui.factories.shared.HasAllowedCharPatternPostProcessor;
import de.codecamp.vaadin.flowdui.factories.shared.HasAutoOpenPostProcessor;
import de.codecamp.vaadin.flowdui.factories.shared.HasClearButtonPostProcessor;
import de.codecamp.vaadin.flowdui.factories.shared.HasPrefixPostProcessor;
import de.codecamp.vaadin.flowdui.factories.shared.HasSuffixPostProcessor;
import de.codecamp.vaadin.flowdui.factories.shared.HasTooltipPostProcessor;
import de.codecamp.vaadin.flowdui.factories.visandint.AccordionFactory;
import de.codecamp.vaadin.flowdui.factories.visandint.AvatarFactory;
import de.codecamp.vaadin.flowdui.factories.visandint.ButtonFactory;
import de.codecamp.vaadin.flowdui.factories.visandint.DetailsFactory;
import de.codecamp.vaadin.flowdui.factories.visandint.DialogFactory;
import de.codecamp.vaadin.flowdui.factories.visandint.GridFactory;
import de.codecamp.vaadin.flowdui.factories.visandint.IconFactory;
import de.codecamp.vaadin.flowdui.factories.visandint.MenuBarFactory;
import de.codecamp.vaadin.flowdui.factories.visandint.MessageListFactory;
import de.codecamp.vaadin.flowdui.factories.visandint.ProgressBarFactory;
import de.codecamp.vaadin.flowdui.factories.visandint.SideNavFactory;
import de.codecamp.vaadin.flowdui.factories.visandint.TabsFactory;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.BiFunction;
import java.util.stream.Stream;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Comment;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The {@link TemplateEngine} is responsible for instantiating DUI templates, as well as perform the
 * associated component {@link Mapped mapping} and {@link Slotted slotting} on template hosts.
 */
public class TemplateEngine
  implements
    TemplateProcessorRegistry
{

  private static final List<ComponentFactory> DEFAULT_FACTORIES = new ArrayList<>();

  private static final List<ComponentPostProcessor> DEFAULT_POST_PROCESSORS = new ArrayList<>();

  static
  {
    registerDefaultFactoryOrPostProcessor(new CommonComponentPostProcessor());
    registerDefaultFactoryOrPostProcessor(new TemplateElementsFactory());

    registerDefaultFactoryOrPostProcessor(new FocusablePostProcessor());
    registerDefaultFactoryOrPostProcessor(new HasEnabledPostProcessor());
    registerDefaultFactoryOrPostProcessor(new HasHelperPostProcessor());
    registerDefaultFactoryOrPostProcessor(new HasLabelPostProcessor());
    registerDefaultFactoryOrPostProcessor(new HasPlaceholderPostProcessor());
    registerDefaultFactoryOrPostProcessor(new HasSizePostProcessor());
    registerDefaultFactoryOrPostProcessor(new HasStylePostProcessor());
    registerDefaultFactoryOrPostProcessor(new HasThemePostProcessor());
    registerDefaultFactoryOrPostProcessor(new HasValidationPostProcessor());
    registerDefaultFactoryOrPostProcessor(new HasValuePostProcessor());
    registerDefaultFactoryOrPostProcessor(new HasComponentsPostProcessor());
    registerDefaultFactoryOrPostProcessor(new HasSingleComponentPostProcessor());


    // shared
    registerDefaultFactoryOrPostProcessor(new HasAllowedCharPatternPostProcessor());
    registerDefaultFactoryOrPostProcessor(new HasAutoOpenPostProcessor());
    registerDefaultFactoryOrPostProcessor(new HasClearButtonPostProcessor());
    registerDefaultFactoryOrPostProcessor(new HasPrefixPostProcessor());
    registerDefaultFactoryOrPostProcessor(new HasSuffixPostProcessor());
    registerDefaultFactoryOrPostProcessor(new HasTooltipPostProcessor());

    // layouts
    registerDefaultFactoryOrPostProcessor(new AppLayoutFactory());
    registerDefaultFactoryOrPostProcessor(new FormLayoutFactory());
    registerDefaultFactoryOrPostProcessor(new LoginFactory());
    registerDefaultFactoryOrPostProcessor(new OrderedLayoutFactory());
    registerDefaultFactoryOrPostProcessor(new ScrollerFactory());
    registerDefaultFactoryOrPostProcessor(new SplitLayoutFactory());

    /*
     * This is intentionally registered after all other post-processors. This allows them to consume
     * attributes before the rest is copied over verbatim.
     */
    registerDefaultFactoryOrPostProcessor(new GenericComponentFactory());

    // form inputs
    registerDefaultFactoryOrPostProcessor(new CheckboxFactory());
    registerDefaultFactoryOrPostProcessor(new ComboBoxFactory());
    registerDefaultFactoryOrPostProcessor(new DatePickerFactory());
    registerDefaultFactoryOrPostProcessor(new DateTimePickerFactory());
    registerDefaultFactoryOrPostProcessor(new ListBoxFactory());
    registerDefaultFactoryOrPostProcessor(new MessageInputFactory());
    registerDefaultFactoryOrPostProcessor(new RadioButtonFactory());
    registerDefaultFactoryOrPostProcessor(new SelectFactory());
    registerDefaultFactoryOrPostProcessor(new TextFieldFactory());
    registerDefaultFactoryOrPostProcessor(new TimePickerFactory());
    registerDefaultFactoryOrPostProcessor(new UploadFactory());

    // visualization & interaction
    registerDefaultFactoryOrPostProcessor(new AccordionFactory());
    registerDefaultFactoryOrPostProcessor(new AvatarFactory());
    registerDefaultFactoryOrPostProcessor(new ButtonFactory());
    registerDefaultFactoryOrPostProcessor(new DetailsFactory());
    registerDefaultFactoryOrPostProcessor(new DialogFactory());
    registerDefaultFactoryOrPostProcessor(new GridFactory());
    registerDefaultFactoryOrPostProcessor(new IconFactory());
    registerDefaultFactoryOrPostProcessor(new MenuBarFactory());
    registerDefaultFactoryOrPostProcessor(new MessageListFactory());
    registerDefaultFactoryOrPostProcessor(new ProgressBarFactory());
    registerDefaultFactoryOrPostProcessor(new SideNavFactory());
    registerDefaultFactoryOrPostProcessor(new TabsFactory());

    // html
    registerDefaultFactoryOrPostProcessor(new HtmlFactory());

    // Pro components might not be present
    if (classExists("com.vaadin.flow.component.board.Board"))
      registerDefaultFactoryOrPostProcessor(new BoardFactory());
    if (classExists("com.vaadin.flow.component.charts.Chart"))
      registerDefaultFactoryOrPostProcessor(new ChartFactory());
    if (classExists("com.vaadin.flow.component.cookieconsent.CookieConsent"))
      registerDefaultFactoryOrPostProcessor(new CookieConsentFactory());
    if (classExists("com.vaadin.flow.component.crud.Crud"))
      registerDefaultFactoryOrPostProcessor(new CrudFactory());
    if (classExists("com.vaadin.flow.component.gridpro.GridPro"))
      registerDefaultFactoryOrPostProcessor(new GridProFactory());
    if (classExists("com.vaadin.flow.component.richtexteditor.RichTextEditor"))
      registerDefaultFactoryOrPostProcessor(new RichTextEditorFactory());
  }


  private static final List<TemplateResolver> DEFAULT_RESOLVERS = new ArrayList<>();
  static
  {
    DEFAULT_RESOLVERS.add(new ClasspathTemplateResolver());
  }

  private static final List<ComponentFactory> ADDITIONAL_FACTORIES = new ArrayList<>();

  private static final List<ComponentPostProcessor> ADDITIONAL_POST_PROCESSORS = new ArrayList<>();

  private static final List<TemplateResolver> ADDITIONAL_RESOLVERS = new ArrayList<>();

  /**
   * whether the applied factories and post processors are filtered based on their
   * {@link DuiComponent} annotations; not configurable right now; logic is kept in case cache
   * causes trouble
   *
   * @deprecated not sure if necessary; maybe assume always on and remove other path
   */
  @Deprecated
  private static final boolean FILTER_APPLICABLE = true;

  private static final Logger LOG = LoggerFactory.getLogger(TemplateEngine.class);


  private static CacheMode defaultCacheMode = CacheMode.AUTO;


  private final List<ComponentFactory> factories;

  private final List<ComponentPostProcessor> postProcessors;

  private final List<TemplateResolver> templateResolvers;


  private CacheMode cacheMode;

  private final ConcurrentMap<String, TemplateDocument> templateCache = new ConcurrentHashMap<>();

  private final ConcurrentMap<Class<?>, List<Class<?>>> hostClassHierarchyCache =
      new ConcurrentHashMap<>();

  private final ConcurrentMap<String, List<ComponentFactory>> applicableFactoriesCache =
      new ConcurrentHashMap<>();

  private final ConcurrentMap<Class<?>, List<ComponentPostProcessor>> applicablePostProcessorsCache =
      new ConcurrentHashMap<>();

  private List<ComponentDec> componentDeclarations;


  /**
   * Constructs a new instance.
   */
  public TemplateEngine()
  {
    this(null, null, null);
  }

  /**
   * Constructs a new instance with the provided component factories, component post-processors and
   * template resolves in addition to the default ones.
   *
   * @param additionalFactories
   *          the additional component factories
   * @param additionalPostProcessors
   *          the additional component post-processors
   * @param additionalResolvers
   *          the additional template resolvers
   */
  public TemplateEngine(List<ComponentFactory> additionalFactories,
      List<ComponentPostProcessor> additionalPostProcessors,
      List<TemplateResolver> additionalResolvers)
  {
    this.factories =
        additionalFactories == null ? new ArrayList<>() : new ArrayList<>(additionalFactories);
    this.factories.addAll(ADDITIONAL_FACTORIES);
    this.factories.addAll(DEFAULT_FACTORIES);

    this.postProcessors = additionalPostProcessors == null ? new ArrayList<>()
        : new ArrayList<>(additionalPostProcessors);
    this.postProcessors.addAll(ADDITIONAL_POST_PROCESSORS);
    this.postProcessors.addAll(DEFAULT_POST_PROCESSORS);

    this.templateResolvers =
        additionalResolvers == null ? new ArrayList<>() : new ArrayList<>(additionalResolvers);
    this.templateResolvers.addAll(ADDITIONAL_RESOLVERS);
    this.templateResolvers.addAll(DEFAULT_RESOLVERS);

    cacheMode = defaultCacheMode;
  }


  /**
   * Returns the cache mode.
   *
   * @return the cache mode
   */
  public CacheMode getCacheMode()
  {
    return cacheMode;
  }

  /**
   * Sets the cache mode.
   *
   * @param cacheMode
   *          the new cache mode
   */
  public void setCacheMode(CacheMode cacheMode)
  {
    requireNonNull(cacheMode, "cacheMode must not be null.");
    this.cacheMode = cacheMode;
  }


  private static void registerDefaultFactoryOrPostProcessor(Object factory)
  {
    if (factory instanceof ComponentFactory)
      DEFAULT_FACTORIES.add((ComponentFactory) factory);

    if (factory instanceof ComponentPostProcessor)
      DEFAULT_POST_PROCESSORS.add((ComponentPostProcessor) factory);
  }

  /**
   * Returns the list of additional {@link ComponentFactory component factories}. This list can be
   * edited to extend the list of supported components.
   * <p>
   * <em>Please note that the Spring Boot integration provides a better way to do this by simply
   * declaring them as Spring beans or by registering them via a {@link TemplateProcessorRegistrar}
   * bean.</em>
   *
   * @return the list of additional {@link ComponentFactory component factories}
   */
  public static List<ComponentFactory> getAdditionalFactories()
  {
    return ADDITIONAL_FACTORIES;
  }

  /**
   * Returns the list of additional {@link ComponentPostProcessor component post processors}. This
   * list can be edited to extend the list of supported attributes.
   * <p>
   * <em>Please note that the Spring Boot integration provides a better way to do this by simply
   * declaring them as Spring beans or by registering them via a {@link TemplateProcessorRegistrar}
   * bean.</em>
   *
   * @return the list of additional {@link ComponentPostProcessor component post processors}
   */
  public static List<ComponentPostProcessor> getAdditionalPostProcessors()
  {
    return ADDITIONAL_POST_PROCESSORS;
  }

  /**
   * Returns the list of additional {@link TemplateResolver template resolvers}. This list can be
   * edited to extend the list of supported attributes.
   * <p>
   * <em>Please note that the Spring Boot integration provides a better way to do this by simply
   * declaring them as Spring beans or by registering them via a {@link TemplateProcessorRegistrar}
   * bean.</em>
   *
   * @return the list of additional {@link TemplateResolver template resolvers}
   */
  public static List<TemplateResolver> getAdditionalResolvers()
  {
    return ADDITIONAL_RESOLVERS;
  }

  /**
   * Sets the default cache mode for new {@link TemplateEngine} instances.
   * <p>
   * <em> Please note that the Spring Boot integration provides a better way to set this via the
   * {@code codecamp.vaadin.dui.cache-mode} application property.</em>
   *
   * @param cacheMode
   *          the default cache mode
   */
  public static void setDefaultCacheMode(CacheMode cacheMode)
  {
    requireNonNull(cacheMode, "CacheMode must not be null.");
    defaultCacheMode = cacheMode;
  }


  /**
   * Returns a {@link TemplateEngine}. This method requires the {@link VaadinService#getCurrent()}
   * to be available.
   *
   * @return a {@link TemplateEngine}
   */
  public static TemplateEngine get()
  {
    return VaadinService.getCurrent().getInstantiator().getOrCreate(TemplateEngine.class);
  }


  @Override
  public void addFactory(ComponentFactory factory)
  {
    factories.add(requireNonNull(factory, "factory must not be null"));

    applicableFactoriesCache.clear();
    componentDeclarations = null;
  }

  @Override
  public List<ComponentFactory> getFactories()
  {
    return Collections.unmodifiableList(factories);
  }

  @Override
  public List<ComponentFactory> getFactoriesFor(String tagName)
  {
    if (FILTER_APPLICABLE)
    {
      if (shouldUseCache())
      {
        return applicableFactoriesCache.computeIfAbsent(tagName,
            this::determineApplicableFactories);
      }
      else
      {
        return determineApplicableFactories(tagName);
      }
    }
    else
    {
      return getFactories();
    }
  }

  private List<ComponentFactory> determineApplicableFactories(String tagName)
  {
    return factories.stream().filter(f ->
    {
      DuiComponent[] duiComponentAts = f.getClass().getAnnotationsByType(DuiComponent.class);
      if (duiComponentAts.length == 0)
        return true;
      for (DuiComponent dc : duiComponentAts)
      {
        if (dc.tagName().equals(tagName) || dc.tagName().equals(DuiComponent.TAG_ALL))
          return true;
      }
      return false;
    }).collect(toUnmodifiableList());
  }


  @Override
  public void addPostProcessor(ComponentPostProcessor postProcessor)
  {
    postProcessors.add(requireNonNull(postProcessor, "postProcessor must not be null"));

    applicablePostProcessorsCache.clear();
    componentDeclarations = null;
  }

  @Override
  public List<ComponentPostProcessor> getPostProcessors()
  {
    return Collections.unmodifiableList(postProcessors);
  }

  @Override
  public List<ComponentPostProcessor> getPostProcessorsFor(Class<? extends Component> componentType)
  {
    if (FILTER_APPLICABLE)
    {
      if (shouldUseCache())
      {
        return applicablePostProcessorsCache.computeIfAbsent(componentType,
            this::determineApplicablePostProcessors);
      }
      else
      {
        return determineApplicablePostProcessors(componentType);
      }
    }
    else
    {
      return getPostProcessors();
    }
  }

  private List<ComponentPostProcessor> determineApplicablePostProcessors(Class<?> componentType)
  {
    return postProcessors.stream().filter(f ->
    {
      DuiComponent[] duiComponentAts = f.getClass().getAnnotationsByType(DuiComponent.class);
      if (duiComponentAts.length == 0)
        return true;
      for (DuiComponent dc : duiComponentAts)
      {
        if (dc.componentType().isAssignableFrom(componentType))
          return true;
      }
      return false;
    }).collect(toUnmodifiableList());
  }


  @Override
  public void addTemplateResolver(TemplateResolver templateResolver)
  {
    templateResolvers.add(requireNonNull(templateResolver, "templateResolver must not be null"));
  }

  @Override
  public List<TemplateResolver> getTemplateResolvers()
  {
    return Collections.unmodifiableList(templateResolvers);
  }


  /**
   * Returns all {@link DuiComponent component declarations} found on component factories or
   * post-processors.
   *
   * @return all component declarations found on component factories or post-processors
   * @see DuiComponent
   */
  public List<ComponentDec> getComponentDeclarations()
  {
    if (componentDeclarations == null)
    {
      Map<DuiComponent, Class<?>> declarations = new LinkedHashMap<>();

      getFactories()
          .forEach(factory -> getDeclarationAnnotations(factory.getClass(), declarations));
      getPostProcessors().forEach(
          postProcessor -> getDeclarationAnnotations(postProcessor.getClass(), declarations));

      componentDeclarations = declarations.entrySet().stream()
          .map(e -> new ComponentDec(e.getKey(), e.getValue())).collect(toList());
    }
    return componentDeclarations;
  }

  private void getDeclarationAnnotations(Class<?> factoryOrProcessorType,
      Map<DuiComponent, Class<?>> declarations)
  {
    Stream.of(factoryOrProcessorType.getAnnotationsByType(DuiComponent.class))
        .forEach(dc -> declarations.put(dc, factoryOrProcessorType));
  }


  /**
   * Builds the component tree based on the template for the given template host.
   * <p>
   * Components from the component tree can be {@link Mapped mapped} to fields of the host. Manually
   * created components in fields of the composite can be {@link Slotted slotted} into the component
   * tree.
   * <p>
   * The template ID is assumed to be the fully qualified name of the subclass, unless an explict ID
   * is provided via {@link Template}.
   *
   * @param templateHost
   *          used to determine template ID; target for the component mapping; the associated class
   *          loader may be used to load the template document
   * @return the root component of the created component tree
   * @throws TemplateException
   *           if the template could not be successfully instantiated
   */
  public Component instantiateTemplate(Object templateHost)
  {
    requireNonNull(templateHost, "templateHost must not be null");

    return parseTemplate(null, templateHost, null).getRootComponent();
  }

  /**
   * Builds the component tree based on the template for the given template ID. If the template ID
   * is {@code null} it will be determined from the template host. <em>So either {@code templateId}
   * or {@code templateHost} must be provided.</em>
   * <p>
   * Components are {@link Mapped mapped} and {@link Slotted slotted} on the given template host, if
   * provided.
   *
   * @param templateId
   *          the template ID; may be null to determine from template host
   * @param templateHost
   *          the template host; the associated class loader may be used to load the template
   *          document; may be null if {@code templateId} is provided
   * @param classLoader
   *          the class loader used to load resources from the classpath; may be null to use either
   *          the class loader of the template host (when provided) or the class loader of this
   *          class
   * @return the parsed template
   * @throws TemplateException
   *           if the template could not be successfully parsed
   */
  public ParsedTemplate parseTemplate(String templateId, Object templateHost,
      ClassLoader classLoader)
  {
    if (templateId == null && templateHost == null)
    {
      throw new IllegalArgumentException(
          "At least one of templateId or templateHost must be provided.");
    }

    return parseTemplate(templateId, templateHost, null, classLoader);
  }

  /**
   * Builds the component tree based on the template for the given template ID. If the template ID
   * is {@code null} it will be determined from the template host class. <em>So either
   * {@code templateId} or {@code templateHostClass} must be provided.</em>
   *
   * @param templateId
   *          the template ID; may be null to determine from template host
   * @param templateHostClass
   *          type of the template host; the associated class loader may be used to load the
   *          template document; may be null if {@code templateId} is provided
   * @param classLoader
   *          the class loader used to load resources from the classpath; may be null to use either
   *          the class loader of the template host (when provided) or the class loader of this
   *          class
   * @return the parsed template
   * @throws TemplateException
   *           if the template could not be successfully parsed
   */
  public ParsedTemplate parseTemplate(String templateId, Class<?> templateHostClass,
      ClassLoader classLoader)
  {
    if (templateId == null && templateHostClass == null)
    {
      throw new IllegalArgumentException(
          "At least one of templateId or templateHostClass must be provided.");
    }

    return parseTemplate(templateId, null, templateHostClass, classLoader);
  }

  private ParsedTemplate parseTemplate(String templateId, Object templateHost,
      Class<?> templateHostClass, ClassLoader classLoader)
  {
    if (templateHostClass == null && templateHost != null)
      templateHostClass = templateHost.getClass();

    if (templateId == null)
    {
      if (templateHost == null && templateHostClass == null)
      {
        throw new IllegalArgumentException(
            "At least one of templateId, templateHost or templateHostClass must be provided.");
      }

      templateId = getTemplateId(templateHostClass);
    }

    if (classLoader == null)
    {
      if (templateHostClass != null)
        classLoader = templateHostClass.getClassLoader();
      else
        classLoader = getClass().getClassLoader();
    }


    long start = System.currentTimeMillis();

    TemplateDocument templateDocument =
        getTemplateDocument(templateId, templateHostClass, null, classLoader);

    LOG.debug("Template document '{}' loaded in {}ms.", templateId,
        System.currentTimeMillis() - start);

    Map<String, Component> slottedComponents = null;
    if (templateHost != null)
    {
      slottedComponents =
          getSlottedComponents(templateHost, templateId, templateDocument.getSource());
    }

    ParsedTemplate parsedTemplate = parseTemplate(templateDocument, slottedComponents);

    if (templateHost != null)
    {
      mapComponents(templateHost, parsedTemplate);
    }

    LOG.debug("Template '{}' instantiated in {}ms.", templateId,
        System.currentTimeMillis() - start);

    return parsedTemplate;
  }

  /**
   * Builds the component tree based on the template fragment for the given fragment host.
   * <p>
   * Components from the component tree can be {@link Mapped mapped} to fields of the host. Manually
   * created components in fields of the host can be {@link Slotted slotted} into the component
   * tree.
   * <p>
   * To ensure that the corresponding template can be determined, the fragment host either needs to
   * be an inner class of the template host, have any enclosing class annotated with
   * {@link Template} or be themselves annotated with {@link Template}.
   * <p>
   * The fragment ID is assumed to be the simple class name of the fragment host, unless an explict
   * ID is provided via {@link Fragment}.
   *
   * @param fragmentHost
   *          used to determine template ID und fragment ID; target for the component mapping; the
   *          associated class loader may be used to load the template document
   * @return the root component of the created component tree
   * @throws TemplateException
   *           if the template could not be successfully instantiated
   */
  public Component instantiateTemplateFragment(Object fragmentHost)
  {
    requireNonNull(fragmentHost, "fragmentHost must not be null");

    return parseTemplateFragment(null, null, fragmentHost, null).getRootComponent();
  }

  /**
   * Builds the component tree based on the template for the given template ID and fragment ID. If
   * the template ID or fragment ID are {@code null} they will be determined from the fragment host.
   * <em>Either {@code templateId} and {@code fragmentId} or {@code templateHost} must be
   * provided.</em>
   * <p>
   * Components are {@link Mapped mapped} and {@link Slotted slotted} on the given fragment host, if
   * provided.
   *
   * @param templateId
   *          the template ID; may be null to determine from fragment host
   * @param fragmentId
   *          the fragment ID; may be null to determine from fragment host
   * @param fragmentHost
   *          the template host of the fragment; the associated class loader may be used to load the
   *          template document; may be null if {@code templateId} and {@code fragmentId} are
   *          provided
   * @param classLoader
   *          the class loader used to load resources from the classpath; may be null to use either
   *          the class loader of the fragment host (when provided) or the class loader of this
   *          class
   * @return the parsed template
   * @throws TemplateException
   *           if the template could not be successfully parsed
   */
  public ParsedTemplate parseTemplateFragment(String templateId, String fragmentId,
      Object fragmentHost, ClassLoader classLoader)
  {
    if ((templateId == null || fragmentId == null) && fragmentHost == null)
    {
      throw new IllegalArgumentException(
          "At least one of templateId + fragmentId or fragmentHost must be provided.");
    }

    return parseTemplateFragment(templateId, fragmentId, fragmentHost, null, classLoader);
  }

  /**
   * Builds the component tree based on the template for the given template ID and fragment ID. If
   * the template ID or fragment ID are {@code null} they will be determined from the fragment host.
   * <em>Either {@code templateId} and {@code fragmentId} or {@code templateHost} must be
   * provided.</em>
   *
   * @param templateId
   *          the template ID; may be null to determine from fragment host
   * @param fragmentId
   *          the fragment ID; may be null to determine from fragment host
   * @param fragmentHostClass
   *          type of the template host of the fragment; the associated class loader may be used to
   *          load the template document; may be null if {@code templateId} and {@code fragmentId}
   *          are provided
   * @param classLoader
   *          the class loader used to load resources from the classpath; may be null to use either
   *          the class loader of the fragment host (when provided) or the class loader of this
   *          class
   * @return the parsed template
   * @throws TemplateException
   *           if the template could not be successfully parsed
   */
  public ParsedTemplate parseTemplateFragment(String templateId, String fragmentId,
      Class<?> fragmentHostClass, ClassLoader classLoader)
  {
    if ((templateId == null || fragmentId == null) && fragmentHostClass == null)
    {
      throw new IllegalArgumentException(
          "At least one of templateId + fragmentId or fragmentHostClass must be provided.");
    }

    return parseTemplateFragment(templateId, fragmentId, null, fragmentHostClass, classLoader);
  }

  private ParsedTemplate parseTemplateFragment(String templateId, String fragmentId,
      Object fragmentHost, Class<?> fragmentHostClass, ClassLoader classLoader)
  {
    if (fragmentHostClass == null && fragmentHost != null)
      fragmentHostClass = fragmentHost.getClass();

    if (templateId == null || fragmentId == null)
    {
      if (fragmentHost == null && fragmentHostClass == null)
      {
        throw new IllegalArgumentException(
            "At least one of templateId + fragmentId, fragmentHost or fragmentHostClass must be provided.");
      }

      if (templateId == null)
        templateId = getTemplateIdForFragment(fragmentHostClass);
      if (fragmentId == null)
        fragmentId = getFragmentId(fragmentHostClass);
    }

    if (classLoader == null)
    {
      if (fragmentHostClass != null)
        classLoader = fragmentHostClass.getClassLoader();
      else
        classLoader = getClass().getClassLoader();
    }


    long start = System.currentTimeMillis();

    TemplateDocument templateDocument =
        getTemplateDocument(templateId, null, fragmentHostClass, classLoader);

    LOG.debug("Template document '{}' loaded in {}ms.", templateId,
        System.currentTimeMillis() - start);

    Elements fragmentSelect = templateDocument.getDocument().select("template#" + fragmentId);

    if (fragmentSelect.isEmpty())
      fragmentSelect = templateDocument.getDocument().select("fragment#" + fragmentId);

    if (fragmentSelect.isEmpty())
    {
      String msg = "No template fragment with ID '%s' found.";
      msg = String.format(msg, fragmentId);
      throw new TemplateException(templateId, templateDocument.getSource(), msg);
    }
    if (fragmentSelect.size() > 1)
    {
      String msg = "More than one template fragment with ID '%s' found.";
      msg = String.format(msg, fragmentId);
      throw new TemplateException(templateId, templateDocument.getSource(), msg);
    }

    Element templateFragment = fragmentSelect.first();

    Map<String, Component> slottedComponents = null;
    if (fragmentHost != null)
    {
      slottedComponents =
          getSlottedComponents(fragmentHost, templateId, templateDocument.getSource());
    }

    ParsedTemplate parsedTemplate =
        parseTemplateFragment(templateDocument, templateFragment, slottedComponents);

    if (fragmentHost != null)
    {
      mapComponents(fragmentHost, parsedTemplate);
    }

    LOG.debug("Template fragment '{}#{}' instantiated in {}ms.", templateId, fragmentId,
        System.currentTimeMillis() - start);

    return parsedTemplate;
  }


  public TemplateReport validateTemplate(Class<?> hostClass)
  {
    String templateId = getTemplateId(hostClass);

    ParsedTemplate parsedTemplate;
    try
    {
      if (isTemplateHost(hostClass))
      {
        parsedTemplate = parseTemplate(null, hostClass, null);
      }
      else if (isTemplateFragmentHost(hostClass))
      {
        parsedTemplate = parseTemplateFragment(null, null, hostClass, null);
      }
      else
      {
        TemplateReport report = new TemplateReport();
        report.addError("Class does not seem to be a template or fragment host.'.");
        return report;
      }
    }
    catch (TemplateException ex)
    {
      TemplateReport report = new TemplateReport();
      report.addError("Failed to instantiate template for ID '%s'.", templateId, ex);
      return report;
    }

    TemplateReport report = parsedTemplate.getReport();

    findMappedFields(hostClass, (field, componentId) ->
    {
      Component component = parsedTemplate.getComponentById(componentId);
      if (component == null)
      {
        report.addError("No component found with ID '%s' for field %s.", componentId, field);
        return;
      }

      if (!field.getType().isAssignableFrom(component.getClass()))
      {
        report.addError("The type '%s' of component '%s' does not match the field '%s'.",
            component.getClass().getName(), componentId, field);
      }
    });

    Set<String> availableSlots = new HashSet<>(parsedTemplate.getSlotNames());
    Set<String> definitelyOccupiedSlots = new HashSet<>();
    findSlottedFields(hostClass, (field, slotName, optional) ->
    {
      if (!availableSlots.contains(slotName))
      {
        report.addError("Template does not contain slot '%s'.", slotName);
      }
      else if (definitelyOccupiedSlots.contains(slotName))
      {
        report.addError("Template slot '%s' occupied by more than one component.", slotName);
      }

      if (!optional)
        definitelyOccupiedSlots.add(slotName);
    });

    return report;
  }

  /**
   * Parses the provided template document. Slots will be validated and replaced by their slotted
   * components, if the corresponding map is provided.
   *
   * @param templateDocument
   *          the template document
   * @param slottedComponents
   *          a map of available slotted components; null to skip skip slotting
   * @return the parsed template
   * @throws TemplateException
   *           if the template could not be successfully parsed
   */
  protected ParsedTemplate parseTemplate(TemplateDocument templateDocument,
      Map<String, Component> slottedComponents)
  {
    TemplateParserContext context = new TemplateParserContext(templateDocument.getTemplateId(),
        templateDocument.getSource(), slottedComponents, this);

    context.parseTemplate(templateDocument);

    if (slottedComponents != null)
    {
      Set<String> leftoverSlottedComponents = new HashSet<>(slottedComponents.keySet());
      leftoverSlottedComponents.removeAll(context.getAvailableSlots());

      if (!leftoverSlottedComponents.isEmpty())
      {
        if (LOG.isErrorEnabled())
        {
          String msg = "Slotted components found that are not referenced in the template: {}";
          msg = addLocationToMessage(templateDocument.getTemplateId(), templateDocument.getSource(),
              null, null, msg);
          LOG.error(msg, leftoverSlottedComponents.stream().collect(joining(", ")));
        }
      }
    }

    return new ParsedTemplate(templateDocument.getTemplateId(), templateDocument.getSource(),
        context.getReport(), context.getRootComponent(), context.getComponentsById(),
        context.getSlotsByName(), context.getTemplateFragmentsById());
  }

  /**
   * Parses the given fragment element from the provided template document.
   *
   * @param templateDocument
   *          the template document
   * @param fragmentElement
   *          the fragment element
   * @param slottedComponents
   *          a map of available slotted components; null to skip skip slotting
   * @return the parsed fragment
   * @throws TemplateException
   *           if the fragment could not be successfully parsed
   */
  protected ParsedTemplate parseTemplateFragment(TemplateDocument templateDocument,
      Element fragmentElement, Map<String, Component> slottedComponents)
  {
    TemplateParserContext context = new TemplateParserContext(templateDocument.getTemplateId(),
        templateDocument.getSource(), slottedComponents, this);

    context.parseTemplateFragment(fragmentElement);

    if (slottedComponents != null)
    {
      Set<String> leftoverSlottedComponents = new HashSet<>(slottedComponents.keySet());
      leftoverSlottedComponents.removeAll(context.getAvailableSlots());

      if (!leftoverSlottedComponents.isEmpty())
      {
        if (LOG.isErrorEnabled())
        {
          String msg = "Slotted components found that are not referenced in the template: {}";
          msg = addLocationToMessage(templateDocument.getTemplateId(), templateDocument.getSource(),
              null, null, msg);
          LOG.error(msg, leftoverSlottedComponents.stream().collect(joining(", ")));
        }
      }
    }

    return new ParsedTemplate(templateDocument.getTemplateId(), templateDocument.getSource(),
        context.getReport(), context.getRootComponent(), context.getComponentsById(),
        context.getSlotsByName(), null);
  }

  private TemplateDocument getTemplateDocument(String templateId, Class<?> templateHostClass,
      Class<?> fragmentHostClass, ClassLoader classLoader)
  {
    requireNonNull(templateId, "templateId must not be null");
    requireNonNull(classLoader, "classLoader must not be null");

    TemplateDocument templateDocument;
    if (shouldUseCache())
    {
      templateDocument = templateCache.computeIfAbsent(templateId, tid ->
      {
        TemplateDocument tr =
            resolveTemplateDocument(tid, templateHostClass, fragmentHostClass, classLoader);
        tr.getDocument().outputSettings().prettyPrint(false);
        return tr;
      });

      /* Document is not immutable, so create a copy to be safe */
      templateDocument = new TemplateDocument(templateId, templateDocument.getDocument().clone(),
          templateDocument.getSource());
    }
    else
    {
      templateDocument =
          resolveTemplateDocument(templateId, templateHostClass, fragmentHostClass, classLoader);
      templateDocument.getDocument().outputSettings().prettyPrint(false);
    }
    return templateDocument;
  }

  private TemplateDocument resolveTemplateDocument(String templateId, Class<?> templateHostClass,
      Class<?> fragmentHostClass, ClassLoader classLoader)
  {
    TemplateDocument templateDocument = null;

    /* Look for @Template annotation on template host */
    if (templateHostClass != null)
    {
      templateDocument = getTemplate(templateId, templateHostClass);
    }

    if (templateDocument == null && fragmentHostClass != null)
    {
      templateDocument = getTemplateForFragment(templateId, fragmentHostClass);
    }

    if (templateDocument == null)
    {
      if (templateResolvers.isEmpty())
        throw new IllegalStateException("No TemplateResolvers registered.");

      for (TemplateResolver resolver : templateResolvers)
      {
        templateDocument = resolver.resolveTemplate(classLoader, templateId).orElse(null);
        if (templateDocument != null)
          break;
      }
    }

    if (templateDocument == null)
    {
      throw new TemplateException(templateId, null,
          "No document found for template ID '%s'.".formatted(templateId));
    }

    return templateDocument;
  }


  /**
   * @param templateId
   *          only for error messages
   * @param templateSourceDescription
   *          only for error messages
   */
  private Map<String, Component> getSlottedComponents(Object host, String templateId,
      String templateSourceDescription)
  {
    Map<String, Component> slottedComponents = new HashMap<>();

    findSlottedFields(host.getClass(), (field, slotName, optional) ->
    {
      Component component;
      try
      {
        component = (Component) ReflectTools.getJavaFieldValue(host, field);
      }
      catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException ex)
      {
        throw new TemplateException(templateId, templateSourceDescription,
            "Unable to get the value from the field " + field.getName() + " in "
                + host.getClass().getName() + ".",
            ex);
      }

      if (component != null)
      {
        if (slottedComponents.containsKey(slotName))
        {
          throw new TemplateException(templateId, templateSourceDescription,
              "Template slot '" + slotName + "' contains more than one component.");
        }
      }
      else if (!optional)
      {
        String msg = "No component set on field '%s' for slot '%s'.";
        msg = String.format(msg, field.getName(), slotName);
        throw new TemplateException(templateId, templateSourceDescription, msg);
      }

      slottedComponents.put(slotName, component);
    });

    return slottedComponents;
  }

  /**
   * Maps components from the given {@link ParsedTemplate} to the {@link Mapped}-annotated fields of
   * the given host.
   *
   * @param host
   *          the host object
   * @param template
   *          the parsed template
   * @see Mapped
   */
  private void mapComponents(Object host, ParsedTemplate template)
  {
    findMappedFields(host.getClass(), (field, componentId) ->
    {
      Component component = template.getComponentById(componentId);
      if (component == null)
      {
        String msg = "No component found with ID '%s' for field %s.";
        msg = String.format(msg, componentId, field);
        throw new TemplateException(template.getTemplateId(),
            template.getTemplateSourceDescription(), msg);
      }

      if (!field.getType().isAssignableFrom(component.getClass()))
      {
        String msg = "The type '%s' of component '%s' does not match field %s.";
        msg = String.format(msg, component.getClass().getName(), componentId, field);
        throw new TemplateException(template.getTemplateId(),
            template.getTemplateSourceDescription(), msg);
      }

      try
      {
        ReflectTools.setJavaFieldValue(host, field, component);
      }
      catch (IllegalArgumentException ex)
      {
        String msg = "Failed to assign component of type '%s' to field %s.";
        msg = String.format(msg, component.getClass().getName(), field);
        throw new TemplateException(template.getTemplateId(),
            template.getTemplateSourceDescription(), msg, ex);
      }
    });
  }

  private void findMappedFields(Class<?> hostClass, FindMappedHandler mappedHandler)
  {
    getHostFieldsStream(hostClass, Mapped.class).forEach(field ->
    {
      Mapped mappedAt = field.getAnnotation(Mapped.class);

      String componentId = mappedAt.value();

      if (componentId.isEmpty())
        componentId = field.getName();

      mappedHandler.accept(field, componentId);
    });
  }

  private void findSlottedFields(Class<?> hostClass, FindSlottedHandler slottedHandler)
  {
    getHostFieldsStream(hostClass, Slotted.class).forEach(field ->
    {
      Slotted slottedAt = field.getAnnotation(Slotted.class);

      String slotName = slottedAt.value();

      if (slotName.isEmpty())
        slotName = field.getName();

      slottedHandler.accept(field, slotName, slottedAt.optional());
    });
  }

  private Stream<Field> getHostFieldsStream(Class<?> hostClass,
      Class<? extends Annotation> requiredAnnotation)
  {
    Stream<Field> stream = getHostClassHierarchy(hostClass).stream()
        .flatMap(clazz -> Stream.of(clazz.getDeclaredFields()));
    if (requiredAnnotation != null)
    {
      stream = stream.filter(f -> f.isAnnotationPresent(requiredAnnotation));
    }
    return stream;
  }

  private List<Class<?>> getHostClassHierarchy(Class<?> hostClass)
  {
    if (shouldUseCache())
    {
      return hostClassHierarchyCache.computeIfAbsent(hostClass, this::computeHostClassHierarchy);
    }
    else
    {
      return computeHostClassHierarchy(hostClass);
    }
  }

  private List<Class<?>> computeHostClassHierarchy(Class<?> hostClass)
  {
    List<Class<?>> hierarchy = new ArrayList<>();

    Class<?> clazz = hostClass;
    while (clazz != null)
    {
      hierarchy.add(clazz);

      clazz = clazz.getSuperclass();

      /* try to exit early */
      if (clazz == Object.class || clazz == TemplateComposite.class
          || clazz == FragmentComposite.class)
      {
        break;
      }
    }

    return List.copyOf(hierarchy);
  }

  /**
   * Returns whether the given class is a template host based on type and annotations.
   *
   * @param clazz
   *          the potential template host
   * @return whether the given class is a template host
   */
  public boolean isTemplateHost(Class<?> clazz)
  {
    if (TemplateComposite.class.isAssignableFrom(clazz))
      return true;

    if (findAnnotation(clazz, Template.class, (annotatedClass, annotation) ->
    {
      return true;
    }) != null)
    {
      return true;
    }

    return false;
  }

  /**
   * Returns whether the given class is a fragment host based on type and annotations.
   *
   * @param clazz
   *          the potential fragment host
   * @return whether the given class is a fragment host
   */
  public boolean isTemplateFragmentHost(Class<?> clazz)
  {
    if (FragmentComposite.class.isAssignableFrom(clazz))
      return true;

    if (findAnnotation(clazz, Fragment.class, (annotatedClass, annotation) ->
    {
      return true;
    }) != null)
    {
      return true;
    }

    return false;
  }

  /**
   * Determines the template ID for the given template host type.
   *
   * @param templateHostClass
   *          the template host type
   * @return the template ID
   */
  private String getTemplateId(Class<?> templateHostClass)
  {
    String templateId = getTemplateIdNoDefault(templateHostClass);

    if (templateId == null)
      templateId = templateHostClass.getName();

    return templateId;
  }

  private String getTemplateIdNoDefault(Class<?> templateHostClass)
  {
    return findAnnotation(templateHostClass, Template.class, (annotatedClass, annotation) ->
    {
      String id = annotation.id();
      if (id.equals(""))
        id = annotatedClass.getName();
      return id;
    });
  }


  private TemplateDocument getTemplate(String templateId, Class<?> templateHostClass)
  {
    return findAnnotation(templateHostClass, Template.class, (annotatedClass, annotation) ->
    {
      String template = annotation.content();
      if (template.equals(""))
        return null;

      Document templateDocument = Jsoup.parseBodyFragment(template);
      return new TemplateDocument(templateId, templateDocument,
          "@Template on " + annotatedClass.getName());
    });
  }

  /**
   * Determines the template ID for the given fragment host type.
   *
   * @param fragmentHostClass
   *          the fragment host type
   * @return the template ID
   */
  private String getTemplateIdForFragment(Class<?> fragmentHostClass)
  {
    String templateId =
        findAnnotation(fragmentHostClass, Fragment.class, (annotatedClass, annotation) ->
        {
          String id = annotation.templateId();
          if (id.equals(""))
            id = null;
          return id;
        });

    /*
     * find an explicit template ID
     */
    if (templateId == null)
    {
      for (Class<?> clazz : getHostClassHierarchy(fragmentHostClass))
      {
        Class<?> enclosingClass = clazz.getEnclosingClass();
        if (enclosingClass == null)
          continue;

        templateId = getTemplateIdNoDefault(enclosingClass);
        if (templateId != null)
          break;
      }
    }

    /*
     * determine the closest default template ID
     */
    if (templateId == null)
    {
      for (Class<?> clazz : getHostClassHierarchy(fragmentHostClass))
      {
        Class<?> enclosingClass = clazz.getEnclosingClass();
        if (enclosingClass == null)
          continue;

        templateId = getTemplateId(enclosingClass);
        if (templateId != null)
          break;
      }
    }

    /* if everything else is implemented correctly there should be a template ID by now */
    if (templateId == null)
    {
      throw new IllegalStateException("Could not find any template ID for fragment host class '"
          + fragmentHostClass.getName() + "'.");
    }

    return templateId;
  }

  private TemplateDocument getTemplateForFragment(String templateId, Class<?> fragmentHostClass)
  {
    TemplateDocument templateDocument = null;

    for (Class<?> clazz : getHostClassHierarchy(fragmentHostClass))
    {
      Class<?> enclosingClass = clazz.getEnclosingClass();
      if (enclosingClass == null)
        continue;

      templateDocument = getTemplate(templateId, enclosingClass);
      if (templateDocument != null)
        break;
    }

    return templateDocument;
  }

  private String getFragmentId(Class<?> fragmentHostClass)
  {
    String fragmentId =
        findAnnotation(fragmentHostClass, Fragment.class, (annotatedClass, annotation) ->
        {
          String id = annotation.id();
          if (id.equals(""))
            id = annotatedClass.getSimpleName();
          return id;
        });

    if (fragmentId == null)
      fragmentId = fragmentHostClass.getSimpleName();

    return fragmentId;
  }

  private <AT extends Annotation, RESULT> RESULT findAnnotation(Class<?> hostClass,
      Class<AT> annotationType, BiFunction<Class<?>, AT, RESULT> processor)
  {
    AT annotation;
    for (Class<?> clazz : getHostClassHierarchy(hostClass))
    {
      annotation = clazz.getAnnotation(annotationType);
      if (annotation != null)
      {
        RESULT value = processor.apply(clazz, annotation);
        if (value != null)
          return value;
      }
    }
    return null;
  }

  private boolean shouldUseCache()
  {
    boolean useCache;
    switch (cacheMode)
    {
      case ALWAYS:
        useCache = true;
        break;

      case NEVER:
        useCache = false;
        break;

      case AUTO:
      default:
        boolean productionMode = Optional.ofNullable(VaadinService.getCurrent())
            .map(service -> service.getDeploymentConfiguration().isProductionMode()).orElse(false);
        useCache = productionMode;
        break;
    }
    return useCache;
  }

  /* package */ static String addLocationToMessage(String templateId,
      String templateSourceDescription, Element element, String attributeName, String message)
  {
    StringBuilder fullMessage = new StringBuilder(50);

    fullMessage.append(message).append("\n");

    fullMessage.append("Template:   ").append(templateId);

    if (templateSourceDescription != null)
      fullMessage.append(" from ").append(templateSourceDescription);

    if (element != null)
    {
      fullMessage.append("\n");

      if (element.hasAttr("id"))
      {
        fullMessage.append("Element ID: ").append(element.attr("id"));
      }
      else
      {
        Element elementClone = element.shallowClone();

        /*
         * A Document is required to turn off pretty printing. Clones do not have one. Formatting
         * without an owning Document will create (and throw away) a Document anyway.
         */
        Document document = new Document("");
        document.body().appendChild(elementClone);
        document.outputSettings().prettyPrint(false);

        if (element.childNodeSize() > 0)
          elementClone.appendChild(new Comment(" content omitted "));

        fullMessage.append("Element:    ").append(elementClone.outerHtml());
      }

      if (attributeName != null)
      {
        fullMessage.append("\n");
        fullMessage.append("Attribute:  ").append(attributeName);
      }
    }

    return fullMessage.toString();
  }


  private static boolean classExists(String className)
  {
    try
    {
      Class.forName(className);
      return true;
    }
    catch (@SuppressWarnings("unused") ClassNotFoundException ex)
    {
      return false;
    }
  }


  /**
   * The cache mode for templates.
   */
  public enum CacheMode
  {
    /**
     * Always cache loaded templates.
     */
    ALWAYS,

    /**
     * Never cache loaded templates.
     */
    NEVER,

    /**
     * Cache loaded templates when in production mode, otherwise don't.
     */
    AUTO
  }


  @FunctionalInterface
  private interface FindMappedHandler
  {

    void accept(Field field, String componentId);

  }

  @FunctionalInterface
  private interface FindSlottedHandler
  {

    void accept(Field field, String slotName, boolean optional);

  }

}
