package de.codecamp.vaadin.flowdui;


import static de.codecamp.vaadin.flowdui.TemplateEngine.addLocationToMessage;

import org.jsoup.nodes.Element;


public class TemplateException
  extends
    RuntimeException
{

  private final String templateId;

  private final String templateSourceDescription;


  public TemplateException(String templateId, String templateSourceDescription, String message)
  {
    super(addLocationToMessage(templateId, templateSourceDescription, null, null, message));
    this.templateId = templateId;
    this.templateSourceDescription = templateSourceDescription;
  }

  public TemplateException(String templateId, String templateSourceDescription, String message,
      Throwable cause)
  {
    super(addLocationToMessage(templateId, templateSourceDescription, null, null, message), cause);
    this.templateId = templateId;
    this.templateSourceDescription = templateSourceDescription;
  }

  public TemplateException(String templateId, String templateSourceDescription, Element element,
      String message)
  {
    super(addLocationToMessage(templateId, templateSourceDescription, element, null, message));
    this.templateId = templateId;
    this.templateSourceDescription = templateSourceDescription;
  }

  public TemplateException(String templateId, String templateSourceDescription, Element element,
      String message, Throwable cause)
  {
    super(addLocationToMessage(templateId, templateSourceDescription, element, null, message),
          cause);
    this.templateId = templateId;
    this.templateSourceDescription = templateSourceDescription;
  }

  public TemplateException(String templateId, String templateSourceDescription, Element element,
      String attributeName, String message)
  {
    super(addLocationToMessage(templateId, templateSourceDescription, element, attributeName,
        message));
    this.templateId = templateId;
    this.templateSourceDescription = templateSourceDescription;
  }

  public TemplateException(String templateId, String templateSourceDescription, Element element,
      String attributeName, String message, Throwable cause)
  {
    super(addLocationToMessage(templateId, templateSourceDescription, element, attributeName,
        message), cause);
    this.templateId = templateId;
    this.templateSourceDescription = templateSourceDescription;
  }


  public String getTemplateId()
  {
    return templateId;
  }

  public String getTemplateSourceDescription()
  {
    return templateSourceDescription;
  }

  @Override
  public synchronized TemplateException initCause(Throwable cause)
  {
    return (TemplateException) super.initCause(cause);
  }

}
