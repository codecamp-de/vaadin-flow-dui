package de.codecamp.vaadin.flowdui;


import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toSet;

import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.flowdui.autoconfigure.TemplateProcessorRegistry;
import de.codecamp.vaadin.flowdui.factories.TemplateElementsFactory;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.StreamSupport;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;


/**
 * Gathers the state while parsing a template and provides methods to parse attributes and child
 * components.
 */
public class TemplateParserContext
{

  private final String templateId;

  private final String templateSourceDescription;


  private final TemplateProcessorRegistry templateProcessorRegistry;


  private final Map<String, Component> slottedComponents;

  private final Set<String> availableSlots = new HashSet<>();


  private final TemplateReport report = new TemplateReport();

  private Component rootComponent;

  private final Map<String, Component> idToComponent = new HashMap<>();

  private final Map<String, TemplateSlot> nameToSlot = new HashMap<>();

  private final Map<String, Element> idToTemplateFragment = new HashMap<>();


  public TemplateParserContext(String templateId, String templateSourceDescription,
      Map<String, Component> slottedComponents, TemplateProcessorRegistry templateProcessorRegistry)
  {
    this.templateId = requireNonNull(templateId, "templateId must not be null");
    this.templateSourceDescription =
        requireNonNull(templateSourceDescription, "templateSourceDescription must not be null");
    this.slottedComponents =
        slottedComponents == null ? null : Collections.unmodifiableMap(slottedComponents);
    this.templateProcessorRegistry =
        requireNonNull(templateProcessorRegistry, "templateProcessorRegistry most not be null");
  }


  /**
   * Returns the ID of the template being parsed.
   *
   * @return the ID of the template being parsed
   */
  public String getTemplateId()
  {
    return templateId;
  }

  /**
   * Returns the source of the template. This is a textual representation useful for logging and
   * error messages.
   *
   * @return the source of the template
   */
  public String getTemplateSourceDescription()
  {
    return templateSourceDescription;
  }


  /**
   * Returns whether this context contains information about slotted components.
   *
   * @return whether this context contains information about slotted components
   */
  public boolean hasSlottedComponentsInfo()
  {
    return slottedComponents != null;
  }

  /**
   * Returns the component for the specified slot, if available. The slot is
   * {@link #registerAvailableSlot(String) automatically registered}.
   *
   * @param slotName
   *          the slot name
   * @return the optional component for the specified slot; <b>{@code null} if no {@link Slotted}
   *         annotation is actually referencing this slot</b>
   */
  public Optional<Component> getComponentInSlot(String slotName)
  {
    if (slottedComponents == null)
    {
      throw new IllegalStateException(
          "No slottedComponents provided. Use hasSlottedComponents() beforehand.");
    }

    if (!slottedComponents.containsKey(slotName))
      return null;

    registerAvailableSlot(slotName);

    return Optional.ofNullable(slottedComponents.get(slotName));
  }

  /**
   * Registers a slot available in a template. {@link #getComponentInSlot(String)} will already
   * implicitly do this.
   *
   * @param slotName
   *          the slot name
   */
  public void registerAvailableSlot(String slotName)
  {
    availableSlots.add(slotName);
  }

  /**
   * Returns the template slots encountered while parsing the template. I.e. this set will only be
   * complete after successfully parsing the template.
   *
   * @return the template slots encountered while parsing the template
   */
  public Set<String> getAvailableSlots()
  {
    return availableSlots;
  }



  public void parseTemplate(TemplateDocument templateDocument)
  {
    Element body = templateDocument.getDocument().body();

    AtomicBoolean hasNonBlankTextNodes = new AtomicBoolean(false);
    doReadChildren(null, body, (slotName, childElement) ->
    {
      switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
      {
        case ComponentFactory.SLOT_DEFAULT:
          if (getRootComponent() != null)
          {
            String msg = "The template '%s' must have exactly one single single root component, but"
                + " found more than one.";
            msg = String.format(msg, templateId);
            throw fail(msg);
          }

          setRootComponent(readComponent(childElement));
          return true;
      }
      return false;
    }, null, textNode ->
    {
      if (!textNode.text().isBlank())
        hasNonBlankTextNodes.set(true);
    });

    if (getRootComponent() == null)
    {
      String msg =
          "The template '%s' must have exactly one single root component, but found" + " none.";
      msg = String.format(msg, templateId);
      throw fail(msg);
    }

    if (hasNonBlankTextNodes.get())
    {
      reportWarning(
          "The template '%s' contains text around its root component that will be ignored.",
          templateId);
    }
  }

  public void parseTemplateFragment(Element fragmentElement)
  {
    doReadChildren(null, fragmentElement, (slotName, childElement) ->
    {
      switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
      {
        case ComponentFactory.SLOT_DEFAULT:
          if (getRootComponent() != null)
          {
            throw fail("The template fragment must have a single root element.");
          }

          setRootComponent(readComponent(childElement));
          return true;
      }
      return false;
    }, null, null);
  }


  /**
   * Creates a component based on the given HTML element.
   *
   * @param element
   *          the HTML to create the component from
   * @return the new component
   */
  public Component readComponent(Element element)
  {
    return readComponent(element, null);
  }

  /**
   * Creates a component based on the given HTML element.
   * <p>
   * An optional {@link ComponentProcessor} may be provided which is called after the component has
   * been created, but before any {@link ComponentPostProcessor post-processors} are called. This is
   * especially interesting for the factories of layout containers that need to consume attributes
   * that are placed on child components. It allows to consume them before they will be rejected as
   * unconsumed or before post-processors consume them.
   *
   * @param element
   *          the HTML to create the component from
   * @param componentProcessor
   *          an optional component processor that will be called after creating the component but
   *          before calling any other component post-processors; may be null
   * @return the new component
   */
  public Component readComponent(Element element, ComponentProcessor componentProcessor)
  {
    if (!element.tag().isKnownTag() && element.tag().isSelfClosing())
    {
      String msg = "It seems the tag '%1$s' has been used as self-closing at least once in the"
          + " template which is not support for (Web) Components and may lead to hard to diagnose"
          + " problems. Please use <%1$s></%1$s> instead of <%1$s/>.";
      msg = TemplateEngine.addLocationToMessage(templateId, templateSourceDescription, element,
          null, msg);
      reportWarning(msg, element.tagName());
    }

    Component component = null;
    ElementParserContext elementContext = new ElementParserContext(this, element);

    for (ComponentFactory f : templateProcessorRegistry.getFactoriesFor(element.tagName()))
    {
      component = f.createComponent(element.tagName(), elementContext);
      if (component != null)
        break;
    }
    if (component == null)
      throw fail(element, "Unknown element: " + element.tagName());

    if (componentProcessor != null)
    {
      componentProcessor.process(component, elementContext);
    }

    for (ComponentPostProcessor postProcessor : templateProcessorRegistry
        .getPostProcessorsFor(component.getClass()))
    {
      postProcessor.postProcessComponent(component, elementContext);
    }

    registerComponent(component);


    Set<String> leftoverAttributes = StreamSupport.stream(element.attributes().spliterator(), false)
        .map(Attribute::getKey).collect(toSet());
    leftoverAttributes.removeAll(elementContext.getConsumedAttributes());

    if (!leftoverAttributes.isEmpty())
    {
      String msg = "Unused attributes found: %s";
      msg = TemplateEngine.addLocationToMessage(templateId, templateSourceDescription, element,
          null, msg);
      reportError(msg, leftoverAttributes.stream().collect(joining(", ")));
    }

    return component;
  }

  /**
   * Creates a component based on the given HTML element. Same as
   * {@link #readComponent(Element, ComponentProcessor)} except that the slot-attribute is
   * automatically marked as consumed.
   *
   * @param element
   *          the element for which to create a component
   * @return the created component
   */
  public Component readComponentForSlot(Element element)
  {
    return readComponentForSlot(element, null);
  }

  /**
   * Creates a component based on the given HTML element. Same as
   * {@link #readComponent(Element, ComponentProcessor)} except that the slot-attribute is
   * automatically marked as consumed.
   *
   * @param element
   *          the element for which to create a component
   * @param componentProcessor
   *          an optional component processor that will be called after creating the component but
   *          before calling any other component post-processors; may be null
   * @return the created component
   */
  public Component readComponentForSlot(Element element, ComponentProcessor componentProcessor)
  {
    return readComponent(element, (childComponent, context) ->
    {
      context.getConsumedAttributes().add(TemplateElementsFactory.ATTR_SLOT);
      if (componentProcessor != null)
        componentProcessor.process(childComponent, context);
    });
  }


  /**
   * Reads the child nodes of the given {@link Element element} one by one. The child {@link Element
   * elements} are passed to the provided {@link ChildElementHandler} and any interested
   * {@link ComponentPostProcessor#handleChildElement(Component, String, Element, TemplateParserContext)
   * post-processors}. Encountered {@link TextNode text nodes} are passed to the
   * {@link TextNodeHandler}.
   * <p>
   * <b>Even if no child nodes are supported, it's still advised to call any variant of
   * {@code readChildren(...)} in order to trigger appropriate errors; otherwise these nodes will
   * simply be ignored.</b>
   *
   * @param parentComponent
   *          the parent component; may be null
   * @param parentElement
   *          the HTML element of the parent component
   * @param childElementHandler
   *          a child element handler; may be null to reject all child elements, post-processors may
   *          still handle them though
   * @param textNodeHandler
   *          a text node handler; may be null to reject all text nodes
   */
  public void readChildren(Component parentComponent, Element parentElement,
      ChildElementHandler childElementHandler, TextNodeHandler textNodeHandler)
  {
    doReadChildren(parentComponent, parentElement, childElementHandler, null, textNodeHandler);
  }

  /**
   * Reads the child nodes of the given {@link Element element} one by one. This method does not
   * accept a {@link ChildElementHandler}, but
   * {@link ComponentPostProcessor#handleChildElement(Component, String, Element, TemplateParserContext)
   * post-processors} are still able to handle child elements.
   * <p>
   * <b>Even if no child nodes are supported, it's still advised to call any variant of
   * {@code readChildren(...)} in order to trigger appropriate errors; otherwise these nodes will
   * simply be ignored.</b>
   *
   * @param parentComponent
   *          the parent component
   * @param parentElement
   *          the HTML element whose child nodes will be read. This is usually the element
   *          corresponding to the parent component, but not necessarily.
   */
  public void readChildren(Component parentComponent, Element parentElement)
  {
    readChildren(parentComponent, parentElement, null, null);
  }

  /**
   * Reads the child nodes of the given {@link Element element} one by one. This method does not
   * accept a {@link ChildElementHandler}, but
   * {@link ComponentPostProcessor#handleChildElement(Component, String, Element, TemplateParserContext)
   * post-processors} are still able to handle child elements.
   * <p>
   * <b>Even if no child nodes are supported, it's still advised to call any variant of
   * {@code readChildren(...)} in order to trigger appropriate errors; otherwise these nodes will
   * simply be ignored.</b>
   *
   * @param parentComponent
   *          the parent component
   * @param parentElement
   *          the HTML element whose child nodes will be read. This is usually the element
   *          corresponding to the parent component, but not necessarily.
   * @param unhandledChildMessage
   *          the error message to show when there are unhandled child elements
   */
  public void readChildren(Component parentComponent, Element parentElement,
      String unhandledChildMessage)
  {
    requireNonNull(parentComponent, "parentComponent must not be null");

    doReadChildren(parentComponent, parentElement, null, unhandledChildMessage, null);
  }

  private void doReadChildren(Component parentComponent, Element parentElement,
      ChildElementHandler childElementHandler, String unhandledChildMessage,
      TextNodeHandler textNodeHandler)
  {
    requireNonNull(parentElement, "parentElement must not be null");

    Set<String> consumedSlot = new HashSet<>();

    for (Node node : parentElement.childNodes())
    {
      if (node instanceof Element)
      {
        Element childElement = (Element) node;

        if (childElement.tagName().equals(TemplateElementsFactory.TAG_FRAGMENT)
            || childElement.tagName().equals("template"))
        {
          registerTemplateFragment(childElement);
          continue;
        }


        String slotName = "";
        if (childElement.hasAttr(TemplateElementsFactory.ATTR_SLOT))
        {
          slotName = childElement.attr(TemplateElementsFactory.ATTR_SLOT);

          /*
           * Remove 'slot' attribute from the element since the slot name is passed as separate
           * parameter anyway.
           */
          childElement.removeAttr(TemplateElementsFactory.ATTR_SLOT);

          if (consumedSlot.contains(slotName))
          {
            String msg = "Slot '%s' already filled.";
            msg = String.format(msg, slotName);
            throw fail(parentElement, msg);
          }
        }

        boolean handled =
            childElementHandler != null && childElementHandler.handle(slotName, childElement);

        if (!handled && parentComponent != null)
        {
          for (ComponentPostProcessor postProcessor : templateProcessorRegistry
              .getPostProcessorsFor(parentComponent.getClass()))
          {
            handled =
                postProcessor.handleChildElement(parentComponent, slotName, childElement, this);
            if (handled)
              break;
          }
        }

        if (!handled)
        {
          if (ComponentFactory.SLOT_DEFAULT.equals(slotName))
          {
            String msg = unhandledChildMessage != null ? unhandledChildMessage
                : "Component cannot have normal child components. The unnamed default slot is not"
                    + " supported here.";
            throw fail(parentElement, msg);
          }
          else
          {
            throw fail(parentElement, "Unknown slot: " + slotName);
          }
        }
      }

      else if (node instanceof TextNode)
      {
        if (textNodeHandler != null)
          textNodeHandler.handle((TextNode) node);
        else if (!((TextNode) node).text().isBlank())
          throw fail(parentElement, "No child text supported.");
      }
    }
  }


  /**
   * Starts mapping the given attribute. By default, attributes that are already consumed will be
   * skipped.
   *
   * @param element
   *          the element from which to read the attribute
   * @param attributeName
   *          the attribute name to be mapped
   * @param consumedAttributes
   *          a set containing the consumed attributes
   * @return the attribute source providing the next step in the mapping
   */
  public AttributeSource mapAttribute(Element element, String attributeName,
      Set<String> consumedAttributes)
  {
    requireNonNull(attributeName, "attributeName must not be null");
    return mapAttribute(element, attributeName, null, consumedAttributes);
  }

  /**
   * Starts mapping the given attribute. By default, attributes that are already consumed will be
   * skipped.
   *
   * @param element
   *          the element from which to read the attribute
   * @param attributeName
   *          the attribute name to be mapped; may be null
   * @param deprecatedAttributeNames
   *          the deprecated or old names for this attribute; may be null
   * @param consumedAttributes
   *          a set containing the consumed attributes
   * @return the attribute source providing the next step in the mapping
   */
  public AttributeSource mapAttribute(Element element, String attributeName,
      Set<String> deprecatedAttributeNames, Set<String> consumedAttributes)
  {
    return mapAttribute(element, attributeName == null ? null : List.of(attributeName),
        deprecatedAttributeNames, consumedAttributes);
  }

  /**
   * Starts mapping the given attribute. By default, attributes that are already consumed will be
   * skipped.
   *
   * @param element
   *          the element from which to read the attribute
   * @param attributeNames
   *          the attribute names to be mapped (one primary and zero or more synonyms); may be null
   * @param deprecatedAttributeNames
   *          the deprecated or old names for this attribute; may be null
   * @param consumedAttributes
   *          a set containing the consumed attributes
   * @return the attribute source providing the next step in the mapping
   */
  public AttributeSource mapAttribute(Element element, List<String> attributeNames,
      Set<String> deprecatedAttributeNames, Set<String> consumedAttributes)
  {
    if ((attributeNames == null || attributeNames.isEmpty())
        && (deprecatedAttributeNames == null || deprecatedAttributeNames.isEmpty()))
    {
      throw new IllegalArgumentException("No active or deprecated attribute names provided.");
    }

    String foundAttrName = null;
    String rawValue = null;
    boolean consumed = false;
    if (attributeNames != null && !attributeNames.isEmpty())
    {
      for (String attributeName : attributeNames)
      {
        if (element.hasAttr(attributeName))
        {
          if (rawValue == null)
          {
            consumed = consumedAttributes != null && !consumedAttributes.add(attributeName);
            rawValue = element.attr(attributeName);
            foundAttrName = attributeName;
          }
          else
          {
            String msg =
                "The attribute names %s are synonymous, so only one of them should be used.";
            msg = TemplateEngine.addLocationToMessage(templateId, templateSourceDescription,
                element, attributeName, msg);
            reportWarning(msg, attributeNames);
          }
        }
      }
    }

    if (deprecatedAttributeNames != null)
    {
      for (String deprecatedAttributeName : deprecatedAttributeNames)
      {
        if (element.hasAttr(deprecatedAttributeName))
        {
          if (attributeNames == null || attributeNames.isEmpty())
          {
            String msg = "The attribute '%s' is deprecated. You should remove it.";
            msg = TemplateEngine.addLocationToMessage(templateId, templateSourceDescription,
                element, deprecatedAttributeName, msg);
            reportWarning(msg, deprecatedAttributeName);
          }
          else
          {
            String msg = "The attribute '%s' is deprecated. You should replace it with '%s'.";
            msg = TemplateEngine.addLocationToMessage(templateId, templateSourceDescription,
                element, deprecatedAttributeName, msg);
            reportWarning(msg, deprecatedAttributeName, attributeNames.get(0));
          }

          consumed = consumedAttributes != null && !consumedAttributes.add(deprecatedAttributeName);

          if (rawValue == null)
          {
            rawValue = element.attr(deprecatedAttributeName);
            foundAttrName = deprecatedAttributeName;
          }
          else
          {
            String msg =
                "The attribute names %s and the deprecated names %s are synonymous, so only one of them should be used.";
            msg = TemplateEngine.addLocationToMessage(templateId, templateSourceDescription,
                element, deprecatedAttributeName, msg);
            reportWarning(msg, attributeNames, attributeNames, deprecatedAttributeNames);
          }
        }
      }
    }

    String effectiveAttrName;
    if (foundAttrName != null)
      effectiveAttrName = foundAttrName;
    else if (attributeNames != null && !attributeNames.isEmpty())
      effectiveAttrName = attributeNames.get(0);
    else if (deprecatedAttributeNames != null && !deprecatedAttributeNames.isEmpty())
      effectiveAttrName = deprecatedAttributeNames.iterator().next();
    else
      effectiveAttrName = null;

    return new AttributeSource(effectiveAttrName, rawValue, consumed, (message, cause) ->
    {
      return fail(element, effectiveAttrName, message, cause);
    });
  }


  /**
   * Copies the given attribute over to the component's {@link com.vaadin.flow.dom.Element} as it
   * is, unless the attribute is already consumed.
   *
   * @param element
   *          the source element
   * @param attributeName
   *          the attribute to copy
   * @param targetComponent
   *          the target component
   * @param consumedAttributes
   *          the consumed attributes
   */
  public void copyAttributeTo(Element element, String attributeName, Component targetComponent,
      Set<String> consumedAttributes)
  {
    mapAttribute(element, attributeName, consumedAttributes).asString()
        .to(v -> targetComponent.getElement().setAttribute(attributeName, v));
  }

  /**
   * Copy all remaining unconsumed attributes over to the component's {@link Element} as they are.
   *
   * @param element
   *          the source element
   * @param targetComponent
   *          the target component
   * @param consumedAttributes
   *          the consumed attributes
   */
  public void copyAttributesTo(Element element, Component targetComponent,
      Set<String> consumedAttributes)
  {
    for (Attribute attr : element.attributes())
    {
      if (consumedAttributes.contains(attr.getKey()))
        continue;

      if (element.hasAttr(attr.getKey()))
      {
        targetComponent.getElement().setAttribute(attr.getKey(), element.attr(attr.getKey()));
        consumedAttributes.add(attr.getKey());
      }
    }
  }


  /**
   * Returns the template report.
   *
   * @return the template report
   */
  public TemplateReport getReport()
  {
    return report;
  }


  /**
   * Returns the parsed root component of the template.
   *
   * @return the parsed root component of the template
   */
  public Component getRootComponent()
  {
    return rootComponent;
  }

  /**
   * Sets the parsed root component of the template.
   *
   * @param rootComponent
   *          the parsed root component of the template
   */
  public void setRootComponent(Component rootComponent)
  {
    this.rootComponent = rootComponent;
  }

  /**
   * Returns all parsed components in a {@link Map} with the {@link Component#getId()} as key.
   *
   * @return all parsed components by ID
   */
  public Map<String, Component> getComponentsById()
  {
    return idToComponent;
  }

  /**
   * Returns all parsed slots in a {@link Map} with the {@link TemplateSlot#getName()} as key.
   *
   * @return all parsed slots by name
   */
  public Map<String, TemplateSlot> getSlotsByName()
  {
    return nameToSlot;
  }

  /**
   * Returns all encountered template fragments in a {@link Map} with their ID as key.
   *
   * @return all encountered template fragments by ID
   */
  public Map<String, Element> getTemplateFragmentsById()
  {
    return idToTemplateFragment;
  }


  private void registerComponent(Component component)
  {
    component.getId().ifPresent(id ->
    {
      if (idToComponent.containsKey(id))
      {
        String msg = "The ID '%s' is already used.";
        msg = String.format(msg, id);
        throw fail(msg);
      }
      idToComponent.put(id, component);
    });

    if (component instanceof TemplateSlot)
    {
      TemplateSlot slot = (TemplateSlot) component;
      if (nameToSlot.containsKey(slot.getName()))
      {
        String msg = "The slot name '%s' is already used.";
        msg = String.format(msg, slot.getName());
        throw fail(msg);
      }

      nameToSlot.put(slot.getName(), slot);
    }
  }

  private void registerTemplateFragment(Element htmlTemplateElement)
  {
    String id = htmlTemplateElement.id();
    if (id.isEmpty())
    {
      throw fail(htmlTemplateElement, "The template fragment is missing an ID.");
    }

    idToTemplateFragment.put(id, htmlTemplateElement);
  }


  private TemplateException fail(String message)
  {
    throw new TemplateException(templateId, templateSourceDescription, message);
  }

  /**
   * Fails the current parsing process by throwing a {@link TemplateException}.
   * <p>
   * Despite the return type being {@link TemplateException} this method never actually returns
   * anything as it always throws the {@link TemplateException}. This is essentially to allow the
   * convenient pattern <b>{@code throw context.fail(...);}</b> because the compiler otherwise
   * wouldn't know that code below a call to this method is never executed.
   *
   * @param element
   *          the element the problem occurred on
   * @param message
   *          the error message
   * @return nothing, as this method never returns regularly
   * @throws TemplateException
   *           always
   */
  public TemplateException fail(Element element, String message)
  {
    throw new TemplateException(templateId, templateSourceDescription, element, message);
  }

  /**
   * Fails the current parsing process by throwing a {@link TemplateException}.
   * <p>
   * Despite the return type being {@link TemplateException} this method never actually returns
   * anything as it always throws the {@link TemplateException}. This is essentially to allow the
   * convenient pattern <b>{@code throw context.fail(...);}</b> because the compiler otherwise
   * wouldn't know that code below a call to this method is never executed.
   *
   * @param element
   *          the element the problem occurred on
   * @param message
   *          the error message
   * @param cause
   *          the cause of the failure
   * @return nothing, as this method never returns regularly
   * @throws TemplateException
   *           always
   */
  public TemplateException fail(Element element, String message, Throwable cause)
  {
    throw new TemplateException(templateId, templateSourceDescription, element, message)
        .initCause(cause);
  }

  /**
   * Fails the current parsing process by throwing a {@link TemplateException}.
   * <p>
   * Despite the return type being {@link TemplateException} this method never actually returns
   * anything as it always throws the {@link TemplateException}. This is essentially to allow the
   * convenient pattern <b>{@code throw context.fail(...);}</b> because the compiler otherwise
   * wouldn't know that code below a call to this method is never executed.
   *
   * @param element
   *          the element the problem occurred on
   * @param attributeName
   *          the name of the attribute the problem occurred on
   * @param message
   *          the error message
   * @return nothing, as this method never returns regularly
   * @throws TemplateException
   *           always
   */
  public TemplateException fail(Element element, String attributeName, String message)
  {
    throw new TemplateException(templateId, templateSourceDescription, element, attributeName,
        message);
  }

  /**
   * Fails the current parsing process by throwing a {@link TemplateException}.
   * <p>
   * Despite the return type being {@link TemplateException} this method never actually returns
   * anything as it always throws the {@link TemplateException}. This is essentially to allow the
   * convenient pattern <b>{@code throw context.fail(...);}</b> because the compiler otherwise
   * wouldn't know that code below a call to this method is never executed.
   *
   * @param element
   *          the element the problem occurred on
   * @param attributeName
   *          the name of the attribute the problem occurred on
   * @param message
   *          the error message
   * @param cause
   *          the cause of the failure
   * @return nothing, as this method never returns regularly
   * @throws TemplateException
   *           always
   */
  public TemplateException fail(Element element, String attributeName, String message,
      Throwable cause)
  {
    throw new TemplateException(templateId, templateSourceDescription, element, attributeName,
        message).initCause(cause);
  }

  public void reportWarning(String message, Object... formatArgs)
  {
    report.addWarning(message, formatArgs);
  }

  public void reportError(String message, Object... foramtArgsAndException)
  {
    report.addError(message, foramtArgsAndException);
  }


  /**
   * Called by {@link TemplateParserContext#readComponent(Element, ComponentProcessor)} and
   * {@link TemplateParserContext#readComponentForSlot(Element, ComponentProcessor)} after creating
   * the component, but before any {@link ComponentPostProcessor post-processors} are called.
   * <p>
   * This mainly allows to handle attributes before the component is finalized and any unconsumed
   * attributes lead to errors or warnings. Like e.g. layout related attributes on a child component
   * that need to be consumed by the containing layout.
   */
  @FunctionalInterface
  public interface ComponentProcessor
    extends
      Serializable
  {

    /**
     * This method is called after the component has been created and before any
     * {@link ComponentPostProcessor#postProcessComponent(Component, ElementParserContext)
     * post-processors} are called.
     *
     * @param component
     *          the child component
     * @param context
     *          the parse context of the element
     */
    void process(Component component, ElementParserContext context);

  }

  /**
   * Called by
   * {@link TemplateParserContext#readChildren(com.vaadin.flow.component.Component, Element, ChildElementHandler, TextNodeHandler)}
   * to handle a child {@link Element element} of a component element in a template.
   * <p>
   * Typically these elements are either parsed as regular child components via
   * {@link TemplateParserContext#readComponent(Element, ComponentProcessor)} or for a certain slot
   * provided by the parent component via
   * {@link TemplateParserContext#readComponentForSlot(Element, ComponentProcessor)}.
   * <p>
   * A child element is only handled once. If this method returns {@code false}, all
   * {@link ComponentPostProcessor#handleChildElement(com.vaadin.flow.component.Component, String, Element, TemplateParserContext)
   * post-processors are called} until one handles it. If the element is not handled at all, it is
   * considered an error.
   * <p>
   * The handling of the child element does not affect the regular post-processing of any components
   * created via {@link TemplateParserContext#readComponent(Element, ComponentProcessor)} or
   * {@link TemplateParserContext#readComponentForSlot(Element, ComponentProcessor)}; here all
   * post-processors are called.
   *
   * @see TemplateParserContext#readComponent(Element, ComponentProcessor)
   * @see TemplateParserContext#readComponentForSlot(Element, ComponentProcessor)
   */
  @FunctionalInterface
  public interface ChildElementHandler
    extends
      Serializable
  {

    /**
     * Handles the given child element.
     *
     * @param slotName
     *          the name of the slot the element is intended for;
     *          ({@link ComponentFactory#SLOT_DEFAULT}) for the unnamed default slot
     * @param childElement
     *          the child element
     * @return whether the child component has been handled; if {@code true} no further
     *         {@link ComponentPostProcessor} is considered
     */
    boolean handle(String slotName, Element childElement);

  }



  public static class AttributeSource
  {

    private final String attrName;

    private final String rawValue;

    private boolean skipConsumed = true;

    private final boolean consumed;

    private final BiFunction<String, Throwable, TemplateException> exceptionProvider;


    /* package */ AttributeSource(String attrName, String rawValue, boolean consumed,
        BiFunction<String, Throwable, TemplateException> exceptionProvider)
    {
      this.attrName = attrName;
      this.rawValue = rawValue;
      this.consumed = consumed;
      this.exceptionProvider = exceptionProvider;
    }


    public AttributeSource dontSkipConsumed()
    {
      this.skipConsumed = false;
      return this;
    }


    @SuppressWarnings("unchecked")
    public <T> ResolvedValue<T> as(Class<T> targetType)
    {
      ResolvedValue<T> result;

      if (targetType == Boolean.class || targetType == boolean.class)
        result = (ResolvedValue<T>) asBoolean();
      else if (targetType == Integer.class || targetType == int.class)
        result = (ResolvedValue<T>) asInteger();
      else if (targetType == Double.class || targetType == double.class)
        result = (ResolvedValue<T>) asDouble();
      else if (targetType == BigDecimal.class)
        result = (ResolvedValue<T>) asBigDecimal();
      else if (targetType == String.class)
        result = (ResolvedValue<T>) asString();
      else if (targetType == LocalDate.class)
        result = (ResolvedValue<T>) asLocalDate();
      else if (targetType == LocalTime.class)
        result = (ResolvedValue<T>) asLocalTime();
      else if (targetType == LocalDateTime.class)
        result = (ResolvedValue<T>) asLocalDateTime();
      else if (targetType == Locale.class)
        result = (ResolvedValue<T>) asLocale();
      else if (targetType.isEnum())
        // This cast is actually required by javac.
        result = (ResolvedValue<T>) asEnum(targetType.asSubclass(Enum.class));
      else
        throw exceptionProvider.apply("Unsupported attribute type: " + targetType, null);

      return result;
    }

    public ResolvedValue<Boolean> asBoolean()
    {
      return asBoolean(null);
    }

    public ResolvedValue<Boolean> asBoolean(Boolean emptyDefault)
    {
      return asConverted(v ->
      {
        if (!v.isEmpty() && !v.equals(attrName))
        {
          String msg = "The value '%s' is not valid for the boolean attribute '%s'. Omit the"
              + " attribute altogether for 'false'; omit the value or set it to the attribute's name"
              + " for 'true'.";
          msg = String.format(msg, v, attrName);
          throw exceptionProvider.apply(msg, null);
        }
        return true;
      }, emptyDefault);
    }

    public ResolvedValue<Integer> asInteger()
    {
      return asInteger(null);
    }

    public ResolvedValue<Integer> asInteger(Integer emptyDefault)
    {
      return asConverted(Integer::valueOf, emptyDefault);
    }

    public ResolvedValue<Long> asLong()
    {
      return asLong(null);
    }

    public ResolvedValue<Long> asLong(Long emptyDefault)
    {
      return asConverted(Long::valueOf, emptyDefault);
    }

    public ResolvedValue<Double> asDouble()
    {
      return asDouble(null);
    }

    public ResolvedValue<Double> asDouble(Double emptyDefault)
    {
      return asConverted(Double::valueOf, emptyDefault);
    }

    public ResolvedValue<BigDecimal> asBigDecimal()
    {
      return asBigDecimal(null);
    }

    public ResolvedValue<BigDecimal> asBigDecimal(BigDecimal emptyDefault)
    {
      return asConverted(BigDecimal::new, emptyDefault);
    }

    public ResolvedValue<String> asString()
    {
      return asString(null);
    }

    public ResolvedValue<String> asString(String emptyDefault)
    {
      return asConverted(s -> s, emptyDefault);
    }

    public <E extends Enum<E>> ResolvedValue<E> asEnum(Class<E> targetType)
    {
      return asEnum(targetType, null);
    }

    public <E extends Enum<E>> ResolvedValue<E> asEnum(Class<E> targetType, E emptyDefault)
    {
      return asEnum(v ->
      {
        return Enum.valueOf(targetType, v.toUpperCase(Locale.ENGLISH).replace("-", "_"));
      }, emptyDefault);
    }

    public <E extends Enum<E>> ResolvedValue<E> asEnum(Function<String, E> enumResolver)
    {
      return asEnum(enumResolver, null);
    }

    public <E extends Enum<E>> ResolvedValue<E> asEnum(Function<String, E> enumResolver,
        E emptyDefault)
    {
      return asConverted(attrString ->
      {
        E enumValue;
        try
        {
          enumValue = enumResolver.apply(attrString);
        }
        catch (IllegalArgumentException ex)
        {
          throw exceptionProvider.apply("No enum constant for value '" + attrString + "' found.",
              ex);
        }
        if (enumValue == null)
          throw exceptionProvider.apply("No enum constant for value '" + attrString + "' found.",
              null);
        return enumValue;
      }, emptyDefault);
    }

    public ResolvedValue<LocalDate> asLocalDate()
    {
      return asLocalDate(null);
    }

    public ResolvedValue<LocalDate> asLocalDate(LocalDate emptyDefault)
    {
      return asConverted(LocalDate::parse, emptyDefault);
    }

    public ResolvedValue<LocalTime> asLocalTime()
    {
      return asLocalTime(null);
    }

    public ResolvedValue<LocalTime> asLocalTime(LocalTime emptyDefault)
    {
      return asConverted(LocalTime::parse, emptyDefault);
    }

    public ResolvedValue<LocalDateTime> asLocalDateTime()
    {
      return asLocalDateTime(null);
    }

    public ResolvedValue<LocalDateTime> asLocalDateTime(LocalDateTime emptyDefault)
    {
      return asConverted(LocalDateTime::parse, emptyDefault);
    }

    public ResolvedValue<Locale> asLocale()
    {
      return asLocale(null);
    }

    public ResolvedValue<Locale> asLocale(Locale emptyDefault)
    {
      return asConverted(Locale::forLanguageTag, emptyDefault);
    }

    public <V> ResolvedValue<V> asConverted(Function<String, V> parser)
    {
      return asConverted(parser, null);
    }

    public <V> ResolvedValue<V> asConverted(Function<String, V> parser, V emptyDefault)
    {
      if (rawValue == null || (consumed && skipConsumed))
        return new ResolvedValue<>(null, exceptionProvider);

      if (emptyDefault != null && rawValue.isEmpty())
        return new ResolvedValue<>(emptyDefault, exceptionProvider);

      try
      {
        V convertedValue = parser.apply(rawValue);
        if (convertedValue == null)
          return new ResolvedValue<>(null, exceptionProvider);

        return new ResolvedValue<>(convertedValue, exceptionProvider);
      }
      catch (TemplateException ex)
      {
        // let TemplateExceptions just pass through
        throw ex;
      }
      catch (RuntimeException ex)
      {
        throw exceptionProvider.apply("Failed to parse attribute value.", ex);
      }
    }

  }

  public static class ResolvedValue<VALUE>
  {

    private final VALUE value;

    private final BiFunction<String, Throwable, TemplateException> exceptionProvider;


    /* package */ ResolvedValue(VALUE value,
        BiFunction<String, Throwable, TemplateException> exceptionProvider)
    {
      this.value = value;
      this.exceptionProvider = exceptionProvider;
    }


    public Optional<VALUE> get()
    {
      return Optional.ofNullable(value);
    }

    public VALUE getRequired()
    {
      return getRequired("The attribute is missing a required value.");
    }

    public VALUE getRequired(String errorMessage)
    {
      if (value == null)
        throw exceptionProvider.apply(errorMessage, null);

      return value;
    }

    public void to(Consumer<VALUE> consumer)
    {
      if (value != null)
        consumer.accept(value);
    }

    public void toRequired(Consumer<VALUE> consumer)
    {
      toRequired(consumer, "The attribute is missing a required value.");
    }

    public void toRequired(Consumer<VALUE> consumer, String errorMessage)
    {
      if (value == null)
        throw exceptionProvider.apply(errorMessage, null);

      consumer.accept(value);
    }

  }

}
