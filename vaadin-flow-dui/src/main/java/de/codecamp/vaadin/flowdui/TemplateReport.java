package de.codecamp.vaadin.flowdui;


import java.util.ArrayList;
import java.util.List;


/**
 * The {@link TemplateReport} contains errors and warning that occurred while instantiating or
 * validating a DUI template.
 */
public class TemplateReport
{

  private final List<Entry> entries = new ArrayList<>();


  /**
   * Constructs a new instance.
   */
  public TemplateReport()
  {
  }


  public void addWarning(String message, Object... formatArgs)
  {
    entries.add(new Entry(EntryType.WARNING, message.formatted(formatArgs), null));
  }

  public void addError(String message, Object... foramtArgsAndException)
  {
    Object[] formatArgs;
    Exception exception = null;
    if (foramtArgsAndException.length > 0
        && foramtArgsAndException[foramtArgsAndException.length - 1] instanceof Exception ex)
    {
      exception = ex;
      formatArgs = new Object[foramtArgsAndException.length - 1];
      System.arraycopy(foramtArgsAndException, 0, formatArgs, 0, formatArgs.length);
    }
    else
    {
      formatArgs = foramtArgsAndException;
    }

    entries.add(new Entry(EntryType.ERROR, message.formatted(formatArgs), exception));
  }


  public List<Entry> getEntries()
  {
    return List.copyOf(entries);
  }


  public boolean isSuccess()
  {
    return !entries.stream().map(Entry::getType).anyMatch(t -> t == EntryType.ERROR);
  }

  public boolean isFailure()
  {
    return entries.stream().map(Entry::getType).anyMatch(t -> t == EntryType.ERROR);
  }

  public boolean hasWarnings()
  {
    return entries.stream().map(Entry::getType).anyMatch(t -> t == EntryType.WARNING);
  }


  public enum EntryType
  {

    ERROR,
    WARNING,

  }

  public static class Entry
  {

    private final EntryType type;

    private final String message;

    private final Exception exception;


    private Entry(EntryType type, String message, Exception exception)
    {
      this.type = type;
      this.message = message;
      this.exception = exception;
    }


    public EntryType getType()
    {
      return type;
    }

    public String getMessage()
    {
      return message;
    }

    public Exception getException()
    {
      return exception;
    }


    @Override
    public String toString()
    {
      return getType() + ": " + getMessage();
    }

  }

}
