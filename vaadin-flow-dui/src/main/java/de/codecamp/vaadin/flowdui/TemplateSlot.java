package de.codecamp.vaadin.flowdui;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.CssImport;


/**
 * Used as dummy component when a template slot is otherwise empty.
 */
@Tag(TemplateSlot.TAG)
@CssImport("./styles/template-slot.css")
public class TemplateSlot
  extends
    Component
{

  public static final String TAG = "template-slot";


  public TemplateSlot(String name)
  {
    setName(name);
  }


  public String getName()
  {
    return getElement().getAttribute("name");
  }

  public void setName(String name)
  {
    getElement().setAttribute("name", name);
  }

}
