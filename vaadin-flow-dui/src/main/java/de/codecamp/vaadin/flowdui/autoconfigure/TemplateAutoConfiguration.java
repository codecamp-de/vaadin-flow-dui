package de.codecamp.vaadin.flowdui.autoconfigure;


import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.TemplateEngine;
import de.codecamp.vaadin.flowdui.TemplateResolver;
import de.codecamp.vaadin.flowdui.declare.DeclarationsFile;
import elemental.json.impl.JsonUtil;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;


@AutoConfiguration
@EnableConfigurationProperties(TemplateProperties.class)
public class TemplateAutoConfiguration
{

  private static final String COMPONENTS_JSON = "components.json";

  private static final Logger LOG = LoggerFactory.getLogger(TemplateAutoConfiguration.class);


  @Bean
  TemplateEngine vaadinTemplateEngine(TemplateProperties properties,
      List<ComponentFactory> additionalFactories,
      List<ComponentPostProcessor> additionalPostProcessors,
      List<TemplateResolver> additionalResolvers, List<TemplateProcessorRegistrar> registrars)
  {
    TemplateEngine bean =
        new TemplateEngine(additionalFactories, additionalPostProcessors, additionalResolvers);

    if (registrars != null)
    {
      for (TemplateProcessorRegistrar registrar : registrars)
        registrar.registerProcessors(bean);
    }

    bean.setCacheMode(properties.getCacheMode());

    if (properties.isWriteComponentDeclarationsFile())
    {
      DeclarationsFile file = new DeclarationsFile(bean.getComponentDeclarations());

      try (Writer out = Files.newBufferedWriter(Paths.get(COMPONENTS_JSON), StandardCharsets.UTF_8))
      {
        out.append(JsonUtil.stringify(file.toJsonObject(), 2));
      }
      catch (IOException ex)
      {
        LOG.error("Failed to write '{}'.", COMPONENTS_JSON, ex);
      }
    }

    return bean;
  }

}
