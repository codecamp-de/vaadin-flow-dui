package de.codecamp.vaadin.flowdui.autoconfigure;


/**
 * Registers components involved in the processing of DUI templates. Spring beans implementing this
 * interface are automatically picked up.
 */
public interface TemplateProcessorRegistrar
{

  /**
   * Register components involved in the processing of templates via the provided
   * {@link TemplateProcessorRegistry}.
   *
   * @param registry
   *          the registry
   */
  void registerProcessors(TemplateProcessorRegistry registry);

}
