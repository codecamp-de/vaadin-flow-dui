package de.codecamp.vaadin.flowdui.autoconfigure;


import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.TemplateResolver;
import java.util.List;


/**
 * A registry of components involved in the processing of DUI templates.
 */
public interface TemplateProcessorRegistry
{

  /**
   * Adds the given component factory to this registry.
   *
   * @param factory
   *          the component factory
   */
  void addFactory(ComponentFactory factory);

  /**
   * Returns an unmodifiable view of all active {@link ComponentFactory} instances.
   *
   * @return an unmodifiable view of all active {@link ComponentFactory} instances
   */
  List<ComponentFactory> getFactories();

  /**
   * Returns an unmodifiable list of all factories that should be considered to handle the given
   * tag, in the order that they should be considered.
   *
   * @param tagName
   *          the tag name
   * @return an unmodifiable list of all factories that should be considered to handle the given tag
   */
  List<ComponentFactory> getFactoriesFor(String tagName);


  /**
   * Adds the given component post-processor to this registry.
   *
   * @param postProcessor
   *          the component post-processor
   */
  void addPostProcessor(ComponentPostProcessor postProcessor);

  /**
   * Returns an unmodifiable view of all active {@link ComponentPostProcessor} instances.
   *
   * @return an unmodifiable view of all active {@link ComponentPostProcessor} instances
   */
  List<ComponentPostProcessor> getPostProcessors();

  /**
   * Returns an unmodifiable list of all post-processors that should be considered for the given
   * component type, in the order that they should be considered.
   *
   * @param componentType
   *          the component type
   * @return an unmodifiable list of all post-processors that should be considered for the given
   *         component type
   */
  List<ComponentPostProcessor> getPostProcessorsFor(Class<? extends Component> componentType);


  /**
   * Adds the given object to this registry, automatically detecting whether it implements
   * {@link ComponentFactory}, {@link ComponentPostProcessor} or both.
   *
   * @param factoryOrPostProcessor
   *          the component factory and/or post-processor
   */
  default void addFactoryOrPostProcessor(Object factoryOrPostProcessor)
  {
    boolean isFactory = false;
    boolean isPostProcessor = false;
    if (factoryOrPostProcessor instanceof ComponentFactory)
    {
      addFactory((ComponentFactory) factoryOrPostProcessor);
      isFactory = true;
    }
    if (factoryOrPostProcessor instanceof ComponentPostProcessor)
    {
      addPostProcessor((ComponentPostProcessor) factoryOrPostProcessor);
      isPostProcessor = true;
    }

    if (!isFactory && !isPostProcessor)
      throw new IllegalArgumentException(
          "The provided object is not of a type managed by this registry.");
  }


  /**
   * Adds the given template resolver to this registry.
   *
   * @param templateResolver
   *          the template resolver
   */
  void addTemplateResolver(TemplateResolver templateResolver);

  /**
   * Returns an unmodifiable view of all active {@link TemplateResolver} instances.
   *
   * @return an unmodifiable view of all active {@link TemplateResolver} instances
   */
  List<TemplateResolver> getTemplateResolvers();

}
