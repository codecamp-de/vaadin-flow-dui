package de.codecamp.vaadin.flowdui.autoconfigure;


import de.codecamp.vaadin.flowdui.TemplateEngine.CacheMode;
import jakarta.validation.constraints.NotNull;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;


/**
 * Configuration properties for {@link TemplateAutoConfiguration}.
 */
@ConfigurationProperties(prefix = TemplateProperties.PREFIX)
@Validated
public class TemplateProperties
{

  public static final String PREFIX = "codecamp.vaadin.dui";


  /**
   * Whether template resources should be cached. AUTO caches based on Vaadin's production mode.
   */
  @NotNull
  private CacheMode cacheMode = CacheMode.AUTO;

  /**
   * Whether all active component declarations should be written to components.json file.
   */
  private boolean writeComponentDeclarationsFile = false;


  public TemplateProperties()
  {
  }


  public CacheMode getCacheMode()
  {
    return cacheMode;
  }

  public void setCacheMode(CacheMode cacheMode)
  {
    this.cacheMode = cacheMode;
  }

  public boolean isWriteComponentDeclarationsFile()
  {
    return writeComponentDeclarationsFile;
  }

  public void setWriteComponentDeclarationsFile(boolean writeComponentDeclarationsFile)
  {
    this.writeComponentDeclarationsFile = writeComponentDeclarationsFile;
  }

}
