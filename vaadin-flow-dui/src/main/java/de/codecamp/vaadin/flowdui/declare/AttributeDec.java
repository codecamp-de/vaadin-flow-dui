package de.codecamp.vaadin.flowdui.declare;


import elemental.json.Json;
import elemental.json.JsonObject;
import java.io.Serializable;


public class AttributeDec
  implements
    Serializable
{

  private final Class<?> componentType;

  private final boolean childAttribute;

  private final String name;

  private final Class<?> type;

  private final boolean required;

  private final boolean custom;

  private final String description;

  private final boolean deprecated;

  private final Class<?> declarationSource;


  public AttributeDec(Class<?> componentType, boolean childAttribute, DuiAttribute attribute,
      Class<?> declarationSource)
  {
    this.componentType = componentType;
    this.childAttribute = childAttribute;
    this.name = attribute.name();
    this.type = attribute.type();
    this.required = attribute.required();
    this.custom = attribute.custom();
    this.description = attribute.description();
    this.deprecated = attribute.deprecated();
    this.declarationSource = declarationSource;
  }


  public Class<?> getComponentType()
  {
    return componentType;
  }

  public boolean isChildAttribute()
  {
    return childAttribute;
  }

  public String getName()
  {
    return name;
  }

  public Class<?> getType()
  {
    return type;
  }

  public boolean isRequired()
  {
    return required;
  }

  public boolean isCustom()
  {
    return custom;
  }

  public String getDescription()
  {
    return description;
  }

  public boolean isDeprecated()
  {
    return deprecated;
  }

  public Class<?> getDeclarationSource()
  {
    return declarationSource;
  }


  public JsonObject toJsonObject()
  {
    JsonObject json = Json.createObject();
    json.put("name", getName());
    json.put("type", getType().getName());
    json.put("required", isRequired());
    json.put("custom", isCustom());
    json.put("description", getDescription());
    json.put("deprecated", isDeprecated());
    json.put("source", getDeclarationSource().getName());
    return json;
  }

}
