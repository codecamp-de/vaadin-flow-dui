package de.codecamp.vaadin.flowdui.declare;


import static java.util.stream.Collectors.toList;

import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;


public class ComponentDec
  implements
    Serializable
{

  private final String tagName;

  private final Class<?> componentType;

  private final boolean mixin;

  private final String description;

  private final String docUrl;

  private final String category;

  private final boolean deprecated;

  private final List<AttributeDec> attributes;

  private final List<String> slots;

  private final List<AttributeDec> childAttributes;

  private final Class<?> declarationSource;


  public ComponentDec(DuiComponent component, Class<?> declarationSource)
  {
    this.tagName = component.tagName();
    this.componentType = component.componentType();
    this.mixin = DuiComponent.TAG_ALL.equals(tagName);
    this.description = component.description();
    this.docUrl = component.docUrl();
    this.category = component.category();
    this.deprecated = component.deprecated();
    this.attributes = Collections.unmodifiableList(Stream.of(component.attributes())
        .map(attr -> new AttributeDec(component.componentType(), false, attr, declarationSource))
        .collect(toList()));
    this.slots = Collections.unmodifiableList(Arrays.asList(component.slots()));
    this.childAttributes = Collections.unmodifiableList(Stream.of(component.childAttributes())
        .map(attr -> new AttributeDec(component.componentType(), true, attr, declarationSource))
        .collect(toList()));
    this.declarationSource = declarationSource;
  }


  public String getTagName()
  {
    return tagName;
  }

  public Class<?> getComponentType()
  {
    return componentType;
  }

  public boolean isMixin()
  {
    return mixin;
  }

  public String getDescription()
  {
    return description;
  }

  public String getDocUrl()
  {
    return docUrl;
  }

  public String getCategory()
  {
    return category;
  }

  public boolean isDeprecated()
  {
    return deprecated;
  }

  public List<AttributeDec> getAttributes()
  {
    return attributes;
  }

  public List<String> getSlots()
  {
    return slots;
  }

  public List<AttributeDec> getChildAttributes()
  {
    return childAttributes;
  }

  public Class<?> getDeclarationSource()
  {
    return declarationSource;
  }


  public JsonObject toJsonObject()
  {
    JsonObject json = Json.createObject();
    json.put("tagName", getTagName());
    json.put("componentType", getComponentType().getName());
    json.put("description", getDescription());
    json.put("docUrl", getDocUrl());
    json.put("category", getCategory());
    json.put("deprecated", isDeprecated());

    JsonArray attributesJson = Json.createArray();
    getAttributes().stream().map(AttributeDec::toJsonObject)
        .forEach(attr -> attributesJson.set(attributesJson.length(), attr));
    json.put("attributes", attributesJson);

    JsonArray slotsJson = Json.createArray();
    getSlots().forEach(slot -> slotsJson.set(slotsJson.length(), slot));
    json.put("slots", slotsJson);

    JsonArray childAttributesJson = Json.createArray();
    getChildAttributes().stream().map(AttributeDec::toJsonObject)
        .forEach(attr -> childAttributesJson.set(childAttributesJson.length(), attr));
    json.put("childAttributesJson", childAttributesJson);

    json.put("source", getDeclarationSource().getName());
    return json;
  }

}
