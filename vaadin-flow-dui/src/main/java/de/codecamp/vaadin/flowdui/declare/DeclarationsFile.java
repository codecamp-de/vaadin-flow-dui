package de.codecamp.vaadin.flowdui.declare;


import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import java.time.Instant;
import java.util.List;


public class DeclarationsFile
{

  private final String generated;

  private final List<ComponentDec> components;


  public DeclarationsFile(List<ComponentDec> components)
  {
    this.generated = Instant.now().toString();
    this.components = components;
  }


  public JsonObject toJsonObject()
  {
    JsonObject json = Json.createObject();

    json.put("generated", generated);
    json.put("comment", "Automatically generated during application start. Contains all"
        + " active DUI component declarations. Does not need to be commited to version control.");

    JsonArray componentsJson = Json.createArray();
    components.stream().map(ComponentDec::toJsonObject)
        .forEach(cj -> componentsJson.set(componentsJson.length(), cj));
    json.put("components", componentsJson);

    return json;
  }

}
