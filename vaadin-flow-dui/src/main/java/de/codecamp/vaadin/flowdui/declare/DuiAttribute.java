package de.codecamp.vaadin.flowdui.declare;


public @interface DuiAttribute
{

  /**
   * Returns the name of the attribute.
   *
   * @return the name of the attribute
   */
  String name();

  /**
   * Returns the type of the attribute.
   *
   * @return the type of the attribute
   */
  Class<?> type();

  /**
   * Returns whether the attribute is required. Almost all attributes are not.
   *
   * @return whether the attribute is required
   */
  boolean required() default false;

  /**
   * Returns whether the attribute is not one of officially supported ones by the corresponding Web
   * Component.
   *
   * @return whether the attribute is not one of officially supported ones by the corresponding Web
   *         Component
   */
  boolean custom() default false;


  /**
   * Returns a description of the attribute or rather anything of note in regard to using the
   * attribute in a DUI template. The purpose is not to replicate the actual documentation of the
   * attribute.
   * <p>
   * The description is treated as HTML.
   *
   * @return a description of the attribute
   */
  String description() default "";

  /**
   * Returns whether the attribute is deprecated.
   *
   * @return whether the attribute is deprecated
   */
  boolean deprecated() default false;

}
