package de.codecamp.vaadin.flowdui.declare;


import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.dev.ComponentDeclarationsView;
import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;


/**
 * Describes a {@link Component} or mix-in interface and how it is represented in a DUI template via
 * its tag and/or attributes. This annotation is applied directly to the {@link ComponentFactory} or
 * {@link ComponentPostProcessor}.
 * <p>
 * {@link DuiComponent} declarations can be used to filter the applicable factories (per
 * {@link #tagName()}) and post-processors (per {@link #componentType()}) in advance. If no
 * {@link DuiComponent} is found they will always be applicable; otherwise only for the declared
 * tags or component types / mix-in interfaces respectively. The tag {@value #TAG_ALL} can be used
 * on factories that should always be considered applicable.
 * <p>
 * Declarations can also be used to automatically generate documentation. Factories and
 * post-processors allow a flexibility that is impossible to express completely in declarations and
 * display in a simple way. As such, declarations should consider this and describe any non-trivial
 * cases in the description-attribute of components and attributes. Either way, conflicting
 * information (multiple declarations of the same tag or multiple handlers for a certain attribute,
 * etc.) may in turn lead to suboptimal presentation of those declarations.
 */
@Target(TYPE)
@Retention(RUNTIME)
@Repeatable(DuiComponent.Container.class)
@Documented
public @interface DuiComponent
{

  /**
   * Using this string as {@link #tagName()} implies This can be used as {@link #tagName()} by
   * mix-in interfaces that are handled by a {@link ComponentPostProcessor} and are applied to all
   * components that implement a certain interface specified in {@link #componentType()}. But it
   * could also be used by a {@link ComponentFactory} if the tags it will handle are really not
   * known in advance.
   */
  String TAG_ALL = "*";

  /**
   * Use this as the name of the unnamed default slot of a component.
   */
  String SLOT_DEFAULT = "";


  /**
   * On {@link ComponentFactory factories} this returns the tag name of a supported component. To
   * always consider the factory applicable {@link #TAG_ALL *} can be used.
   * <p>
   * On {@link ComponentPostProcessor post-processors} it should always be {@link #TAG_ALL *} (the
   * default).
   *
   * @return the tag name of the component or {@link #TAG_ALL *}
   */
  String tagName() default TAG_ALL;

  /**
   * Returns whether the tag <em>doesn't</em> represent an actual Web Component.
   *
   * @return whether the tag <em>doesn't</em> represent an actual Web Component
   */
  boolean customTag() default false;

  /**
   * Returns the type of the component or mix-in interface.
   * <p>
   * On {@link ComponentFactory factories} this is the type of component being created for the
   * {@link #tagName()}; {@link Component} may be used in rare cases where the type is dynamic and
   * not known beforehand.
   * <p>
   * On {@link ComponentPostProcessor post-processors} this is the component type or mix-in
   * interface it will be applied to.
   *
   *
   * @return the type of the component or mix-in interface
   */
  Class<?> componentType();


  /**
   * Returns a description of the component or rather anything of note in regard to using the
   * component in a DUI template. The purpose is not to replicate the actual documentation of the
   * component.
   * <p>
   * The description is treated as HTML.
   *
   * @return a description of the component
   */
  String description() default "";

  /**
   * Returns a URL to documentation of the component.
   *
   * @return a URL to documentation of the component; may be empty
   */
  String docUrl() default "";

  /**
   * Returns the category of the component. Categories are a simple mechanism to sort components
   * into distinct "boxes". The {@link ComponentDeclarationsView} uses it to provide a filtered view
   * of all registered components (or rather component factories).
   *
   * @return the category of the component
   */
  String category() default "Miscellaneous";

  /**
   * Returns whether the component requires a Vaadin Pro subscription.
   *
   * @return whether the component requires a Vaadin Pro subscription
   */
  boolean pro() default false;

  /**
   * Returns whether this component is deprecated.
   *
   * @return whether this component is deprecated
   */
  boolean deprecated() default false;


  /**
   * Returns the supported slots. {@link #SLOT_DEFAULT} represents the default / unnamed slot.
   *
   * @return the supported slots
   */
  String[] slots() default {};

  /**
   * Returns the attributes consumed by this factory or post-processor.
   *
   * @return the attributes consumed by this factory or post-processor
   */
  DuiAttribute[] attributes() default {};

  /**
   * Returns the attributes on child components in the unnamed default slot consumed by this factory
   * or post-processor.
   *
   * @return the attributes on child components in the unnamed default slot consumed by this factory
   *         or post-processor
   */
  DuiAttribute[] childAttributes() default {};


  /**
   * Container for repeated {@link DuiComponent @DuiComponent} annotations.
   */
  @Target(TYPE)
  @Retention(RUNTIME)
  @Documented
  @interface Container
  {

    /**
     * Returns the {@link DuiComponent @DuiComponent} annotations.
     *
     * @return the {@link DuiComponent @DuiComponent} annotations
     */
    DuiComponent[] value();

  }

}
