package de.codecamp.vaadin.flowdui.dev;


import com.vaadin.flow.function.DeploymentConfiguration;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServiceInitListener;
import com.vaadin.flow.server.startup.ApplicationRouteRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Registers a route for the {@link ComponentDeclarationsView} when Vaadin is NOT running in
 * {@link DeploymentConfiguration#isProductionMode() production mode}.
 */
public class ComponentDeclarationsVaadinServiceInitListener
  implements
    VaadinServiceInitListener
{

  private static final String COMPONENT_DECLARATIONS_ROUTE_PATH = "component-declarations";

  private static final String TEMPLATE_PREVIEW_ROUTE_PATH = "template-preview";

  private static final Logger LOG =
      LoggerFactory.getLogger(ComponentDeclarationsVaadinServiceInitListener.class);


  @Override
  public void serviceInit(ServiceInitEvent event)
  {
    VaadinService service = event.getSource();
    if (!service.getDeploymentConfiguration().isProductionMode())
    {
      LOG.warn(
          "Registering Declarative UI's component overview as route '{}'. This only happens while NOT in production mode.",
          COMPONENT_DECLARATIONS_ROUTE_PATH);
      ApplicationRouteRegistry.getInstance(service.getContext())
          .setRoute(COMPONENT_DECLARATIONS_ROUTE_PATH, ComponentDeclarationsView.class, null);

      LOG.warn(
          "Registering DUI template preview as route '{}'. This only happens while NOT in production mode.",
          TEMPLATE_PREVIEW_ROUTE_PATH);
      ApplicationRouteRegistry.getInstance(service.getContext())
          .setRoute(TEMPLATE_PREVIEW_ROUTE_PATH, TemplatePreviewView.class, null);
    }
  }

}
