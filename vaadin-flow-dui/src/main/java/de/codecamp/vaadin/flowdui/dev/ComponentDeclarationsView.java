package de.codecamp.vaadin.flowdui.dev;


import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyModifier;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.component.html.ListItem;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.html.UnorderedList;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.provider.hierarchy.TreeData;
import com.vaadin.flow.data.provider.hierarchy.TreeDataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import de.codecamp.vaadin.base.Composite;
import de.codecamp.vaadin.base.css.CssProperties;
import de.codecamp.vaadin.base.lumo.LumoBorderRadius;
import de.codecamp.vaadin.base.lumo.LumoColor;
import de.codecamp.vaadin.base.lumo.LumoSpace;
import de.codecamp.vaadin.flowdui.TemplateEngine;
import de.codecamp.vaadin.flowdui.declare.AttributeDec;
import de.codecamp.vaadin.flowdui.declare.ComponentDec;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.jsoup.Jsoup;
import org.jsoup.safety.Safelist;


@CssImport("./styles/ComponentDeclarationsView.css")
public class ComponentDeclarationsView
  extends
    Composite
{

  private static final Safelist HTML_SAFELIST = Safelist.basic().addTags("s");


  private MenuItem categoriesMenu;

  private final SortedSet<String> activeCategories = new TreeSet<>();


  private TreeGrid<Entry> tree;

  private TreeDataProvider<Entry> treeDataProvider;


  private boolean showMixin;

  private boolean showInherited;


  public ComponentDeclarationsView()
  {
  }


  @Override
  protected Component createContent()
  {
    VerticalLayout content = new VerticalLayout();
    content.setSizeFull();
    content.setAlignItems(Alignment.STRETCH);

    treeDataProvider = new TreeDataProvider<>(new TreeData<>());

    HorizontalLayout optionsBar = new HorizontalLayout();
    optionsBar.setWidthFull();
    optionsBar.setAlignItems(Alignment.CENTER);
    content.add(optionsBar);

    TextField filterField = new TextField();
    filterField.setWidth("30em");
    filterField.setPrefixComponent(new Icon(VaadinIcon.SEARCH));
    filterField.setPlaceholder("Filter components (Ctrl+F)");
    filterField.setClearButtonVisible(true);
    filterField.setValueChangeMode(ValueChangeMode.TIMEOUT);
    filterField.addValueChangeListener(event ->
    {
      String filterText = event.getValue();
      if (filterText.isBlank())
      {
        treeDataProvider.setFilter(null);
        return;
      }

      treeDataProvider.setFilter(entry ->
      {
        return entry.getComponent().getTagName().contains(filterText)
            || entry.getComponent().getComponentType().getName().contains(filterText)
            || entry.getSource().contains(filterText);
      });
    });
    filterField.setAutofocus(true);
    filterField.addFocusShortcut(Key.KEY_F, KeyModifier.CONTROL);
    optionsBar.add(filterField);
    optionsBar.expand(filterField);

    MenuBar menuBar = new MenuBar();
    categoriesMenu = menuBar.addItem("Categories");
    optionsBar.add(menuBar);

    Checkbox showMixinInterfacesCheckbox = new Checkbox("Show mix-in interfaces");
    showMixinInterfacesCheckbox.addValueChangeListener(event ->
    {
      showMixin = event.getValue();
      refreshGrid();
    });
    showMixinInterfacesCheckbox.setValue(false);
    optionsBar.add(showMixinInterfacesCheckbox);

    Checkbox showInheritedAttributesCheckbox = new Checkbox("Show inherited attributes");
    showInheritedAttributesCheckbox.addValueChangeListener(event ->
    {
      showInherited = event.getValue();
      refreshGrid();
    });
    showInheritedAttributesCheckbox.setValue(true);
    optionsBar.add(showInheritedAttributesCheckbox);


    tree = new TreeGrid<>();
    tree.setId("component-declarations-grid");
    tree.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
    tree.setDetailsVisibleOnClick(true);
    tree.setItemDetailsRenderer(new ComponentRenderer<>(this::createDetails));
    content.add(tree);

    tree.setDataProvider(treeDataProvider);


    tree.addComponentHierarchyColumn(entry -> new Html(entry.getTagHtml())) //
        .setHeader("Tag") //
        .setComparator(Entry::getTagSortString).setResizable(true);

    Column<Entry> componentTypeColumn = tree.addColumn(Entry::getComponentType) //
        .setHeader("Component Type") //
        .setComparator(Entry::getComponentType) //
        .setResizable(true);

    tree.addComponentColumn(entry -> new Html(entry.getAttributeHtml())) //
        .setHeader("Attribute") //
        .setComparator(Entry::getAttributeSortString).setResizable(true);

    tree.addComponentColumn(entry ->
    {
      return new Html(entry.getSlots().stream().map(s -> s.equals("") ? "<em>default</em>" : s)
          .collect(joining(", ", "<span>", "</span>")));
    }).setHeader("Slots") //
        .setResizable(true);

    tree.addComponentColumn(entry -> new Html(entry.getChildAttributeHtml()))
        .setHeader("Child Attribute") //
        .setComparator(Entry::getChildAttributeSortString).setResizable(true);

    tree.addComponentColumn(entry ->
    {
      if (entry.isComponentRow())
      {
        String docUrl = entry.getComponent().getDocUrl();
        if (docUrl == null || docUrl.isBlank())
        {
          return new Span();
        }
        else
        {
          Anchor anchor = new Anchor();
          anchor.setHref(docUrl);
          anchor.add(new Icon(VaadinIcon.INFO_CIRCLE_O));
          anchor.setTarget(AnchorTarget.BLANK);
          return anchor;
        }
      }
      else
      {
        return new Span();
      }
    }).setHeader("Doc") //
        .setAutoWidth(true).setFlexGrow(0);

    tree.addComponentColumn(entry -> new Html(entry.getDescriptionHtml())) //
        .setHeader("Description") //
        .setResizable(true).setFlexGrow(4);

    tree.addColumn(Entry::getSource) //
        .setHeader("Source") //
        .setSortable(true).setResizable(true);


    tree.setMultiSort(true);
    tree.sort(GridSortOrder.asc(componentTypeColumn).build());

    tree.setPartNameGenerator(entry ->
    {
      if (entry.isComponentRow())
        return "component-row";
      else
        return "attribute-row";
    });

    tree.addExpandListener(event ->
    {
      tree.recalculateColumnWidths();
    });


    VerticalLayout attributeTypeDetailsContent = new VerticalLayout();
    attributeTypeDetailsContent.setPadding(false);
    attributeTypeDetailsContent.setSpacing(false);
    Details legend = new Details("Legend");
    legend.add(attributeTypeDetailsContent);
    content.add(legend);

    attributeTypeDetailsContent.add(new UnorderedList( //
        new ListItem(new Html(
            "<span><b>* before the name:</b> A custom tag or attribute; not part of the official Web Components.</span>")),
        new ListItem(new Html(
            "<span><b><s>Struckthrough name</s>:</b> The custom tag or attribute is deprecated.</span>")),
        new ListItem(new Html("<span><b>Bold attribute name</b>: A required attribute.</span>")),
        new ListItem(new Html(
            "<span><b>Type: <span style=\"color: var(--lumo-primary-text-color);\">Boolean</span>:</b> Boolean attributes are either present (true) or absent (false). They don't have an explicit value in the template.</span>")),
        new ListItem(new Html(
            "<span><b>Type: <span style=\"color: var(--lumo-primary-text-color);\">LocalDate</span>:</b> ISO-8601 formatted local date, e.g. <code>2007-12-03</code></span>")),
        new ListItem(new Html(
            "<span><b>Type: <span style=\"color: var(--lumo-primary-text-color);\">LocalTime</span>:</b> ISO-8601 formatted local time, e.g. <code>10:15 10:15:30</code></span>")),
        new ListItem(new Html(
            "<span><b>Type: <span style=\"color: var(--lumo-primary-text-color);\">LocalDateTime</span>:</b> ISO-8601 formatted local date-time, e.g. <code>2007-12-03T10:15:30</code></span>")),
        new ListItem(new Html(
            "<span><b>Type: <span style=\"color: var(--lumo-primary-text-color);\">Locale</span>:</b> IETF BCP 47 language tag, e.g. <code>en-US</code></span>")) //
    ));

    return content;
  }

  @Override
  protected void contentCreated()
  {
    refreshGrid();
  }

  private void refreshGrid()
  {
    if (tree == null)
      return;

    List<ComponentDec> allDeclarations = TemplateEngine.get().getComponentDeclarations();

    SortedSet<String> categories = new TreeSet<>();
    allDeclarations.forEach(dec -> categories.add(dec.getCategory()));
    SubMenu subMenu = categoriesMenu.getSubMenu();
    subMenu.removeAll();
    MenuItem allCategoriesItem = subMenu.addItem("All", event ->
    {
      if (activeCategories.isEmpty())
        return;

      activeCategories.clear();
      refreshGrid();
    });
    allCategoriesItem.setCheckable(true);
    if (activeCategories.isEmpty())
      allCategoriesItem.setChecked(true);

    for (String category : categories)
    {
      MenuItem categoryItem = subMenu.addItem(category, event ->
      {
        if (activeCategories.contains(category))
          return;

        activeCategories.clear();
        activeCategories.add(category);
        refreshGrid();
      });
      categoryItem.setCheckable(true);
      if (activeCategories.contains(category))
        categoryItem.setChecked(true);
    }

    categoriesMenu.setText("Categories: "
        + (activeCategories.isEmpty() ? "All" : activeCategories.stream().collect(joining(", "))));


    List<Entry> shownDeclarations = new ArrayList<>();
    for (ComponentDec compDec : allDeclarations)
    {
      if (!activeCategories.isEmpty() && !activeCategories.contains(compDec.getCategory()))
        continue;

      if (compDec.isMixin())
      {
        if (showMixin)
        {
          shownDeclarations.add(new Entry(compDec));
        }
      }
      else
      {
        if (showInherited)
        {
          Set<String> slots = new LinkedHashSet<>(compDec.getSlots());
          List<AttributeDec> attributes = new ArrayList<>(compDec.getAttributes());
          attributes.addAll(compDec.getChildAttributes());

          for (ComponentDec dec : allDeclarations)
          {
            if (!dec.isMixin())
              continue;

            if (dec.getComponentType().isAssignableFrom(compDec.getComponentType()))
            {
              slots.addAll(dec.getSlots());
              attributes.addAll(dec.getAttributes());
              attributes.addAll(dec.getChildAttributes());
            }
          }

          shownDeclarations.add(new Entry(compDec, new ArrayList<>(slots), attributes));
        }
        else
        {
          shownDeclarations.add(new Entry(compDec));
        }
      }
    }

    treeDataProvider.getTreeData().clear();
    treeDataProvider.getTreeData().addItems(shownDeclarations, Entry::getChildren);
    treeDataProvider.refreshAll();
  }

  private Component createDetails(Entry entry)
  {
    HtmlField descriptionField = new HtmlField();
    descriptionField.setWidthFull();
    descriptionField.setLabel("Description");
    descriptionField.setReadOnly(true);
    descriptionField.setValue(entry.getDescriptionHtml());
    return descriptionField;
  }


  private static class Entry
  {

    private final ComponentDec component;

    private final List<AttributeDec> attributes;

    private final List<String> slots;


    private final AttributeDec attribute;


    Entry(ComponentDec component)
    {
      this.component = component;
      this.attributes = new ArrayList<>(component.getAttributes());
      this.attributes.addAll(component.getChildAttributes());
      this.slots = component.getSlots();

      this.attribute = null;
    }

    Entry(ComponentDec component, List<String> slots, List<AttributeDec> attributes)
    {
      this.component = component;
      this.slots = slots;
      this.attributes = attributes;

      this.attribute = null;
    }


    Entry(ComponentDec component, AttributeDec attribute)
    {
      this.component = component;
      this.attribute = attribute;

      this.slots = null;
      this.attributes = null;
    }


    public String getTagHtml()
    {
      if (isComponentRow())
      {
        StringBuilder html = new StringBuilder();
        html.append("<span>");

        if (getComponent().isDeprecated())
          html.append("<s>");
        html.append(getComponent().getTagName());
        if (getComponent().isDeprecated())
          html.append("</s>");

        html.append("</span>");
        return html.toString();
      }
      else
      {
        return "<span></span>";
      }
    }

    public String getTagSortString()
    {
      return isComponentRow() ? getComponent().getTagName() : "";
    }

    public String getComponentType()
    {
      Class<?> componentType;
      if (isComponentRow())
        componentType = getComponent().getComponentType();
      else if (getComponent().getComponentType() != getAttribute().getComponentType())
        componentType = getAttribute().getComponentType();
      else
        componentType = null;

      if (componentType == null)
        return "";
      else if (componentType == Void.class)
        return "";
      else if (componentType.getName().startsWith("com.vaadin.flow."))
        return componentType.getSimpleName();
      else
        return componentType.getSimpleName() + " (" + componentType.getPackage().getName() + ")";
    }

    public List<String> getSlots()
    {
      return isComponentRow() ? slots : Collections.emptyList();
    }

    public List<Entry> getChildren()
    {
      if (isComponentRow())
        return attributes.stream().map(a -> new Entry(getComponent(), a)).collect(toList());
      else
        return Collections.emptyList();
    }


    public String getAttributeHtml()
    {
      if (isComponentRow())
      {
        return "<span></span>";
      }
      else if (getAttribute().isChildAttribute())
      {
        return "<span></span>";
      }
      else
      {
        return attributeToHtml();
      }
    }

    public String getAttributeSortString()
    {
      if (isComponentRow())
      {
        return "";
      }
      else if (getAttribute().isChildAttribute())
      {
        return "";
      }
      else
      {
        return getAttribute().getName();
      }
    }

    public String getChildAttributeHtml()
    {
      if (isComponentRow())
      {
        return "<span></span>";
      }
      else if (getAttribute().isChildAttribute())
      {
        return attributeToHtml();
      }
      else
      {
        return "<span></span>";
      }
    }

    private String attributeToHtml()
    {
      StringBuilder html = new StringBuilder(200);

      html.append("<span>");

      if (getAttribute().isCustom())
        html.append(" * ");

      if (getAttribute().isDeprecated())
        html.append("<s>");
      if (getAttribute().isRequired())
        html.append("<b>");
      html.append(getAttribute().getName());
      if (getAttribute().isRequired())
        html.append("</b>");
      if (getAttribute().isDeprecated())
        html.append("</s>");

      html.append(" <b style=\"color: var(--lumo-primary-text-color);\">");
      if (getAttribute().getType().getName().startsWith("java."))
        html.append(getAttribute().getType().getSimpleName());
      else
        html.append(getAttribute().getType().getName());
      html.append("</b>");

      if (getAttribute().isRequired())
        html.append(" <em>(required)</em>");

      html.append("</span>");

      return html.toString();
    }

    public String getChildAttributeSortString()
    {
      if (isComponentRow())
      {
        return "";
      }
      else if (getAttribute().isChildAttribute())
      {
        return getAttribute().getName();
      }
      else
      {
        return "";
      }
    }

    public String getSource()
    {
      Class<?> source;
      if (isComponentRow())
        source = getComponent().getDeclarationSource();
      else
        source = getAttribute().getDeclarationSource();

      if (source.getName().startsWith("de.codecamp.vaadin.flowdui.factories."))
        return source.getSimpleName();
      else
        return source.getName();
    }

    public String getDescriptionHtml()
    {
      String html =
          isComponentRow() ? getComponent().getDescription() : getAttribute().getDescription();
      return "<span>" + Jsoup.clean(html, HTML_SAFELIST) + "</span>";
    }


    public boolean isComponentRow()
    {
      return getAttribute() == null;
    }

    public ComponentDec getComponent()
    {
      return component;
    }

    public AttributeDec getAttribute()
    {
      return attribute;
    }

  }

  private static class HtmlField
    extends
      CustomField<String>
  {

    private Html html = new Html("<div></div>");


    HtmlField()
    {
      super("");
    }


    @Override
    protected String generateModelValue()
    {
      throw new UnsupportedOperationException();
    }

    @Override
    protected void setPresentationValue(String newPresentationValue)
    {
      if (html != null)
        remove(html);

      html = new Html("<div>" + newPresentationValue + "</div>");
      html.getElement().getStyle() //
          .set(CssProperties.border, "1px dashed " + LumoColor.contrast10Pct.var())
          .set(CssProperties.borderRadius, LumoBorderRadius.M.var())
          .set(CssProperties.paddingLeft, LumoSpace.M.var())
          .set(CssProperties.paddingRight, LumoSpace.M.var());

      add(html);
    }

  }

}
