package de.codecamp.vaadin.flowdui.dev;


import static org.apache.commons.lang3.StringUtils.getCommonPrefix;
import static org.apache.commons.lang3.StringUtils.removeStart;
import static org.apache.commons.lang3.StringUtils.substringBeforeLast;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasText.WhiteSpace;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.di.Instantiator;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.spring.VaadinConfigurationProperties;
import de.codecamp.vaadin.base.lumo.LumoColor;
import de.codecamp.vaadin.base.lumo.LumoIconSize;
import de.codecamp.vaadin.flowdui.Fragment;
import de.codecamp.vaadin.flowdui.FragmentComposite;
import de.codecamp.vaadin.flowdui.Mapped;
import de.codecamp.vaadin.flowdui.Template;
import de.codecamp.vaadin.flowdui.TemplateComposite;
import de.codecamp.vaadin.flowdui.TemplateEngine;
import de.codecamp.vaadin.flowdui.TemplateException;
import de.codecamp.vaadin.flowdui.TemplateReport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.AssignableTypeFilter;


@Template(
    content = """
        <vaadin-vertical-layout id="content" size-full>
          <vaadin-vertical-layout id="previewFrame" width-full default-align="stretch">

            <vaadin-horizontal-layout padding spacing default-align="center" style="box-shadow: var(--lumo-box-shadow-xs);">
              <vaadin-text-field id="templateField" width-full readonly></vaadin-text-field>
              <span>Press Enter to (re)load.</span>
              <vaadin-button id="reloadTemplateButton" icon="vaadin:refresh"></vaadin-button>
            </vaadin-horizontal-layout>

            <vaadin-horizontal-layout height-full spacing>
              <vaadin-vertical-layout id="sidebarContainer" padding spacing style="box-shadow: var(--lumo-box-shadow-xs);">
                <vaadin-button id="rediscoverTemplatesButton" icon="vaadin:refresh" align="stretch"></vaadin-button>
                <vaadin-list-box id="templatesListBox" height-full></vaadin-list-box>
              </vaadin-vertical-layout>
              <vaadin-vertical-layout id="templateContainer" width-full padding>
              </vaadin-vertical-layout>
            </vaadin-horizontal-layout>

          </vaadin-vertical-layout>
        </vaadin-vertical-layout>
        """)
public class TemplatePreviewView
  extends
    TemplateComposite
  implements
    HasUrlParameter<String>
{

  private String commonIdPrefix;


  private Class<?> currentHostClass;

  private boolean withFrame;


  private boolean showingReport = false;


  @Mapped
  private VerticalLayout content;

  @Mapped
  private VerticalLayout previewFrame;

  @Mapped
  private TextField templateField;

  @Mapped
  private Button reloadTemplateButton;

  @Mapped
  private Button rediscoverTemplatesButton;

  @Mapped
  private ListBox<Class<?>> templatesListBox;

  @Mapped
  private VerticalLayout templateContainer;


  @Override
  protected void contentCreated()
  {
    reloadTemplateButton.addClickShortcut(Key.ENTER);
    reloadTemplateButton.addClickListener(event -> reloadCurrentTemplate());


    rediscoverTemplatesButton.addClickListener(event -> discoverTemplates());

    templatesListBox.setRenderer(new ComponentRenderer<>(item ->
    {
      boolean selected = templatesListBox.getValue() == item;

      HorizontalLayout comp = new HorizontalLayout();
      comp.setDefaultVerticalComponentAlignment(Alignment.CENTER);
      Icon icon;
      Button showReportButton = null;
      TemplateReport report = TemplateEngine.get().validateTemplate(item);
      if (report.isSuccess())
      {
        if (!report.hasWarnings())
        {
          icon = new Icon(VaadinIcon.CHECK_CIRCLE);
          icon.setColor(LumoColor.successColor.var());
        }
        else
        {
          icon = new Icon(VaadinIcon.CHECK_CIRCLE);
          icon.setColor(LumoColor.warningColor.var());
        }
      }
      else
      {
        icon = new Icon(VaadinIcon.CLOSE_CIRCLE);
        icon.setColor(LumoColor.errorColor.var());
      }

      if (selected && (!report.isSuccess() || report.hasWarnings()))
      {
        showReportButton = new Button("Toggle report");
        showReportButton.addThemeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY);
        showReportButton.addClickListener(event ->
        {
          if (showingReport)
            reloadCurrentTemplate();
          else
            showCurrentReport();
        });
      }

      icon.setSize(LumoIconSize.S.var());
      comp.add(icon);
      comp.addAndExpand(new Span(removeStart(item.getName(), commonIdPrefix)));
      if (showReportButton != null)
        comp.add(showReportButton);
      return comp;
    }));
    templatesListBox.addValueChangeListener(event ->
    {
      showTemplate(event.getValue());
    });


    discoverTemplates();
  }

  @Override
  public void setParameter(BeforeEvent event, @OptionalParameter String templateId)
  {
    showTemplate(templateId,
        event.getLocation().getQueryParameters().getSingleParameter("noframe").isEmpty());
  }

  private void discoverTemplates()
  {
    VaadinConfigurationProperties vaadinConfig =
        Instantiator.get(UI.getCurrent()).getOrCreate(VaadinConfigurationProperties.class);

    List<String> packagesToBeScanned = new ArrayList<>();
    packagesToBeScanned.addAll(vaadinConfig.getAllowedPackages());


    ClassPathScanningCandidateComponentProvider provider =
        new ClassPathScanningCandidateComponentProvider(false);
    provider.addIncludeFilter(new AssignableTypeFilter(TemplateComposite.class));
    provider.addIncludeFilter(new AnnotationTypeFilter(Template.class));
    provider.addIncludeFilter(new AssignableTypeFilter(FragmentComposite.class));
    provider.addIncludeFilter(new AnnotationTypeFilter(Fragment.class));

    List<Class<?>> foundTemplateHosts = new ArrayList<>();
    for (String pkg : packagesToBeScanned)
    {
      for (BeanDefinition beanDef : provider.findCandidateComponents(pkg))
      {
        try
        {
          Class<?> hostClass = Class.forName(beanDef.getBeanClassName());
          foundTemplateHosts.add(hostClass);
        }
        catch (ClassNotFoundException ex)
        {
          // just skip
        }
      }
    }

    Collections.sort(foundTemplateHosts, Comparator.comparing(Class::getName));

    commonIdPrefix =
        getCommonPrefix(foundTemplateHosts.stream().map(Class::getName).toArray(String[]::new));
    if (!commonIdPrefix.endsWith("."))
      commonIdPrefix = substringBeforeLast(commonIdPrefix, ".") + ".";

    Class<?> selectedTemplateHost = templatesListBox.getValue();
    templatesListBox.setItems(foundTemplateHosts);
    templatesListBox.setValue(selectedTemplateHost);
  }

  private void reloadCurrentTemplate()
  {
    showTemplate(this.currentHostClass, null, withFrame);
  }

  public void showTemplate(Class<?> hostClass)
  {
    showTemplate(hostClass, null, this.withFrame);
  }

  public void showTemplate(String hostClassName, boolean withFrame)
  {
    showTemplate(null, hostClassName, withFrame);
  }

  private void showTemplate(Class<?> hostClass, String hostClassName, boolean withFrame)
  {
    Component templateComponent = null;
    Exception exception = null;

    try
    {
      if (hostClass == null && hostClassName != null)
      {
        hostClass = Class.forName(hostClassName);
      }

      if (templatesListBox.getListDataView().contains(hostClass))
      {
        templatesListBox.setValue(hostClass);
        templatesListBox.getDataProvider().refreshItem(hostClass);
      }
      else
      {
        templatesListBox.clear();
      }

      if (currentHostClass != null)
        templatesListBox.getDataProvider().refreshItem(currentHostClass);


      this.currentHostClass = hostClass;
      this.withFrame = withFrame;

      updateRouteParameters();

      if (hostClass == null)
      {
        content.removeAll();
        content.addAndExpand(previewFrame);
        templateContainer.removeAll();
        templateField.clear();
        return;
      }


      TemplateEngine templateEngine = TemplateEngine.get();

      if (templateEngine.isTemplateHost(hostClass))
      {
        templateComponent = templateEngine.parseTemplate(null, hostClass, null).getRootComponent();
      }
      else if (templateEngine.isTemplateFragmentHost(hostClass))
      {
        templateComponent =
            templateEngine.parseTemplateFragment(null, null, hostClass, null).getRootComponent();
      }
      else
      {
        exception = new RuntimeException("Class is neither template nor fragment host.");
      }
    }
    catch (ClassNotFoundException | TemplateException ex)
    {
      exception = ex;
    }

    if (withFrame)
    {
      content.removeAll();
      content.addAndExpand(previewFrame);

      templateField.setValue(currentHostClass.getName());

      templateContainer.removeAll();
      if (templateComponent != null)
      {
        templateContainer.add(templateComponent);
      }
      else if (exception != null)
      {
        templateContainer.add(getErrorComponent(exception));
      }
      else
      {
        templateContainer.add(new Div("not found"));
      }
    }
    else
    {
      content.removeAll();
      if (templateComponent != null)
      {
        content.add(templateComponent);
      }
      else if (exception != null)
      {
        content.add(getErrorComponent(exception));
      }
      else
      {
        content.add(new Div("not found"));
      }
    }

    showingReport = false;
  }

  private void updateRouteParameters()
  {
    getUI().ifPresent(ui ->
    {
      String url = RouteConfiguration.forSessionScope().getUrl(TemplatePreviewView.class,
          currentHostClass == null ? null : currentHostClass.getName());
      if (!withFrame)
        url += "?noframe";
      ui.getPage().getHistory().pushState(null, url);
    });
  }

  private Component getErrorComponent(Exception exception)
  {
    Div result = new Div();
    result.setWhiteSpace(WhiteSpace.PRE_WRAP);
    result.setText(
        "Failed to create preview of template.\n\n" + ExceptionUtils.getStackTrace(exception));
    return result;
  }

  public void showCurrentReport()
  {
    if (currentHostClass == null)
      return;

    TemplateReport report = TemplateEngine.get().validateTemplate(currentHostClass);

    StringBuilder text = new StringBuilder();

    boolean first = true;
    for (TemplateReport.Entry entry : report.getEntries())
    {
      if (first)
        first = false;
      else
        text.append("\n\n");

      text.append(entry.toString());

      if (entry.getException() != null)
        text.append("\n").append(ExceptionUtils.getStackTrace(entry.getException()));
    }
    templateContainer.removeAll();

    templateContainer.add(new H3("Template Report"));

    Paragraph p = new Paragraph(text.toString());
    p.setWhiteSpace(WhiteSpace.PRE_WRAP);
    templateContainer.add(p);


    showingReport = true;
  }

}
