package de.codecamp.vaadin.flowdui.factories;


import static de.codecamp.vaadin.flowdui.factories.CommonComponentPostProcessor.ATTR_HIDDEN;
import static de.codecamp.vaadin.flowdui.factories.CommonComponentPostProcessor.ATTR_ID;

import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    componentType = Component.class,
    attributes = { //
        @DuiAttribute(name = ATTR_ID, type = String.class), //
        @DuiAttribute(name = ATTR_HIDDEN, type = Boolean.class, custom = true) //
    })
public class CommonComponentPostProcessor
  implements
    ComponentPostProcessor
{

  public static final String ATTR_ID = "id";

  public static final String ATTR_HIDDEN = "hidden";


  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    context.mapAttribute(ATTR_ID).asString().to(component::setId);
    context.mapAttribute(ATTR_HIDDEN).asBoolean().to(v -> component.setVisible(!v));
  }

}
