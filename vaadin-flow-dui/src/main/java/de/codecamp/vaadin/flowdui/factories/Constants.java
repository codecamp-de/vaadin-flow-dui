package de.codecamp.vaadin.flowdui.factories;


public final class Constants
{

  public static final String CATEGORY_DATA_ENTRY = "Data Entry";

  public static final String CATEGORY_VISUALIZATION_AND_INTERACTION = "Visualization & Interaction";

  public static final String CATEGORY_LAYOUTS = "Layouts";


  private Constants()
  {
    // utility class
  }

}
