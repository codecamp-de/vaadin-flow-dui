package de.codecamp.vaadin.flowdui.factories;


import static de.codecamp.vaadin.flowdui.factories.FocusablePostProcessor.ATTR_TABINDEX;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Focusable;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    componentType = Focusable.class,
    attributes = { //
        @DuiAttribute(name = ATTR_TABINDEX, type = Integer.class) //
    })
public class FocusablePostProcessor
  implements
    ComponentPostProcessor
{

  public static final String ATTR_TABINDEX = "tabindex";


  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    if (component instanceof Focusable)
    {
      Focusable<?> focusable = (Focusable<?>) component;

      context.mapAttribute(ATTR_TABINDEX).asInteger().to(focusable::setTabIndex);
    }
  }

}
