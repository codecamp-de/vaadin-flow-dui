package de.codecamp.vaadin.flowdui.factories;


import static de.codecamp.vaadin.flowdui.factories.GenericComponentFactory.ATTR_CLASS;
import static de.codecamp.vaadin.flowdui.factories.GenericComponentFactory.TAG_COMPONENT;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.di.Instantiator;
import com.vaadin.flow.internal.ReflectTools;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import org.jsoup.nodes.Attribute;
import org.springframework.beans.BeanUtils;


@DuiComponent(
    tagName = TAG_COMPONENT,
    customTag = true,
    componentType = Component.class,
    description = "Allows to create components without a dedicated factory. After post-processors"
        + " have been applied, the still unconsumed attributes are copied over as is to the Vaadin"
        + " element. Mapping to setters is not (yet?) possible.",
    attributes = { //
        @DuiAttribute(
            name = ATTR_CLASS,
            type = String.class,
            required = true,
            custom = true,
            description = "The fully qualified class name of the component which requires a public"
                + " no-args constructor.") //
    })
public class GenericComponentFactory
  implements
    ComponentFactory,
    ComponentPostProcessor
{

  public static final String TAG_COMPONENT = "component";

  public static final String ATTR_TYPE = "type";

  public static final String ATTR_CLASS = "class";


  private static final Pattern PATTERN_HYPHENATED_TO_CAMEL_CASE = Pattern.compile("-([a-z])");


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_COMPONENT:
      {
        String className = context.mapAttribute(ATTR_CLASS, Set.of(ATTR_TYPE)).asString()
            .getRequired("The class name of the Component must be specified.");

        Class<?> clazz;
        try
        {
          clazz = Class.forName(className);
        }
        catch (ClassNotFoundException ex)
        {
          String msg = "The class '%s' could not be found.";
          msg = String.format(msg, className);
          throw context.fail(ATTR_TYPE, msg, ex);
        }

        if (!Component.class.isAssignableFrom(clazz))
        {
          String msg = "The class '%s' is not a Vaadin Component.";
          msg = String.format(msg, className);
          throw context.fail(ATTR_TYPE, msg);
        }

        Class<? extends Component> componentClass = clazz.asSubclass(Component.class);
        Component component;

        UI ui = UI.getCurrent();
        if (ui == null)
        {
          component = ReflectTools.createInstance(componentClass);
        }
        else
        {
          component = Instantiator.get(ui).getOrCreate(componentClass);
        }

        context.readChildren(component);

        return component;
      }
    }

    return null;
  }

  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    if (context.getElement().tagName().equals(TAG_COMPONENT))
    {
      /*
       * Map all attributes to bean properties.
       */
      for (Attribute attr : context.getElement().attributes())
      {
        String attributeName = attr.getKey();
        if (context.getConsumedAttributes().contains(attributeName))
          continue;

        PropertyDescriptor propertyDescriptor;
        try
        {
          /*
           * Only BeanUtils from Spring considers getters/setters that are default implementations
           * provided by an interface. Everything implemented to the official Java Bean Spec
           * doesn't.
           */
          propertyDescriptor = BeanUtils.getPropertyDescriptor(component.getClass(), attributeName);
          if (propertyDescriptor == null)
          {
            propertyDescriptor = BeanUtils.getPropertyDescriptor(component.getClass(),
                hyphenatedToCamelCase(attributeName));
          }
          /*
           * Jsoup converts attribute names to lower case; there's no direct case insensitive
           * look-up.
           */
          if (propertyDescriptor == null)
          {
            propertyDescriptor = Stream.of(BeanUtils.getPropertyDescriptors(component.getClass()))
                .filter(desc -> desc.getName().toLowerCase(Locale.ROOT).equals(attributeName))
                .findFirst().orElse(null);
          }
        }
        catch (RuntimeException ex)
        {
          throw context.fail("An error occurred during Bean property lookup.", ex);
        }

        if (propertyDescriptor != null)
        {
          Method writeMethod = propertyDescriptor.getWriteMethod();
          if (writeMethod == null)
            continue;

          context.mapAttribute(attributeName).as(propertyDescriptor.getPropertyType()).to(value ->
          {
            try
            {
              writeMethod.invoke(component, value);
            }
            catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex)
            {
              context.fail("An error occurred while assigning the Bean property value.", ex);
            }
          });
        }
      }
    }
  }

  private static String hyphenatedToCamelCase(String s)
  {
    return PATTERN_HYPHENATED_TO_CAMEL_CASE.matcher(s)
        .replaceAll(match -> Character.toString(Character.toUpperCase(match.group().charAt(1))));
  }

}
