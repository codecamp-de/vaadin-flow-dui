package de.codecamp.vaadin.flowdui.factories;


import static de.codecamp.vaadin.flowdui.factories.HasAriaLabelPostProcessor.ATTR_ARIA_LABEL;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasAriaLabel;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    componentType = HasAriaLabel.class,
    attributes = { //
        @DuiAttribute(name = ATTR_ARIA_LABEL, type = String.class) //
    })
public class HasAriaLabelPostProcessor
  implements
    ComponentPostProcessor
{

  public static final String ATTR_ARIA_LABEL = "aria-label";


  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    if (component instanceof HasAriaLabel)
    {
      HasAriaLabel hasAriaLabel = (HasAriaLabel) component;
      context.mapAttribute(ATTR_ARIA_LABEL).asString().to(hasAriaLabel::setAriaLabel);
    }
  }

}
