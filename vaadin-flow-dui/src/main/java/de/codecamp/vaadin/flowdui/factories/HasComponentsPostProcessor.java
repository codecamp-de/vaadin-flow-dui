package de.codecamp.vaadin.flowdui.factories;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.TemplateParserContext;
import org.jsoup.nodes.Element;


/**
 * A generic handler for {@link HasComponents} that will only be used as a last resort.
 */
public class HasComponentsPostProcessor
  implements
    ComponentPostProcessor
{

  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    // nothing to do
  }

  @Override
  public boolean handleChildElement(Component parentComponent, String slotName,
      Element childElement, TemplateParserContext context)
  {
    if (parentComponent instanceof HasComponents)
    {
      switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
      {
        case SLOT_DEFAULT:
          HasComponents hasComponents = (HasComponents) parentComponent;
          hasComponents.add(context.readComponentForSlot(childElement));
          return true;
      }
    }

    return false;
  }

}
