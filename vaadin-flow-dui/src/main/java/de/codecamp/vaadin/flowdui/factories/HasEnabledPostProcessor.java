package de.codecamp.vaadin.flowdui.factories;


import static de.codecamp.vaadin.flowdui.factories.HasEnabledPostProcessor.ATTR_DISABLED;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasEnabled;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    componentType = HasEnabled.class,
    attributes = { //
        @DuiAttribute(name = ATTR_DISABLED, type = Boolean.class) //
    })
public class HasEnabledPostProcessor
  implements
    ComponentPostProcessor
{

  public static final String ATTR_DISABLED = "disabled";


  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    if (component instanceof HasEnabled)
    {
      HasEnabled hasEnabled = (HasEnabled) component;
      context.mapAttribute(ATTR_DISABLED).asBoolean().to(v -> hasEnabled.setEnabled(!v));
    }
  }

}
