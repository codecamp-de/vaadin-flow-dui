package de.codecamp.vaadin.flowdui.factories;


import static de.codecamp.vaadin.flowdui.factories.HasHelperPostProcessor.ATTR_HELPER_TEXT;
import static de.codecamp.vaadin.flowdui.factories.HasHelperPostProcessor.SLOT_HELPER;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasHelper;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.TemplateParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import org.jsoup.nodes.Element;


@DuiComponent(
    componentType = HasHelper.class,
    attributes = { //
        @DuiAttribute(name = ATTR_HELPER_TEXT, type = String.class) //
    },
    slots = {SLOT_HELPER})
public class HasHelperPostProcessor
  implements
    ComponentPostProcessor
{

  public static final String ATTR_HELPER_TEXT = "helper-text";

  public static final String SLOT_HELPER = "helper";


  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    if (component instanceof HasHelper)
    {
      HasHelper hasHelper = (HasHelper) component;
      context.mapAttribute(ATTR_HELPER_TEXT).asString().to(hasHelper::setHelperText);
    }
  }

  @Override
  public boolean handleChildElement(Component parentComponent, String slotName,
      Element childElement, TemplateParserContext context)
  {
    if (parentComponent instanceof HasHelper)
    {
      switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
      {
        case SLOT_HELPER:
          HasHelper hasHelper = (HasHelper) parentComponent;
          hasHelper.setHelperComponent(context.readComponentForSlot(childElement));
          return true;
      }
    }

    return false;
  }

}
