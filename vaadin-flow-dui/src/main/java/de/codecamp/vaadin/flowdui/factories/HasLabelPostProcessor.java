package de.codecamp.vaadin.flowdui.factories;


import static de.codecamp.vaadin.flowdui.factories.HasLabelPostProcessor.ATTR_LABEL;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasLabel;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    componentType = HasLabel.class,
    attributes = { //
        @DuiAttribute(name = ATTR_LABEL, type = String.class) //
    })
public class HasLabelPostProcessor
  implements
    ComponentPostProcessor
{

  public static final String ATTR_LABEL = "label";


  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    if (component instanceof HasLabel)
    {
      HasLabel hasLabel = (HasLabel) component;
      context.mapAttribute(ATTR_LABEL).asString().to(hasLabel::setLabel);
    }
  }

}
