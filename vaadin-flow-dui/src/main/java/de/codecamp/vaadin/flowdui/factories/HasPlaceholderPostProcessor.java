package de.codecamp.vaadin.flowdui.factories;


import static de.codecamp.vaadin.flowdui.factories.HasPlaceholderPostProcessor.ATTR_PLACEHOLDER;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasPlaceholder;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    componentType = HasPlaceholder.class,
    attributes = { //
        @DuiAttribute(name = ATTR_PLACEHOLDER, type = String.class) //
    })
public class HasPlaceholderPostProcessor
  implements
    ComponentPostProcessor
{

  public static final String ATTR_PLACEHOLDER = "placeholder";


  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    if (component instanceof HasPlaceholder)
    {
      HasPlaceholder hasPlaceholder = (HasPlaceholder) component;
      context.mapAttribute(ATTR_PLACEHOLDER).asString().to(hasPlaceholder::setPlaceholder);
    }
  }

}
