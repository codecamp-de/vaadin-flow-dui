package de.codecamp.vaadin.flowdui.factories;


import static de.codecamp.vaadin.flowdui.declare.DuiComponent.SLOT_DEFAULT;
import static de.codecamp.vaadin.flowdui.factories.HasSingleComponentPostProcessor.CATTR_HEIGHT_FULL;
import static de.codecamp.vaadin.flowdui.factories.HasSingleComponentPostProcessor.CATTR_SIZE_FULL;
import static de.codecamp.vaadin.flowdui.factories.HasSingleComponentPostProcessor.CATTR_WIDTH_FULL;

import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.base.HasSingleComponent;
import de.codecamp.vaadin.base.util.SizeUtils;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.TemplateParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import org.jsoup.nodes.Element;


@DuiComponent(
    componentType = HasSingleComponent.class,
    description = "Can only have a single child component as content.",
    slots = {SLOT_DEFAULT},
    childAttributes = { //
        @DuiAttribute(name = CATTR_WIDTH_FULL, type = Boolean.class),
        @DuiAttribute(name = CATTR_HEIGHT_FULL, type = Boolean.class),
        @DuiAttribute(name = CATTR_SIZE_FULL, type = Boolean.class) //
    })
public class HasSingleComponentPostProcessor
  implements
    ComponentPostProcessor
{

  public static final String CATTR_WIDTH_FULL = SizeUtils.ATTR_WIDTH_FULL;

  public static final String CATTR_HEIGHT_FULL = SizeUtils.ATTR_HEIGHT_FULL;

  public static final String CATTR_SIZE_FULL = SizeUtils.ATTR_SIZE_FULL;


  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    // nothing to do
  }

  @Override
  public boolean handleChildElement(Component parentComponent, String slotName,
      Element childElement, TemplateParserContext context)
  {
    if (parentComponent instanceof HasSingleComponent)
    {
      switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
      {
        case SLOT_DEFAULT:
          HasSingleComponent hasSingleComponent = (HasSingleComponent) parentComponent;
          if (hasSingleComponent.getContent() != null)
            throw context.fail(childElement, "Only one content component supported.");
          context.readComponent(childElement, (childComponent, childContext) ->
          {
            hasSingleComponent.setContent(childComponent);

            childContext.mapAttribute(CATTR_SIZE_FULL).asBoolean()
                .to(v -> SizeUtils.setSizeFull(childComponent));
            childContext.mapAttribute(CATTR_WIDTH_FULL).asBoolean()
                .to(v -> SizeUtils.setWidthFull(childComponent));
            childContext.mapAttribute(CATTR_HEIGHT_FULL).asBoolean()
                .to(v -> SizeUtils.setHeightFull(childComponent));
          });
          return true;
      }
    }

    return false;
  }

}
