package de.codecamp.vaadin.flowdui.factories;


import static de.codecamp.vaadin.flowdui.factories.HasSizePostProcessor.ATTR_HEIGHT;
import static de.codecamp.vaadin.flowdui.factories.HasSizePostProcessor.ATTR_HEIGHT_AUTO;
import static de.codecamp.vaadin.flowdui.factories.HasSizePostProcessor.ATTR_HEIGHT_FULL;
import static de.codecamp.vaadin.flowdui.factories.HasSizePostProcessor.ATTR_MAX_HEIGHT;
import static de.codecamp.vaadin.flowdui.factories.HasSizePostProcessor.ATTR_MAX_WIDTH;
import static de.codecamp.vaadin.flowdui.factories.HasSizePostProcessor.ATTR_MIN_HEIGHT;
import static de.codecamp.vaadin.flowdui.factories.HasSizePostProcessor.ATTR_MIN_WIDTH;
import static de.codecamp.vaadin.flowdui.factories.HasSizePostProcessor.ATTR_SIZE_AUTO;
import static de.codecamp.vaadin.flowdui.factories.HasSizePostProcessor.ATTR_SIZE_FULL;
import static de.codecamp.vaadin.flowdui.factories.HasSizePostProcessor.ATTR_WIDTH;
import static de.codecamp.vaadin.flowdui.factories.HasSizePostProcessor.ATTR_WIDTH_AUTO;
import static de.codecamp.vaadin.flowdui.factories.HasSizePostProcessor.ATTR_WIDTH_FULL;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasSize;
import de.codecamp.vaadin.base.util.SizeUtils;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    componentType = HasSize.class,
    attributes = { //
        @DuiAttribute(
            name = ATTR_SIZE_FULL,
            type = Boolean.class,
            custom = true,
            description = "Sets width and height to 100%. Layouts usually offer better and safer options to do this."), //
        @DuiAttribute(name = ATTR_SIZE_AUTO, type = Boolean.class, custom = true), //
        @DuiAttribute(name = ATTR_WIDTH, type = String.class, custom = true), //
        @DuiAttribute(
            name = ATTR_WIDTH_FULL,
            type = Boolean.class,
            custom = true,
            description = "Sets the width to 100%. Layouts usually offer better and safer options to do this."), //
        @DuiAttribute(name = ATTR_WIDTH_AUTO, type = Boolean.class, custom = true), //
        @DuiAttribute(name = ATTR_MIN_WIDTH, type = String.class, custom = true), //
        @DuiAttribute(name = ATTR_MAX_WIDTH, type = String.class, custom = true), //
        @DuiAttribute(name = ATTR_HEIGHT, type = String.class, custom = true), //
        @DuiAttribute(
            name = ATTR_HEIGHT_FULL,
            type = Boolean.class,
            custom = true,
            description = "Sets the height to 100%. Layouts usually offer better and safer options to do this."), //
        @DuiAttribute(name = ATTR_HEIGHT_AUTO, type = Boolean.class, custom = true), //
        @DuiAttribute(name = ATTR_MIN_HEIGHT, type = String.class, custom = true), //
        @DuiAttribute(name = ATTR_MAX_HEIGHT, type = String.class, custom = true) //
    })
@DuiComponent(
    componentType = Component.class,
    attributes = { //
        @DuiAttribute(
            name = ATTR_SIZE_FULL,
            type = Boolean.class,
            custom = true,
            description = "Sets width and height to 100%. Layouts usually offer better and safer options to do this."), //
        @DuiAttribute(name = ATTR_SIZE_AUTO, type = Boolean.class, custom = true), //
        @DuiAttribute(name = ATTR_WIDTH, type = String.class, custom = true), //
        @DuiAttribute(
            name = ATTR_WIDTH_FULL,
            type = Boolean.class,
            custom = true,
            description = "Sets the width to 100%. Layouts usually offer better and safer options to do this."), //
        @DuiAttribute(name = ATTR_WIDTH_AUTO, type = Boolean.class, custom = true), //
        @DuiAttribute(name = ATTR_MIN_WIDTH, type = String.class, custom = true), //
        @DuiAttribute(name = ATTR_MAX_WIDTH, type = String.class, custom = true), //
        @DuiAttribute(name = ATTR_HEIGHT, type = String.class, custom = true), //
        @DuiAttribute(
            name = ATTR_HEIGHT_FULL,
            type = Boolean.class,
            custom = true,
            description = "Sets the height to 100%. Layouts usually offer better and safer options to do this."), //
        @DuiAttribute(name = ATTR_HEIGHT_AUTO, type = Boolean.class, custom = true), //
        @DuiAttribute(name = ATTR_MIN_HEIGHT, type = String.class, custom = true), //
        @DuiAttribute(name = ATTR_MAX_HEIGHT, type = String.class, custom = true) //
    })
public class HasSizePostProcessor
  implements
    ComponentPostProcessor
{

  public static final String ATTR_WIDTH = "width";

  public static final String ATTR_WIDTH_FULL = SizeUtils.ATTR_WIDTH_FULL;

  public static final String ATTR_WIDTH_AUTO = "width-auto";

  public static final String ATTR_MIN_WIDTH = "min-width";

  public static final String ATTR_MAX_WIDTH = "max-width";

  public static final String ATTR_HEIGHT = "height";

  public static final String ATTR_HEIGHT_FULL = SizeUtils.ATTR_HEIGHT_FULL;

  public static final String ATTR_HEIGHT_AUTO = "height-auto";

  public static final String ATTR_MIN_HEIGHT = "min-height";

  public static final String ATTR_MAX_HEIGHT = "max-height";

  public static final String ATTR_SIZE_FULL = SizeUtils.ATTR_SIZE_FULL;

  public static final String ATTR_SIZE_AUTO = "size-auto";


  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    context.mapAttribute(ATTR_SIZE_FULL).asBoolean().to(v -> SizeUtils.setSizeFull(component));
    context.mapAttribute(ATTR_SIZE_AUTO).asBoolean().to(v -> SizeUtils.setSizeAuto(component));

    context.mapAttribute(ATTR_WIDTH).asString().to(v -> SizeUtils.setWidth(component, v));
    context.mapAttribute(ATTR_WIDTH_FULL).asBoolean().to(v -> SizeUtils.setWidthFull(component));
    context.mapAttribute(ATTR_WIDTH_AUTO).asBoolean().to(v -> SizeUtils.setWidthAuto(component));
    context.mapAttribute(ATTR_MIN_WIDTH).asString().to(v -> SizeUtils.setMinWidth(component, v));
    context.mapAttribute(ATTR_MAX_WIDTH).asString().to(v -> SizeUtils.setMaxWidth(component, v));

    context.mapAttribute(ATTR_HEIGHT).asString().to(v -> SizeUtils.setHeight(component, v));
    context.mapAttribute(ATTR_HEIGHT_FULL).asBoolean().to(v -> SizeUtils.setHeightFull(component));
    context.mapAttribute(ATTR_HEIGHT_AUTO).asBoolean().to(v -> SizeUtils.setHeightAuto(component));
    context.mapAttribute(ATTR_MIN_HEIGHT).asString().to(v -> SizeUtils.setMinHeight(component, v));
    context.mapAttribute(ATTR_MAX_HEIGHT).asString().to(v -> SizeUtils.setMaxHeight(component, v));
  }

}
