package de.codecamp.vaadin.flowdui.factories;


import static de.codecamp.vaadin.flowdui.factories.HasStylePostProcessor.ATTR_CLASS;
import static de.codecamp.vaadin.flowdui.factories.HasStylePostProcessor.ATTR_STYLE;
import static de.codecamp.vaadin.flowdui.factories.HasStylePostProcessor.ATTR_STYLE_PREFIX;
import static java.util.stream.Collectors.toList;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.dom.ClassList;
import com.vaadin.flow.dom.Style;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Element;


/**
 * Handles the "class" and "style" attributes on all components. When implemented, the
 * {@link HasStyle} interface will be used to set them; otherwise the {@link Element} will be
 * accessed directly.
 */
@DuiComponent(
    componentType = HasStyle.class,
    attributes = { //
        @DuiAttribute(name = ATTR_CLASS, type = String.class), //
        @DuiAttribute(name = ATTR_STYLE, type = String.class),
        @DuiAttribute(
            name = ATTR_STYLE_PREFIX,
            type = String.class,
            custom = true,
            description = "CSS properties can be set individually as attributes using this syntax:"
                + " <code>s:css-property=&quot;value&quot;</code>, e.g. <code>s:background-color=&quot;red&quot;</code>.")//
    })
@DuiComponent(
    componentType = Component.class,
    attributes = { //
        @DuiAttribute(name = ATTR_CLASS, type = String.class), //
        @DuiAttribute(name = ATTR_STYLE, type = String.class), //
        @DuiAttribute(
            name = ATTR_STYLE_PREFIX,
            type = String.class,
            custom = true,
            description = "CSS properties can be set individually as attributes using this syntax:"
                + " <code>s:css-property=&quot;value&quot;</code>, e.g. <code>s:background-color=&quot;red&quot;</code>.") //
    })
public class HasStylePostProcessor
  implements
    ComponentPostProcessor
{

  public static final String ATTR_CLASS = "class";

  public static final String ATTR_CLASS_PREFIX = "c:";

  public static final String ATTR_STYLE = "style";

  public static final String ATTR_STYLE_PREFIX = "s:";


  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    //  map the "class" attribute
    context.mapAttribute(ATTR_CLASS)
        .asConverted(
            v -> Stream.of(v.split("\\s+")).filter(StringUtils::isNotEmpty).collect(toList()))
        .to(v -> component.getClassNames().addAll(v));

    //  map classes with the "c:" syntax
    if (context.getElement().attributesSize() > 0)
    {
      ClassList classList = null;
      for (Attribute attribute : context.getElement().attributes())
      {
        String attributeName = attribute.getKey();
        if (attributeName.startsWith(ATTR_CLASS_PREFIX))
        {
          if (classList == null)
            classList = component.getClassNames();
          classList.set(attributeName.substring(ATTR_CLASS_PREFIX.length()), true);
          context.getConsumedAttributes().add(attributeName);
        }
      }
    }


    //  map the "style" attribute
    context.mapAttribute(ATTR_STYLE).asString().to(styleString ->
    {
      for (String propertyString : styleString.split(";"))
      {
        String[] propertyTokens = propertyString.trim().split(":", 2);
        String property = propertyTokens[0];
        String value = propertyTokens[1];

        component.getStyle().set(property, value);
      }
    });

    //  map styles with the "s:" syntax
    if (context.getElement().attributesSize() > 0)
    {
      Style style = null;
      for (Attribute attribute : context.getElement().attributes())
      {
        String attributeName = attribute.getKey();
        if (attributeName.startsWith(ATTR_STYLE_PREFIX))
        {
          if (style == null)
            style = component.getStyle();
          style.set(attributeName.substring(ATTR_STYLE_PREFIX.length()), attribute.getValue());
          context.getConsumedAttributes().add(attributeName);
        }
      }
    }
  }

}
