package de.codecamp.vaadin.flowdui.factories;


import static de.codecamp.vaadin.flowdui.factories.HasThemePostProcessor.ATTR_THEME;
import static java.util.stream.Collectors.toList;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasTheme;
import com.vaadin.flow.dom.ThemeList;
import com.vaadin.flow.dom.impl.ThemeListImpl;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Element;


/**
 * Handles the "theme" attribute on all components. When implemented, the {@link HasTheme} interface
 * will be used to set it; otherwise the {@link Element} will be accessed directly.
 */
@DuiComponent(
    componentType = HasTheme.class,
    attributes = { //
        @DuiAttribute(name = ATTR_THEME, type = String.class) //
    })
@DuiComponent(
    componentType = Component.class,
    attributes = { //
        @DuiAttribute(name = ATTR_THEME, type = String.class) //
    })
public class HasThemePostProcessor
  implements
    ComponentPostProcessor
{

  public static final String ATTR_THEME = ThemeListImpl.THEME_ATTRIBUTE_NAME;

  public static final String ATTR_THEME_PREFIX = "t:";


  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    context.mapAttribute(ATTR_THEME)
        .asConverted(
            v -> Stream.of(v.split("\\s+")).filter(StringUtils::isNotEmpty).collect(toList()))
        .to(v -> getThemeList(component).addAll(v));

    //  map themes with the "t:" syntax
    if (context.getElement().attributesSize() > 0)
    {
      ThemeList themeList = null;
      for (Attribute attribute : context.getElement().attributes())
      {
        String attributeName = attribute.getKey();
        if (attributeName.startsWith(ATTR_THEME_PREFIX))
        {
          if (themeList == null)
            themeList = getThemeList(component);
          themeList.set(attributeName.substring(ATTR_THEME_PREFIX.length()), true);
          context.getConsumedAttributes().add(attributeName);
        }
      }
    }
  }

  private static ThemeList getThemeList(Component component)
  {
    if (component instanceof HasTheme)
    {
      HasTheme hasTheme = (HasTheme) component;
      return hasTheme.getThemeNames();
    }
    else
    {
      return component.getElement().getThemeList();
    }
  }

}
