package de.codecamp.vaadin.flowdui.factories;


import static de.codecamp.vaadin.flowdui.factories.HasValidationPostProcessor.ATTR_ERROR_MESSAGE;
import static de.codecamp.vaadin.flowdui.factories.HasValidationPostProcessor.ATTR_INVALID;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasValidation;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    componentType = HasValidation.class,
    attributes = { //
        @DuiAttribute(name = ATTR_INVALID, type = Boolean.class),
        @DuiAttribute(name = ATTR_ERROR_MESSAGE, type = String.class) //
    })
public class HasValidationPostProcessor
  implements
    ComponentPostProcessor
{

  public static final String ATTR_INVALID = "invalid";

  public static final String ATTR_ERROR_MESSAGE = "error-message";


  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    if (component instanceof HasValidation)
    {
      HasValidation hasValidation = (HasValidation) component;
      context.mapAttribute(ATTR_INVALID).asBoolean().to(hasValidation::setInvalid);
      context.mapAttribute(ATTR_ERROR_MESSAGE).asString().to(hasValidation::setErrorMessage);
    }
  }

}
