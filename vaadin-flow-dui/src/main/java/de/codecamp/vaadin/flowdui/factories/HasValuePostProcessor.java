package de.codecamp.vaadin.flowdui.factories;


import static de.codecamp.vaadin.flowdui.factories.HasValuePostProcessor.ATTR_READONLY;
import static de.codecamp.vaadin.flowdui.factories.HasValuePostProcessor.ATTR_REQUIRED;
import static de.codecamp.vaadin.flowdui.factories.HasValuePostProcessor.ATTR_VALUE;

import com.googlecode.gentyref.GenericTypeReflector;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasValue;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;


@DuiComponent(
    componentType = HasValue.class,
    attributes = { //
        @DuiAttribute(name = ATTR_REQUIRED, type = Boolean.class),
        @DuiAttribute(name = ATTR_READONLY, type = Boolean.class),
        @DuiAttribute(name = ATTR_VALUE, type = Object.class) //
    })
public class HasValuePostProcessor
  implements
    ComponentPostProcessor
{

  public static final String ATTR_REQUIRED = "required";

  public static final String ATTR_READONLY = "readonly";

  public static final String ATTR_VALUE = "value";


  @Override
  @SuppressWarnings("unchecked")
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    if (component instanceof HasValue)
    {
      HasValue<?, ?> hasValue = (HasValue<?, ?>) component;
      context.mapAttribute(ATTR_REQUIRED).asBoolean().to(hasValue::setRequiredIndicatorVisible);
      context.mapAttribute(ATTR_READONLY).asBoolean().to(hasValue::setReadOnly);

      Type valueType = GenericTypeReflector.getTypeParameter(component.getClass(),
          HasValue.class.getTypeParameters()[1]);
      if (valueType == Boolean.class)
      {
        context.mapAttribute(ATTR_VALUE).asString().to(v ->
        {
          boolean value;
          switch (v)
          {
            case "":
            case ATTR_VALUE:
            case "true":
            case "1":
            case "on":
              value = true;
              break;

            case "false":
            case "0":
            case "off":
              value = false;
              break;

            default:
              throw context.fail(ATTR_VALUE,
                  "Only true/false, 1/0, on/off and regular HTML way are supported for boolean component values.");
          }
          ((HasValue<?, Boolean>) hasValue).setValue(value);
        });
      }
      else if (valueType == Integer.class)
      {
        context.mapAttribute(ATTR_VALUE).asInteger()
            .to(((HasValue<?, Integer>) hasValue)::setValue);
      }
      else if (valueType == Double.class)
      {
        context.mapAttribute(ATTR_VALUE).asDouble().to(((HasValue<?, Double>) hasValue)::setValue);
      }
      else if (valueType == BigDecimal.class)
      {
        context.mapAttribute(ATTR_VALUE).asBigDecimal()
            .to(((HasValue<?, BigDecimal>) hasValue)::setValue);
      }
      else if (valueType == String.class)
      {
        context.mapAttribute(ATTR_VALUE).asString().to(((HasValue<?, String>) hasValue)::setValue);
      }
      else if (valueType == LocalDate.class)
      {
        context.mapAttribute(ATTR_VALUE).asLocalDate()
            .to(((HasValue<?, LocalDate>) hasValue)::setValue);
      }
      else if (valueType == LocalTime.class)
      {
        context.mapAttribute(ATTR_VALUE).asLocalTime()
            .to(((HasValue<?, LocalTime>) hasValue)::setValue);
      }
      else if (valueType == LocalDateTime.class)
      {
        context.mapAttribute(ATTR_VALUE).asLocalDateTime()
            .to(((HasValue<?, LocalDateTime>) hasValue)::setValue);
      }
    }
  }

}
