package de.codecamp.vaadin.flowdui.factories;


import static de.codecamp.vaadin.flowdui.factories.TemplateElementsFactory.ATTR_ID;
import static de.codecamp.vaadin.flowdui.factories.TemplateElementsFactory.ATTR_NAME;
import static de.codecamp.vaadin.flowdui.factories.TemplateElementsFactory.TAG_FRAGMENT;
import static de.codecamp.vaadin.flowdui.factories.TemplateElementsFactory.TAG_SLOT;

import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.Style;
import de.codecamp.vaadin.flowdui.TemplateSlot;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;


@DuiComponent(
    tagName = TemplateSlot.TAG,
    componentType = Component.class,
    description = "If you need to create a Component manually, you can add a slot tag, like for a Web Component, and annotate the field with your component with @Slotted(\"slotName\"). The component will be inserted into the slot.",
    attributes = { //
        @DuiAttribute(name = ATTR_NAME, type = String.class, required = true) //
    })
@DuiComponent(
    tagName = TAG_SLOT,
    componentType = Component.class,
    description = "If you need to create a Component manually, you can add a slot tag, like for a Web Component, and annotate the field with your component with @Slotted(\"slotName\"). The component will be inserted into the slot.",
    attributes = { //
        @DuiAttribute(name = ATTR_NAME, type = String.class, required = true) //
    })
@DuiComponent(
    tagName = TAG_FRAGMENT,
    componentType = Void.class,
    description = "Defines a UI fragment that is not rendered immediately, but can be created programmatically whenever and as often as you need it. See the example in the documentation on how to use fragments.",
    attributes = { //
        @DuiAttribute(name = ATTR_ID, type = String.class) //
    })
@DuiComponent(tagName = "style", componentType = Component.class, description = "Copied verbatim.")
public class TemplateElementsFactory
  implements
    ComponentFactory
{

  public static final String TAG_FRAGMENT = "fragment";

  public static final String TAG_SLOT = "slot";


  public static final String ATTR_SLOT = "slot";

  public static final String ATTR_NAME = "name";

  public static final String ATTR_ID = "id";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case "slot":
      case TemplateSlot.TAG:
      {
        String slotName = context.mapAttribute(ATTR_NAME).asString().getRequired();

        if (context.hasSlottedComponentsInfo())
        {
          Optional<Component> componentInSlot = context.getComponentInSlot(slotName);
          if (componentInSlot == null)
          {
            throw context
                .fail(String.format("No @Slotted component found for slot '%s'.", slotName));
          }

          if (componentInSlot.isPresent())
          {
            return componentInSlot.get();
          }
          else
          {
            AtomicReference<Component> defaultChild = new AtomicReference<>();
            // parse the default slot content from the template
            context.readChildren(null, (sn, childElement) ->
            {
              switch (sn) // NOPMD:SwitchStmtsShouldHaveDefault
              {
                case SLOT_DEFAULT:
                  defaultChild.set(context.readComponent(childElement));
                  return true;
              }
              return false;
            }, null);

            if (defaultChild.get() != null)
              return defaultChild.get();

            return new TemplateSlot(slotName);
          }
        }
        else
        {
          context.registerAvailableSlot(slotName);
          return new TemplateSlot(slotName);
        }
      }

      case Style.TAG:
      {
        Style style = new Style();
        style.getElement().setProperty("innerHTML", context.getElement().data());
        return style;
      }
    }

    return null;
  }

}
