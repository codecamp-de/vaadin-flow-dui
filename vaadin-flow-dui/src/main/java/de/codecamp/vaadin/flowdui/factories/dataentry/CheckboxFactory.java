package de.codecamp.vaadin.flowdui.factories.dataentry;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_DATA_ENTRY;
import static de.codecamp.vaadin.flowdui.factories.dataentry.CheckboxFactory.ATTR_CHECKED;
import static de.codecamp.vaadin.flowdui.factories.dataentry.CheckboxFactory.ATTR_INDETERMINATE;
import static de.codecamp.vaadin.flowdui.factories.dataentry.CheckboxFactory.TAG_VAADIN_CHECKBOX;
import static de.codecamp.vaadin.flowdui.factories.dataentry.CheckboxFactory.TAG_VAADIN_CHECKBOX_GROUP;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.checkbox.CheckboxGroup;
import de.codecamp.vaadin.base.i18n.ComponentI18n;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_CHECKBOX,
    componentType = Checkbox.class,
    docUrl = "https://vaadin.com/docs/latest/components/checkbox",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(name = ATTR_CHECKED, type = Boolean.class), //
        @DuiAttribute(name = ATTR_INDETERMINATE, type = Boolean.class) //
    })
@DuiComponent(
    tagName = TAG_VAADIN_CHECKBOX_GROUP,
    componentType = CheckboxGroup.class,
    docUrl = "https://vaadin.com/docs/latest/components/checkbox",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
    })
public class CheckboxFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_CHECKBOX = "vaadin-checkbox";

  public static final String TAG_VAADIN_CHECKBOX_GROUP = "vaadin-checkbox-group";


  public static final String SLOT_LABEL = "label";


  public static final String ATTR_CHECKED = "checked";

  public static final String ATTR_INDETERMINATE = "indeterminate";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_CHECKBOX:
      {
        Checkbox component = new Checkbox();
        context.mapAttribute(ATTR_CHECKED).asBoolean().to(component::setValue);
        context.mapAttribute(ATTR_INDETERMINATE).asBoolean().to(component::setIndeterminate);

        ComponentI18n.localize(component);

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_LABEL:
              component.setLabelComponent(context.readComponent(childElement));
              return true;
          }
          return false;
        }, null);

        return component;
      }

      case TAG_VAADIN_CHECKBOX_GROUP:
      {
        CheckboxGroup<?> component = new CheckboxGroup<>();

        ComponentI18n.localize(component);

        context.readChildren(component,
            "CheckboxGroup cannot be populated using a template. Use its Java API instead.");

        return component;
      }
    }

    return null;
  }

}
