package de.codecamp.vaadin.flowdui.factories.dataentry;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_DATA_ENTRY;
import static de.codecamp.vaadin.flowdui.factories.dataentry.ComboBoxFactory.ATTR_ALLOW_CUSTOM_VALUE;
import static de.codecamp.vaadin.flowdui.factories.dataentry.ComboBoxFactory.ATTR_AUTOFOCUS;
import static de.codecamp.vaadin.flowdui.factories.dataentry.ComboBoxFactory.ATTR_AUTO_EXPAND_HORIZONTALLY;
import static de.codecamp.vaadin.flowdui.factories.dataentry.ComboBoxFactory.ATTR_AUTO_EXPAND_VERTICALLY;
import static de.codecamp.vaadin.flowdui.factories.dataentry.ComboBoxFactory.ATTR_KEEP_FILTER;
import static de.codecamp.vaadin.flowdui.factories.dataentry.ComboBoxFactory.ATTR_SELECTED_ITEMS_ON_TOP;
import static de.codecamp.vaadin.flowdui.factories.dataentry.ComboBoxFactory.TAG_VAADIN_COMBO_BOX;
import static de.codecamp.vaadin.flowdui.factories.dataentry.ComboBoxFactory.TAG_VAADIN_MULTI_SELECT_COMBO_BOX;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.combobox.MultiSelectComboBox;
import com.vaadin.flow.component.combobox.MultiSelectComboBox.AutoExpandMode;
import de.codecamp.vaadin.base.i18n.ComponentI18n;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_COMBO_BOX,
    componentType = ComboBox.class,
    docUrl = "https://vaadin.com/docs/latest/components/combo-box",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(name = ATTR_AUTOFOCUS, type = Boolean.class), //
        @DuiAttribute(name = ATTR_ALLOW_CUSTOM_VALUE, type = Boolean.class) //
    })
@DuiComponent(
    tagName = TAG_VAADIN_MULTI_SELECT_COMBO_BOX,
    componentType = MultiSelectComboBox.class,
    docUrl = "https://vaadin.com/docs/latest/components/multi-select-combo-box",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(name = ATTR_AUTOFOCUS, type = Boolean.class), //
        @DuiAttribute(name = ATTR_ALLOW_CUSTOM_VALUE, type = Boolean.class), //
        @DuiAttribute(name = ATTR_SELECTED_ITEMS_ON_TOP, type = Boolean.class), //
        @DuiAttribute(name = ATTR_AUTO_EXPAND_HORIZONTALLY, type = Boolean.class), //
        @DuiAttribute(name = ATTR_AUTO_EXPAND_VERTICALLY, type = Boolean.class), //
        @DuiAttribute(name = ATTR_KEEP_FILTER, type = Boolean.class) //
    })
public class ComboBoxFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_COMBO_BOX = "vaadin-combo-box";

  public static final String TAG_VAADIN_MULTI_SELECT_COMBO_BOX = "vaadin-multi-select-combo-box";


  public static final String ATTR_AUTOFOCUS = "autofocus";

  public static final String ATTR_ALLOW_CUSTOM_VALUE = "allow-custom-value";

  public static final String ATTR_SELECTED_ITEMS_ON_TOP = "selected-items-on-top";

  public static final String ATTR_AUTO_EXPAND_HORIZONTALLY = "auto-expand-horizontally";

  public static final String ATTR_AUTO_EXPAND_VERTICALLY = "auto-expand-vertically";

  public static final String ATTR_KEEP_FILTER = "keep-filter";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_COMBO_BOX:
      {
        ComboBox<?> component = new ComboBox<>();
        context.mapAttribute(ATTR_AUTOFOCUS).asBoolean().to(component::setAutofocus);
        context.mapAttribute(ATTR_ALLOW_CUSTOM_VALUE).asBoolean()
            .to(component::setAllowCustomValue);

        ComponentI18n.localize(component);

        context.readChildren(component,
            "ComboBox cannot be populated using a template. Use its Java API instead.");

        return component;
      }

      case TAG_VAADIN_MULTI_SELECT_COMBO_BOX:
      {
        MultiSelectComboBox<?> component = new MultiSelectComboBox<>();
        context.mapAttribute(ATTR_AUTOFOCUS).asBoolean().to(component::setAutofocus);
        context.mapAttribute(ATTR_ALLOW_CUSTOM_VALUE).asBoolean()
            .to(component::setAllowCustomValue);
        context.mapAttribute(ATTR_SELECTED_ITEMS_ON_TOP).asBoolean()
            .to(component::setSelectedItemsOnTop);

        AutoExpandMode autoExpandMode;
        boolean expandHorizontally =
            context.mapAttribute(ATTR_AUTO_EXPAND_HORIZONTALLY).asBoolean().get().orElse(false);
        boolean expandVertically =
            context.mapAttribute(ATTR_AUTO_EXPAND_VERTICALLY).asBoolean().get().orElse(false);
        if (expandHorizontally)
        {
          if (expandVertically)
            autoExpandMode = AutoExpandMode.BOTH;
          else
            autoExpandMode = AutoExpandMode.HORIZONTAL;
        }
        else if (expandVertically)
        {
          autoExpandMode = AutoExpandMode.VERTICAL;
        }
        else
        {
          autoExpandMode = AutoExpandMode.NONE;
        }
        component.setAutoExpand(autoExpandMode);
        context.mapAttribute(ATTR_KEEP_FILTER).asBoolean().to(component::setKeepFilter);

        ComponentI18n.localize(component);

        context.readChildren(component,
            "MultiSelectComboBox cannot be populated using a template. Use its Java API instead.");

        return component;
      }
    }

    return null;
  }

}
