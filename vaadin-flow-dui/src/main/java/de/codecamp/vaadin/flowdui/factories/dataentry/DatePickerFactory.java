package de.codecamp.vaadin.flowdui.factories.dataentry;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_DATA_ENTRY;
import static de.codecamp.vaadin.flowdui.factories.dataentry.DatePickerFactory.ATTR_INITIAL_POSITION;
import static de.codecamp.vaadin.flowdui.factories.dataentry.DatePickerFactory.ATTR_LOCALE;
import static de.codecamp.vaadin.flowdui.factories.dataentry.DatePickerFactory.ATTR_MAX;
import static de.codecamp.vaadin.flowdui.factories.dataentry.DatePickerFactory.ATTR_MIN;
import static de.codecamp.vaadin.flowdui.factories.dataentry.DatePickerFactory.ATTR_SHOW_WEEK_NUMBERS;
import static de.codecamp.vaadin.flowdui.factories.dataentry.DatePickerFactory.TAG_VAADIN_DATE_PICKER;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.datepicker.DatePicker;
import de.codecamp.vaadin.base.i18n.ComponentI18n;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import java.time.LocalDate;
import java.util.Locale;


@DuiComponent(
    tagName = TAG_VAADIN_DATE_PICKER,
    componentType = DatePicker.class,
    docUrl = "https://vaadin.com/docs/latest/components/date-picker",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(name = ATTR_MIN, type = LocalDate.class),
        @DuiAttribute(name = ATTR_MAX, type = LocalDate.class),
        @DuiAttribute(name = ATTR_INITIAL_POSITION, type = LocalDate.class),
        @DuiAttribute(name = ATTR_SHOW_WEEK_NUMBERS, type = Boolean.class),
        @DuiAttribute(name = ATTR_LOCALE, type = Locale.class, custom = true) //
    })
public class DatePickerFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_DATE_PICKER = "vaadin-date-picker";


  public static final String ATTR_INITIAL_POSITION = "initial-position";

  public static final String ATTR_MAX = "max";

  public static final String ATTR_MIN = "min";

  public static final String ATTR_SHOW_WEEK_NUMBERS = "show-week-numbers";

  public static final String ATTR_LOCALE = "locale";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_DATE_PICKER:
      {
        DatePicker component = new DatePicker();
        context.mapAttribute(ATTR_MIN).asLocalDate().to(component::setMin);
        context.mapAttribute(ATTR_MAX).asLocalDate().to(component::setMax);
        context.mapAttribute(ATTR_INITIAL_POSITION).asLocalDate().to(component::setInitialPosition);
        context.mapAttribute(ATTR_SHOW_WEEK_NUMBERS).asBoolean()
            .to(component::setWeekNumbersVisible);
        context.mapAttribute(ATTR_LOCALE).asLocale().to(component::setLocale);

        ComponentI18n.localize(component);

        context.readChildren(component);

        return component;
      }
    }

    return null;
  }

}
