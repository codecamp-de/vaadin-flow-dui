package de.codecamp.vaadin.flowdui.factories.dataentry;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_DATA_ENTRY;
import static de.codecamp.vaadin.flowdui.factories.dataentry.DateTimePickerFactory.ATTR_DATE_PLACEHOLDER;
import static de.codecamp.vaadin.flowdui.factories.dataentry.DateTimePickerFactory.ATTR_LOCALE;
import static de.codecamp.vaadin.flowdui.factories.dataentry.DateTimePickerFactory.ATTR_MAX;
import static de.codecamp.vaadin.flowdui.factories.dataentry.DateTimePickerFactory.ATTR_MIN;
import static de.codecamp.vaadin.flowdui.factories.dataentry.DateTimePickerFactory.ATTR_SHOW_WEEK_NUMBERS;
import static de.codecamp.vaadin.flowdui.factories.dataentry.DateTimePickerFactory.ATTR_STEP;
import static de.codecamp.vaadin.flowdui.factories.dataentry.DateTimePickerFactory.ATTR_TIME_PLACEHOLDER;
import static de.codecamp.vaadin.flowdui.factories.dataentry.DateTimePickerFactory.TAG_VAADIN_DATE_TIME_PICKER;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import de.codecamp.vaadin.base.i18n.ComponentI18n;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Locale;


@DuiComponent(
    tagName = TAG_VAADIN_DATE_TIME_PICKER,
    componentType = DateTimePicker.class,
    docUrl = "https://vaadin.com/docs/latest/components/date-time-picker",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(name = ATTR_DATE_PLACEHOLDER, type = String.class),
        @DuiAttribute(name = ATTR_TIME_PLACEHOLDER, type = String.class),
        @DuiAttribute(name = ATTR_MIN, type = LocalDateTime.class),
        @DuiAttribute(name = ATTR_MAX, type = LocalDateTime.class),
        @DuiAttribute(name = ATTR_STEP, type = Double.class),
        @DuiAttribute(name = ATTR_SHOW_WEEK_NUMBERS, type = Boolean.class),
        @DuiAttribute(name = ATTR_LOCALE, type = Locale.class, custom = true) //
    })
public class DateTimePickerFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_DATE_TIME_PICKER = "vaadin-date-time-picker";


  public static final String ATTR_DATE_PLACEHOLDER = "date-placeholder";

  public static final String ATTR_TIME_PLACEHOLDER = "time-placeholder";

  public static final String ATTR_MIN = "min";

  public static final String ATTR_MAX = "max";

  public static final String ATTR_STEP = "step";

  public static final String ATTR_SHOW_WEEK_NUMBERS = "show-week-numbers";

  public static final String ATTR_LOCALE = "locale";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_DATE_TIME_PICKER:
      {
        DateTimePicker component = new DateTimePicker();
        context.mapAttribute(ATTR_DATE_PLACEHOLDER).asString().to(component::setDatePlaceholder);
        context.mapAttribute(ATTR_TIME_PLACEHOLDER).asString().to(component::setTimePlaceholder);
        context.mapAttribute(ATTR_MIN).asLocalDateTime().to(component::setMin);
        context.mapAttribute(ATTR_MAX).asLocalDateTime().to(component::setMax);
        context.mapAttribute(ATTR_STEP).asLong().to(v -> component.setStep(Duration.ofSeconds(v)));
        context.mapAttribute(ATTR_SHOW_WEEK_NUMBERS).asBoolean()
            .to(component::setWeekNumbersVisible);
        context.mapAttribute(ATTR_LOCALE).asLocale().to(component::setLocale);

        ComponentI18n.localize(component);

        context.readChildren(component);

        return component;
      }
    }

    return null;
  }

}
