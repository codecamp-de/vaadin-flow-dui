package de.codecamp.vaadin.flowdui.factories.dataentry;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_DATA_ENTRY;
import static de.codecamp.vaadin.flowdui.factories.dataentry.ListBoxFactory.ATTR_MULTIPLE;
import static de.codecamp.vaadin.flowdui.factories.dataentry.ListBoxFactory.TAG_VAADIN_LIST_BOX;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.listbox.ListBoxBase;
import com.vaadin.flow.component.listbox.MultiSelectListBox;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_LIST_BOX,
    componentType = ListBox.class,
    docUrl = "https://vaadin.com/docs/latest/components/list-box",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(
            name = ATTR_MULTIPLE,
            type = Boolean.class,
            description = "Must not be set; otherwise a MultiSelectListBox is created.") //
    })
@DuiComponent(
    tagName = TAG_VAADIN_LIST_BOX,
    componentType = MultiSelectListBox.class,
    docUrl = "https://vaadin.com/docs/latest/components/list-box",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(
            name = ATTR_MULTIPLE,
            type = Boolean.class,
            description = "Must be set; otherwise a ListBox is created.") //
    })
public class ListBoxFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_LIST_BOX = "vaadin-list-box";


  public static final String ATTR_MULTIPLE = "multiple";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_LIST_BOX:
      {
        ListBoxBase<?, ?, ?> component;
        if (context.getElement().hasAttr(ATTR_MULTIPLE))
          component = new MultiSelectListBox<>();
        else
          component = new ListBox<>();

        context.readChildren(component,
            "ListBox/MultiSelectListBox cannot be populated using a template. Use its Java API instead.");

        return component;
      }
    }

    return null;
  }

}
