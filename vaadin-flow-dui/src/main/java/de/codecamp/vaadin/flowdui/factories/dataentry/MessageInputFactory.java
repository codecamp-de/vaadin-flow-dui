package de.codecamp.vaadin.flowdui.factories.dataentry;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_DATA_ENTRY;
import static de.codecamp.vaadin.flowdui.factories.dataentry.MessageInputFactory.TAG_VAADIN_MESSAGE_INPUT;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.messages.MessageInput;
import de.codecamp.vaadin.base.i18n.ComponentI18n;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_MESSAGE_INPUT,
    componentType = MessageInput.class,
    docUrl = "https://vaadin.com/docs/latest/components/message-input",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
    })
public class MessageInputFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_MESSAGE_INPUT = "vaadin-message-input";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_MESSAGE_INPUT:
      {
        MessageInput component = new MessageInput();

        ComponentI18n.localize(component);

        context.readChildren(component);

        return component;
      }
    }

    return null;
  }

}
