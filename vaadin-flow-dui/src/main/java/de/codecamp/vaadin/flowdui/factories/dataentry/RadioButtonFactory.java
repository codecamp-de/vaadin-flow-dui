package de.codecamp.vaadin.flowdui.factories.dataentry;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_DATA_ENTRY;
import static de.codecamp.vaadin.flowdui.factories.dataentry.RadioButtonFactory.TAG_VAADIN_RADIO_BUTTON;
import static de.codecamp.vaadin.flowdui.factories.dataentry.RadioButtonFactory.TAG_VAADIN_RADIO_GROUP;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import de.codecamp.vaadin.base.i18n.ComponentI18n;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_RADIO_GROUP,
    componentType = RadioButtonGroup.class,
    docUrl = "https://vaadin.com/docs/latest/components/radio-button",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
    })
@DuiComponent(
    tagName = TAG_VAADIN_RADIO_BUTTON,
    componentType = Void.class,
    docUrl = "https://vaadin.com/docs/latest/components/radio-button",
    category = CATEGORY_DATA_ENTRY,
    description = "Can't be created directly. Use Java API of RadioButtonGroup.",
    deprecated = true)
public class RadioButtonFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_RADIO_GROUP = "vaadin-radio-group";

  public static final String TAG_VAADIN_RADIO_BUTTON = "vaadin-radio-button";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_RADIO_GROUP:
      {
        RadioButtonGroup<?> component = new RadioButtonGroup<>();

        ComponentI18n.localize(component);

        context.readChildren(component,
            "RadioButtonGroup cannot be populated using a template. Use its Java API instead.");

        return component;
      }
    }

    return null;
  }

}
