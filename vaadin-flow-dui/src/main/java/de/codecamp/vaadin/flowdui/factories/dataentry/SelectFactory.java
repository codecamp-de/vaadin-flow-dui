package de.codecamp.vaadin.flowdui.factories.dataentry;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_DATA_ENTRY;
import static de.codecamp.vaadin.flowdui.factories.dataentry.SelectFactory.ATTR_VAADIN_SELECT;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.select.Select;
import de.codecamp.vaadin.base.i18n.ComponentI18n;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = ATTR_VAADIN_SELECT,
    componentType = Select.class,
    description = "No children; use Java API.",
    docUrl = "https://vaadin.com/docs/latest/components/select",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
    })
public class SelectFactory
  implements
    ComponentFactory
{

  public static final String ATTR_VAADIN_SELECT = "vaadin-select";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case ATTR_VAADIN_SELECT:
      {
        Select<?> component = new Select<>();

        ComponentI18n.localize(component);

        context.readChildren(component,
            "Select cannot be populated using a template. Use its Java API instead.");

        return component;
      }
    }

    return null;
  }

}
