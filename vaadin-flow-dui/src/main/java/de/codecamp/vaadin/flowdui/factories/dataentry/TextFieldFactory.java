package de.codecamp.vaadin.flowdui.factories.dataentry;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_DATA_ENTRY;
import static de.codecamp.vaadin.flowdui.factories.dataentry.TextFieldFactory.ATTR_AUTOFOCUS;
import static de.codecamp.vaadin.flowdui.factories.dataentry.TextFieldFactory.ATTR_AUTOSELECT;
import static de.codecamp.vaadin.flowdui.factories.dataentry.TextFieldFactory.ATTR_MAX;
import static de.codecamp.vaadin.flowdui.factories.dataentry.TextFieldFactory.ATTR_MAXLENGTH;
import static de.codecamp.vaadin.flowdui.factories.dataentry.TextFieldFactory.ATTR_MIN;
import static de.codecamp.vaadin.flowdui.factories.dataentry.TextFieldFactory.ATTR_MINLENGTH;
import static de.codecamp.vaadin.flowdui.factories.dataentry.TextFieldFactory.ATTR_PATTERN;
import static de.codecamp.vaadin.flowdui.factories.dataentry.TextFieldFactory.ATTR_REVEAL_BUTTON_HIDDEN;
import static de.codecamp.vaadin.flowdui.factories.dataentry.TextFieldFactory.ATTR_STEP;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.Autocapitalize;
import com.vaadin.flow.component.textfield.Autocomplete;
import com.vaadin.flow.component.textfield.BigDecimalField;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.HasAutocapitalize;
import com.vaadin.flow.component.textfield.HasAutocomplete;
import com.vaadin.flow.component.textfield.HasAutocorrect;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import de.codecamp.vaadin.base.i18n.ComponentI18n;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


@DuiComponent(
    tagName = TextFieldFactory.TAG_VAADIN_TEXT_FIELD,
    componentType = TextArea.class,
    docUrl = "https://vaadin.com/docs/latest/components/text-field",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(name = ATTR_AUTOFOCUS, type = Boolean.class), //
        @DuiAttribute(name = ATTR_AUTOSELECT, type = Boolean.class), //
        @DuiAttribute(name = ATTR_MINLENGTH, type = Integer.class), //
        @DuiAttribute(name = ATTR_MAXLENGTH, type = Integer.class), //
        @DuiAttribute(name = ATTR_PATTERN, type = String.class) //
    })
@DuiComponent(
    tagName = TextFieldFactory.TAG_VAADIN_TEXT_AREA,
    componentType = TextField.class,
    docUrl = "https://vaadin.com/docs/latest/components/text-area",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(name = ATTR_AUTOFOCUS, type = Boolean.class), //
        @DuiAttribute(name = ATTR_AUTOSELECT, type = Boolean.class), //
        @DuiAttribute(name = ATTR_MINLENGTH, type = Integer.class), //
        @DuiAttribute(name = ATTR_MAXLENGTH, type = Integer.class), //
        @DuiAttribute(name = ATTR_PATTERN, type = String.class) //
    })
@DuiComponent(
    tagName = TextFieldFactory.TAG_VAADIN_PASSWORD_FIELD,
    componentType = PasswordField.class,
    docUrl = "https://vaadin.com/docs/latest/components/password-field",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(name = ATTR_AUTOFOCUS, type = Boolean.class), //
        @DuiAttribute(name = ATTR_AUTOSELECT, type = Boolean.class), //
        @DuiAttribute(name = ATTR_MINLENGTH, type = Integer.class), //
        @DuiAttribute(name = ATTR_MAXLENGTH, type = Integer.class), //
        @DuiAttribute(name = ATTR_PATTERN, type = String.class), //
        @DuiAttribute(name = ATTR_REVEAL_BUTTON_HIDDEN, type = Boolean.class) //
    })
@DuiComponent(
    tagName = TextFieldFactory.TAG_VAADIN_EMAIL_FIELD,
    componentType = EmailField.class,
    docUrl = "https://vaadin.com/docs/latest/components/email-field",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(name = ATTR_AUTOFOCUS, type = Boolean.class), //
        @DuiAttribute(name = ATTR_AUTOSELECT, type = Boolean.class), //
        @DuiAttribute(name = ATTR_MINLENGTH, type = Integer.class), //
        @DuiAttribute(name = ATTR_MAXLENGTH, type = Integer.class), //
        @DuiAttribute(name = ATTR_PATTERN, type = String.class) //
    })
@DuiComponent(
    tagName = TextFieldFactory.TAG_VAADIN_NUMBER_FIELD,
    componentType = NumberField.class,
    docUrl = "https://vaadin.com/docs/latest/components/number-field",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(name = ATTR_AUTOFOCUS, type = Boolean.class), //
        @DuiAttribute(name = ATTR_AUTOSELECT, type = Boolean.class), //
        @DuiAttribute(name = ATTR_MIN, type = Double.class), //
        @DuiAttribute(name = ATTR_MAX, type = Double.class), //
        @DuiAttribute(name = ATTR_STEP, type = Double.class) //
    })
@DuiComponent(
    tagName = TextFieldFactory.TAG_VAADIN_INTEGER_FIELD,
    componentType = IntegerField.class,
    docUrl = "https://vaadin.com/docs/latest/components/number-field",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(name = ATTR_AUTOFOCUS, type = Boolean.class), //
        @DuiAttribute(name = ATTR_AUTOSELECT, type = Boolean.class), //
        @DuiAttribute(name = ATTR_MIN, type = Integer.class), //
        @DuiAttribute(name = ATTR_MAX, type = Integer.class), //
        @DuiAttribute(name = ATTR_STEP, type = Integer.class) //
    })
@DuiComponent(
    tagName = TextFieldFactory.TAG_VAADIN_BIG_DECIMAL_FIELD,
    componentType = BigDecimalField.class,
    docUrl = "https://vaadin.com/docs/latest/components/number-field",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(name = ATTR_AUTOFOCUS, type = Boolean.class), //
        @DuiAttribute(name = ATTR_AUTOSELECT, type = Boolean.class) //
    })
@DuiComponent(
    componentType = HasAutocorrect.class,
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(
            name = HasAutocorrect.AUTOCORRECT_ATTRIBUTE,
            type = String.class,
            description = "Allowed values: 'on' or 'off'") //
    })
@DuiComponent(
    componentType = HasAutocapitalize.class,
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(
            name = HasAutocapitalize.AUTOCAPITALIZE_ATTRIBUTE,
            type = String.class,
            description = "Allowed values: 'none', 'sentences', 'words' or 'characters'") //
    })
@DuiComponent(
    componentType = HasAutocomplete.class,
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(
            name = HasAutocomplete.AUTOCOMPLETE_ATTRIBUTE,
            type = String.class,
            description = "Allowed values: See 'com.vaadin.flow.component.textfield.Autocomplete'") //
    })
public class TextFieldFactory
  implements
    ComponentFactory,
    ComponentPostProcessor
{

  public static final String TAG_VAADIN_TEXT_AREA = "vaadin-text-area";

  public static final String TAG_VAADIN_TEXT_FIELD = "vaadin-text-field";

  public static final String TAG_VAADIN_PASSWORD_FIELD = "vaadin-password-field";

  public static final String TAG_VAADIN_EMAIL_FIELD = "vaadin-email-field";

  public static final String TAG_VAADIN_NUMBER_FIELD = "vaadin-number-field";

  public static final String TAG_VAADIN_INTEGER_FIELD = "vaadin-integer-field";

  public static final String TAG_VAADIN_BIG_DECIMAL_FIELD = "vaadin-big-decimal-field";


  public static final String ATTR_AUTOFOCUS = "autofocus";

  public static final String ATTR_AUTOSELECT = "autoselect";

  public static final String ATTR_MINLENGTH = "minlength";

  public static final String ATTR_MAXLENGTH = "maxlength";

  public static final String ATTR_PATTERN = "pattern";

  public static final String ATTR_REVEAL_BUTTON_HIDDEN = "reveal-button-hidden";

  public static final String ATTR_MIN = "min";

  public static final String ATTR_MAX = "max";

  public static final String ATTR_STEP = "step";

  public static final String ATTR_STEP_BUTTONS_VISIBLE = "step-buttons-visible";

  @Deprecated
  public static final String ATTR_HAS_CONTROLS = "has-controls";


  private static final Map<String, Autocapitalize> AUTOCAPITALIZE_ENUM_MAPPING = new HashMap<>();
  static
  {
    AUTOCAPITALIZE_ENUM_MAPPING.put("none", Autocapitalize.NONE);
    AUTOCAPITALIZE_ENUM_MAPPING.put("sentences", Autocapitalize.SENTENCES);
    AUTOCAPITALIZE_ENUM_MAPPING.put("words", Autocapitalize.WORDS);
    AUTOCAPITALIZE_ENUM_MAPPING.put("characters", Autocapitalize.CHARACTERS);
  }

  private static final Map<String, Autocomplete> AUTOCOMPLETE_ENUM_MAPPING = new HashMap<>();
  static
  {
    try
    {
      Field valueField = Autocomplete.class.getDeclaredField("value");
      valueField.setAccessible(true);
      for (Autocomplete a : Autocomplete.values())
      {
        String value = (String) valueField.get(a);
        AUTOCOMPLETE_ENUM_MAPPING.put(value, a);
      }
    }
    catch (NoSuchFieldException | IllegalAccessException | RuntimeException ex)
    {
      throw new AssertionError("Failed to extract possible attribute values from Autocomplete.",
          ex);
    }
  }


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_TEXT_FIELD:
      {
        TextField component = new TextField();
        context.mapAttribute(ATTR_AUTOFOCUS).asBoolean().to(component::setAutofocus);
        context.mapAttribute(ATTR_AUTOSELECT).asBoolean().to(component::setAutoselect);
        context.mapAttribute(ATTR_MINLENGTH).asInteger().to(component::setMinLength);
        context.mapAttribute(ATTR_MAXLENGTH).asInteger().to(component::setMaxLength);
        context.mapAttribute(ATTR_PATTERN).asString().to(component::setPattern);

        ComponentI18n.localize(component);

        context.readChildren(component);

        return component;
      }

      case TAG_VAADIN_TEXT_AREA:
      {
        TextArea component = new TextArea();
        context.mapAttribute(ATTR_AUTOFOCUS).asBoolean().to(component::setAutofocus);
        context.mapAttribute(ATTR_AUTOSELECT).asBoolean().to(component::setAutoselect);
        context.mapAttribute(ATTR_MINLENGTH).asInteger().to(component::setMinLength);
        context.mapAttribute(ATTR_MAXLENGTH).asInteger().to(component::setMaxLength);

        ComponentI18n.localize(component);

        context.readChildren(component);

        return component;
      }

      case TAG_VAADIN_PASSWORD_FIELD:
      {
        PasswordField component = new PasswordField();
        context.mapAttribute(ATTR_AUTOFOCUS).asBoolean().to(component::setAutofocus);
        context.mapAttribute(ATTR_AUTOSELECT).asBoolean().to(component::setAutoselect);
        context.mapAttribute(ATTR_MINLENGTH).asInteger().to(component::setMinLength);
        context.mapAttribute(ATTR_MAXLENGTH).asInteger().to(component::setMaxLength);
        context.mapAttribute(ATTR_PATTERN).asString().to(component::setPattern);
        context.mapAttribute(ATTR_REVEAL_BUTTON_HIDDEN).asBoolean()
            .to(v -> component.setRevealButtonVisible(!v));

        ComponentI18n.localize(component);

        context.readChildren(component);

        return component;
      }

      case TAG_VAADIN_EMAIL_FIELD:
      {
        EmailField component = new EmailField();
        context.mapAttribute(ATTR_AUTOFOCUS).asBoolean().to(component::setAutofocus);
        context.mapAttribute(ATTR_AUTOSELECT).asBoolean().to(component::setAutoselect);
        context.mapAttribute(ATTR_MINLENGTH).asInteger().to(component::setMinLength);
        context.mapAttribute(ATTR_MAXLENGTH).asInteger().to(component::setMaxLength);
        context.mapAttribute(ATTR_PATTERN).asString().to(component::setPattern);

        ComponentI18n.localize(component);

        context.readChildren(component);

        return component;
      }

      case TAG_VAADIN_NUMBER_FIELD:
      {
        NumberField component = new NumberField();
        context.mapAttribute(ATTR_AUTOFOCUS).asBoolean().to(component::setAutofocus);
        context.mapAttribute(ATTR_AUTOSELECT).asBoolean().to(component::setAutoselect);
        context.mapAttribute(ATTR_MIN).asDouble().to(component::setMin);
        context.mapAttribute(ATTR_MAX).asDouble().to(component::setMax);
        context.mapAttribute(ATTR_STEP).asDouble().to(component::setStep);
        context.mapAttribute(ATTR_STEP_BUTTONS_VISIBLE, Set.of(ATTR_HAS_CONTROLS)).asBoolean()
            .to(component::setStepButtonsVisible);

        ComponentI18n.localize(component);

        context.readChildren(component);

        return component;
      }

      case TAG_VAADIN_INTEGER_FIELD:
      {
        IntegerField component = new IntegerField();
        context.mapAttribute(ATTR_AUTOFOCUS).asBoolean().to(component::setAutofocus);
        context.mapAttribute(ATTR_AUTOSELECT).asBoolean().to(component::setAutoselect);
        context.mapAttribute(ATTR_MIN).asInteger().to(component::setMin);
        context.mapAttribute(ATTR_MAX).asInteger().to(component::setMax);
        context.mapAttribute(ATTR_STEP).asInteger().to(component::setStep);
        context.mapAttribute(ATTR_STEP_BUTTONS_VISIBLE, Set.of(ATTR_HAS_CONTROLS)).asBoolean()
            .to(component::setStepButtonsVisible);

        ComponentI18n.localize(component);

        context.readChildren(component);

        return component;
      }

      case TAG_VAADIN_BIG_DECIMAL_FIELD:
      {
        BigDecimalField component = new BigDecimalField();
        context.mapAttribute(ATTR_AUTOFOCUS).asBoolean().to(component::setAutofocus);
        context.mapAttribute(ATTR_AUTOSELECT).asBoolean().to(component::setAutoselect);

        ComponentI18n.localize(component);

        context.readChildren(component);

        return component;
      }
    }

    return null;
  }

  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    if (component instanceof HasAutocorrect)
    {
      HasAutocorrect hasAutocorrect = (HasAutocorrect) component;
      context.mapAttribute(HasAutocorrect.AUTOCORRECT_ATTRIBUTE).asConverted(this::parseAutocorrect)
          .to(hasAutocorrect::setAutocorrect);
    }
    if (component instanceof HasAutocapitalize)
    {
      HasAutocapitalize hasAutocapitalize = (HasAutocapitalize) component;
      context.mapAttribute(HasAutocapitalize.AUTOCAPITALIZE_ATTRIBUTE)
          .asEnum(AUTOCAPITALIZE_ENUM_MAPPING::get).to(hasAutocapitalize::setAutocapitalize);
    }
    if (component instanceof HasAutocomplete)
    {
      HasAutocomplete hasAutocomplete = (HasAutocomplete) component;
      context.mapAttribute(HasAutocomplete.AUTOCOMPLETE_ATTRIBUTE)
          .asEnum(AUTOCOMPLETE_ENUM_MAPPING::get).to(hasAutocomplete::setAutocomplete);
    }
  }

  private boolean parseAutocorrect(String valueString)
  {
    boolean value;
    if ("on".equals(valueString))
    {
      value = true;
    }
    else if ("off".equals(valueString))
    {
      value = false;
    }
    else
    {
      String msg = "Illegal value for attribute '%s' found: %s";
      msg = String.format(msg, HasAutocorrect.AUTOCORRECT_ATTRIBUTE, valueString);
      throw new IllegalArgumentException(msg);
    }
    return value;
  }

}
