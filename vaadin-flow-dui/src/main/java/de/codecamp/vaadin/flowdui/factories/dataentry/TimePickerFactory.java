package de.codecamp.vaadin.flowdui.factories.dataentry;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_DATA_ENTRY;
import static de.codecamp.vaadin.flowdui.factories.dataentry.TimePickerFactory.ATTR_LOCALE;
import static de.codecamp.vaadin.flowdui.factories.dataentry.TimePickerFactory.ATTR_MAX;
import static de.codecamp.vaadin.flowdui.factories.dataentry.TimePickerFactory.ATTR_MIN;
import static de.codecamp.vaadin.flowdui.factories.dataentry.TimePickerFactory.ATTR_STEP;
import static de.codecamp.vaadin.flowdui.factories.dataentry.TimePickerFactory.TAG_VAADIN_TIME_PICKER;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.timepicker.TimePicker;
import de.codecamp.vaadin.base.i18n.ComponentI18n;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Locale;


@DuiComponent(
    tagName = TAG_VAADIN_TIME_PICKER,
    componentType = TimePicker.class,
    docUrl = "https://vaadin.com/docs/latest/components/time-picker",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(
            name = ATTR_MIN,
            type = LocalTime.class,
            description = "ISO-8601 formatted local time, e.g. 10:15 10:15:30"),
        @DuiAttribute(
            name = ATTR_MAX,
            type = LocalTime.class,
            description = "ISO-8601 formatted local time, e.g. 10:15 10:15:30"),
        @DuiAttribute(name = ATTR_STEP, type = Double.class),
        @DuiAttribute(name = ATTR_LOCALE, type = Locale.class, custom = true) //
    })
public class TimePickerFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_TIME_PICKER = "vaadin-time-picker";


  public static final String ATTR_MIN = "min";

  public static final String ATTR_MAX = "max";

  public static final String ATTR_STEP = "step";

  public static final String ATTR_LOCALE = "locale";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_TIME_PICKER:
      {
        TimePicker component = new TimePicker();
        context.mapAttribute(ATTR_MIN).asLocalTime().to(component::setMin);
        context.mapAttribute(ATTR_MAX).asLocalTime().to(component::setMax);
        context.mapAttribute(ATTR_STEP).asLong().to(v -> component.setStep(Duration.ofMillis((v))));
        context.mapAttribute(ATTR_LOCALE).asLocale().to(component::setLocale);

        ComponentI18n.localize(component);

        context.readChildren(component);

        return component;
      }
    }

    return null;
  }

}
