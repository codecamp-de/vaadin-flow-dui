package de.codecamp.vaadin.flowdui.factories.dataentry;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_DATA_ENTRY;
import static de.codecamp.vaadin.flowdui.factories.dataentry.UploadFactory.ATTR_ACCEPT;
import static de.codecamp.vaadin.flowdui.factories.dataentry.UploadFactory.ATTR_CAPTURE;
import static de.codecamp.vaadin.flowdui.factories.dataentry.UploadFactory.ATTR_MAX_FILES;
import static de.codecamp.vaadin.flowdui.factories.dataentry.UploadFactory.ATTR_MAX_FILE_SIZE;
import static de.codecamp.vaadin.flowdui.factories.dataentry.UploadFactory.ATTR_NODROP;
import static de.codecamp.vaadin.flowdui.factories.dataentry.UploadFactory.TAG_VAADIN_UPLOAD;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.upload.Upload;
import de.codecamp.vaadin.base.i18n.ComponentI18n;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_UPLOAD,
    componentType = Upload.class,
    docUrl = "https://vaadin.com/docs/latest/components/upload",
    category = CATEGORY_DATA_ENTRY,
    attributes = { //
        @DuiAttribute(name = ATTR_NODROP, type = Boolean.class), //
        @DuiAttribute(name = ATTR_ACCEPT, type = String.class), //
        @DuiAttribute(name = ATTR_MAX_FILES, type = Integer.class), //
        @DuiAttribute(name = ATTR_MAX_FILE_SIZE, type = Integer.class), //
        @DuiAttribute(name = ATTR_CAPTURE, type = String.class) //
    })
public class UploadFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_UPLOAD = "vaadin-upload";


  public static final String SLOT_ADD_BUTTON = "add-button";

  public static final String SLOT_DROP_LABEL = "drop-label";

  public static final String SLOT_DROP_LABEL_ICON = "drop-label-icon";


  public static final String ATTR_NODROP = "nodrop";

  public static final String ATTR_ACCEPT = "accept";

  public static final String ATTR_MAX_FILES = "max-files";

  public static final String ATTR_MAX_FILE_SIZE = "max-file-size";

  public static final String ATTR_CAPTURE = "capture";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_UPLOAD:
      {
        Upload component = new Upload();

        Component defaultUploadButton = component.getUploadButton();
        Component defaultDropLabel = component.getDropLabel();
        Component defaultDropLabelIcon = component.getDropLabelIcon();

        context.mapAttribute(ATTR_NODROP).asBoolean().to(v -> component.setDropAllowed(!v));
        context.mapAttribute(ATTR_ACCEPT).asString()
            .to(v -> component.setAcceptedFileTypes(v.split(",")));
        context.mapAttribute(ATTR_MAX_FILES).asInteger().to(component::setMaxFiles);
        context.mapAttribute(ATTR_MAX_FILE_SIZE).asInteger().to(component::setMaxFileSize);
        context.copyAttributeTo(ATTR_CAPTURE, component);

        ComponentI18n.localize(component);

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_ADD_BUTTON:
              if (component.getUploadButton() != defaultUploadButton) // NOPMD:CompareObjectsWithEquals
                throw context.fail("Slot 'add-button' already filled.");
              component.setUploadButton(context.readComponentForSlot(childElement));
              return true;

            case SLOT_DROP_LABEL:
              if (component.getDropLabel() != defaultDropLabel) // NOPMD:CompareObjectsWithEquals
                throw context.fail("Slot 'drop-label' already filled.");
              component.setDropLabel(context.readComponentForSlot(childElement));
              return true;

            case SLOT_DROP_LABEL_ICON:
              if (component.getDropLabelIcon() != defaultDropLabelIcon) // NOPMD:CompareObjectsWithEquals
                throw context.fail("Slot 'drop-label-icon' already filled.");
              component.setDropLabelIcon(context.readComponentForSlot(childElement));
              return true;
          }
          return false;
        }, null);

        return component;
      }
    }

    return null;
  }

}
