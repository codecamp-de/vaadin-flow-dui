package de.codecamp.vaadin.flowdui.factories.layouts;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_LAYOUTS;
import static de.codecamp.vaadin.flowdui.factories.layouts.AppLayoutFactory.ATTR_PRIMARY_SECTION;
import static de.codecamp.vaadin.flowdui.factories.layouts.AppLayoutFactory.SLOT_DRAWER;
import static de.codecamp.vaadin.flowdui.factories.layouts.AppLayoutFactory.SLOT_NAVBAR;
import static de.codecamp.vaadin.flowdui.factories.layouts.AppLayoutFactory.TAG_VAADIN_APP_LAYOUT;
import static de.codecamp.vaadin.flowdui.factories.layouts.AppLayoutFactory.TAG_VAADIN_DRAWER_TOGGLE;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.AppLayout.Section;
import com.vaadin.flow.component.applayout.DrawerToggle;
import de.codecamp.vaadin.base.i18n.ComponentI18n;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_APP_LAYOUT,
    componentType = AppLayout.class,
    docUrl = "https://vaadin.com/docs/latest/components/app-layout",
    category = CATEGORY_LAYOUTS,
    attributes = { //
        @DuiAttribute(
            name = ATTR_PRIMARY_SECTION,
            type = String.class,
            description = "Allowed values: 'navbar' or 'drawer'") //
    },
    slots = {SLOT_NAVBAR, SLOT_DRAWER})
@DuiComponent(
    tagName = TAG_VAADIN_DRAWER_TOGGLE,
    componentType = DrawerToggle.class,
    docUrl = "https://vaadin.com/docs/latest/components/app-layout",
    category = CATEGORY_LAYOUTS)
public class AppLayoutFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_APP_LAYOUT = "vaadin-app-layout";

  public static final String TAG_VAADIN_DRAWER_TOGGLE = "vaadin-drawer-toggle";


  public static final String SLOT_NAVBAR = "navbar";

  public static final String SLOT_DRAWER = "drawer";

  public static final String SLOT_TOUCH_OPTIMIZED = "touch-optimized";


  public static final String ATTR_PRIMARY_SECTION = "primary-section";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_APP_LAYOUT:
      {
        AppLayout component = new AppLayout();
        context.mapAttribute(ATTR_PRIMARY_SECTION).asEnum(Section::fromWebcomponentValue)
            .to(component::setPrimarySection);

        ComponentI18n.localize(component);

        context.readChildren(component, (slotName, childElement) ->
        {
          boolean touchOptimized = false;
          if (slotName.contains(SLOT_TOUCH_OPTIMIZED))
          {
            touchOptimized = true;
            slotName = slotName.replace(SLOT_TOUCH_OPTIMIZED, "").trim();
          }
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_DEFAULT:
              if (component.getContent() != null)
                throw context.fail("Only one content component supported.");
              component.setContent(context.readComponent(childElement));
              return true;

            case SLOT_NAVBAR:
              component.addToNavbar(touchOptimized, context.readComponentForSlot(childElement));
              return true;

            case SLOT_DRAWER:
              component.addToDrawer(context.readComponentForSlot(childElement));
              return true;
          }
          return false;
        }, null);

        return component;
      }

      case TAG_VAADIN_DRAWER_TOGGLE:
      {
        DrawerToggle component = new DrawerToggle();

        context.readChildren(component);

        return component;
      }
    }

    return null;
  }

}
