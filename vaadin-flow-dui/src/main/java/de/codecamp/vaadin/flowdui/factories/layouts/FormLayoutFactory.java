package de.codecamp.vaadin.flowdui.factories.layouts;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_LAYOUTS;
import static de.codecamp.vaadin.flowdui.factories.layouts.FormLayoutFactory.CATTR_COLSPAN;
import static de.codecamp.vaadin.flowdui.factories.layouts.FormLayoutFactory.SLOT_LABEL;
import static de.codecamp.vaadin.flowdui.factories.layouts.FormLayoutFactory.TAG_VAADIN_FORM_ITEM;
import static de.codecamp.vaadin.flowdui.factories.layouts.FormLayoutFactory.TAG_VAADIN_FORM_LAYOUT;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.FormItem;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_FORM_LAYOUT,
    componentType = FormLayout.class,
    docUrl = "https://vaadin.com/docs/latest/components/form-layout",
    category = CATEGORY_LAYOUTS,
    childAttributes = { //
        @DuiAttribute(name = CATTR_COLSPAN, type = Integer.class) //
    })
@DuiComponent(
    tagName = TAG_VAADIN_FORM_ITEM,
    componentType = FormItem.class,
    docUrl = "https://vaadin.com/docs/latest/components/form-layout",
    category = CATEGORY_LAYOUTS,
    slots = {SLOT_LABEL})
public class FormLayoutFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_FORM_LAYOUT = "vaadin-form-layout";

  public static final String TAG_VAADIN_FORM_ITEM = "vaadin-form-item";


  public static final String SLOT_LABEL = "label";


  public static final String CATTR_COLSPAN = "colspan";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_FORM_LAYOUT:
      {
        FormLayout component = new FormLayout();

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_DEFAULT:
              Component cc = context.readComponent(childElement, (childComponent, childContext) ->
              {
                childContext.mapAttribute(CATTR_COLSPAN).asInteger()
                    .to(colspan -> component.setColspan(childComponent, colspan));
              });

              component.add(cc);

              return true;
          }
          return false;
        }, textNode ->
        {
          component.add(textNode.text());
        });

        return component;
      }

      case TAG_VAADIN_FORM_ITEM:
      {
        ExtFormItem component = new ExtFormItem();

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_DEFAULT:
              component.add(context.readComponent(childElement));
              return true;

            case SLOT_LABEL:
              component.addToLabel(context.readComponentForSlot(childElement));
              return true;
          }
          return false;
        }, textNode ->
        {
          component.add(textNode.text());
        });

        return component;
      }
    }

    return null;
  }


  private static final class ExtFormItem
    extends
      FormItem
  {

    @Override
    public void addToLabel(Component... components)
    {
      super.addToLabel(components);
    }

  }

}
