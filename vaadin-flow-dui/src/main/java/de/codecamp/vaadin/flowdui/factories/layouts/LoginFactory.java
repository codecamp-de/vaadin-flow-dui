package de.codecamp.vaadin.flowdui.factories.layouts;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_LAYOUTS;
import static de.codecamp.vaadin.flowdui.factories.layouts.LoginFactory.TAG_VAADIN_LOGIN_FORM;
import static de.codecamp.vaadin.flowdui.factories.layouts.LoginFactory.TAG_VAADIN_LOGIN_OVERLAY;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.login.LoginOverlay;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_LOGIN_FORM,
    componentType = LoginForm.class,
    docUrl = "https://vaadin.com/docs/latest/components/login",
    category = CATEGORY_LAYOUTS)
@DuiComponent(
    tagName = TAG_VAADIN_LOGIN_OVERLAY,
    componentType = LoginOverlay.class,
    docUrl = "https://vaadin.com/docs/latest/components/login",
    category = CATEGORY_LAYOUTS)
public class LoginFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_LOGIN_FORM = "vaadin-login-form";

  public static final String TAG_VAADIN_LOGIN_OVERLAY = "vaadin-login-overlay";


  public static final String SLOT_CUSTOM_FORM_AREA = "custom-form-area";

  public static final String SLOT_FOOTER = "footer";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_LOGIN_FORM:
      {
        LoginForm component = new LoginForm();

        context.readChildren(component);

        return component;
      }

      case TAG_VAADIN_LOGIN_OVERLAY:
      {
        LoginOverlay component = new LoginOverlay();

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_CUSTOM_FORM_AREA:
              component.getCustomFormArea().add(context.readComponentForSlot(childElement));
              return true;

            case SLOT_FOOTER:
              component.getFooter().add(context.readComponentForSlot(childElement));
              return true;
          }
          return false;
        }, null);

        return component;
      }
    }

    return null;
  }

}
