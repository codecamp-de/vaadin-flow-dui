package de.codecamp.vaadin.flowdui.factories.layouts;


import static de.codecamp.vaadin.flowdui.declare.DuiComponent.SLOT_DEFAULT;
import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_LAYOUTS;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.ATTR_ALIGN_CONTENT;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.ATTR_ALIGN_ITEMS;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.ATTR_BOX_SIZING;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.ATTR_DIRECTION;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.ATTR_JUSTIFY_CONTENT;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.ATTR_MARGIN;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.ATTR_PADDING;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.ATTR_SPACING;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.ATTR_WRAP;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.CATTR_ALIGN;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.CATTR_BASIS;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.CATTR_EXPAND;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.CATTR_GROW;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.CATTR_HEIGHT_FULL;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.CATTR_SHRINK;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.CATTR_SIZE_FULL;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.CATTR_WIDTH_FULL;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.TAG_VAADIN_FLEX_LAYOUT;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.TAG_VAADIN_HORIZONTAL_LAYOUT;
import static de.codecamp.vaadin.flowdui.factories.layouts.OrderedLayoutFactory.TAG_VAADIN_VERTICAL_LAYOUT;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.BoxSizing;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.FlexLayout.FlexDirection;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.ThemableLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import de.codecamp.vaadin.base.util.SizeUtils;
import de.codecamp.vaadin.base.util.ThemableLayoutSpacing;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@DuiComponent(
    tagName = TAG_VAADIN_HORIZONTAL_LAYOUT,
    componentType = HorizontalLayout.class,
    description = "Default spacing of the Java component is removed per default.",
    docUrl = "https://vaadin.com/docs/latest/components/basic-layouts",
    category = CATEGORY_LAYOUTS,
    childAttributes = { //
        @DuiAttribute(
            name = CATTR_WIDTH_FULL,
            type = Double.class,
            custom = true,
            description = "Shorthand for <code>setFlexGrow(...)</code> (empty value will default to 1)."),
        @DuiAttribute(
            name = CATTR_HEIGHT_FULL,
            type = Double.class,
            custom = true,
            description = "Shorthand for <code>setAlignSelf(Alignment.STRETCH, ...)</code>."),
        @DuiAttribute(
            name = CATTR_SIZE_FULL,
            type = Double.class,
            custom = true,
            description = "Shorthand for <code>setFlexGrow(...)</code> (empty value will default to 1)  and <code>setAlignSelf(Alignment.STRETCH, ...)</code>.") //
    })
@DuiComponent(
    tagName = TAG_VAADIN_VERTICAL_LAYOUT,
    componentType = VerticalLayout.class,
    description = "Default width, padding, spacing of the Java component is removed per default.",
    docUrl = "https://vaadin.com/docs/latest/components/basic-layouts",
    category = CATEGORY_LAYOUTS,
    childAttributes = { //
        @DuiAttribute(
            name = CATTR_WIDTH_FULL,
            type = String.class,
            custom = true,
            description = "Shorthand for <code>setAlignSelf(Alignment.STRETCH, ...)</code>."),
        @DuiAttribute(
            name = CATTR_HEIGHT_FULL,
            type = String.class,
            custom = true,
            description = "Shorthand for <code>setFlexGrow(...)</code> (empty value will default to 1)."),
        @DuiAttribute(
            name = CATTR_SIZE_FULL,
            type = String.class,
            custom = true,
            description = "Shorthand for <code>setFlexGrow(...)</code> (empty value will default to 1) and <code>setAlignSelf(Alignment.STRETCH, ...)</code>.") //
    })
@DuiComponent(
    tagName = TAG_VAADIN_FLEX_LAYOUT,
    customTag = true,
    componentType = FlexLayout.class,
    description = "Doesn't exist as Web Component. Just a div with flexbox layout.",
    docUrl = "https://vaadin.com/docs/latest/components/basic-layouts",
    category = CATEGORY_LAYOUTS,
    attributes = { //
        @DuiAttribute(
            name = ATTR_DIRECTION,
            type = String.class,
            custom = true,
            description = "Allowed values: 'column', 'column-reverse', 'row' or 'row-reverse'"),
        @DuiAttribute(
            name = ATTR_WRAP,
            type = String.class,
            custom = true,
            description = "Allowed values: 'nowrap', 'wrap' or 'wrap-reverse'"),
        @DuiAttribute(
            name = ATTR_ALIGN_CONTENT,
            type = String.class,
            custom = true,
            description = "Allowed values: 'flex-start', 'flex-end', 'center', 'stretch', 'space-between' or 'space-around'") //
    },
    childAttributes = { //
        @DuiAttribute(name = CATTR_SHRINK, type = Double.class, custom = true, description = ""),
        @DuiAttribute(name = CATTR_BASIS, type = String.class, custom = true),
        @DuiAttribute(
            name = CATTR_WIDTH_FULL,
            type = String.class,
            custom = true,
            description = "For horizontal orientations a shorthand for <code>setFlexGrow(...)</code> (empty value will default to 1). For vertical orientations a shorthand for <code>setAlignSelf(Alignment.STRETCH, ...)</code>."),
        @DuiAttribute(
            name = CATTR_HEIGHT_FULL,
            type = String.class,
            custom = true,
            description = "For horizontal orientations a shorthand for <code>setAlignSelf(Alignment.STRETCH, ...)</code>. For vertical orientations a shorthand for <code>setFlexGrow(...)</code> (empty value will default to 1)."),
        @DuiAttribute(
            name = CATTR_SIZE_FULL,
            type = String.class,
            custom = true,
            description = "The same as calling <code>setFlexGrow(...)</code> (empty value will default to 1) and <code>setAlignSelf(Alignment.STRETCH, ...)</code>.") //
    })
@DuiComponent(
    componentType = ThemableLayout.class,
    category = CATEGORY_LAYOUTS,
    attributes = { //
        @DuiAttribute(name = ATTR_MARGIN, type = Boolean.class, custom = true),
        @DuiAttribute(name = ATTR_PADDING, type = Boolean.class, custom = true),
        @DuiAttribute(
            name = ATTR_SPACING,
            type = String.class,
            custom = true,
            description = "Allowed values: empty (defaults to 'm'), 'xs', 's', 'm' (default), 'l' or 'xl'"),
        @DuiAttribute(
            name = ATTR_BOX_SIZING,
            type = String.class,
            custom = true,
            description = "Allowed values: 'border-box' or 'content-box'") //
    })
@DuiComponent(
    componentType = FlexComponent.class,
    category = CATEGORY_LAYOUTS,
    attributes = { //
        @DuiAttribute(
            name = ATTR_ALIGN_ITEMS,
            type = String.class,
            custom = true,
            description = "Allowed values: '(flex-)start', '(flex-)end', 'center', 'stretch', 'baseline' or 'auto'"),
        @DuiAttribute(
            name = ATTR_JUSTIFY_CONTENT,
            type = String.class,
            custom = true,
            description = "Allowed values: '(flex-)start', '(flex-)end', 'center', 'space-between', 'space-around' or 'space-evenly'") //
    },
    slots = {SLOT_DEFAULT},
    childAttributes = { //
        @DuiAttribute(name = CATTR_ALIGN, type = String.class, custom = true, description = ""),
        @DuiAttribute(name = CATTR_GROW, type = Double.class, custom = true, description = ""),
        @DuiAttribute(
            name = CATTR_EXPAND,
            type = Boolean.class,
            custom = true,
            description = "The same as calling <code>expand(...)</code> or setting <code>flex-grow=\"1\"</code>. Consider using <code>width-full</code> or <code>height-full</code> instead.")

    })
public class OrderedLayoutFactory
  implements
    ComponentFactory,
    ComponentPostProcessor
{

  public static final String TAG_VAADIN_HORIZONTAL_LAYOUT = "vaadin-horizontal-layout";

  public static final String TAG_VAADIN_VERTICAL_LAYOUT = "vaadin-vertical-layout";

  public static final String TAG_VAADIN_FLEX_LAYOUT = "vaadin-flex-layout";


  public static final String ATTR_DIRECTION = "direction";

  public static final String ATTR_WRAP = "wrap";


  public static final String ATTR_JUSTIFY_CONTENT = "justify-content";

  public static final String ATTR_ALIGN_CONTENT = "align-content";

  public static final String ATTR_ALIGN_ITEMS = "align-items";


  public static final String ATTR_DEFAULT_ALIGN = "default-align";


  public static final String ATTR_MARGIN = "margin";

  public static final String ATTR_PADDING = "padding";

  public static final String ATTR_SPACING = "spacing";

  public static final String ATTR_BOX_SIZING = "box-sizing";


  public static final String CATTR_WIDTH_FULL = SizeUtils.ATTR_WIDTH_FULL;

  public static final String CATTR_HEIGHT_FULL = SizeUtils.ATTR_HEIGHT_FULL;

  public static final String CATTR_SIZE_FULL = SizeUtils.ATTR_SIZE_FULL;

  public static final String CATTR_ALIGN = "align";

  public static final String CATTR_GROW = "grow";

  public static final String CATTR_EXPAND = "expand";

  public static final String CATTR_SHRINK = "shrink";

  public static final String CATTR_BASIS = "basis";


  private static final Map<String, FlexComponent.Alignment> VALUES_ALIGNMENT = new HashMap<>();
  static
  {
    VALUES_ALIGNMENT.put("start", FlexComponent.Alignment.START);
    VALUES_ALIGNMENT.put("flex-start", FlexComponent.Alignment.START);
    VALUES_ALIGNMENT.put("end", FlexComponent.Alignment.END);
    VALUES_ALIGNMENT.put("flex-end", FlexComponent.Alignment.END);
    VALUES_ALIGNMENT.put("center", FlexComponent.Alignment.CENTER);
    VALUES_ALIGNMENT.put("stretch", FlexComponent.Alignment.STRETCH);
    VALUES_ALIGNMENT.put("baseline", FlexComponent.Alignment.BASELINE);
    VALUES_ALIGNMENT.put("auto", FlexComponent.Alignment.AUTO);
  }

  private static final Map<String, FlexComponent.JustifyContentMode> VALUES_JUSTIFY_CONTENT =
      new HashMap<>();
  static
  {
    VALUES_JUSTIFY_CONTENT.put("start", FlexComponent.JustifyContentMode.START);
    VALUES_JUSTIFY_CONTENT.put("flex-start", FlexComponent.JustifyContentMode.START);
    VALUES_JUSTIFY_CONTENT.put("end", FlexComponent.JustifyContentMode.END);
    VALUES_JUSTIFY_CONTENT.put("flex-end", FlexComponent.JustifyContentMode.END);
    VALUES_JUSTIFY_CONTENT.put("center", FlexComponent.JustifyContentMode.CENTER);
    VALUES_JUSTIFY_CONTENT.put("space-between", FlexComponent.JustifyContentMode.BETWEEN);
    VALUES_JUSTIFY_CONTENT.put("space-around", FlexComponent.JustifyContentMode.AROUND);
    VALUES_JUSTIFY_CONTENT.put("space-evenly", FlexComponent.JustifyContentMode.EVENLY);
  }

  private static final Map<String, FlexLayout.FlexWrap> VALUES_FLEX_WRAP = new HashMap<>();
  static
  {
    VALUES_FLEX_WRAP.put("nowrap", FlexLayout.FlexWrap.NOWRAP);
    VALUES_FLEX_WRAP.put("wrap", FlexLayout.FlexWrap.WRAP);
    VALUES_FLEX_WRAP.put("wrap-reverse", FlexLayout.FlexWrap.WRAP_REVERSE);
  }

  private static final Map<String, FlexLayout.FlexDirection> VALUES_FLEX_DIRECTION =
      new HashMap<>();
  static
  {
    VALUES_FLEX_DIRECTION.put("column", FlexLayout.FlexDirection.COLUMN);
    VALUES_FLEX_DIRECTION.put("column-reverse", FlexLayout.FlexDirection.COLUMN_REVERSE);
    VALUES_FLEX_DIRECTION.put("row", FlexLayout.FlexDirection.ROW);
    VALUES_FLEX_DIRECTION.put("row-reverse", FlexLayout.FlexDirection.ROW_REVERSE);
  }

  private static final Map<String, FlexLayout.ContentAlignment> VALUES_CONTENT_ALIGNMENT =
      new HashMap<>();
  static
  {
    VALUES_CONTENT_ALIGNMENT.put("start", FlexLayout.ContentAlignment.START);
    VALUES_CONTENT_ALIGNMENT.put("flex-start", FlexLayout.ContentAlignment.START);
    VALUES_CONTENT_ALIGNMENT.put("end", FlexLayout.ContentAlignment.END);
    VALUES_CONTENT_ALIGNMENT.put("flex-end", FlexLayout.ContentAlignment.END);
    VALUES_CONTENT_ALIGNMENT.put("center", FlexLayout.ContentAlignment.CENTER);
    VALUES_CONTENT_ALIGNMENT.put("stretch", FlexLayout.ContentAlignment.STRETCH);
    VALUES_CONTENT_ALIGNMENT.put("space-between", FlexLayout.ContentAlignment.SPACE_BETWEEN);
    VALUES_CONTENT_ALIGNMENT.put("space-around", FlexLayout.ContentAlignment.SPACE_AROUND);
  }

  private static final Map<String, BoxSizing> VALUES_BOX_SIZING = new HashMap<>();
  static
  {
    VALUES_BOX_SIZING.put("border-box", BoxSizing.BORDER_BOX);
    VALUES_BOX_SIZING.put("content-box", BoxSizing.CONTENT_BOX);
  }


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    Component component = null;
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_HORIZONTAL_LAYOUT:
      {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        // remove defaults that only exist in the Java API
        horizontalLayout.setSpacing(false);
        component = horizontalLayout;
        break;
      }

      case TAG_VAADIN_VERTICAL_LAYOUT:
      {
        VerticalLayout verticalLayout = new VerticalLayout();
        // remove defaults that only exist in the Java API
        verticalLayout.setWidth(null);
        verticalLayout.setPadding(false);
        verticalLayout.setSpacing(false);
        component = verticalLayout;
        break;
      }

      case TAG_VAADIN_FLEX_LAYOUT:
      {
        FlexLayout flexLayout = new FlexLayout();
        context.mapAttribute(ATTR_DIRECTION).asEnum(VALUES_FLEX_DIRECTION::get)
            .to(flexLayout::setFlexDirection);
        context.mapAttribute(ATTR_WRAP).asEnum(VALUES_FLEX_WRAP::get).to(flexLayout::setFlexWrap);
        context.mapAttribute(ATTR_ALIGN_CONTENT).asEnum(VALUES_CONTENT_ALIGNMENT::get)
            .to(flexLayout::setAlignContent);
        component = flexLayout;
        break;
      }
    }

    if (component instanceof FlexComponent)
    {
      FlexComponent flexComponent = (FlexComponent) component;

      context.mapAttribute(ATTR_JUSTIFY_CONTENT).asEnum(VALUES_JUSTIFY_CONTENT::get)
          .to(flexComponent::setJustifyContentMode);
      context.mapAttribute(List.of(ATTR_DEFAULT_ALIGN, ATTR_ALIGN_ITEMS))
          .asEnum(VALUES_ALIGNMENT::get).to(flexComponent::setAlignItems);

      FlexLayout flexLayout;
      if (flexComponent instanceof FlexLayout)
        flexLayout = (FlexLayout) flexComponent;
      else
        flexLayout = null;

      Boolean isHorizontalLayout;
      if (flexComponent instanceof HorizontalLayout
          || (flexLayout != null && (flexLayout.getFlexDirection() == FlexDirection.ROW
              || flexLayout.getFlexDirection() == FlexDirection.ROW_REVERSE)))
      {
        isHorizontalLayout = true;
      }
      else if (flexComponent instanceof VerticalLayout
          || (flexLayout != null && (flexLayout.getFlexDirection() == FlexDirection.COLUMN
              || flexLayout.getFlexDirection() == FlexDirection.COLUMN_REVERSE)))
      {
        isHorizontalLayout = false;
      }
      else
      {
        isHorizontalLayout = null;
      }

      context.readChildren(component, (slotName, childElement) ->
      {
        switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
        {
          case ComponentFactory.SLOT_DEFAULT:

            context.readComponent(childElement, (childComponent, childContext) ->
            {
              flexComponent.add(childComponent);

              childContext.mapAttribute(CATTR_ALIGN).asEnum(VALUES_ALIGNMENT::get)
                  .to(alignSelf -> flexComponent.setAlignSelf(alignSelf, childComponent));

              childContext.mapAttribute(CATTR_EXPAND).asBoolean()
                  .to(v -> flexComponent.expand(childComponent));

              if (isHorizontalLayout != null)
              {
                // the attributes are only supported where the orientation/direction can be determined
                Double sizeFull =
                    childContext.mapAttribute(CATTR_SIZE_FULL).asDouble(1D).get().orElse(null);

                Double widthFull =
                    childContext.mapAttribute(CATTR_WIDTH_FULL).asDouble(1D).get().orElse(sizeFull);
                Double heightFull = childContext.mapAttribute(CATTR_HEIGHT_FULL).asDouble(1D).get()
                    .orElse(sizeFull);

                if (widthFull != null)
                {
                  SizeUtils.setWidthFullAdaptive(flexComponent, new Component[] {childComponent},
                      isHorizontalLayout, widthFull);
                }
                if (heightFull != null)
                {
                  SizeUtils.setHeightFullAdaptive(flexComponent, new Component[] {childComponent},
                      isHorizontalLayout, heightFull);
                }
              }

              childContext.mapAttribute(CATTR_GROW).asDouble()
                  .to(flexGrow -> flexComponent.setFlexGrow(flexGrow, childComponent));

              if (flexLayout != null)
              {
                childContext.mapAttribute(CATTR_BASIS).asString()
                    .to(flexBasis -> flexLayout.setFlexBasis(flexBasis, childComponent));
                childContext.mapAttribute(CATTR_SHRINK).asDouble()
                    .to(flexShrink -> flexLayout.setFlexShrink(flexShrink, childComponent));
              }
            });

            return true;
        }
        return false;
      }, textNode ->
      {
        flexComponent.add(textNode.text());
      });
    }

    return component;
  }

  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    if (component instanceof ThemableLayout)
    {
      ThemableLayout themableLayout = (ThemableLayout) component;

      context.mapAttribute(ATTR_MARGIN).asBoolean().to(themableLayout::setMargin);
      context.mapAttribute(ATTR_PADDING).asBoolean().to(themableLayout::setPadding);
      context.mapAttribute(ATTR_SPACING).asString().to(value ->
      {
        switch (value)
        {
          case "":
            themableLayout.setSpacing(true);
            break;

          case "xs":
            ThemableLayoutSpacing.XS.applyTo(themableLayout);
            break;

          case "s":
            ThemableLayoutSpacing.S.applyTo(themableLayout);
            break;

          case "m":
            ThemableLayoutSpacing.M.applyTo(themableLayout);
            break;

          case "l":
            ThemableLayoutSpacing.L.applyTo(themableLayout);
            break;

          case "xl":
            ThemableLayoutSpacing.XL.applyTo(themableLayout);
            break;

          default:
            throw context.fail(String.format("Unsupported value found for attribute '%s': '%s'",
                ATTR_SPACING, value));
        }
      });
      context.mapAttribute(ATTR_BOX_SIZING).asEnum(VALUES_BOX_SIZING::get)
          .to(themableLayout::setBoxSizing);
    }
  }

}
