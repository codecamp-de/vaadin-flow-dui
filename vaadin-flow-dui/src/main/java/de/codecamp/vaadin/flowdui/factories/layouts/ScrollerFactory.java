package de.codecamp.vaadin.flowdui.factories.layouts;


import static de.codecamp.vaadin.flowdui.declare.DuiComponent.SLOT_DEFAULT;
import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_LAYOUTS;
import static de.codecamp.vaadin.flowdui.factories.layouts.ScrollerFactory.ATTR_SCROLL_DIRECTION;
import static de.codecamp.vaadin.flowdui.factories.layouts.ScrollerFactory.TAG_VAADIN_SCROLLER;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.orderedlayout.Scroller.ScrollDirection;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import java.util.HashMap;
import java.util.Map;


@DuiComponent(
    tagName = TAG_VAADIN_SCROLLER,
    componentType = Scroller.class,
    description = "Can only have a single child component as content.",
    docUrl = "https://vaadin.com/docs/latest/components/scroller",
    category = CATEGORY_LAYOUTS,
    attributes = { //
        @DuiAttribute(
            name = ATTR_SCROLL_DIRECTION,
            type = String.class,
            description = "Allowed values: 'both', 'horizontal', 'vertical' or 'none'") //
    },
    slots = {SLOT_DEFAULT})
public class ScrollerFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_SCROLLER = "vaadin-scroller";


  public static final String ATTR_SCROLL_DIRECTION = "scroll-direction";


  public static final Map<String, ScrollDirection> VALUES_SCROLL_DIRECTION = new HashMap<>();
  static
  {
    VALUES_SCROLL_DIRECTION.put("both", Scroller.ScrollDirection.BOTH);
    VALUES_SCROLL_DIRECTION.put("horizontal", Scroller.ScrollDirection.HORIZONTAL);
    VALUES_SCROLL_DIRECTION.put("vertical", Scroller.ScrollDirection.VERTICAL);
    VALUES_SCROLL_DIRECTION.put("none", Scroller.ScrollDirection.NONE);
  }


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_SCROLLER:
      {
        Scroller component = new Scroller();
        context.mapAttribute(ATTR_SCROLL_DIRECTION).asEnum(VALUES_SCROLL_DIRECTION::get)
            .to(component::setScrollDirection);

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_DEFAULT:
              if (component.getContent() != null)
                throw context.fail("Only one content component supported.");
              component.setContent(context.readComponent(childElement));
              return true;
          }
          return false;
        }, null);

        return component;
      }
    }

    return null;
  }

}
