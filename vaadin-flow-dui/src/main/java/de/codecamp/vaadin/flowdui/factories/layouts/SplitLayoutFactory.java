package de.codecamp.vaadin.flowdui.factories.layouts;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_LAYOUTS;
import static de.codecamp.vaadin.flowdui.factories.layouts.SplitLayoutFactory.ATTR_ORIENTATION;
import static de.codecamp.vaadin.flowdui.factories.layouts.SplitLayoutFactory.ATTR_VERTICAL;
import static de.codecamp.vaadin.flowdui.factories.layouts.SplitLayoutFactory.TAG_VAADIN_SPLIT_LAYOUT;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout.Orientation;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import java.util.Locale;


@DuiComponent(
    tagName = TAG_VAADIN_SPLIT_LAYOUT,
    componentType = SplitLayout.class,
    docUrl = "https://vaadin.com/docs/latest/components/split-layout",
    category = CATEGORY_LAYOUTS,
    attributes = { //
        @DuiAttribute(
            name = ATTR_ORIENTATION,
            type = String.class,
            description = "Allowed values: 'horizontal' (default) or 'vertical'"),
        @DuiAttribute(name = ATTR_VERTICAL, type = Boolean.class, custom = true) //
    })
public class SplitLayoutFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_SPLIT_LAYOUT = "vaadin-split-layout";


  public static final String SLOT_PRIMARY = "primary";

  public static final String SLOT_SECONDARY = "secondary";


  public static final String ATTR_ORIENTATION = "orientation";

  public static final String ATTR_VERTICAL = "vertical";


  public static final String VALUE_HORIZONTAL = "horizontal";

  public static final String VALUE_VERTICAL = "vertical";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_SPLIT_LAYOUT:
      {
        SplitLayout component = new SplitLayout();
        context.mapAttribute(ATTR_ORIENTATION)
            .asEnum(v -> Orientation.valueOf(v.toUpperCase(Locale.ENGLISH)))
            .to(component::setOrientation);
        context.mapAttribute(ATTR_VERTICAL).asBoolean()
            .to(v -> component.setOrientation(v ? Orientation.VERTICAL : Orientation.HORIZONTAL));

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_DEFAULT:
              if (component.getPrimaryComponent() == null)
                component.addToPrimary(context.readComponentForSlot(childElement));
              else if (component.getSecondaryComponent() == null)
                component.addToSecondary(context.readComponentForSlot(childElement));
              else
                throw context.fail("Primary and secondary component of SplitLayout already set.");
              return true;

            case SLOT_PRIMARY:
              if (component.getPrimaryComponent() != null)
                throw context.fail("Primary component of SplitLayout already set.");
              component.addToPrimary(context.readComponentForSlot(childElement));
              return true;

            case SLOT_SECONDARY:
              if (component.getSecondaryComponent() != null)
                throw context.fail("Secondary component of SplitLayout already set.");
              component.addToSecondary(context.readComponentForSlot(childElement));
              return true;
          }
          return false;
        }, null);

        return component;
      }
    }

    return null;
  }

}
