package de.codecamp.vaadin.flowdui.factories.pro;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_LAYOUTS;
import static de.codecamp.vaadin.flowdui.factories.pro.BoardFactory.TAG_VAADIN_BOARD;
import static de.codecamp.vaadin.flowdui.factories.pro.BoardFactory.TAG_VAADIN_BOARD_ROW;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.board.Board;
import com.vaadin.flow.component.board.Row;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_BOARD,
    componentType = Board.class,
    docUrl = "https://vaadin.com/docs/latest/components/board",
    category = CATEGORY_LAYOUTS,
    pro = true)
@DuiComponent(
    tagName = TAG_VAADIN_BOARD_ROW,
    componentType = Row.class,
    docUrl = "https://vaadin.com/docs/latest/components/board",
    category = CATEGORY_LAYOUTS,
    pro = true)
public class BoardFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_BOARD = "vaadin-board";

  public static final String TAG_VAADIN_BOARD_ROW = "vaadin-board-row";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_BOARD:
      {
        Board component = new Board();

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_DEFAULT:
              component.add(context.readComponent(childElement));
              return true;
          }
          return false;
        }, textNode ->
        {
          component.add(textNode.text());
        });

        return component;
      }

      case TAG_VAADIN_BOARD_ROW:
      {
        Row component = new Row();

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_DEFAULT:
              component.add(context.readComponent(childElement));
              return true;
          }
          return false;
        }, textNode ->
        {
          component.add(textNode.text());
        });

        return component;
      }
    }

    return null;
  }

}
