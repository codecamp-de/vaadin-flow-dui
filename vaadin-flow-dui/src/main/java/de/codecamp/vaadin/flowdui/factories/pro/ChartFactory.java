package de.codecamp.vaadin.flowdui.factories.pro;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_VISUALIZATION_AND_INTERACTION;
import static de.codecamp.vaadin.flowdui.factories.pro.ChartFactory.TAG_VAADIN_CHART;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.charts.Chart;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_CHART,
    componentType = Chart.class,
    docUrl = "https://vaadin.com/docs/latest/components/charts",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    pro = true)
public class ChartFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_CHART = "vaadin-chart";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_CHART:
      {
        Chart component = new Chart();

        context.readChildren(component);

        return component;
      }
    }

    return null;
  }

}
