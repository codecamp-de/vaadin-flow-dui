package de.codecamp.vaadin.flowdui.factories.pro;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_VISUALIZATION_AND_INTERACTION;
import static de.codecamp.vaadin.flowdui.factories.pro.CookieConsentFactory.ATTR_DISMISS;
import static de.codecamp.vaadin.flowdui.factories.pro.CookieConsentFactory.ATTR_LEARN_MORE;
import static de.codecamp.vaadin.flowdui.factories.pro.CookieConsentFactory.ATTR_LEARN_MORE_LINK;
import static de.codecamp.vaadin.flowdui.factories.pro.CookieConsentFactory.ATTR_MESSAGE;
import static de.codecamp.vaadin.flowdui.factories.pro.CookieConsentFactory.TAG_VAADIN_COOKIE_CONSENT;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.cookieconsent.CookieConsent;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_COOKIE_CONSENT,
    componentType = CookieConsent.class,
    docUrl = "https://vaadin.com/docs/latest/components/cookie-consent",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    pro = true,
    attributes = { //
        @DuiAttribute(name = ATTR_MESSAGE, type = String.class), //
        @DuiAttribute(name = ATTR_DISMISS, type = String.class), //
        @DuiAttribute(name = ATTR_LEARN_MORE, type = String.class), //
        @DuiAttribute(name = ATTR_LEARN_MORE_LINK, type = String.class) //
    })
public class CookieConsentFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_COOKIE_CONSENT = "vaadin-cookie-consent";


  public static final String ATTR_MESSAGE = "message";

  public static final String ATTR_DISMISS = "dismiss";

  public static final String ATTR_LEARN_MORE = "learn-more";

  public static final String ATTR_LEARN_MORE_LINK = "learn-more-link";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_COOKIE_CONSENT:
      {
        CookieConsent component = new CookieConsent();
        context.mapAttribute(ATTR_MESSAGE).asString().to(component::setMessage);
        context.mapAttribute(ATTR_DISMISS).asString().to(component::setDismissLabel);
        context.mapAttribute(ATTR_LEARN_MORE).asString().to(component::setLearnMoreLabel);
        context.mapAttribute(ATTR_LEARN_MORE_LINK).asString().to(component::setLearnMoreLink);

        context.readChildren(component);

        return component;
      }
    }

    return null;
  }

}
