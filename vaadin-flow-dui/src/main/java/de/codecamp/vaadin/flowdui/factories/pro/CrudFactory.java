package de.codecamp.vaadin.flowdui.factories.pro;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_VISUALIZATION_AND_INTERACTION;
import static de.codecamp.vaadin.flowdui.factories.pro.CrudFactory.TAG_VAADIN_CRUD;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.crud.Crud;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_CRUD,
    componentType = Crud.class,
    docUrl = "https://vaadin.com/docs/latest/components/crud",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    pro = true)
public class CrudFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_CRUD = "vaadin-crud";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_CRUD:
      {
        Crud<?> component = new Crud<>();

        context.readChildren(component);

        return component;
      }
    }

    return null;
  }

}
