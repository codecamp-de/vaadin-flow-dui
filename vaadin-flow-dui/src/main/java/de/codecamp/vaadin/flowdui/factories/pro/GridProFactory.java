package de.codecamp.vaadin.flowdui.factories.pro;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_VISUALIZATION_AND_INTERACTION;
import static de.codecamp.vaadin.flowdui.factories.pro.GridProFactory.TAG_VAADIN_GRID_PRO;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.gridpro.GridPro;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_GRID_PRO,
    componentType = GridPro.class,
    docUrl = "https://vaadin.com/docs/latest/components/grid-pro",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    pro = true)
public class GridProFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_GRID_PRO = "vaadin-grid-pro";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_GRID_PRO:
      {
        GridPro<?> component = new GridPro<>();

        context.readChildren(component,
            "GridPro cannot be populated using a template. Use its Java API instead.");

        return component;
      }
    }

    return null;
  }

}
