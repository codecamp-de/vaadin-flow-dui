package de.codecamp.vaadin.flowdui.factories.pro;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_DATA_ENTRY;
import static de.codecamp.vaadin.flowdui.factories.pro.RichTextEditorFactory.TAG_VAADIN_RICH_TEXT_EDITOR;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.richtexteditor.RichTextEditor;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_RICH_TEXT_EDITOR,
    componentType = RichTextEditor.class,
    docUrl = "https://vaadin.com/docs/latest/components/rich-text-editor",
    category = CATEGORY_DATA_ENTRY,
    pro = true)
public class RichTextEditorFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_RICH_TEXT_EDITOR = "vaadin-rich-text-editor";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_RICH_TEXT_EDITOR:
      {
        RichTextEditor component = new RichTextEditor();

        context.readChildren(component);

        return component;
      }
    }

    return null;
  }

}
