package de.codecamp.vaadin.flowdui.factories.shared;


import static de.codecamp.vaadin.flowdui.factories.shared.HasAllowedCharPatternPostProcessor.ATTR_ALLOWED_CHAR_PATTERN;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.shared.HasAllowedCharPattern;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    componentType = HasAllowedCharPattern.class,
    attributes = { //
        @DuiAttribute(name = ATTR_ALLOWED_CHAR_PATTERN, type = String.class) //
    })
public class HasAllowedCharPatternPostProcessor
  implements
    ComponentPostProcessor
{

  public static final String ATTR_ALLOWED_CHAR_PATTERN = "allowed-char-pattern";


  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    if (component instanceof HasAllowedCharPattern)
    {
      HasAllowedCharPattern hasAllowedCharPattern = (HasAllowedCharPattern) component;
      context.mapAttribute(ATTR_ALLOWED_CHAR_PATTERN).asString()
          .to(hasAllowedCharPattern::setAllowedCharPattern);
    }
  }

}
