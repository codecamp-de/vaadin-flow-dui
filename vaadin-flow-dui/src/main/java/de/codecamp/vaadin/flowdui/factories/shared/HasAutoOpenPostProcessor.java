package de.codecamp.vaadin.flowdui.factories.shared;


import static de.codecamp.vaadin.flowdui.factories.shared.HasAutoOpenPostProcessor.ATTR_AUTO_OPEN_DISABLED;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.shared.HasAutoOpen;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    componentType = HasAutoOpen.class,
    attributes = { //
        @DuiAttribute(name = ATTR_AUTO_OPEN_DISABLED, type = Boolean.class) //
    })
public class HasAutoOpenPostProcessor
  implements
    ComponentPostProcessor
{

  public static final String ATTR_AUTO_OPEN_DISABLED = "auto-open-disabled";


  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    if (component instanceof HasAutoOpen)
    {
      HasAutoOpen hasAutoOpen = (HasAutoOpen) component;
      context.mapAttribute(ATTR_AUTO_OPEN_DISABLED).asBoolean()
          .to(v -> hasAutoOpen.setAutoOpen(!v));
    }
  }

}
