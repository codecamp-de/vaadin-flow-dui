package de.codecamp.vaadin.flowdui.factories.shared;


import static de.codecamp.vaadin.flowdui.factories.shared.HasClearButtonPostProcessor.ATTR_CLEAR_BUTTON_VISIBLE;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.shared.HasClearButton;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    componentType = HasClearButton.class,
    attributes = { //
        @DuiAttribute(name = ATTR_CLEAR_BUTTON_VISIBLE, type = Boolean.class) //
    })
public class HasClearButtonPostProcessor
  implements
    ComponentPostProcessor
{

  public static final String ATTR_CLEAR_BUTTON_VISIBLE = "clear-button-visible";


  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    if (component instanceof HasClearButton)
    {
      HasClearButton hasClearButton = (HasClearButton) component;
      context.mapAttribute(ATTR_CLEAR_BUTTON_VISIBLE).asBoolean()
          .to(hasClearButton::setClearButtonVisible);
    }
  }

}
