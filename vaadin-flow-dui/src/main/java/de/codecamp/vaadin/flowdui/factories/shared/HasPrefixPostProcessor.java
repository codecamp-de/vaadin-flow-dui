package de.codecamp.vaadin.flowdui.factories.shared;


import static de.codecamp.vaadin.flowdui.factories.shared.HasPrefixPostProcessor.SLOT_PREFIX;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.shared.HasPrefix;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.TemplateParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import org.jsoup.nodes.Element;


@DuiComponent(
    componentType = HasPrefix.class, //
    slots = {SLOT_PREFIX})
public class HasPrefixPostProcessor
  implements
    ComponentPostProcessor
{

  public static final String SLOT_PREFIX = "prefix";


  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    // nothing to do
  }

  @Override
  public boolean handleChildElement(Component parentComponent, String slotName,
      Element childElement, TemplateParserContext context)
  {
    if (parentComponent instanceof HasPrefix)
    {
      HasPrefix hasPrefix = (HasPrefix) parentComponent;
      switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
      {
        case SLOT_PREFIX:
          if (hasPrefix.getPrefixComponent() != null)
            throw context.fail(childElement, "Slot 'prefix' already filled.");
          hasPrefix.setPrefixComponent(context.readComponentForSlot(childElement));
          return true;
      }
    }

    return false;
  }

}
