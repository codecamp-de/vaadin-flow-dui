package de.codecamp.vaadin.flowdui.factories.shared;


import static de.codecamp.vaadin.flowdui.factories.shared.HasSuffixPostProcessor.SLOT_SUFFIX;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.shared.HasSuffix;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.TemplateParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import org.jsoup.nodes.Element;


@DuiComponent(
    componentType = HasSuffix.class, //
    slots = {SLOT_SUFFIX})
public class HasSuffixPostProcessor
  implements
    ComponentPostProcessor
{

  public static final String SLOT_SUFFIX = "suffix";


  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    // nothing to do
  }

  @Override
  public boolean handleChildElement(Component parentComponent, String slotName,
      Element childElement, TemplateParserContext context)
  {
    if (parentComponent instanceof HasSuffix)
    {
      HasSuffix hasSuffix = (HasSuffix) parentComponent;
      switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
      {
        case SLOT_SUFFIX:
          if (hasSuffix.getSuffixComponent() != null)
            throw context.fail(childElement, "Slot 'suffix' already filled.");
          hasSuffix.setSuffixComponent(context.readComponentForSlot(childElement));
          return true;
      }
    }

    return false;
  }

}
