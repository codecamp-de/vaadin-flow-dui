package de.codecamp.vaadin.flowdui.factories.shared;


import static de.codecamp.vaadin.flowdui.factories.shared.HasTooltipPostProcessor.ATTR_TOOLTIP;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.shared.HasTooltip;
import de.codecamp.vaadin.flowdui.ComponentPostProcessor;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    componentType = HasTooltip.class,
    attributes = { //
        @DuiAttribute(name = ATTR_TOOLTIP, type = String.class, custom = true) //
    })
public class HasTooltipPostProcessor
  implements
    ComponentPostProcessor
{

  public static final String ATTR_TOOLTIP = "tooltip";


  @Override
  public void postProcessComponent(Component component, ElementParserContext context)
  {
    if (component instanceof HasTooltip)
    {
      HasTooltip hasTooltip = (HasTooltip) component;
      context.mapAttribute(ATTR_TOOLTIP).asString().to(hasTooltip::setTooltipText);
    }
  }

}
