package de.codecamp.vaadin.flowdui.factories.visandint;


import static de.codecamp.vaadin.flowdui.declare.DuiComponent.SLOT_DEFAULT;
import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_VISUALIZATION_AND_INTERACTION;
import static de.codecamp.vaadin.flowdui.factories.visandint.AccordionFactory.ATTR_CLOSED;
import static de.codecamp.vaadin.flowdui.factories.visandint.AccordionFactory.SLOT_SUMMARY;
import static de.codecamp.vaadin.flowdui.factories.visandint.AccordionFactory.TAG_VAADIN_ACCORDION;
import static de.codecamp.vaadin.flowdui.factories.visandint.AccordionFactory.TAG_VAADIN_ACCORDION_PANEL;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.accordion.AccordionPanel;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_ACCORDION,
    componentType = Accordion.class,
    docUrl = "https://vaadin.com/docs/latest/components/accordion",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    attributes = { //
        @DuiAttribute(name = ATTR_CLOSED, type = Boolean.class, custom = true) //
    },
    slots = {SLOT_DEFAULT})
@DuiComponent(
    tagName = TAG_VAADIN_ACCORDION_PANEL,
    componentType = AccordionPanel.class,
    docUrl = "https://vaadin.com/docs/latest/components/accordion",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    attributes = { //
        @DuiAttribute(name = ATTR_CLOSED, type = Boolean.class, custom = true) //
    },
    slots = {SLOT_DEFAULT, SLOT_SUMMARY})
public class AccordionFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_ACCORDION = "vaadin-accordion";

  public static final String TAG_VAADIN_ACCORDION_PANEL = "vaadin-accordion-panel";


  public static final String SLOT_SUMMARY = "summary";


  public static final String ATTR_CLOSED = "closed";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_ACCORDION:
      {
        Accordion component = new Accordion();
        context.mapAttribute(ATTR_CLOSED).asBoolean().to(v -> component.close());

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_DEFAULT:
              if (!childElement.tagName().equals(TAG_VAADIN_ACCORDION_PANEL))
              {
                throw context.fail("Accordion only supports AccordionPanel as child component.");
              }

              AccordionPanel accordionPanel = (AccordionPanel) context.readComponent(childElement);
              component.add(accordionPanel);
              return true;
          }
          return false;
        }, null);

        return component;
      }

      case TAG_VAADIN_ACCORDION_PANEL:
      {
        AccordionPanel component = new AccordionPanel();

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_DEFAULT:
              component.add(context.readComponent(childElement));
              return true;

            case SLOT_SUMMARY:
              if (component.getSummary() != null)
              {
                throw context.fail("Only one component for the 'summary' slot supported.");
              }
              component.setSummary(context.readComponentForSlot(childElement));
              return true;
          }
          return false;
        }, textNode ->
        {
          component.add(new Text(textNode.text()));
        });

        return component;
      }
    }

    return null;
  }

}
