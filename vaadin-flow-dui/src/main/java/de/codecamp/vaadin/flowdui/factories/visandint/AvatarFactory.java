package de.codecamp.vaadin.flowdui.factories.visandint;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_VISUALIZATION_AND_INTERACTION;
import static de.codecamp.vaadin.flowdui.factories.visandint.AvatarFactory.ATTR_ABBR;
import static de.codecamp.vaadin.flowdui.factories.visandint.AvatarFactory.ATTR_COLOR_INDEX;
import static de.codecamp.vaadin.flowdui.factories.visandint.AvatarFactory.ATTR_IMG;
import static de.codecamp.vaadin.flowdui.factories.visandint.AvatarFactory.ATTR_MAX;
import static de.codecamp.vaadin.flowdui.factories.visandint.AvatarFactory.ATTR_MAX_ITEMS_VISIBLE;
import static de.codecamp.vaadin.flowdui.factories.visandint.AvatarFactory.ATTR_NAME;
import static de.codecamp.vaadin.flowdui.factories.visandint.AvatarFactory.TAG_VAADIN_AVATAR;
import static de.codecamp.vaadin.flowdui.factories.visandint.AvatarFactory.TAG_VAADIN_AVATAR_GROUP;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.avatar.AvatarGroup;
import de.codecamp.vaadin.base.i18n.ComponentI18n;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import java.util.Set;


@DuiComponent(
    tagName = TAG_VAADIN_AVATAR,
    componentType = Avatar.class,
    docUrl = "https://vaadin.com/docs/latest/components/avatar",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    attributes = { //
        @DuiAttribute(name = ATTR_NAME, type = String.class),
        @DuiAttribute(name = ATTR_ABBR, type = String.class),
        @DuiAttribute(name = ATTR_IMG, type = String.class),
        @DuiAttribute(name = ATTR_COLOR_INDEX, type = Integer.class) //
    })
@DuiComponent(
    tagName = TAG_VAADIN_AVATAR_GROUP,
    componentType = AvatarGroup.class,
    docUrl = "https://vaadin.com/docs/latest/components/avatar",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    attributes = { //
        @DuiAttribute(name = ATTR_MAX, type = Integer.class),
        @DuiAttribute(name = ATTR_MAX_ITEMS_VISIBLE, type = Integer.class) //
    })
public class AvatarFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_AVATAR = "vaadin-avatar";

  public static final String TAG_VAADIN_AVATAR_GROUP = "vaadin-avatar-group";


  public static final String ATTR_NAME = "name";

  public static final String ATTR_ABBR = "abbr";

  public static final String ATTR_IMG = "img";

  public static final String ATTR_COLOR_INDEX = "color-index";

  public static final String ATTR_WITH_TOOLTIP = "with-tooltip";

  public static final String ATTR_MAX = "max";

  public static final String ATTR_MAX_ITEMS_VISIBLE = "max-items-visible";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_AVATAR:
      {
        Avatar component = new Avatar();
        context.mapAttribute(ATTR_NAME).asString().to(component::setName);
        context.mapAttribute(ATTR_ABBR).asString().to(component::setAbbreviation);
        context.mapAttribute(ATTR_IMG).asString().to(component::setImage);
        context.mapAttribute(ATTR_COLOR_INDEX).asInteger().to(component::setColorIndex);
        context.mapAttribute(ATTR_WITH_TOOLTIP).asBoolean().to(component::setTooltipEnabled);

        ComponentI18n.localize(component);

        context.readChildren(component);

        return component;
      }

      case TAG_VAADIN_AVATAR_GROUP:
      {
        AvatarGroup component = new AvatarGroup();
        context.mapAttribute(ATTR_MAX_ITEMS_VISIBLE, Set.of(ATTR_MAX)).asInteger()
            .to(component::setMaxItemsVisible);

        ComponentI18n.localize(component);

        context.readChildren(component);

        return component;
      }
    }

    return null;
  }

}
