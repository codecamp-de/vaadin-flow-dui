package de.codecamp.vaadin.flowdui.factories.visandint;


import static de.codecamp.vaadin.flowdui.declare.DuiComponent.SLOT_DEFAULT;
import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_VISUALIZATION_AND_INTERACTION;
import static de.codecamp.vaadin.flowdui.factories.visandint.ButtonFactory.ATTR_AUTOFOCUS;
import static de.codecamp.vaadin.flowdui.factories.visandint.ButtonFactory.ATTR_DISABLE_ON_CLICK;
import static de.codecamp.vaadin.flowdui.factories.visandint.ButtonFactory.ATTR_ICON;
import static de.codecamp.vaadin.flowdui.factories.visandint.ButtonFactory.TAG_VAADIN_BUTTON;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.lang3.EnumUtils;


@DuiComponent(
    tagName = TAG_VAADIN_BUTTON,
    componentType = Button.class,
    description = "Slots support only one component each. This is a limit of the Java Component.",
    docUrl = "https://vaadin.com/docs/latest/components/button",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    attributes = { //
        @DuiAttribute(name = ATTR_AUTOFOCUS, type = Boolean.class),
        @DuiAttribute(name = ATTR_DISABLE_ON_CLICK, type = Boolean.class, custom = true),
        @DuiAttribute(
            name = ATTR_ICON,
            type = String.class,
            custom = true,
            description = "Use <iconName> for Vaadin icons. Use <collection>:<icon> for icons of other collections.")},
    slots = {SLOT_DEFAULT})
public class ButtonFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_BUTTON = "vaadin-button";


  public static final String ATTR_AUTOFOCUS = "autofocus";

  public static final String ATTR_DISABLE_ON_CLICK = "disable-on-click";

  public static final String ATTR_ICON = "icon";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_BUTTON:
      {
        Button component = new Button();
        context.mapAttribute(ATTR_AUTOFOCUS).asBoolean().to(component::setAutofocus);
        context.mapAttribute(ATTR_DISABLE_ON_CLICK).asBoolean().to(component::setDisableOnClick);

        AtomicBoolean iconFound = new AtomicBoolean(false);
        context.mapAttribute(ATTR_ICON).asString().to(iconString ->
        {
          Icon icon;
          if (iconString.contains(":"))
          {
            String[] iconStringTokens = iconString.split(":", 2);
            icon = new Icon(iconStringTokens[0], iconStringTokens[1]);
          }
          else
          {
            icon = Optional.ofNullable(EnumUtils.getEnum(VaadinIcon.class, iconString))
                .map(VaadinIcon::create).orElseGet(() -> new Icon(iconString));
          }
          component.setIcon(icon);
          iconFound.set(true);
        });

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_DEFAULT:
              component.getElement().appendChild(context.readComponent(childElement).getElement());
              return true;
          }
          return false;
        }, textNode ->
        {
          component.getElement()
              .appendChild(com.vaadin.flow.dom.Element.createText(textNode.text()));
        });

        return component;
      }
    }

    return null;
  }

}
