package de.codecamp.vaadin.flowdui.factories.visandint;


import static de.codecamp.vaadin.flowdui.declare.DuiComponent.SLOT_DEFAULT;
import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_VISUALIZATION_AND_INTERACTION;
import static de.codecamp.vaadin.flowdui.factories.visandint.DetailsFactory.ATTR_OPENED;
import static de.codecamp.vaadin.flowdui.factories.visandint.DetailsFactory.SLOT_SUMMARY;
import static de.codecamp.vaadin.flowdui.factories.visandint.DetailsFactory.TAG_VAADIN_DETAILS;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.details.Details;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_DETAILS,
    componentType = Details.class,
    docUrl = "https://vaadin.com/docs/latest/components/details",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    attributes = { //
        @DuiAttribute(name = ATTR_OPENED, type = Boolean.class) //
    },
    slots = {SLOT_DEFAULT, SLOT_SUMMARY})
public class DetailsFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_DETAILS = "vaadin-details";


  public static final String SLOT_SUMMARY = "summary";


  public static final String ATTR_OPENED = "opened";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_DETAILS:
      {
        Details component = new Details();
        context.mapAttribute(ATTR_OPENED).asBoolean().to(component::setOpened);

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_DEFAULT:
              component.add(context.readComponent(childElement));
              return true;

            case SLOT_SUMMARY:
              if (component.getSummary() != null)
                throw context.fail("Only one component for the 'summary' slot supported.");
              component.setSummary(context.readComponentForSlot(childElement));
              return true;
          }
          return false;
        }, textNode ->
        {
          component.add(new Text(textNode.text()));
        });

        return component;
      }
    }

    return null;
  }

}
