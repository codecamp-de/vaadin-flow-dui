package de.codecamp.vaadin.flowdui.factories.visandint;


import static de.codecamp.vaadin.flowdui.declare.DuiComponent.SLOT_DEFAULT;
import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_VISUALIZATION_AND_INTERACTION;
import static de.codecamp.vaadin.flowdui.factories.visandint.DialogFactory.ATTR_NO_CLOSE_ON_ESC;
import static de.codecamp.vaadin.flowdui.factories.visandint.DialogFactory.ATTR_NO_CLOSE_ON_OUTSIDE_CLICK;
import static de.codecamp.vaadin.flowdui.factories.visandint.DialogFactory.TAG_VAADIN_DIALOG;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dialog.Dialog;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_DIALOG,
    componentType = Dialog.class,
    docUrl = "https://vaadin.com/docs/latest/components/dialog",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    attributes = { //
        @DuiAttribute(name = ATTR_NO_CLOSE_ON_ESC, type = Boolean.class), //
        @DuiAttribute(name = ATTR_NO_CLOSE_ON_OUTSIDE_CLICK, type = Boolean.class) //
    },
    slots = {SLOT_DEFAULT})
public class DialogFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_DIALOG = "vaadin-dialog";


  public static final String ATTR_NO_CLOSE_ON_ESC = "no-close-on-esc";

  public static final String ATTR_NO_CLOSE_ON_OUTSIDE_CLICK = "no-close-on-outside-click";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_DIALOG:
      {
        Dialog component = new Dialog();
        context.mapAttribute(ATTR_NO_CLOSE_ON_ESC).asBoolean().to(v -> component.setCloseOnEsc(!v));
        context.mapAttribute(ATTR_NO_CLOSE_ON_OUTSIDE_CLICK).asBoolean()
            .to(v -> component.setCloseOnOutsideClick(!v));

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_DEFAULT:
              component.add(context.readComponent(childElement));
              return true;
          }
          return false;
        }, textNode ->
        {
          component.add(textNode.text());
        });

        return component;
      }
    }

    return null;
  }

}
