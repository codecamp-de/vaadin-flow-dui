package de.codecamp.vaadin.flowdui.factories.visandint;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_VISUALIZATION_AND_INTERACTION;
import static de.codecamp.vaadin.flowdui.factories.visandint.GridFactory.ATTR_ALL_ROWS_VISIBLE;
import static de.codecamp.vaadin.flowdui.factories.visandint.GridFactory.TAG_VAADIN_GRID;
import static de.codecamp.vaadin.flowdui.factories.visandint.GridFactory.TAG_VAADIN_TREE_GRID;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.treegrid.TreeGrid;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_GRID,
    componentType = Grid.class,
    docUrl = "https://vaadin.com/docs/latest/components/grid",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    attributes = { //
        @DuiAttribute(name = ATTR_ALL_ROWS_VISIBLE, type = Boolean.class) //
    })
@DuiComponent(
    tagName = TAG_VAADIN_TREE_GRID,
    customTag = true,
    componentType = TreeGrid.class,
    docUrl = "https://vaadin.com/docs/latest/components/tree-grid",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    attributes = { //
        @DuiAttribute(name = ATTR_ALL_ROWS_VISIBLE, type = Boolean.class) //
    })
public class GridFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_GRID = "vaadin-grid";

  public static final String TAG_VAADIN_TREE_GRID = "vaadin-tree-grid";


  public static final String ATTR_ALL_ROWS_VISIBLE = "all-rows-visible";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_GRID:
      {
        Grid<?> component = new Grid<>();
        context.mapAttribute(ATTR_ALL_ROWS_VISIBLE).asBoolean().to(component::setAllRowsVisible);

        context.readChildren(component,
            "Grid cannot be populated using a template. Use its Java API instead.");

        return component;
      }

      case TAG_VAADIN_TREE_GRID:
      {
        // does not actually exist as a separate web component
        TreeGrid<?> component = new TreeGrid<>();
        context.mapAttribute(ATTR_ALL_ROWS_VISIBLE).asBoolean().to(component::setAllRowsVisible);

        context.readChildren(component,
            "TreeGrid cannot be populated using a template. Use its Java API instead.");

        return component;
      }
    }

    return null;
  }

}
