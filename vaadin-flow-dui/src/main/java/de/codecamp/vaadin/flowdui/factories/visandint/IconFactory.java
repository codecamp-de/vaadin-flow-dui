package de.codecamp.vaadin.flowdui.factories.visandint;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_VISUALIZATION_AND_INTERACTION;
import static de.codecamp.vaadin.flowdui.factories.visandint.IconFactory.ATTR_CHAR;
import static de.codecamp.vaadin.flowdui.factories.visandint.IconFactory.ATTR_COLOR;
import static de.codecamp.vaadin.flowdui.factories.visandint.IconFactory.ATTR_FONT_FAMILY;
import static de.codecamp.vaadin.flowdui.factories.visandint.IconFactory.ATTR_ICON;
import static de.codecamp.vaadin.flowdui.factories.visandint.IconFactory.ATTR_ICON_CLASS;
import static de.codecamp.vaadin.flowdui.factories.visandint.IconFactory.ATTR_LIGATURE;
import static de.codecamp.vaadin.flowdui.factories.visandint.IconFactory.ATTR_SIZE;
import static de.codecamp.vaadin.flowdui.factories.visandint.IconFactory.ATTR_SRC;
import static de.codecamp.vaadin.flowdui.factories.visandint.IconFactory.ATTR_SYMBOL;
import static de.codecamp.vaadin.flowdui.factories.visandint.IconFactory.TAG_VAADIN_ICON;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.icon.AbstractIcon;
import com.vaadin.flow.component.icon.FontIcon;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.SvgIcon;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_ICON,
    componentType = Icon.class,
    docUrl = "https://vaadin.com/docs/latest/ds/foundation/icons",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    description = "Depending on the used attributes, will create an Icon, SvgIcon or FontIcon.",
    attributes = { //
        @DuiAttribute(
            name = ATTR_ICON,
            type = String.class,
            description = "Creates a regular Icon."), //
        @DuiAttribute(name = ATTR_SRC, type = String.class, description = "Creates an SvgIcon."), //
        @DuiAttribute(name = ATTR_SYMBOL, type = String.class, description = "Creates an SvgIcon."), //
        @DuiAttribute(name = ATTR_CHAR, type = String.class, description = "Creates a FontIcon."), //
        @DuiAttribute(
            name = ATTR_FONT_FAMILY,
            type = String.class,
            description = "Creates a FontIcon."), //
        @DuiAttribute(
            name = ATTR_ICON_CLASS,
            type = String.class,
            description = "Creates a FontIcon."), //
        @DuiAttribute(
            name = ATTR_LIGATURE,
            type = String.class,
            description = "Creates a FontIcon."), //
        @DuiAttribute(name = ATTR_SIZE, type = String.class, custom = true), //
        @DuiAttribute(name = ATTR_COLOR, type = String.class, custom = true) //
    })
public class IconFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_ICON = "vaadin-icon";


  /* Icon (icon collections, like vaadin and lumo) */

  public static final String ATTR_ICON = "icon";

  /* SvgIcon */

  public static final String ATTR_SRC = "src";

  public static final String ATTR_SYMBOL = "symbol";

  /* FontIcon */

  public static final String ATTR_CHAR = "char";

  public static final String ATTR_FONT_FAMILY = "font-family";

  public static final String ATTR_ICON_CLASS = "icon-class";

  public static final String ATTR_LIGATURE = "ligature";

  /* common */

  public static final String ATTR_SIZE = "size";

  public static final String ATTR_COLOR = "color";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_ICON:
      {
        AbstractIcon<?> component;

        if (context.hasAttribute(ATTR_SRC, ATTR_SYMBOL))
        {
          SvgIcon icon = new SvgIcon();

          context.mapAttribute(ATTR_SRC).asString().to(icon::setSrc);
          context.mapAttribute(ATTR_SYMBOL).asString().to(icon::setSymbol);

          component = icon;
        }

        else if (context.hasAttribute(ATTR_CHAR, ATTR_FONT_FAMILY, ATTR_ICON_CLASS, ATTR_LIGATURE))
        {
          FontIcon icon = new FontIcon();

          context.mapAttribute(ATTR_CHAR).asString().to(icon::setCharCode);
          context.mapAttribute(ATTR_FONT_FAMILY).asString().to(icon::setFontFamily);
          context.mapAttribute(ATTR_ICON_CLASS).asString().to(icon::setIconClassNames);
          context.mapAttribute(ATTR_LIGATURE).asString().to(icon::setLigature);

          component = icon;
        }

        else
        {
          String attrIcon = context.mapAttribute(ATTR_ICON).asString().get().orElse(null);

          Icon icon;
          if (attrIcon == null)
          {
            icon = new Icon();
          }
          else
          {
            String[] attrIconTokens = attrIcon.split(":", 2);
            if (attrIconTokens.length > 1)
              icon = new Icon(attrIconTokens[0], attrIconTokens[1]);
            else
              icon = new Icon(attrIcon);
          }
          component = icon;
        }

        context.mapAttribute(ATTR_COLOR).asString().to(component::setColor);
        context.mapAttribute(ATTR_SIZE).asString().to(component::setSize);

        context.readChildren(component);

        return component;
      }
    }

    return null;
  }

}
