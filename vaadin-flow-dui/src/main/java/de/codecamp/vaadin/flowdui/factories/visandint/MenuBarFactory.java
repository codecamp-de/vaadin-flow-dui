package de.codecamp.vaadin.flowdui.factories.visandint;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_VISUALIZATION_AND_INTERACTION;
import static de.codecamp.vaadin.flowdui.factories.visandint.MenuBarFactory.ATTR_OPEN_ON_HOVER;
import static de.codecamp.vaadin.flowdui.factories.visandint.MenuBarFactory.ATTR_REVERSE_COLLAPSE;
import static de.codecamp.vaadin.flowdui.factories.visandint.MenuBarFactory.TAG_VAADIN_MENU_BAR;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.menubar.MenuBar;
import de.codecamp.vaadin.base.i18n.ComponentI18n;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_MENU_BAR,
    componentType = MenuBar.class,
    docUrl = "https://vaadin.com/docs/latest/components/menu-bar",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    attributes = { //
        @DuiAttribute(name = ATTR_OPEN_ON_HOVER, type = Boolean.class), //
        @DuiAttribute(name = ATTR_REVERSE_COLLAPSE, type = Boolean.class) //
    })
public class MenuBarFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_MENU_BAR = "vaadin-menu-bar";


  public static final String ATTR_OPEN_ON_HOVER = "open-on-hover";

  public static final String ATTR_REVERSE_COLLAPSE = "reverse-collapse";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_MENU_BAR:
      {
        MenuBar component = new MenuBar();
        context.mapAttribute(ATTR_OPEN_ON_HOVER).asBoolean().to(component::setOpenOnHover);
        context.mapAttribute(ATTR_REVERSE_COLLAPSE).asBoolean()
            .to(component::setReverseCollapseOrder);

        ComponentI18n.localize(component);

        context.readChildren(component);

        return component;
      }
    }

    return null;
  }

}
