package de.codecamp.vaadin.flowdui.factories.visandint;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_VISUALIZATION_AND_INTERACTION;
import static de.codecamp.vaadin.flowdui.factories.visandint.MessageListFactory.TAG_VAADIN_MESSAGE_LIST;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.messages.MessageList;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_MESSAGE_LIST,
    componentType = MessageList.class,
    docUrl = "https://vaadin.com/docs/latest/components/message-list",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    attributes = { //
    })
public class MessageListFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_MESSAGE_LIST = "vaadin-message-list";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_MESSAGE_LIST:
      {
        MessageList component = new MessageList();

        context.readChildren(component);

        return component;
      }
    }

    return null;
  }

}
