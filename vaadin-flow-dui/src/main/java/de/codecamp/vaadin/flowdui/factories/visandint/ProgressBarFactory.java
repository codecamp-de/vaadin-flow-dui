package de.codecamp.vaadin.flowdui.factories.visandint;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_VISUALIZATION_AND_INTERACTION;
import static de.codecamp.vaadin.flowdui.factories.visandint.ProgressBarFactory.ATTR_INDETERMINATE;
import static de.codecamp.vaadin.flowdui.factories.visandint.ProgressBarFactory.ATTR_MAX;
import static de.codecamp.vaadin.flowdui.factories.visandint.ProgressBarFactory.ATTR_MIN;
import static de.codecamp.vaadin.flowdui.factories.visandint.ProgressBarFactory.ATTR_VALUE;
import static de.codecamp.vaadin.flowdui.factories.visandint.ProgressBarFactory.TAG_VAADIN_PROGRESS_BAR;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.progressbar.ProgressBar;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_PROGRESS_BAR,
    componentType = ProgressBar.class,
    docUrl = "https://vaadin.com/docs/latest/components/progress-bar",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    attributes = { //
        @DuiAttribute(name = ATTR_INDETERMINATE, type = Boolean.class), //
        @DuiAttribute(name = ATTR_MIN, type = Double.class), //
        @DuiAttribute(name = ATTR_MAX, type = Double.class), //
        @DuiAttribute(name = ATTR_VALUE, type = Double.class) //
    })
public class ProgressBarFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_PROGRESS_BAR = "vaadin-progress-bar";


  public static final String ATTR_INDETERMINATE = "indeterminate";

  public static final String ATTR_MIN = "min";

  public static final String ATTR_MAX = "max";

  public static final String ATTR_VALUE = "value";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_PROGRESS_BAR:
      {
        ProgressBar component = new ProgressBar();
        context.mapAttribute(ATTR_INDETERMINATE).asBoolean().to(component::setIndeterminate);
        context.mapAttribute(ATTR_MIN).asDouble().to(component::setMin);
        context.mapAttribute(ATTR_MAX).asDouble().to(component::setMax);
        context.mapAttribute(ATTR_VALUE).asDouble().to(component::setValue);

        context.readChildren(component);

        return component;
      }
    }

    return null;
  }

}
