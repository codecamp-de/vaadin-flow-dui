package de.codecamp.vaadin.flowdui.factories.visandint;


import static de.codecamp.vaadin.flowdui.declare.DuiComponent.SLOT_DEFAULT;
import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_VISUALIZATION_AND_INTERACTION;
import static de.codecamp.vaadin.flowdui.factories.visandint.SideNavFactory.ATTR_COLLAPSED;
import static de.codecamp.vaadin.flowdui.factories.visandint.SideNavFactory.ATTR_COLLAPSIBLE;
import static de.codecamp.vaadin.flowdui.factories.visandint.SideNavFactory.ATTR_PATH;
import static de.codecamp.vaadin.flowdui.factories.visandint.SideNavFactory.SLOT_LABEL;
import static de.codecamp.vaadin.flowdui.factories.visandint.SideNavFactory.TAG_VAADIN_SIDE_NAV;
import static de.codecamp.vaadin.flowdui.factories.visandint.SideNavFactory.TAG_VAADIN_SIDE_NAV_ITEM;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.sidenav.SideNav;
import com.vaadin.flow.component.sidenav.SideNavItem;
import de.codecamp.vaadin.base.i18n.ComponentI18n;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_SIDE_NAV,
    componentType = SideNav.class,
    description = "Due to limitations in the Java component, the 'label' slot does not support elements, so only the text nodes are used.",
    docUrl = "https://vaadin.com/docs/latest/components/side-nav",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    attributes = { //
        @DuiAttribute(name = ATTR_COLLAPSIBLE, type = Boolean.class), //
        @DuiAttribute(name = ATTR_COLLAPSED, type = Boolean.class) //
    },
    slots = {SLOT_DEFAULT, SLOT_LABEL})
@DuiComponent(
    tagName = TAG_VAADIN_SIDE_NAV_ITEM,
    componentType = SideNavItem.class,
    description = "Due to limitations in the Java component, the label (i.e. the default slot) does not support elements, so only the text nodes are used.",
    docUrl = "https://vaadin.com/docs/latest/components/side-nav",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    attributes = { //
        @DuiAttribute(name = ATTR_COLLAPSED, type = Boolean.class), //
        @DuiAttribute(name = ATTR_PATH, type = String.class) //
    },
    slots = {SLOT_DEFAULT})
public class SideNavFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_SIDE_NAV = "vaadin-side-nav";

  public static final String TAG_VAADIN_SIDE_NAV_ITEM = "vaadin-side-nav-item";


  public static final String SLOT_LABEL = "label";

  public static final String SLOT_CHILDREN = "children";


  public static final String ATTR_COLLAPSIBLE = "collapsible";

  public static final String ATTR_COLLAPSED = "collapsed";

  public static final String ATTR_PATH = "path";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_SIDE_NAV:
      {
        SideNav component = new SideNav();
        context.mapAttribute(ATTR_COLLAPSIBLE).asBoolean().to(component::setCollapsible);
        context.mapAttribute(ATTR_COLLAPSED).asBoolean().to(v -> component.setExpanded(!v));

        ComponentI18n.localize(component);

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_DEFAULT:
              if (!childElement.tagName().equals(TAG_VAADIN_SIDE_NAV_ITEM))
                throw context.fail("SideNav only supports SideNavItem as child component.");
              component.addItem((SideNavItem) context.readComponent(childElement));
              return true;

            case SLOT_LABEL:
              component.setLabel(childElement.text());
              return true;
          }
          return false;
        }, null);

        return component;
      }

      case TAG_VAADIN_SIDE_NAV_ITEM:
      {
        SideNavItem component = new SideNavItem("");
        context.mapAttribute(ATTR_COLLAPSED).asBoolean().to(v -> component.setExpanded(!v));
        context.mapAttribute(ATTR_PATH).asString().to(component::setPath);

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_CHILDREN:
              if (!childElement.tagName().equals(TAG_VAADIN_SIDE_NAV_ITEM))
                throw context.fail("SideNav only supports SideNavItem as child component.");
              component.addItem((SideNavItem) context.readComponent(childElement));
              return true;

            case SLOT_DEFAULT:
              component.setLabel(childElement.text());
              return true;
          }
          return false;
        }, textNode ->
        {
          component.setLabel(textNode.text());
        });

        return component;
      }
    }

    return null;
  }

}
