package de.codecamp.vaadin.flowdui.factories.visandint;


import static de.codecamp.vaadin.flowdui.declare.DuiComponent.SLOT_DEFAULT;
import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_VISUALIZATION_AND_INTERACTION;
import static de.codecamp.vaadin.flowdui.factories.visandint.TabsFactory.ATTR_ORIENTATION;
import static de.codecamp.vaadin.flowdui.factories.visandint.TabsFactory.ATTR_SELECTED;
import static de.codecamp.vaadin.flowdui.factories.visandint.TabsFactory.ATTR_VERTICAL;
import static de.codecamp.vaadin.flowdui.factories.visandint.TabsFactory.SLOT_PREFIX;
import static de.codecamp.vaadin.flowdui.factories.visandint.TabsFactory.SLOT_SUFFIX;
import static de.codecamp.vaadin.flowdui.factories.visandint.TabsFactory.SLOT_TABS;
import static de.codecamp.vaadin.flowdui.factories.visandint.TabsFactory.TAG_VAADIN_TAB;
import static de.codecamp.vaadin.flowdui.factories.visandint.TabsFactory.TAG_VAADIN_TABS;
import static de.codecamp.vaadin.flowdui.factories.visandint.TabsFactory.TAG_VAADIN_TABSHEET;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.TabSheet;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.Tabs.Orientation;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiAttribute;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import org.jsoup.nodes.Element;


@DuiComponent(
    tagName = TAG_VAADIN_TABS,
    componentType = Tabs.class,
    docUrl = "https://vaadin.com/docs/latest/components/tabs",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    description = "Only &lt;vaadin-tab&gt; are supported as child elements.",
    attributes = { //
        @DuiAttribute(
            name = ATTR_ORIENTATION,
            type = String.class,
            description = "Allowed values: 'horizontal' (default) or 'vertical'"),
        @DuiAttribute(name = ATTR_VERTICAL, type = Boolean.class, custom = true),
        @DuiAttribute(name = ATTR_SELECTED, type = Integer.class) //
    },
    slots = {SLOT_DEFAULT})
@DuiComponent(
    tagName = TAG_VAADIN_TAB,
    componentType = Tab.class,
    docUrl = "https://vaadin.com/docs/latest/components/tabs",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    slots = {SLOT_DEFAULT})
@DuiComponent(
    tagName = TAG_VAADIN_TABSHEET,
    componentType = TabSheet.class,
    docUrl = "https://vaadin.com/docs/latest/components/tabs",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION,
    slots = {SLOT_DEFAULT, SLOT_PREFIX, SLOT_SUFFIX, SLOT_TABS})
public class TabsFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_TABS = "vaadin-tabs";

  public static final String TAG_VAADIN_TAB = "vaadin-tab";

  public static final String TAG_VAADIN_TABSHEET = "vaadin-tabsheet";


  public static final String SLOT_PREFIX = "prefix";

  public static final String SLOT_SUFFIX = "suffix";

  public static final String SLOT_TABS = "tabs";


  public static final String ATTR_ORIENTATION = "orientation";

  public static final String ATTR_VERTICAL = "vertical";

  public static final String ATTR_SELECTED = "selected";

  public static final String ATTR_TAB = "tab";

  public static final String ATTR_ID = "id";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_TABS:
      {
        Tabs component = new Tabs();
        context.mapAttribute(ATTR_ORIENTATION)
            .asEnum(v -> Orientation.valueOf(v.toUpperCase(Locale.ENGLISH)))
            .to(component::setOrientation);
        context.mapAttribute(ATTR_VERTICAL).asBoolean()
            .to(v -> component.setOrientation(v ? Orientation.VERTICAL : Orientation.HORIZONTAL));

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_DEFAULT:
              if (!childElement.tagName().equals(TAG_VAADIN_TAB))
                throw context.fail("Tabs only supports Tab as child component.");
              component.add((Tab) context.readComponent(childElement));
              return true;
          }
          return false;
        }, null);

        context.mapAttribute(ATTR_SELECTED).asInteger().to(component::setSelectedIndex);
        return component;
      }

      case TAG_VAADIN_TAB:
      {
        Tab component = new Tab();

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_DEFAULT:
              component.add(context.readComponent(childElement));
              return true;
          }
          return false;
        }, textNode ->
        {
          component.add(textNode.text());
        });

        return component;
      }

      case TAG_VAADIN_TABSHEET:
      {
        TabSheet component = new TabSheet();

        Map<String, Tab> tabs = new LinkedHashMap<>();
        Map<String, Component> tabContents = new HashMap<>();

        context.readChildren(component, (slotName, childElement) ->
        {
          switch (slotName) // NOPMD:SwitchStmtsShouldHaveDefault
          {
            case SLOT_DEFAULT:
            {
              context.readComponent(childElement, (childComponent, childContext) ->
              {
                String tabId = childContext.mapAttribute(ATTR_TAB).asString().getRequired();
                tabContents.put(tabId, childComponent);
              });
              return true;
            }

            case SLOT_PREFIX:
              component.setPrefixComponent(context.readComponentForSlot(childElement));
              return true;

            case SLOT_SUFFIX:
              component.setSuffixComponent(context.readComponentForSlot(childElement));
              return true;

            case SLOT_TABS:
              if (!childElement.tagName().equals(TAG_VAADIN_TABS))
              {
                throw context.fail(
                    String.format("Only <%s> allowed in the %s-slot.", TAG_VAADIN_TABS, SLOT_TABS));
              }
              for (Element tabElement : childElement.children())
              {
                context.readComponent(tabElement, (childComponent, childContext) ->
                {
                  Tab tab = (Tab) childComponent;
                  String tabId = childContext.mapAttribute(ATTR_ID).asString().getRequired();
                  tabs.put(tabId, tab);
                });
              }
              return true;
          }
          return false;
        }, null);

        for (String tabId : tabs.keySet())
        {
          component.add(tabs.get(tabId), tabContents.get(tabId));
        }

        return component;
      }
    }

    return null;
  }

}
