package de.codecamp.vaadin.flowdui.factories.visandint;


import static de.codecamp.vaadin.flowdui.factories.Constants.CATEGORY_VISUALIZATION_AND_INTERACTION;
import static de.codecamp.vaadin.flowdui.factories.visandint.VirtualListFactory.TAG_VAADIN_VIRTUAL_LIST;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.virtuallist.VirtualList;
import de.codecamp.vaadin.flowdui.ComponentFactory;
import de.codecamp.vaadin.flowdui.ElementParserContext;
import de.codecamp.vaadin.flowdui.declare.DuiComponent;


@DuiComponent(
    tagName = TAG_VAADIN_VIRTUAL_LIST,
    componentType = VirtualList.class,
    docUrl = "https://vaadin.com/docs/latest/components/virtual-list",
    category = CATEGORY_VISUALIZATION_AND_INTERACTION)
public class VirtualListFactory
  implements
    ComponentFactory
{

  public static final String TAG_VAADIN_VIRTUAL_LIST = "vaadin-virtual-list";


  @Override
  public Component createComponent(String tagName, ElementParserContext context)
  {
    switch (tagName) // NOPMD:SwitchStmtsShouldHaveDefault
    {
      case TAG_VAADIN_VIRTUAL_LIST:
      {
        VirtualList<?> component = new VirtualList<>();
        context.readChildren(component);
        return component;
      }
    }

    return null;
  }

}
