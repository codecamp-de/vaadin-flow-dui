package de.codecamp.vaadin.fluent.annotations;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.shared.HasThemeVariant;
import com.vaadin.flow.component.shared.ThemeVariant;
import com.vaadin.flow.data.provider.DataView;
import com.vaadin.flow.data.provider.SortOrder;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Declares and customizes the mapping of a {@link Component} type to the fluent wrapper that will
 * be generated for it. This includes mix-in interfaces and abstract base classes.
 */
@Target({ElementType.PACKAGE, ElementType.TYPE})
@Retention(RetentionPolicy.CLASS)
@Repeatable(FluentMapping.Container.class)
@Documented
public @interface FluentMapping
{

  /**
   * The type variable used for the {@link Component} type.
   */
  String TYPEVAR_COMPONENT = "C";

  /**
   * The type variable used for the corresponding fluent type.
   */
  String TYPEVAR_FLUENT = "F";

  /**
   * The type variable used for the value type.
   */
  String TYPEVAR_VALUE = "VALUE";

  /**
   * The type variable used for the item type.
   */
  String TYPEVAR_ITEM = "ITEM";

  /**
   * The type variable used for the event type.
   */
  String TYPEVAR_EVENT = "EVENT";

  /**
   * The type variable used for the {@link DataView data view} type.
   */
  String TYPEVAR_DATAVIEW = "DATAVIEW";

  /**
   * The type variable used for the filter type.
   */
  String TYPEVAR_FILTER = "FILTER";

  /**
   * The type variable used for the {@link SortOrder sort order} type.
   */
  String TYPEVAR_SORT = "SORT";

  /**
   * The type variable used for the {@link ThemeVariant theme variant} enum type.
   */
  String TYPEVAR_VARIANT = "VARIANT";


  /**
   * Returns the component type for which to generate a fluent API. This is typically a
   * {@link Component} or mix-in interface but may work on others too. The type can be omitted
   * (<em>and only then</em>) when this annotation is placed on the component type directly.
   *
   * @return type for which to generate a fluent API
   */
  Class<?> componentType() default Void.class;

  /**
   * Returns the target package relative to the {@link FluentMappingSet#basePackage() base package
   * of the associated mapping set}.
   *
   * @return target package relative to the global base package
   */
  String targetPackage() default "";

  /**
   * Returns the canonical type variables for the type parameters of the component. When a component
   * type uses ambiguous type variables (e.g. {@code T} which could be the value or item type of a
   * component), they can be overridden here to resolve the ambiguity. Each type variable
   * corresponds to the type parameter at the same position. To keep the original type variable the
   * empty string can be used. This array may be shorter than the number of type parameters.
   *
   * @return the canonical type variables for the type parameters of the component
   * @see #TYPEVAR_COMPONENT
   * @see #TYPEVAR_FLUENT
   * @see #TYPEVAR_VALUE
   * @see #TYPEVAR_ITEM
   * @see #TYPEVAR_EVENT
   * @see #TYPEVAR_DATAVIEW
   * @see #TYPEVAR_FILTER
   * @see #TYPEVAR_SORT
   * @see #TYPEVAR_VARIANT
   */
  String[] typeVars() default {};

  String[] innerTypeVarMapping() default {};


  /**
   * Returns whether to additionally create an artificial abstract base type. The leaf types (the
   * fluent types generated for concrete {@link Component} types) are no longer extensible without
   * breaking the fluent pattern. Introducing an artificial base type can help with that.
   * <p>
   * Example: When {@link #createBase()} is {@code true}, for the {@code Button} component the base
   * type {@code FluentButtonBase} will be generated in addition to the {@code FluentButton}. For
   * {@code SuperButton} which extends {@code Button} the generated {@code FluentSuperButton} would
   * then extend {@code FluentButtonBase} instead of {@code FluentButton}.
   *
   * @return whether to additionally create an artificial abstract base type
   */
  boolean createBase() default false;


  /**
   * Returns the type of the i18n utility class that will be used to localize the component
   *
   * @return the
   */
  Class<?> i18nUtils() default void.class;


  /**
   * Returns the interfaces that will be ignored when generating the fluent API, but only for this
   * component being mapped.
   * <p>
   * To avoid warnings about encountered interfaces that don't have a fluent counterpart, they can
   * be ignored globally by setting
   * {@link GlobalFluentMappingSettings#ignoredComponentInterfaces()}.
   *
   * @return the interfaces that should be ignored when creating the fluent API for this component
   * @see GlobalFluentMappingSettings#ignoredComponentInterfaces()
   */
  Class<?>[] ignoredComponentInterfaces() default {};


  /**
   * Returns the explicit method mappings. The can be used for additional mappings or override
   * automatic mappings for specific methods.
   *
   * @return the explicit method mappings
   */
  FluentMethod[] mapped() default {};


  /**
   * Returns the enum containing the theme variants. Only necessary if the component doesn't
   * implement {@link HasThemeVariant}.
   *
   * @return the enum containing the theme variants. Only necessary if the component doesn't
   *         implement {@link HasThemeVariant}
   */
  Class<?> themeVariantType() default void.class;

  /**
   * Returns the theme variants as groups of mutually exclusive variants. Fluent methods will be
   * generated that ensure that only one variant of each group is active.
   *
   * @return the theme variants as groups of mutually exclusive variants
   */
  ThemeGroup[] themes() default {};

  /**
   * Returns imports that will be added to the generated fluent API.
   *
   * @return imports that will be added to the generated fluent API
   */
  String[] imports() default {};

  /**
   * Returns interfaces that will be added verbatim to the generated fluent API.
   *
   * @return interfaces that will be added verbatim to the generated fluent API
   */
  String[] interfaces() default {};

  /**
   * Returns methods that will be added verbatim to the generated fluent API.
   *
   * @return methods that will be added verbatim to the generated fluent API
   */
  String[] methods() default {};

  /**
   * Returns whether to generate the default static factory methods (if a static factory class is
   * generated).
   *
   * @return whether to generate the default static factory methods (if a static factory class is
   *         generated)
   */
  boolean autoStaticFactories() default true;

  /**
   * Returns static factory methods that will be added to the generated fluent API. The
   * {@code public} and {@code static} modifiers will be added automatically, so they can be omitted
   * here.
   *
   * @return static factory methods that will be added to the generated fluent API
   */
  String[] staticFactories() default {};


  /**
   * Container for repeated {@link FluentMapping} annotations.
   */
  @Target({ElementType.PACKAGE, ElementType.TYPE})
  @Retention(RetentionPolicy.CLASS)
  @Documented
  @interface Container
  {

    /**
     * Returns the {@link FluentMapping} annotations.
     *
     * @return the {@link FluentMapping} annotations
     */
    FluentMapping[] value();

  }

}
