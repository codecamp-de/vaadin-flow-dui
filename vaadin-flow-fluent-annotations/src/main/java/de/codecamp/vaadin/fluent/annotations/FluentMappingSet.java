package de.codecamp.vaadin.fluent.annotations;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Declares a mapping set for the fluent mapping. This annotation can be placed on any class or
 * package (via {@code package-info.java}) and groups all {@link FluentMapping} annotations in the
 * same package (not including sub-packages!). The only exception is the {@link #isDefault()
 * default} mapping set; it captures {@link FluentMapping} annotations from everywhere that aren't
 * captured by any other mapping set.
 * <p>
 * This annotation is optional if only one mapping set is needed and all {@link FluentMapping}
 * annotations are contained in the same package and the fluent API should be created in exactly
 * that package.
 * <p>
 * More than one mapping sets may be necessary in cases like Vaadin's Pro components that may not be
 * available everywhere (as in the classes may not be on the classpath) , so at the very least they
 * need separate static factory classes.
 */
@Target({ElementType.PACKAGE, ElementType.TYPE})
@Retention(RetentionPolicy.CLASS)
@Documented
public @interface FluentMappingSet
{

  /**
   * The default simple class name for static static factory classes.
   */
  String DEFAULT_STATIC_FACTORY_CLASS = "Fluent";


  /**
   * Returns this mapping set's base package used as target for the generated classes. The default
   * will be the package containing this annotation.
   * <p>
   * Please note, that the base package is also used as the name of a mapping set.
   *
   * @return this mapping set's base package used as target for the generated classes
   */
  String basePackage() default "";

  /**
   * Returns this mapping set's target class (within the {@link #basePackage()} in which to generate
   * the static factory methods.
   *
   * @return this mapping set's target class (within the {@link #basePackage()} in which to generate
   *         the static factory methods
   */
  String staticFactoryClass() default DEFAULT_STATIC_FACTORY_CLASS;

  /**
   * Returns whether this is the default mapping set. There can only be one default mapping set.
   *
   * @return whether this is the default mapping set
   */
  boolean isDefault() default false;

}
