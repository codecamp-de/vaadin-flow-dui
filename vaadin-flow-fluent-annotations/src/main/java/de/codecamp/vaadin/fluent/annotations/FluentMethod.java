package de.codecamp.vaadin.fluent.annotations;


import java.lang.annotation.Documented;


@Documented
public @interface FluentMethod
{

  /**
   * Returns the name of the method in the component type to be mapped.
   *
   * @return the name of the method in the component type to be mapped
   */
  String name();

  Class<?>[] params() default {void.class};

  boolean skip() default false;

  String[] fluent() default {};

  boolean withTranslation() default false;

  boolean expandEnum() default true;

}
