package de.codecamp.vaadin.fluent.annotations;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Global settings for the fluent mapping. This annotation can be placed on any class or package
 * (via {@code package-info.java}), but may only be present once.
 */
@Target({ElementType.PACKAGE, ElementType.TYPE})
@Retention(RetentionPolicy.CLASS)
@Documented
public @interface GlobalFluentMappingSettings
{

  /**
   * The default template directory, relative to the project directory.
   */
  String DEFAULT_TEMPLATE_DIR = "src/main/fluent";


  /**
   * Returns the mapping sets to import. This is required if component types are used that have been
   * mapped in a different project.
   *
   * @return the mapping sets to import
   */
  Class<?>[] imports() default {};

  /**
   * Returns the directory containing templates for the generated classes. This path is relative to
   * the project directory, which must be specified via the annotation processor options.
   *
   * @return the directory containing templates for the generated classes
   */
  String templateDir() default DEFAULT_TEMPLATE_DIR;

  /**
   * Returns the component interfaces to ignore. This can be used to get rid of some warnings.
   *
   * @return the component interfaces to ignore
   */
  Class<?>[] ignoredComponentInterfaces() default {};

}
