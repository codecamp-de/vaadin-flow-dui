package de.codecamp.vaadin.fluent.annotations;


import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.RetentionPolicy.CLASS;

import com.vaadin.flow.component.shared.ThemeVariant;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;


/**
 * Describes a set of <em>mutually exclusive</em> {@link ThemeVariant theme variants} and the fluent
 * method names they should be mapped to. There must be exactly as many variants as there are fluent
 * mappings. For the default state that doesn't have its own variant and where none of the variants
 * are present, use the empty string.
 */
@Documented
@Retention(CLASS)
@Target(ANNOTATION_TYPE)
public @interface ThemeGroup
{

  /**
   * Returns the names of the enum constants.
   *
   * @return the names of the enum constants
   */
  String[] variant();

  /**
   * Returns the names of the fluent methods to be created.
   *
   * @return the names of the fluent methods to be created
   */
  String[] fluent() default {};

}
