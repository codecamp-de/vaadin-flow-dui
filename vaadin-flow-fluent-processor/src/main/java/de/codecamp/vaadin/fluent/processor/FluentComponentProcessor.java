package de.codecamp.vaadin.fluent.processor;


import static de.codecamp.vaadin.fluent.annotations.FluentMapping.TYPEVAR_COMPONENT;
import static de.codecamp.vaadin.fluent.annotations.FluentMapping.TYPEVAR_DATAVIEW;
import static de.codecamp.vaadin.fluent.annotations.FluentMapping.TYPEVAR_EVENT;
import static de.codecamp.vaadin.fluent.annotations.FluentMapping.TYPEVAR_FILTER;
import static de.codecamp.vaadin.fluent.annotations.FluentMapping.TYPEVAR_FLUENT;
import static de.codecamp.vaadin.fluent.annotations.FluentMapping.TYPEVAR_ITEM;
import static de.codecamp.vaadin.fluent.annotations.FluentMapping.TYPEVAR_SORT;
import static de.codecamp.vaadin.fluent.annotations.FluentMapping.TYPEVAR_VALUE;
import static de.codecamp.vaadin.fluent.annotations.FluentMapping.TYPEVAR_VARIANT;
import static de.codecamp.vaadin.fluent.processor.FluentComponentProcessor.OPT_DEBUG;
import static de.codecamp.vaadin.fluent.processor.FluentComponentProcessor.OPT_PROJECT_DIR;
import static java.util.stream.Collectors.joining;
import static org.apache.commons.lang3.StringUtils.removeEnd;
import static org.apache.commons.lang3.StringUtils.removeStart;
import static org.apache.commons.lang3.StringUtils.trimToNull;

import com.google.auto.common.MoreElements;
import com.google.auto.common.MoreTypes;
import com.google.auto.service.AutoService;
import com.google.common.base.CaseFormat;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.IconFactory;
import com.vaadin.flow.component.shared.HasThemeVariant;
import com.vaadin.flow.i18n.LocaleChangeObserver;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.RouterLayout;
import de.codecamp.vaadin.fluent.annotations.FluentMapping;
import de.codecamp.vaadin.fluent.annotations.FluentMappingSet;
import de.codecamp.vaadin.fluent.annotations.GlobalFluentMappingSettings;
import io.toolisticon.aptk.tools.AbstractAnnotationProcessor;
import io.toolisticon.aptk.tools.TypeMirrorWrapper;
import io.toolisticon.aptk.tools.wrapper.ExecutableElementWrapper;
import io.toolisticon.aptk.tools.wrapper.TypeElementWrapper;
import io.toolisticon.aptk.tools.wrapper.TypeParameterElementWrapper;
import io.toolisticon.aptk.tools.wrapper.VariableElementWrapper;
import java.beans.Introspector;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javax.annotation.processing.Generated;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedOptions;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.tools.Diagnostic.Kind;
import javax.tools.FileObject;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import org.jboss.forge.roaster.Roaster;
import org.jboss.forge.roaster.model.source.GenericCapableSource;
import org.jboss.forge.roaster.model.source.InterfaceCapableSource;
import org.jboss.forge.roaster.model.source.JavaClassSource;
import org.jboss.forge.roaster.model.source.JavaInterfaceSource;
import org.jboss.forge.roaster.model.source.JavaSource;
import org.jboss.forge.roaster.model.source.MethodSource;
import org.jboss.forge.roaster.model.source.PropertyHolderSource;
import org.jboss.forge.roaster.model.source.TypeHolderSource;
import org.jboss.forge.roaster.model.source.TypeVariableSource;
import org.jboss.forge.roaster.model.util.Types;


@AutoService(Processor.class)
@SupportedOptions({OPT_PROJECT_DIR, OPT_DEBUG})
public class FluentComponentProcessor
  extends
    AbstractAnnotationProcessor
{

  /* package */ static final String OPT_PROJECT_DIR = "projectDir";

  /* package */ static final String OPT_DEBUG = "fluent.debug";


  /**
   * <b>This must be identical to the main mapping set name (which should be the base package) used
   * in the {@code vaadin-flow-fluent} project.</b> This is used to auto-import the mappings of
   * Vaadin's standard (non-Pro) components.
   */
  private static final String MAIN_BASE_PACKAGE = "de.codecamp.vaadin.fluent";


  private static final String MAPPING_SET_REF_CLASS_NAME = "MappingSet";

  private static final String MAPPING_SET_DATA_FILENAME = MAPPING_SET_REF_CLASS_NAME + ".json";

  private static final String DEFAULT_SCOPE_PACKAGE = "";

  private static final String FLUENT_TYPE_PREFIX = "Fluent";

  private static final String FLUENT_BASE_SUFFIX = "Base";

  private static final List<String> DEFAULT_TYPEVAR_ORDER =
      List.of(TYPEVAR_COMPONENT, TYPEVAR_FLUENT, TYPEVAR_ITEM, TYPEVAR_EVENT, TYPEVAR_VALUE,
          TYPEVAR_DATAVIEW, TYPEVAR_FILTER, TYPEVAR_SORT, TYPEVAR_VARIANT);

  private static final Map<String, String> TYPEVAR_JAVADOC_DESCRIPTIONS = new HashMap<>();
  static
  {
    TYPEVAR_JAVADOC_DESCRIPTIONS.put(TYPEVAR_COMPONENT, "the component type");
    TYPEVAR_JAVADOC_DESCRIPTIONS.put(TYPEVAR_FLUENT, "the fluent type");
    TYPEVAR_JAVADOC_DESCRIPTIONS.put(TYPEVAR_ITEM, "the item type");
    TYPEVAR_JAVADOC_DESCRIPTIONS.put(TYPEVAR_EVENT, "the event type");
    TYPEVAR_JAVADOC_DESCRIPTIONS.put(TYPEVAR_VALUE, "the value type");
    TYPEVAR_JAVADOC_DESCRIPTIONS.put(TYPEVAR_DATAVIEW, "the data view type");
    TYPEVAR_JAVADOC_DESCRIPTIONS.put(TYPEVAR_FILTER, "the filter type");
    TYPEVAR_JAVADOC_DESCRIPTIONS.put(TYPEVAR_SORT, "the sort order type");
    TYPEVAR_JAVADOC_DESCRIPTIONS.put(TYPEVAR_VARIANT, "the theme variant type");
  }

  private static final String TRANSLATE_METHOD_SUFFIX = "I18n";

  /**
   * Used to log errors with a throwable that would otherwise be lost.
   */
  private static final Logger LOG = Logger.getLogger(FluentComponentProcessor.class.getName());


  private Path projectDir;

  private Path templateDir;

  private final Map<String, String> basePackagePerSourcePackage = new HashMap<>();

  private final Map<String, String> staticFactoryClassPerSourcePackage = new HashMap<>();

  private final Map<String, JavaClassSource> staticFactoryClassOutputPerSourcePackage =
      new HashMap<>();

  private final Map<String, FluentMappingSetData> mappingSetDataByName = new HashMap<>();


  private final Set<String> importedMappingSets = new LinkedHashSet<>();

  private final Set<String> ignoredComponentInterfaces = new HashSet<>(
      List.of(AfterNavigationObserver.class.getName(), LocaleChangeObserver.class.getName(),
          RouterLayout.class.getName(), "de.codecamp.vaadin.fluent.FluentLayoutSupport",
          "de.codecamp.vaadin.fluent.FluentLayoutSupportIndexed",
          "de.codecamp.vaadin.fluent.FluentLayoutSupportSingle",
          "de.codecamp.vaadin.fluent.FluentLayoutSupportSlotted"));

  private boolean debug;


  /**
   * component FQCN -> fluent FQCN
   */
  private final Map<String, String> componentToFluentMappingCache = new HashMap<>();

  /**
   * component FQCN -> fluent type variables
   */
  private final Map<String, List<String>> fluentTypeVarsCache = new HashMap<>();

  /**
   * component FQCN -> canonicalized component type variables
   */
  private final Map<String, List<String>> componentTypeVarsCache = new HashMap<>();

  /**
   * component FQCN -> substitutions for type variable names in component types
   */
  private final Map<String, Map<String, String>> typeVarSubstitutionsCache = new HashMap<>();


  public FluentComponentProcessor()
  {
  }


  @Override
  public Set<String> getSupportedAnnotationTypes()
  {
    return Set.of(FluentMapping.class.getCanonicalName(),
        FluentMapping.Container.class.getCanonicalName(),
        GlobalFluentMappingSettings.class.getCanonicalName(),
        FluentMappingSet.class.getCanonicalName());
  }


  @Override
  public boolean processAnnotations(Set<? extends TypeElement> annotations,
      RoundEnvironment roundEnv)
  {
    projectDir = Optional.ofNullable(trimToNull(processingEnv.getOptions().get(OPT_PROJECT_DIR)))
        .map(Paths::get).orElse(null);
    debug = Boolean.parseBoolean(processingEnv.getOptions().get(OPT_DEBUG));


    Set<? extends Element> mappingSourceElements =
        ProcessorUtils.getElementsAnnotatedWith(roundEnv, FluentMapping.class);
    if (mappingSourceElements.isEmpty())
      return true;

    /* Find global settings. */
    {
      Set<? extends Element> settingsSourceElements =
          ProcessorUtils.getElementsAnnotatedWith(roundEnv, GlobalFluentMappingSettings.class);
      if (settingsSourceElements.size() > 1)
      {
        printError(
            String.format("Only one @%s annotation is allowed.",
                GlobalFluentMappingSettings.class.getSimpleName()),
            null, settingsSourceElements.iterator().next());
        return true;
      }

      if (settingsSourceElements.isEmpty())
      {
        if (projectDir != null)
        {
          templateDir =
              projectDir.resolve(Paths.get(GlobalFluentMappingSettings.DEFAULT_TEMPLATE_DIR));
        }
      }
      else
      {
        Element settingsSourceElement = settingsSourceElements.iterator().next();

        List<AnnotationMirror> settingsMirrors = LangModelUtils
            .getAnnotationMirrors(settingsSourceElement, GlobalFluentMappingSettings.class);
        for (AnnotationMirror settingsMirror : settingsMirrors)
        {
          GlobalFluentMappingSettingsWrapper settings =
              GlobalFluentMappingSettingsWrapper.wrap(settingsSourceElement, settingsMirror);

          Stream.of(settings.importsAsTypeMirrorWrapper()).map(TypeMirrorWrapper::getPackage)
              .forEach(importedMappingSets::add);

          if (!settings.templateDirIsDefaultValue())
          {
            if (projectDir == null)
            {
              printError(
                  String.format("'templateDir' has been set without providing '%s' in the"
                      + " annotation processor's options.", OPT_PROJECT_DIR),
                  null, settingsSourceElement);
              return true;
            }
          }

          if (projectDir != null)
            templateDir = projectDir.resolve(settings.templateDir());

          ignoredComponentInterfaces
              .addAll(Arrays.asList(settings.ignoredComponentInterfacesAsFqn()));
        }
      }

      /* Auto-import mappings of Vaadin's standard (non-Pro) components. */
      if (!projectDir.endsWith("vaadin-flow-fluent"))
      {
        importedMappingSets.add(MAIN_BASE_PACKAGE);
      }

      if (importedMappingSets.isEmpty())
      {
        printNote("No mapping sets imported.", null, null);
      }
      else
      {
        printNote(
            String.format("Imported mapping sets: %s", String.join(", ", importedMappingSets)),
            null, null);
      }

      for (String mappingSetName : importedMappingSets)
      {
        if (!loadImportedMappingSet(null, mappingSetName))
          return true;
      }


      if (!ignoredComponentInterfaces.isEmpty())
      {
        printNote(String.format("Ignored component interfaces: %s",
            String.join(", ", ignoredComponentInterfaces)), null, null);
      }

      String globalTargetBasePackage = basePackagePerSourcePackage.get(DEFAULT_SCOPE_PACKAGE);
      if (globalTargetBasePackage == null)
      {
        printNote("No global target base package specified.", null, null);
      }
      else
      {
        printNote(String.format("Global target base package: %s", globalTargetBasePackage), null,
            null);
      }

      String globalStaticFactoryClass =
          staticFactoryClassPerSourcePackage.get(DEFAULT_SCOPE_PACKAGE);
      if (globalStaticFactoryClass == null)
      {
        printNote("No global static factory class specified.", null, null);
      }
      else
      {
        printNote(String.format("Global static factory class: %s.%s", globalTargetBasePackage,
            globalStaticFactoryClass), null, null);
      }
    }

    /* Find mapping sets. */
    {
      Set<? extends Element> mappingSetSourceElements =
          ProcessorUtils.getElementsAnnotatedWith(roundEnv, FluentMappingSet.class);

      if (mappingSetSourceElements.isEmpty())
      {
        String basePackage = null;
        for (Element mappingSourceElement : mappingSourceElements)
        {
          String sourcePackage =
              processingEnv.getElementUtils().getPackageOf(mappingSourceElement).toString();

          if (basePackage == null)
          {
            basePackage = sourcePackage;
          }
          else if (!sourcePackage.equals(basePackage))
          {
            printError(String.format(
                "@%s in multiple directories found. Please one default @%s or multiple @%s annotation as appropriate.",
                FluentMapping.class.getSimpleName(), FluentMappingSet.class.getSimpleName(),
                FluentMappingSet.class.getSimpleName()), null, null);
            return true;
          }
        }
        basePackagePerSourcePackage.put(DEFAULT_SCOPE_PACKAGE, basePackage);

        staticFactoryClassPerSourcePackage.put(DEFAULT_SCOPE_PACKAGE,
            FluentMappingSet.DEFAULT_STATIC_FACTORY_CLASS);
      }
      else
      {
        for (Element mappingSetSourceElement : mappingSetSourceElements)
        {
          PackageElement sourcePackageElement =
              processingEnv.getElementUtils().getPackageOf(mappingSetSourceElement);
          String sourcePackageName = sourcePackageElement.getQualifiedName().toString();

          List<AnnotationMirror> mappingSetMirrors =
              LangModelUtils.getAnnotationMirrors(mappingSetSourceElement, FluentMappingSet.class);
          for (AnnotationMirror mappingSetMirror : mappingSetMirrors)
          {
            FluentMappingSetWrapper mappingSet =
                FluentMappingSetWrapper.wrap(mappingSetSourceElement, mappingSetMirror);

            if (!mappingSet.basePackageIsDefaultValue()
                && basePackagePerSourcePackage.get(sourcePackageName) != null)
            {
              printError("'basePackage' must only be set once.", null, mappingSetSourceElement);
              return true;
            }

            String basePackage = trimToNull(mappingSet.basePackage());
            if (basePackage == null)
              basePackage = sourcePackageName;

            basePackagePerSourcePackage.put(sourcePackageName, basePackage);
            if (mappingSet.isDefault())
              basePackagePerSourcePackage.put(DEFAULT_SCOPE_PACKAGE, basePackage);

            String staticFactoryClass = mappingSet.staticFactoryClass();
            staticFactoryClassPerSourcePackage.put(sourcePackageName, staticFactoryClass);
            if (mappingSet.isDefault())
              staticFactoryClassPerSourcePackage.put(DEFAULT_SCOPE_PACKAGE, staticFactoryClass);

            printNote(String.format(
                "Mapping set for source package '%s' declared (base package = %s, static factory class = %s).",
                sourcePackageName, basePackage, staticFactoryClass), null, null);
          }
        }
      }
    }


    /*
     * Gather information about all fluent mappings before processing them.
     */
    for (Element mappingSourceElement : mappingSourceElements)
    {
      String sourcePackage =
          processingEnv.getElementUtils().getPackageOf(mappingSourceElement).toString();

      printNote(String.format("Processing @%s annotations on '%s'...",
          FluentMapping.class.getSimpleName(), mappingSourceElement.toString()), null, null);

      String basePackage = basePackagePerSourcePackage.get(sourcePackage);
      if (basePackage == null)
        basePackage = basePackagePerSourcePackage.get(DEFAULT_SCOPE_PACKAGE);

      if (basePackage == null)
      {
        printError(String.format("No mapping set declared for source package '%s'.", sourcePackage),
            null, mappingSourceElement);
        continue;
      }

      FluentMappingSetData mappingSet = mappingSetDataByName.get(basePackage);
      if (mappingSet == null)
      {
        try
        {
          FileObject mappingSetFile = processingEnv.getFiler()
              .getResource(StandardLocation.CLASS_OUTPUT, basePackage, MAPPING_SET_DATA_FILENAME);
          try (InputStream in = mappingSetFile.openInputStream())
          {
            mappingSet = JsonUtils.readFrom(in, FluentMappingSetData.class);
          }
        }
        catch (@SuppressWarnings("unused") FileNotFoundException | NoSuchFileException ex)
        {
          mappingSet = new FluentMappingSetData();
        }
        catch (IOException | RuntimeException ex)
        {
          String msg = String.format("Failed to read fluent mapping set '%s'.", basePackage);
          printError(msg, ex, mappingSourceElement);
          return true;
        }
        mappingSetDataByName.put(basePackage, mappingSet);
      }

      mappingSet.getImportedSets().addAll(importedMappingSets);

      try
      {
        List<AnnotationMirror> annotationMirrors =
            LangModelUtils.getAnnotationMirrors(mappingSourceElement, FluentMapping.class);
        for (AnnotationMirror mappingAt : annotationMirrors)
        {
          prepareMapping(mappingSourceElement, mappingAt, mappingSet);
        }
      }
      catch (RuntimeException ex)
      {
        printError("Failed to prepare fluent mapping.", ex, mappingSourceElement);
      }
    }

    mappingSetDataByName.forEach((basePackage, mappingSetData) ->
    {
      try
      {
        FileObject mappingSetFile = processingEnv.getFiler()
            .createResource(StandardLocation.CLASS_OUTPUT, basePackage, MAPPING_SET_DATA_FILENAME);
        try (OutputStream out = mappingSetFile.openOutputStream())
        {
          JsonUtils.writeTo(mappingSetData, out);
        }

        JavaClassSource javaClassSource = Roaster.create(JavaClassSource.class);
        javaClassSource.setPackage(basePackage).setName(MAPPING_SET_REF_CLASS_NAME).setPublic()
            .setFinal(true);
        javaClassSource.addMethod().setConstructor(true).setPrivate().setBody("");
        javaClassSource.getJavaDoc().setText("This class represents the {@code " + basePackage
            + "} mapping set and can be used to reference it in {@link de.codecamp.vaadin.fluent.annotations.GlobalFluentMappingSettings#imports()}.");

        JavaFileObject file =
            processingEnv.getFiler().createSourceFile(javaClassSource.getCanonicalName());
        try (Writer writer = file.openWriter())
        {
          writer.write(javaClassSource.toString());
          writer.flush();
        }
      }
      catch (IOException | RuntimeException ex)
      {
        String msg = String.format("Failed to write fluent mapping set '%s'.", basePackage);
        printError(msg, ex);
      }
    });


    prepareStaticFactoryClassOutputs();

    /*
     * Process mappings and create the fluent classes.
     */
    for (Element sourceElement : mappingSourceElements)
    {
      try
      {
        List<AnnotationMirror> annotationMirrors =
            LangModelUtils.getAnnotationMirrors(sourceElement, FluentMapping.class);
        for (AnnotationMirror mappingAt : annotationMirrors)
        {
          processMapping(sourceElement, mappingAt);
        }
      }
      catch (RuntimeException ex)
      {
        printError("Failed to process fluent mapping.", ex, sourceElement);
      }
    }

    writeStaticFactoryClassOutputs();

    return true;
  }

  /**
   * Transitively load the fluent mapping set with the given ID.
   *
   * @param sourceElement
   *          the source element on which the import is declared
   * @param mappingSetName
   *          the mapping set ID
   * @return whether the mapping set could be loaded successfully
   */
  protected boolean loadImportedMappingSet(Element sourceElement, String mappingSetName)
  {
    printNote(String.format("Importing fluent mapping set '%s'...", mappingSetName), sourceElement,
        null);

    try
    {
      FileObject mappingSetFile = processingEnv.getFiler().getResource(StandardLocation.CLASS_PATH,
          mappingSetName, MAPPING_SET_DATA_FILENAME);
      try (InputStream in = mappingSetFile.openInputStream())
      {
        FluentMappingSetData mappingSet = JsonUtils.readFrom(in, FluentMappingSetData.class);

        componentToFluentMappingCache.putAll(mappingSet.getComponentToFluent());
        fluentTypeVarsCache.putAll(mappingSet.getFluentTypeVars());
        componentTypeVarsCache.putAll(mappingSet.getComponentTypeVars());
        typeVarSubstitutionsCache.putAll(mappingSet.getTypeVarSubstitutions());

        for (String importedMappingSetName : mappingSet.getImportedSets())
        {
          if (!loadImportedMappingSet(sourceElement, importedMappingSetName))
            return false;
        }
        return true;
      }
    }
    catch (IOException ex)
    {
      String msg =
          String.format("Failed to read imported fluent mapping set '%s'.", mappingSetName);
      printError(msg, ex, sourceElement);
      return false;
    }
  }

  protected void prepareStaticFactoryClassOutputs()
  {
    String fqDefaultStaticFactoryClass;
    {
      String basePackage = basePackagePerSourcePackage.get(DEFAULT_SCOPE_PACKAGE);
      String staticFactoryClass = staticFactoryClassPerSourcePackage.get(DEFAULT_SCOPE_PACKAGE);
      if (basePackage != null && staticFactoryClass != null)
        fqDefaultStaticFactoryClass = basePackage + "." + staticFactoryClass;
      else
        fqDefaultStaticFactoryClass = null;
    }

    staticFactoryClassPerSourcePackage.forEach((sourcePackage, staticFactoryClass) ->
    {
      if (sourcePackage.equals(DEFAULT_SCOPE_PACKAGE))
        return;

      String basePackage = basePackagePerSourcePackage.get(sourcePackage);
      if (basePackage == null)
        basePackage = basePackagePerSourcePackage.get(DEFAULT_SCOPE_PACKAGE);

      JavaClassSource output = null;

      try
      {
        FileObject mappingSetFile = processingEnv.getFiler()
            .getResource(StandardLocation.SOURCE_OUTPUT, basePackage, staticFactoryClass + ".java");
        try (InputStream in = mappingSetFile.openInputStream())
        {
          output = Roaster.parse(JavaClassSource.class, in);
        }
      }
      catch (@SuppressWarnings("unused") IOException ex)
      {
        // ignore
        // TODO distinguish between "not found" and actual problems
      }

      if (output == null)
      {
        Path templatePath = templateDir.resolve(basePackage.replace(".", "/"))
            .resolve(staticFactoryClass + ".java");
        if (Files.isRegularFile(templatePath))
        {
          try (InputStream in = Files.newInputStream(templatePath))
          {
            output = Roaster.parse(JavaClassSource.class, in);
          }
          catch (@SuppressWarnings("unused") IOException ex)
          {
            // ignore
            // TODO distinguish between "not found" and actual problems
          }
        }
      }

      if (output == null)
      {
        output = Roaster.create(JavaClassSource.class);
      }

      output.setPackage(basePackage);
      output.setName(staticFactoryClass);
      output.setPublic();
      output.setFinal(true);
      output.getJavaDoc().setText("Static factory methods for the {@link " + basePackage + "."
          + MAPPING_SET_REF_CLASS_NAME + " " + basePackage + "} mapping set.");

      boolean noArgsCtorFound = false;
      for (MethodSource<?> m : output.getMethods())
      {
        if (m.isConstructor() && m.getParameters().isEmpty())
        {
          noArgsCtorFound = true;
          break;
        }
      }
      if (!noArgsCtorFound)
      {
        output.addMethod().setConstructor(true).setPrivate().setBody("");
      }

      staticFactoryClassOutputPerSourcePackage.put(sourcePackage, output);

      if ((basePackage + "." + staticFactoryClass).equals(fqDefaultStaticFactoryClass))
      {
        staticFactoryClassOutputPerSourcePackage.put(DEFAULT_SCOPE_PACKAGE, output);
      }
    });
  }

  protected void writeStaticFactoryClassOutputs()
  {
    staticFactoryClassOutputPerSourcePackage.forEach((sourcePackage, staticFactoryClassOutput) ->
    {
      if (sourcePackage.equals(DEFAULT_SCOPE_PACKAGE))
        return;

      try
      {
        boolean writeSuccess = false;
        JavaFileObject file =
            processingEnv.getFiler().createSourceFile(staticFactoryClassOutput.getCanonicalName());
        try (Writer writer = file.openWriter())
        {
          writer.write(staticFactoryClassOutput.toString());
          writer.flush();
          writeSuccess = true;
        }
        finally
        {
          if (!writeSuccess)
          {
            file.delete();
          }
        }
      }
      catch (IOException ex)
      {
        printError(String.format("Failed to write static factory method host file '%s'.",
            staticFactoryClassOutput.getCanonicalName()), ex);
      }
    });
  }

  /**
   * Prepare some information about the given mapping in preparation for the actual processing and
   * generation of the fluent API.
   *
   * @param sourceElement
   *          the source element on which the mapping is declared
   * @param mappingAt
   *          the mapping
   * @param mappingSet
   *          the container used to store the mapping set
   */
  protected void prepareMapping(Element sourceElement, AnnotationMirror mappingAt,
      FluentMappingSetData mappingSet)
  {
    FluentMappingWrapper mapping = FluentMappingWrapper.wrap(sourceElement, mappingAt);
    TypeElementWrapper componentTypeElement;
    if (mapping.componentTypeIsDefaultValue())
      componentTypeElement = TypeElementWrapper.wrap((TypeElement) sourceElement);
    else
      componentTypeElement = mapping.componentTypeAsTypeMirrorWrapper().getTypeElement().get();

    boolean isAspect =
        componentTypeElement.hasModifiers(Modifier.ABSTRACT) || componentTypeElement.isInterface();
    /* createBase is ignored for aspects */
    boolean createBase = mapping.createBase() && !isAspect;
    boolean createAspect = isAspect || createBase;

    List<String> ignoredInterfaces = Arrays.asList(mapping.ignoredComponentInterfacesAsFqn());
    List<TypeMirrorWrapper> componentInterfaces = componentTypeElement.getInterfaces().stream()
        .filter(iface -> !ignoredInterfaces.contains(iface.getQualifiedName())
            && !ignoredComponentInterfaces.contains(iface.getQualifiedName()))
        .toList();


    String fluentFqcn = toTargetPackage(mapping, sourceElement) + "." + FLUENT_TYPE_PREFIX
        + componentTypeElement.getSimpleName();
    if (createBase)
      fluentFqcn += FLUENT_BASE_SUFFIX;


    componentToFluentMappingCache.put(componentTypeElement.getQualifiedName(), fluentFqcn);
    mappingSet.getComponentToFluent().put(componentTypeElement.getQualifiedName(), fluentFqcn);

    List<String> componentTypeVars = new ArrayList<>();
    List<String> fluentTypeVars = new ArrayList<>();
    Map<String, String> typeVarSubstitutions = new HashMap<>();

    List<TypeParameterElementWrapper> typeParameters = componentTypeElement.getTypeParameters();
    String[] canonicalTypeVars = mapping.typeVars();
    for (int i = 0; i < typeParameters.size(); i++)
    {
      TypeParameterElementWrapper typeParam = typeParameters.get(i);

      boolean explicitName = false;
      String canonicalTypeVar = null;
      if (canonicalTypeVars.length > i)
      {
        canonicalTypeVar = trimToNull(canonicalTypeVars[i]);
        explicitName = true;
      }
      if (canonicalTypeVar == null)
        canonicalTypeVar = typeParam.getSimpleName();

      componentTypeVars.add(canonicalTypeVar);
      typeVarSubstitutions.put(typeParam.getSimpleName(), canonicalTypeVar);

      if (!explicitName && !DEFAULT_TYPEVAR_ORDER.contains(typeParam.getSimpleName()))
      {
        printWarning(String.format(
            "Unrecognized type variable '%s' in component type '%s'. Consider providing an explicit name, especially in case of compile errors.",
            typeParam.getSimpleName(), componentTypeElement.getQualifiedName()), sourceElement,
            mappingAt);
      }
    }

    for (String itvm : mapping.innerTypeVarMapping())
    {
      String[] tokens = itvm.split(":");
      typeVarSubstitutions.put(tokens[0], tokens[1]);
    }

    for (TypeParameterElementWrapper typeParam : typeParameters)
    {
      /* Don't add the Component type variable a second time. */
      if (typeParam.getSimpleName().equals(TYPEVAR_COMPONENT))
        continue;

      String canonicalTypeVar = typeVarSubstitutions.get(typeParam.getSimpleName());

      fluentTypeVars.add(canonicalTypeVar);
    }

    if (createAspect)
    {
      boolean interfacesUseComponentType = componentInterfaces.stream()
          .flatMap(iface -> iface.hasTypeArguments() ? iface.getWrappedTypeArguments().stream()
              : Stream.empty())
          .anyMatch(arg ->
          {
            return arg.isAssignableTo(componentTypeElement);
          });

      boolean hasComponentTypeParam = !createBase || componentTypeVars.contains(TYPEVAR_COMPONENT)
          || !interfacesUseComponentType;

      if (hasComponentTypeParam)
        fluentTypeVars.add(TYPEVAR_COMPONENT);
      fluentTypeVars.add(TYPEVAR_FLUENT);
    }

    fluentTypeVars = sortTypeVars(fluentTypeVars);

    componentTypeVarsCache.put(componentTypeElement.getQualifiedName(),
        List.copyOf(componentTypeVars));
    mappingSet.getComponentTypeVars().put(componentTypeElement.getQualifiedName(),
        List.copyOf(componentTypeVars));

    fluentTypeVarsCache.put(componentTypeElement.getQualifiedName(), List.copyOf(fluentTypeVars));
    mappingSet.getFluentTypeVars().put(componentTypeElement.getQualifiedName(),
        List.copyOf(fluentTypeVars));

    typeVarSubstitutionsCache.put(componentTypeElement.getQualifiedName(),
        Map.copyOf(typeVarSubstitutions));
    mappingSet.getTypeVarSubstitutions().put(componentTypeElement.getQualifiedName(),
        Map.copyOf(typeVarSubstitutions));
  }

  /**
   * Process the given mapping and generate the fluent API.
   *
   * @param sourceElement
   *          the source element on which the mapping is declared
   * @param mappingAt
   *          the mapping
   */
  protected void processMapping(Element sourceElement, AnnotationMirror mappingAt)
  {
    FluentMappingWrapper mapping = FluentMappingWrapper.wrap(sourceElement, mappingAt);
    TypeElementWrapper componentTypeElement;
    if (mapping.componentTypeIsDefaultValue())
      componentTypeElement = TypeElementWrapper.wrap((TypeElement) sourceElement);
    else
      componentTypeElement = mapping.componentTypeAsTypeMirrorWrapper().getTypeElement().get();

    boolean isAspect =
        componentTypeElement.hasModifiers(Modifier.ABSTRACT) || componentTypeElement.isInterface();
    /* createBase is ignored for aspects */
    boolean createBase = mapping.createBase() && !isAspect;
    boolean createAspect = isAspect || createBase;

    String sourcePackageName =
        processingEnv.getElementUtils().getPackageOf(sourceElement).toString();

    List<String> ignoredInterfaces = Arrays.asList(mapping.ignoredComponentInterfacesAsFqn());
    List<TypeMirrorWrapper> componentInterfaces = componentTypeElement.getInterfaces().stream()
        .filter(iface -> !ignoredInterfaces.contains(iface.getQualifiedName())
            && !ignoredComponentInterfaces.contains(iface.getQualifiedName()))
        .toList();

    boolean interfacesUseComponentType = componentInterfaces.stream()
        .flatMap(iface -> iface.hasTypeArguments() ? iface.getWrappedTypeArguments().stream()
            : Stream.empty())
        .anyMatch(arg ->
        {
          return arg.isAssignableTo(componentTypeElement);
        });

    boolean hasComponentTypeParam =
        !createBase || componentTypeVarsCache.get(componentTypeElement.getQualifiedName())
            .contains(TYPEVAR_COMPONENT) || !interfacesUseComponentType;

    String fluentSimpleName = FLUENT_TYPE_PREFIX + componentTypeElement.getSimpleName()
        + (createBase ? FLUENT_BASE_SUFFIX : "");

    printDebug(sourceElement, componentTypeElement.getSimpleName() + " -> " + fluentSimpleName);
    printDebug(sourceElement,
        String.format("  isAspect=%s, createAspect=%s, createBase=%s, hasComponentTypeParam=%s",
            isAspect, createAspect, createBase, hasComponentTypeParam));


    Map<String, String> typeVarSubstitutions =
        typeVarSubstitutionsCache.get(componentTypeElement.getQualifiedName());

    List<String> leafTypeParams = Collections.emptyList();


    Output<?> output = prepareOutput(mapping, fluentSimpleName, componentTypeElement.isInterface(),
        createAspect, sourceElement);

    output.asAny().addImport(componentTypeElement.getQualifiedName());

    output.asAny().getJavaDoc()
        .setText("The fluent counterpart of {@link " + componentTypeElement.getSimpleName() + "}.");
    {
      List<String> typeVars = fluentTypeVarsCache.get(componentTypeElement.getQualifiedName());
      for (String typeVar : typeVars)
      {
        String description = TYPEVAR_JAVADOC_DESCRIPTIONS.get(typeVar);
        if (description != null)
          output.asAny().getJavaDoc().addTagValue("@param <" + typeVar + ">", description);
      }
    }
    output.asAny().getJavaDoc().addTagValue("@see", componentTypeElement.getSimpleName());

    if (createAspect && output.isClass()
        && !output.asAny().getCanonicalName().equals("de.codecamp.vaadin.fluent.FluentComponent"))
    {
      output.asClass().setAbstract(true);
    }

    /* TYPE PARAMETERS */

    {
      List<String> componentTypeVars =
          componentTypeVarsCache.get(componentTypeElement.getQualifiedName());
      List<String> fluentTypeVars =
          fluentTypeVarsCache.get(componentTypeElement.getQualifiedName());
      Map<String, String[]> fluentTypeVarBounds = new HashMap<>();

      for (TypeParameterElementWrapper typeParam : componentTypeElement.getTypeParameters())
      {
        /* Don't add the Component type variable a second time. */
        if (typeParam.getSimpleName().equals(TYPEVAR_COMPONENT))
          continue;

        String canonicalTypeVar = typeVarSubstitutions.get(typeParam.getSimpleName());

        fluentTypeVarBounds.put(canonicalTypeVar,
            typeParam.getBounds().stream()
                .map(b -> substituteTypeVars(b.unwrap().toString(), typeVarSubstitutions))
                .toArray(String[]::new));
      }

      if (createAspect)
      {
        if (hasComponentTypeParam)
        {
          String componentTypeVarBound =
              componentTypeElement.getSimpleName() + (componentTypeVars.isEmpty() ? ""
                  : "<" + String.join(", ", componentTypeVars) + ">");
          if (output.isInterface() && componentTypeVars.contains(TYPEVAR_COMPONENT))
          {
            fluentTypeVarBounds.put(TYPEVAR_COMPONENT,
                new String[] {Component.class.getCanonicalName(), componentTypeVarBound});
          }
          else
          {
            fluentTypeVarBounds.put(TYPEVAR_COMPONENT, new String[] {componentTypeVarBound});
          }
        }

        fluentTypeVarBounds.put(TYPEVAR_FLUENT, null);
        fluentTypeVarBounds = sortTypeVars(fluentTypeVarBounds);
        fluentTypeVarBounds.put(TYPEVAR_FLUENT, new String[] {fluentSimpleName
            + (fluentTypeVars.isEmpty() ? "" : "<" + String.join(", ", fluentTypeVars) + ">")});
      }
      else
      {
        fluentTypeVarBounds = sortTypeVars(fluentTypeVarBounds);
      }

      if (createBase)
      {
        leafTypeParams = new ArrayList<>(fluentTypeVarBounds.keySet());
      }

      fluentTypeVarBounds.forEach((name, bounds) ->
      {
        TypeVariableSource<?> typeVariableSource = output.asAny().addTypeVariable().setName(name);
        if (bounds.length > 0 && !(bounds.length == 1 && bounds[0].equals(Object.class.getName())))
        {
          typeVariableSource.setBounds(bounds);
        }
      });
    }


    /* SUPERCLASS */

    if (output.isClass())
    {
      if (componentTypeElement.getSuperclass().getQualifiedName().equals(Object.class.getName()))
      {
        if (output.asClass().getSuperType().equals(Object.class.getName()))
        {
          output.asClass().setSuperType("de.codecamp.vaadin.fluent.FluentWrapper<"
              + componentTypeElement.getSimpleName() + ", " + fluentSimpleName + ">");
        }
      }
      else
      {
        TypeMirrorWrapper sclass = componentTypeElement.getSuperclass();
        while (!sclass.getTypeElement().get().hasModifiers(Modifier.PUBLIC))
          sclass = sclass.getTypeElement().get().getSuperclass();

        String fluentSclassFqcn = componentToFluentMappingCache.get(sclass.getQualifiedName());
        if (fluentSclassFqcn == null)
        {
          printError(
              String.format("No fluent type found for superclass '%s' of '%s'.",
                  sclass.getQualifiedName(), componentTypeElement.getQualifiedName()),
              null, sourceElement, mappingAt);
        }
        else
        {
          List<String> componentTypeVars =
              componentTypeVarsCache.get(componentTypeElement.getQualifiedName());
          List<String> sclassComponentTypeVars =
              componentTypeVarsCache.get(sclass.getQualifiedName());
          Map<String, String> typeArgs = new HashMap<>();

          if (createAspect)
          {
            if (hasComponentTypeParam)
            {
              typeArgs.put(TYPEVAR_COMPONENT, TYPEVAR_COMPONENT);
            }
            else
            {
              typeArgs.put(TYPEVAR_COMPONENT,
                  componentTypeElement.getSimpleName() + (componentTypeVars.isEmpty() ? ""
                      : componentTypeVars.stream().collect(joining(", ", "<", ">"))));
            }

            typeArgs.put(TYPEVAR_FLUENT, TYPEVAR_FLUENT);
          }
          else
          {
            if (hasComponentTypeParam)
            {
              typeArgs.put(TYPEVAR_COMPONENT,
                  componentTypeElement.getSimpleName() + (componentTypeVars.isEmpty() ? ""
                      : componentTypeVars.stream().collect(joining(", ", "<", ">"))));
            }

            List<String> fluentTypeVars =
                fluentTypeVarsCache.get(componentTypeElement.getQualifiedName());
            typeArgs.put(TYPEVAR_FLUENT, fluentSimpleName + (fluentTypeVars.isEmpty() ? ""
                : fluentTypeVars.stream().collect(joining(", ", "<", ">"))));
          }

          printDebug(sourceElement, "  + " + sclass.unwrap().toString());

          if (sclass.hasTypeArguments())
          {
            List<TypeMirrorWrapper> ifaceTypeArgs = sclass.getWrappedTypeArguments();
            for (int i = 0; i < ifaceTypeArgs.size(); i++)
            {
              TypeMirrorWrapper ifaceTypeArg = ifaceTypeArgs.get(i);
              String canonicalComponentTypeVar = sclassComponentTypeVars.get(i);

              if (ifaceTypeArg.getKind() == TypeKind.TYPEVAR)
              {
                typeArgs.put(canonicalComponentTypeVar, canonicalComponentTypeVar);
              }
              else
              {
                typeArgs.put(canonicalComponentTypeVar, ifaceTypeArg.unwrap().toString());
              }
            }
          }

          typeArgs = sortTypeVars(typeArgs);
          typeArgs.replaceAll((n1, n2) -> substituteTypeVars(n2, typeVarSubstitutions));

          output.asClass().setSuperType(
              fluentSclassFqcn + typeArgs.values().stream().collect(joining(", ", "<", ">")));
        }
      }
    }


    /* INTERFACES */

    {
      /*
       * Make sure that every aspect interface has access to its wrapped component by implementing
       * SerializableSupplier. All classes already do this by extending FluentComponent.
       */
      if (output.isInterface() && !componentTypeElement.asType().isAssignableTo(HasElement.class)
          || componentTypeElement.asType().getQualifiedName()
              .equals(HasElement.class.getCanonicalName()))
      {
        output.asAny().addInterface(
            "com.vaadin.flow.function.SerializableSupplier<" + TYPEVAR_COMPONENT + ">");
      }

      for (TypeMirrorWrapper iface : componentInterfaces)
      {
        if (iface.getQualifiedName().equals(java.io.Serializable.class.getName()))
          continue;

        String fluentIfaceFqcn = componentToFluentMappingCache.get(iface.getQualifiedName());
        if (fluentIfaceFqcn == null)
        {
          printWarning(
              String.format("No fluent type found for interface '%s' of '%s'.",
                  iface.getQualifiedName(), componentTypeElement.getQualifiedName()),
              sourceElement, mappingAt);
          continue;
        }

        List<String> componentTypeVars =
            componentTypeVarsCache.get(componentTypeElement.getQualifiedName());
        List<String> ifaceComponentTypeVars = componentTypeVarsCache.get(iface.getQualifiedName());
        Map<String, String> typeArgs = new HashMap<>();

        if (createAspect)
        {
          if (hasComponentTypeParam)
          {
            typeArgs.put(TYPEVAR_COMPONENT, TYPEVAR_COMPONENT);
          }
          else
          {
            typeArgs.put(TYPEVAR_COMPONENT,
                componentTypeElement.getSimpleName() + (componentTypeVars.isEmpty() ? ""
                    : componentTypeVars.stream().collect(joining(", ", "<", ">"))));
          }

          typeArgs.put(TYPEVAR_FLUENT, TYPEVAR_FLUENT);
        }
        else
        {
          typeArgs.put(TYPEVAR_COMPONENT,
              componentTypeElement.getSimpleName() + (componentTypeVars.isEmpty() ? ""
                  : componentTypeVars.stream().collect(joining(", ", "<", ">"))));

          List<String> fluentTypeVars =
              fluentTypeVarsCache.get(componentTypeElement.getQualifiedName());
          typeArgs.put(TYPEVAR_FLUENT, fluentSimpleName + (fluentTypeVars.isEmpty() ? ""
              : fluentTypeVars.stream().collect(joining(", ", "<", ">"))));
        }

        printDebug(sourceElement, "  + " + iface.unwrap().toString());

        if (iface.hasTypeArguments())
        {
          List<TypeMirrorWrapper> ifaceTypeArgs = iface.getWrappedTypeArguments();
          for (int i = 0; i < ifaceTypeArgs.size(); i++)
          {
            TypeMirrorWrapper ifaceTypeArg = ifaceTypeArgs.get(i);
            String canonicalComponentTypeVar = ifaceComponentTypeVars.get(i);

            if (ifaceTypeArg.getKind() == TypeKind.TYPEVAR)
            {
              typeArgs.put(canonicalComponentTypeVar, canonicalComponentTypeVar);
            }
            else
            {
              typeArgs.put(canonicalComponentTypeVar, ifaceTypeArg.unwrap().toString());
            }
          }
        }

        typeArgs = sortTypeVars(typeArgs);
        typeArgs.replaceAll((n1, n2) -> substituteTypeVars(n2, typeVarSubstitutions));

        output.asAny().addInterface(
            fluentIfaceFqcn + typeArgs.values().stream().collect(joining(", ", "<", ">")));
      }
    }


    /* CONSTRUCTORS */

    if (output.isClass() && !output.asClass().getSuperType().equals(Object.class.getName()))
    {
      if (createAspect)
      {
        if (hasComponentTypeParam)
        {
          if (output.hasConstructor(TYPEVAR_COMPONENT))
          {
            printDebug(sourceElement, String.format("  '%s(%s)' -> skipped! (already in template)",
                fluentSimpleName, TYPEVAR_COMPONENT));
          }
          else
          {
            MethodSource<JavaClassSource> ctor = output.asClass().addMethod().setConstructor(true)
                .setProtected().setBody("super(component);");
            ctor.addParameter(TYPEVAR_COMPONENT, "component");
            ctor.getJavaDoc()
                .setText("Constructs a new instance configuring the given {@link "
                    + componentTypeElement.getSimpleName() + "}.")
                .addTagValue("@param component", "the component to be configured");
          }
        }
        else
        {
          if (output.hasConstructor(componentTypeElement.getSimpleName()))
          {
            printDebug(sourceElement, String.format("  '%s(%s)' -> skipped! (already in template)",
                fluentSimpleName, componentTypeElement.getSimpleName()));
          }
          else
          {
            MethodSource<JavaClassSource> ctor = output.asClass().addMethod().setConstructor(true)
                .setProtected().setBody("super(component);");
            ctor.addParameter(componentTypeElement.getSimpleName(), "component");
            ctor.getJavaDoc()
                .setText("Constructs a new instance configuring the given {@link "
                    + componentTypeElement.getSimpleName() + "}.")
                .addTagValue("@param component", "the component to be configured");
          }
        }
      }
      else
      {
        if (componentTypeElement.getConstructors(Modifier.PUBLIC).stream()
            .anyMatch(ctor -> ctor.getParameters().isEmpty()))
        {
          if (output.hasConstructor())
          {
            printDebug(sourceElement,
                String.format("  '%s()' -> skipped! (already in template)", fluentSimpleName));
          }
          else
          {
            MethodSource<JavaClassSource> ctor =
                output.asClass().addMethod().setConstructor(true).setPublic();
            if (mapping.i18nUtilsIsDefaultValue())
            {
              ctor.setBody("this(new " + componentTypeElement.getSimpleName()
                  + (componentTypeElement.hasTypeParameters() ? "<>" : "") + "());");
              ctor.getJavaDoc().setText("Constructs a new instance configuring a new {@link "
                  + componentTypeElement.getSimpleName() + "}.");
            }
            else
            {
              ctor.setBody("this(new " + componentTypeElement.getSimpleName()
                  + (componentTypeElement.hasTypeParameters() ? "<>" : "") + "()); localize();");
              ctor.getJavaDoc().setText(
                  "Constructs a new instance configuring a new {@link #localize() localized} {@link "
                      + componentTypeElement.getSimpleName() + "}.");
            }
          }
        }

        {
          List<String> componentTypeVars =
              componentTypeVarsCache.get(componentTypeElement.getQualifiedName());
          String ctorParamType =
              componentTypeElement.getSimpleName() + (componentTypeVars.isEmpty() ? ""
                  : componentTypeVars.stream().collect(joining(", ", "<", ">")));
          if (output.hasConstructor(ctorParamType))
          {
            printDebug(sourceElement, String.format("  '%s(%s)' -> skipped! (already in template)",
                fluentSimpleName, ctorParamType));
          }
          else
          {
            MethodSource<JavaClassSource> ctor = output.asClass().addMethod().setConstructor(true)
                .setPublic().setBody("super(component);");
            ctor.addParameter(ctorParamType, "component");
            ctor.getJavaDoc()
                .setText("Constructs a new instance configuring the given {@link "
                    + componentTypeElement.getSimpleName() + "}.")
                .addTagValue("@param component", "the component to be configured");
          }
        }
      }
    }


    /* METHODS */

    {
      List<ExecutableElementWrapper> methods = componentTypeElement.getMethods(Modifier.PUBLIC);
      Stream
          .iterate(componentTypeElement.getSuperclass().getTypeElement().orElse(null),
              cte -> cte != null && !cte.hasModifiers(Modifier.PUBLIC),
              cte -> cte.getSuperclass().getTypeElement().orElse(null))
          .forEach(cte -> methods.addAll(cte.getMethods(Modifier.PUBLIC)));

      methodLoop: for (ExecutableElementWrapper method : methods)
      {
        if (method.hasModifiers(Modifier.STATIC))
          continue;

        /* Skip methods with @Override annotation. */
        if (getAllSupertypes(componentTypeElement.unwrap().asType()).anyMatch(superType ->
        {
          return MoreTypes.asElement(superType).getEnclosedElements().stream()
              .filter(e -> e.getKind() == ElementKind.METHOD).map(ExecutableElement.class::cast)
              .filter(e -> e.getSimpleName().toString().equals(method.getSimpleName())) //
              .filter(e -> MoreElements.overrides(method.unwrap(), e, componentTypeElement.unwrap(),
                  processingEnv.getTypeUtils())) //
              .findAny().isPresent();
        }))
        {
          printDebug(sourceElement,
              String.format("  '%s' -> skipped! (@Override)", method.getMethodSignature()));
          continue;
        }


        String methodName = method.getSimpleName();
        List<VariableElementWrapper> parameters = method.getParameters();
        List<String> methodParams =
            parameters.stream().map(p -> p.asType().erasure().getQualifiedName()).toList();

        String[] mappedMethodNames = null;
        FluentMethodWrapper explicitMethodMapping = null;
        for (AnnotationMirror methodMappingAt : mapping.mappedAsAnnotationMirrorArray())
        {
          FluentMethodWrapper methodMapping = FluentMethodWrapper.wrap(methodMappingAt);

          if (methodMapping.name().equals(methodName) && (methodMapping.paramsIsDefaultValue()
              || Arrays.asList(methodMapping.paramsAsFqn()).equals(methodParams)))
          {
            if (methodMapping.skip())
            {
              printDebug(sourceElement, String.format(
                  "  '%s' -> skipped! (@MappedMethod(skip=true))", method.getMethodSignature()));
              continue methodLoop;
            }

            mappedMethodNames = methodMapping.fluent();
            explicitMethodMapping = methodMapping;
            break;
          }
        }

        /* automatic mapping */

        if (mappedMethodNames == null)
        {
          if (methodName.startsWith("get") || methodName.startsWith("is")
              || methodName.startsWith("has"))
          {
            continue;
          }

          if (parameters.size() != 1 && !methodName.startsWith("set"))
            continue;
        }

        if (mappedMethodNames == null || mappedMethodNames.length == 0)
        {
          if (methodName.startsWith("set"))
          {
            mappedMethodNames =
                new String[] {Introspector.decapitalize(removeStart(methodName, "set"))};
          }
          else if (methodName.startsWith("add") && methodName.endsWith("Listener"))
          {
            mappedMethodNames = new String[] {
                "on" + removeEnd(removeEnd(removeStart(methodName, "add"), "Listener"), "Event")};
          }
          else if (mappedMethodNames != null)
          {
            mappedMethodNames = new String[] {methodName};
          }
        }

        if (mappedMethodNames != null)
        {
          List<String> paramTypes = new ArrayList<>();
          List<String> paramNames = new ArrayList<>();
          printDebug(sourceElement,
              "  '" + method.getMethodSignature() + "' -> " + Arrays.asList(mappedMethodNames));
          for (VariableElementWrapper param : parameters)
          {
            String paramType = switch (param.asType().getKind())
            {
              case TYPEVAR -> ((TypeVariable) param.asType().unwrap()).asElement().getSimpleName()
                  .toString();
              default -> param.asType().unwrap().toString();
            };
            paramType = substituteTypeVars(paramType, typeVarSubstitutions);

            String paramName = param.getSimpleName();

            paramTypes.add(paramType);
            paramNames.add(paramName);
          }

          for (String mappedMethodName : mappedMethodNames)
          {
            /*
             * add the primary method
             */
            if (output.hasMethod(mappedMethodName, paramTypes))
            {
              printDebug(sourceElement,
                  String.format("  '%s(%s)' -> skipped! (already in template)", mappedMethodName,
                      paramTypes.stream().collect(joining(", "))));
            }
            else
            {
              MethodSource<?> fluentMethod =
                  addMethod(output, createAspect, componentTypeElement, fluentSimpleName)
                      .setName(mappedMethodName);
              addJavaDocSeeToComponentMethod(fluentMethod, componentTypeElement, method);

              if (!method.getTypeParameters().isEmpty())
              {
                method.getTypeParameters().stream().forEach(tp ->
                {
                  TypeVariableSource<?> typeArg =
                      fluentMethod.addTypeVariable().setName(tp.getSimpleName());

                  List<TypeMirrorWrapper> bounds = tp.getBounds();
                  if (!bounds.isEmpty())
                  {
                    typeArg.setBounds(bounds.stream().map(b ->
                    {
                      output.asAny().addImport(b.getQualifiedName());
                      return b.getTypeDeclaration();
                    }).toArray(String[]::new));
                  }
                });
              }

              for (int i = 0; i < paramTypes.size(); i++)
              {
                String paramType = paramTypes.get(i);
                String paramName = paramNames.get(i);


                /* If the last parameter is an array, turn it into varargs. */
                if (i == paramTypes.size() - 1 && paramType.endsWith("[]"))
                {
                  paramType = removeEnd(paramType, "[]");
                  fluentMethod.addParameter(paramType, paramName).setVarArgs(true);
                }
                else
                {
                  /*
                   * Replace fully qualified type arguments with an import. Ideally Roaster would
                   * already do this, but it doesn't.
                   */
                  List<String> typeToImport = new ArrayList<>();
                  Queue<String> typeQueue = new ArrayDeque<>();
                  typeQueue.add(paramType);
                  while (!typeQueue.isEmpty())
                  {
                    String type = typeQueue.poll();

                    /*
                     * splitGenerics leaves "? extends" in and turns it into "?extends". So e.g.
                     * "? extends Component" turns into "?extendsComponent". Remove "?extends" if
                     * present.
                     */
                    type = removeStart(type, "?extends");

                    if (Types.isQualified(type) && !Types.isJavaLang(type))
                      typeToImport.add(Types.stripGenerics(type));

                    if (Types.isGeneric(type))
                    {
                      typeQueue.addAll(Arrays.asList(Types.splitGenerics(type)));
                    }
                  }
                  if (!typeToImport.isEmpty())
                  {
                    typeToImport.forEach(type -> output.asAny().addImport(type));
                  }
                  paramType = Types.toSimpleName(paramType);

                  fluentMethod.addParameter(paramType, paramName);
                }
              }

              if (createAspect)
              {
                fluentMethod.setBody("get()." + method.getSimpleName() + "("
                    + String.join(", ", paramNames) + "); return (F) this;");
              }
              else
              {
                fluentMethod.setBody("get()." + method.getSimpleName() + "("
                    + String.join(", ", paramNames) + "); return this;");
              }
            }

            /*
             * for boolean setters also provide a parameterless overload
             */
            if (paramTypes.size() == 1 && paramTypes.get(0).equals(boolean.class.getName()))
            {
              if (output.hasMethod(mappedMethodName))
              {
                printDebug(sourceElement,
                    String.format("  '%s()' -> skipped! (already in template)", mappedMethodName));
              }
              else
              {
                MethodSource<?> fluentMethod =
                    addMethod(output, createAspect, componentTypeElement, fluentSimpleName)
                        .setName(mappedMethodName);
                addJavaDocSeeToComponentMethod(fluentMethod, componentTypeElement, method);

                fluentMethod.setBody("return " + mappedMethodName + "(true);");
              }
            }

            /*
             * for setters that accept an Icon also provide a version that accepts an IconFactory
             */
            if (paramTypes.size() == 1 && paramTypes.get(0).equals(Icon.class.getName()))
            {
              if (output.hasMethod(mappedMethodName, IconFactory.class.getName()))
              {
                printDebug(sourceElement,
                    String.format("  '%s(%s)' -> skipped! (already in template)", mappedMethodName,
                        IconFactory.class.getName()));
              }
              else
              {
                MethodSource<?> fluentMethod =
                    addMethod(output, createAspect, componentTypeElement, fluentSimpleName)
                        .setName(mappedMethodName);
                fluentMethod.addParameter(IconFactory.class, "icon");
                addJavaDocSeeToComponentMethod(fluentMethod, componentTypeElement, method);

                fluentMethod.setBody("return %s(icon.create());".formatted(mappedMethodName));
              }
            }

            /*
             * for enum setters optionally also provide one method per element
             */
            if ((explicitMethodMapping == null || explicitMethodMapping.expandEnum())
                && paramTypes.size() == 1 && parameters.get(0).asType().isEnum())
            {
              for (VariableElement enumConstant : LangModelUtils
                  .getEnumConstants(MoreTypes.asTypeElement(parameters.get(0).asType().unwrap())))
              {
                String suffix = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL,
                    enumConstant.getSimpleName().toString());
                MethodSource<?> fluentMethod =
                    addMethod(output, createAspect, componentTypeElement, fluentSimpleName)
                        .setName(mappedMethodName + suffix);

                TypeElementWrapper typeElementWrapper =
                    parameters.get(0).asType().getTypeElement().get();

                fluentMethod.setBody("return %s(%s.%s);".formatted(mappedMethodName,
                    typeElementWrapper.getSimpleName(), enumConstant.getSimpleName().toString()));

                addJavaDocSeeToComponentMethod(fluentMethod, componentTypeElement, method);
                fluentMethod.getJavaDoc() //
                    .addTagValue("@see", typeElementWrapper.getSimpleName() + "#"
                        + enumConstant.getSimpleName().toString());
              }
            }

            /*
             * add overloads that take message keys
             */
            if (explicitMethodMapping != null && explicitMethodMapping.withTranslation())
            {
              if (paramTypes.size() != 1 || !paramTypes.get(0).equals(String.class.getName()))
              {
                printError(String.format(
                    "Cannot create translation overload for method '%s'. Only supported for methods with a single String parameter.",
                    method.unwrap().toString()), null, sourceElement, mappingAt);
              }
              else
              {
                String mappedMethodNameL10n = mappedMethodName + TRANSLATE_METHOD_SUFFIX;

                output.asAny().addImport("de.codecamp.vaadin.base.i18n.TranslationUtils");

                MethodSource<?> fluentMethod1 =
                    addMethod(output, createAspect, componentTypeElement, fluentSimpleName)
                        .setName(mappedMethodNameL10n);
                fluentMethod1.addParameter(String.class, "key");
                fluentMethod1.addParameter(Object.class, "params").setVarArgs(true);
                fluentMethod1.setBody("return %s(TranslationUtils.getTranslation(key, params));"
                    .formatted(mappedMethodName));

                fluentMethod1.getJavaDoc().setText(
                    "Gets the translation for the given message key and sets it as {@link #%s(String)}."
                        .formatted(mappedMethodName))
                    .addTagValue("@param", "key the message key")
                    .addTagValue("@param", "params the message arguments")
                    .addTagValue("@see", "TranslationUtils#getTranslation(String, Object...)");
                addJavaDocSeeToComponentMethod(fluentMethod1, componentTypeElement, method);


                MethodSource<?> fluentMethod2 =
                    addMethod(output, createAspect, componentTypeElement, fluentSimpleName)
                        .setName(mappedMethodNameL10n);
                fluentMethod2.addParameter(Object.class, "key");
                fluentMethod2.addParameter(Object.class, "params").setVarArgs(true);
                fluentMethod2.setBody("return %s(TranslationUtils.getTranslation(key, params));"
                    .formatted(mappedMethodName));

                fluentMethod2.getJavaDoc().setText(
                    "Gets the translation for the given message key and sets it as {@link #%s(String)}."
                        .formatted(mappedMethodName))
                    .addTagValue("@param", "key the message key")
                    .addTagValue("@param", "params the message arguments")
                    .addTagValue("@see", "TranslationUtils#getTranslation(Object, Object...)");
                addJavaDocSeeToComponentMethod(fluentMethod2, componentTypeElement, method);
              }
            }
          }
        }
        else
        {
          printDebug(sourceElement, "  '" + method.getMethodSignature() + "' -> skipped!");
        }
      }


      /*
       * localize-method
       */
      if (!mapping.i18nUtilsIsDefaultValue())
      {
        addMethod(output, createAspect, componentTypeElement, fluentSimpleName).setName("localize")
            .setBody(mapping.i18nUtilsAsFqn() + ".localize(get()); return this;");
      }


      /*
       * theme variant methods
       */
      TypeMirrorWrapper detectedThemeVariantType = detectThemeVariantType(componentTypeElement);
      if (mapping.themesIsDefaultValue())
      {
        if (detectedThemeVariantType != null)
        {
          printError(String.format(
              "No @ThemeGroups defined for component type '%s', but ThemeVariant type '%s' detected.",
              componentTypeElement.getQualifiedName(), detectedThemeVariantType.getQualifiedName()),
              null, sourceElement);
        }
      }
      else
      {
        TypeMirrorWrapper themeVariantType;
        if (mapping.themeVariantTypeIsDefaultValue())
        {
          themeVariantType = detectedThemeVariantType;
          if (themeVariantType == null)
          {
            printError(String.format(
                "Failed to automatically determine ThemeVariant type for component type '%s'. Specify it explicitly.",
                componentTypeElement.getQualifiedName()), null, sourceElement);
            return;
          }
        }
        else
        {
          themeVariantType = mapping.themeVariantTypeAsTypeMirrorWrapper();
        }

        Set<String> allThemeVariants = new HashSet<>();
        for (VariableElementWrapper enumConstant : themeVariantType.getTypeElement().get()
            .getEnumValues())
        {
          String simpleName = enumConstant.getSimpleName();
          if (simpleName.startsWith("MATERIAL_"))
            continue;

          allThemeVariants.add(simpleName);
        }
        Set<String> unusedThemeVariants = new HashSet<>(allThemeVariants);

        output.asAny().addImport(themeVariantType.getQualifiedName());

        themeLoop: for (ThemeGroupWrapper themeGroup : mapping.themes())
        {
          String[] themeGroupVariants = themeGroup.variant();
          String[] themeGroupFluents = themeGroup.fluent();

          if (themeGroupVariants.length == 0 && themeGroupFluents.length == 0)
          {
            printError(String.format("Empty @ThemeGroup found for component type '%s'.",
                componentTypeElement.getQualifiedName()), null, sourceElement, mappingAt);
            continue;
          }

          unusedThemeVariants.removeAll(Arrays.asList(themeGroupVariants));

          if (themeGroupVariants.length == 1)
          {
            if (themeGroupFluents.length > 1)
            {
              printError(
                  String.format(
                      "@ThemeGroup %s for component type '%s' must have 0 or 1 fluent mapping.",
                      Arrays.asList(themeGroupVariants), componentTypeElement.getQualifiedName()),
                  null, sourceElement, mappingAt);
              continue;
            }

            String variantName = themeGroupVariants[0];
            String fluentName = themeGroupFluents.length > 0 ? themeGroupFluents[0] : "";
            boolean invert = fluentName.startsWith("!");
            fluentName = removeStart(fluentName, "!");
            if (fluentName.isEmpty())
            {
              fluentName = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL,
                  removeStart(variantName, "LUMO_"));
            }

            addMethod(output, createAspect, componentTypeElement, fluentSimpleName)
                .setName(fluentName).setBody("return " + fluentName + "(true);") //
                .getJavaDoc()
                .setText((invert ? "Removes" : "Adds") + " the {@link "
                    + themeVariantType.getSimpleName() + "#" + variantName + "} theme variant.")
                .addTagValue("@see", themeVariantType.getSimpleName() + "#" + variantName);

            MethodSource<?> variantMethod =
                addMethod(output, createAspect, componentTypeElement, fluentSimpleName)
                    .setName(fluentName)
                    .setBody("return themeVariant(" + themeVariantType.getSimpleName() + "."
                        + variantName + ", " + (invert ? "!" : "") + fluentName + ");");

            variantMethod.addParameter(boolean.class, fluentName);
            variantMethod.getJavaDoc()
                .setText("Toggles the {@link " + themeVariantType.getSimpleName() + "#"
                    + variantName + "} theme variant.")
                .addTagValue("@see", themeVariantType.getSimpleName() + "#" + variantName);
          }
          else
          {
            boolean defaultVariant = Arrays.asList(themeGroupVariants).contains("");
            if (defaultVariant)
            {
              if (themeGroupFluents.length != 1
                  && themeGroupVariants.length != themeGroupFluents.length)
              {
                printError(String.format(
                    "@ThemeGroup %s for component type '%s' must have 1 fluent mappings (for the unnamed default variant) or the same number as variants.",
                    Arrays.asList(themeGroupVariants), componentTypeElement.getQualifiedName()),
                    null, sourceElement, mappingAt);
                continue;
              }
            }
            else
            {
              if (themeGroupFluents.length != 0
                  && themeGroupVariants.length != themeGroupFluents.length)
              {
                printError(String.format(
                    "@ThemeGroup %s for component type '%s' must have 0 fluent mappings or the same number as variants.",
                    Arrays.asList(themeGroupVariants), componentTypeElement.getQualifiedName()),
                    null, sourceElement, mappingAt);
                continue;
              }
            }

            for (int i = 0; i < themeGroupVariants.length; i++)
            {
              String variantName = themeGroupVariants[i];
              String fluentName;
              if (themeGroupVariants.length == themeGroupFluents.length)
                fluentName = themeGroupFluents[i];
              else if (variantName.isEmpty())
                fluentName = themeGroupFluents[0];
              else
                fluentName = "";

              if (fluentName.isEmpty())
              {
                if (variantName.isEmpty())
                {
                  printError(String.format(
                      "@ThemeGroup %s for component type '%s' must have non-empty fluent mapping for the unnamed default variant.",
                      Arrays.asList(themeGroupVariants), componentTypeElement.getQualifiedName()),
                      null, sourceElement, mappingAt);
                  continue themeLoop;
                }

                fluentName = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL,
                    removeStart(variantName, "LUMO_"));
              }

              List<String> bodyStmts = new ArrayList<>();

              List<String> variantsToRemove = Stream.of(themeGroupVariants)
                  .filter(v -> !v.isBlank() && !v.equals(variantName)).toList();
              if (!variantsToRemove.isEmpty())
              {
                bodyStmts.add("removeThemeVariants(" + variantsToRemove.stream()
                    .map(v -> themeVariantType.getSimpleName() + "." + v).collect(joining(", "))
                    + ");");
              }

              if (!variantName.isBlank())
              {
                bodyStmts.add("addThemeVariants(" + themeVariantType.getSimpleName() + "."
                    + variantName + ");");
              }

              if (createAspect)
                bodyStmts.add("return (F) this;");
              else
                bodyStmts.add("return this;");


              MethodSource<?> variantMethod =
                  addMethod(output, createAspect, componentTypeElement, fluentSimpleName)
                      .setName(fluentName).setBody(String.join("\n", bodyStmts));

              String removedVariantsJavaDoc =
                  Stream.of(themeGroupVariants).filter(v -> !v.isBlank() && !v.equals(variantName))
                      .map(v -> "{@link " + themeVariantType.getSimpleName() + "#" + v + "}")
                      .collect(joining(", "));

              if (variantName.isBlank())
              {
                variantMethod.getJavaDoc().setText("Removes " + removedVariantsJavaDoc + ".");
              }
              else
              {
                variantMethod.getJavaDoc()
                    .setText("Activates the {@link " + themeVariantType.getSimpleName() + "#"
                        + variantName + "} theme variant. This implicitly removes "
                        + removedVariantsJavaDoc + ".")
                    .addTagValue("@see", themeVariantType.getSimpleName() + "#" + variantName);
              }

              Stream.of(themeGroupVariants).filter(v -> !v.isBlank() && !v.equals(variantName))
                  .forEach(removedVariant ->
                  {
                    variantMethod.getJavaDoc().addTagValue("@see",
                        themeVariantType.getSimpleName() + "#" + removedVariant);
                  });
            }
          }
        }

        if (!unusedThemeVariants.isEmpty())
        {
          printError(
              String.format("Component type '%s' has unused theme variants: %s",
                  componentTypeElement.getQualifiedName(), unusedThemeVariants),
              null, sourceElement, mappingAt);
        }
      }
    }


    /* VERBATIM DECLARATIONS */

    for (String importt : mapping.imports())
    {
      output.asAny().addImport(importt);
    }

    for (String iface : mapping.interfaces())
    {
      output.asAny().addInterface(iface);
    }

    for (String method : mapping.methods())
    {
      output.asAny().addMethod(method);
    }


    /* SAVE */

    createSourceFile(output.asAny(), sourceElement);


    String fluentLeafSimpleName = fluentSimpleName;
    String fluentLeafCanonicalName = output.getCanonicalName();


    /* CREATE LEAF TYPE FOR BASE */

    if (createBase)
    {
      fluentLeafSimpleName = FLUENT_TYPE_PREFIX + componentTypeElement.getSimpleName();

      printDebug(sourceElement,
          componentTypeElement.getSimpleName() + " -> " + fluentLeafSimpleName);

      Output<?> leafOutput =
          prepareOutput(mapping, fluentLeafSimpleName, false, false, sourceElement);
      fluentLeafCanonicalName = leafOutput.getCanonicalName();

      leafOutput.asAny().getJavaDoc().setText(
          "The fluent counterpart of {@link " + componentTypeElement.getSimpleName() + "}.");
      {
        List<String> typeVars = fluentTypeVarsCache.get(componentTypeElement.getQualifiedName());
        for (String typeVar : typeVars)
        {
          if (typeVar.equals(TYPEVAR_COMPONENT) || typeVar.equals(TYPEVAR_FLUENT))
            continue;

          String description = TYPEVAR_JAVADOC_DESCRIPTIONS.get(typeVar);
          if (description != null)
            leafOutput.asAny().getJavaDoc().addTagValue("@param <" + typeVar + ">", description);
        }
      }
      leafOutput.asAny().getJavaDoc().addTagValue("@see", componentTypeElement.getSimpleName());

      /* TYPE PARAMETERS */

      leafTypeParams.remove(TYPEVAR_COMPONENT);
      leafTypeParams.remove(TYPEVAR_FLUENT);
      for (String typeVar : leafTypeParams)
      {
        leafOutput.asAny().addTypeVariable().setName(typeVar);
      }

      /* SUPERCLASS */

      List<String> leafSuperTypeArgs = new ArrayList<>();

      if (hasComponentTypeParam)
      {
        leafSuperTypeArgs.add(componentTypeElement.getSimpleName()
            + (leafTypeParams.isEmpty() ? "" : "<" + String.join(", ", leafTypeParams) + ">"));
      }
      leafSuperTypeArgs.add(fluentLeafSimpleName
          + (leafTypeParams.isEmpty() ? "" : "<" + String.join(", ", leafTypeParams) + ">"));
      leafSuperTypeArgs.addAll(leafTypeParams);

      leafOutput.asClass().setSuperType(
          fluentSimpleName + leafSuperTypeArgs.stream().collect(joining(", ", "<", ">")));

      /* CONSTRUCTORS */

      if (componentTypeElement.getConstructors(Modifier.PUBLIC).stream()
          .anyMatch(ctor -> ctor.getParameters().isEmpty()))
      {
        MethodSource<JavaClassSource> ctor = leafOutput.asClass().addMethod().setConstructor(true)
            .setPublic().setBody("this(new " + componentTypeElement.getSimpleName()
                + (componentTypeElement.hasTypeParameters() ? "<>" : "") + "());");
        ctor.getJavaDoc().setText("Constructs a new instance configuring a new {@link "
            + componentTypeElement.getSimpleName() + "}.");
      }

      List<String> componentTypeVars =
          componentTypeVarsCache.get(componentTypeElement.getQualifiedName());
      MethodSource<JavaClassSource> ctor = leafOutput.asClass().addMethod().setConstructor(true)
          .setPublic().setBody("super(component);");
      ctor.addParameter(componentTypeElement.getQualifiedName() + (componentTypeVars.isEmpty() ? ""
          : componentTypeVars.stream().collect(joining(", ", "<", ">"))), "component");
      ctor.getJavaDoc()
          .setText("Constructs a new instance configuring the given {@link "
              + componentTypeElement.getSimpleName() + "}.")
          .addTagValue("@param component", "the component to be configured");

      /* SAVE */

      createSourceFile(leafOutput.asAny(), sourceElement);
    }


    /* STATIC FACTORY METHODS */

    if (!isAspect)
    {
      JavaClassSource staticFactoryClassOutput =
          staticFactoryClassOutputPerSourcePackage.get(sourcePackageName);
      if (staticFactoryClassOutput == null)
      {
        staticFactoryClassOutput =
            staticFactoryClassOutputPerSourcePackage.get(DEFAULT_SCOPE_PACKAGE);
      }

      /* VERBATIM EXPLICIT FACTORY METHODS */

      for (String factory : mapping.staticFactories())
      {
        staticFactoryClassOutput.addMethod(factory).setPublic().setStatic(true);
      }

      /* IMPLICIT FACTORY METHODS */

      if (mapping.autoStaticFactories())
      {
        boolean hasItemTypeVar = componentTypeVarsCache.get(componentTypeElement.getQualifiedName())
            .contains(TYPEVAR_ITEM);

        String methodName =
            componentTypeElement.getSimpleName().substring(0, 1).toLowerCase(Locale.ENGLISH)
                + componentTypeElement.getSimpleName().substring(1);

        String returnType =
            fluentLeafCanonicalName + (hasItemTypeVar ? "<" + TYPEVAR_ITEM + ">" : "");

        if (componentTypeElement.getConstructors(Modifier.PUBLIC).stream()
            .anyMatch(ctor -> ctor.getParameters().isEmpty())
            && !staticFactoryClassOutput.hasMethodSignature(methodName))
        {
          MethodSource<JavaClassSource> methodSource = staticFactoryClassOutput.addMethod()
              .setName(methodName) //
              .setPublic().setStatic(true) //
              .setReturnType(returnType) //
              .setBody("return new " + fluentLeafSimpleName + (hasItemTypeVar ? "<>" : "") + "();");
          methodSource.getJavaDoc() //
              .setText("Returns a fluent wrapper configuring a new {@link "
                  + componentTypeElement.getQualifiedName() + "} instance.")
              .addTagValue("@return", "a new fluent wrapper with a new component instance")
              .addTagValue("@see", componentTypeElement.getQualifiedName());

          if (hasItemTypeVar)
            methodSource.addTypeVariable(TYPEVAR_ITEM);
        }

        if (componentTypeVarsCache.get(componentTypeElement.getQualifiedName()).contains(
            TYPEVAR_ITEM) && !staticFactoryClassOutput.hasMethodSignature(methodName, Class.class))
        {
          MethodSource<JavaClassSource> methodSource = staticFactoryClassOutput.addMethod()
              .setName(methodName) //
              .setPublic().setStatic(true) //
              .setReturnType(returnType) //
              .setBody("return new " + fluentLeafSimpleName + (hasItemTypeVar ? "<>" : "") + "();");
          methodSource.getJavaDoc() //
              .setText("Returns a fluent wrapper configuring a new {@link "
                  + componentTypeElement.getQualifiedName() + "} instance.")
              .addTagValue("@return", "a new fluent wrapper with a new component instance")
              .addTagValue("@see", componentTypeElement.getQualifiedName());

          if (hasItemTypeVar)
            methodSource.addTypeVariable(TYPEVAR_ITEM);

          methodSource.addParameter("Class<" + TYPEVAR_ITEM + ">", "itemType")
              .addAnnotation(SuppressWarnings.class).setStringValue("unused");
        }

        {
          MethodSource<JavaClassSource> methodSource =
              staticFactoryClassOutput.addMethod().setName("fluent") //
                  .setPublic().setStatic(true) //
                  .setReturnType(returnType) //
                  .setBody("return new " + fluentLeafSimpleName + (hasItemTypeVar ? "<>" : "")
                      + "(component);");
          methodSource.getJavaDoc() //
              .setText("Returns a fluent wrapper configuring the given {@link "
                  + componentTypeElement.getQualifiedName() + "}.")
              .addTagValue("@param component", "the component to be configured")
              .addTagValue("@return", "a new fluent wrapper for the given component")
              .addTagValue("@see", componentTypeElement.getQualifiedName());

          if (hasItemTypeVar)
            methodSource.addTypeVariable(TYPEVAR_ITEM);

          methodSource.addParameter(componentTypeElement.getQualifiedName()
              + (hasItemTypeVar ? "<" + TYPEVAR_ITEM + ">" : ""), "component");
        }
      }
    }
  }

  private Output<?> prepareOutput(FluentMappingWrapper mapping, String fluentSimpleName,
      boolean iface, boolean aspect, Element sourceElement)
  {
    Output<?> output = null;

    if (templateDir != null)
    {
      try
      {
        Path templatePath =
            templateDir.resolve(toTargetPackage(mapping, sourceElement).replace(".", "/"))
                .resolve(fluentSimpleName + ".java");

        try (InputStream in = Files.newInputStream(templatePath))
        {
          if (iface)
            output = new Output<>(Roaster.parse(JavaInterfaceSource.class, in));
          else
            output = new Output<>(Roaster.parse(JavaClassSource.class, in));
        }
        printDebug("  Template: " + projectDir.relativize(templatePath));
      }
      catch (@SuppressWarnings("unused") IOException ex)
      {
        // assume file does not exist
      }
    }

    if (output == null)
    {
      if (iface)
        output = new Output<>(Roaster.create(JavaInterfaceSource.class));
      else
        output = new Output<>(Roaster.create(JavaClassSource.class));
    }

    output.asAny().setName(fluentSimpleName);
    output.asAny().setPackage(toTargetPackage(mapping, sourceElement));
    output.asAny().setPublic();

    output.asAny().addAnnotation(Generated.class).setStringValue("value", getClass().getName());
    if (aspect)
    {
      output.asAny().addAnnotation(SuppressWarnings.class).setStringArrayValue("value",
          new String[] {"unchecked"});
    }

    return output;
  }

  private String toTargetPackage(FluentMappingWrapper mapping, Element sourceElement)
  {
    String sourcePackageName =
        processingEnv.getElementUtils().getPackageOf(sourceElement).toString();

    String basePackage = basePackagePerSourcePackage.get(sourcePackageName);
    if (basePackage == null)
      basePackage = basePackagePerSourcePackage.get(DEFAULT_SCOPE_PACKAGE);

    String targetPackage = Stream.of(basePackage, trimToNull(mapping.targetPackage()))
        .filter(Objects::nonNull).collect(joining("."));

    if (targetPackage.isEmpty())
    {
      printError("No base package configured and no target package set in the mapping.", null,
          sourceElement);
    }

    return targetPackage;
  }

  private Stream<TypeMirror> getAllSupertypes(TypeMirror type)
  {
    return doGetAllSupertypes(type).skip(1).filter(distinctByKey());
  }

  private Stream<TypeMirror> doGetAllSupertypes(TypeMirror type)
  {
    return Stream.concat(
        Stream.concat(Stream.of(type),
            processingEnv.getTypeUtils().directSupertypes(type).stream()),
        processingEnv.getTypeUtils().directSupertypes(type).stream()
            .flatMap(this::doGetAllSupertypes));
  }

  private Predicate<TypeMirror> distinctByKey()
  {
    List<TypeMirror> allSeen = new ArrayList<>();
    return t ->
    {
      boolean seen = allSeen.stream().anyMatch(s ->
      {
        return processingEnv.getTypeUtils().isSameType(s, t);
      });
      if (!seen)
        allSeen.add(t);
      return !seen;
    };
  }

  private TypeMirrorWrapper detectThemeVariantType(TypeElementWrapper componentTypeElement)
  {
    return componentTypeElement.getInterfaces().stream()
        .filter(i -> i.getQualifiedName().equals(HasThemeVariant.class.getCanonicalName()))
        .map(i -> TypeMirrorWrapper.wrap(i.getTypeArguments().get(0))).findFirst().orElse(null);
  }

  private static <T> Map<String, T> sortTypeVars(Map<String, T> typeVars)
  {
    Map<String, T> sorted = new LinkedHashMap<>();

    DEFAULT_TYPEVAR_ORDER.forEach(name ->
    {
      if (typeVars.containsKey(name))
      {
        sorted.put(name, typeVars.get(name));
      }
    });

    typeVars.forEach(sorted::putIfAbsent);

    return sorted;
  }

  private static List<String> sortTypeVars(List<String> typeVars)
  {
    List<String> sorted = new ArrayList<>(DEFAULT_TYPEVAR_ORDER);
    sorted.retainAll(typeVars);

    typeVars.forEach(typeVar ->
    {
      if (!sorted.contains(typeVar))
        sorted.add(typeVar);
    });

    return sorted;
  }

  private static String substituteTypeVars(String type, Map<String, String> typeVarSubstitutions)
  {
    for (Entry<String, String> entry : typeVarSubstitutions.entrySet())
    {
      type = type.replaceAll("(\\W+?|^)" + entry.getKey() + "(\\W+?|$)",
          "$1" + entry.getValue() + "$2");
    }
    return type;
  }

  private MethodSource<?> addMethod(Output<?> output, boolean createAspect,
      TypeElementWrapper componentTypeElement, String fluentSimpleName)
  {
    List<String> fluentTypeVars = fluentTypeVarsCache.get(componentTypeElement.getQualifiedName());

    MethodSource<?> fluentMethod = output.asAny().addMethod();

    if (createAspect)
    {
      fluentMethod.setReturnType(TYPEVAR_FLUENT);
      if (output.isInterface())
        fluentMethod.setDefault(true);
      else
        fluentMethod.setPublic();
    }
    else
    {
      fluentMethod.setPublic().setReturnType(fluentSimpleName + (fluentTypeVars.isEmpty() ? ""
          : fluentTypeVars.stream().collect(joining(", ", "<", ">"))));
    }

    fluentMethod.getJavaDoc().addTagValue("@return", "this");

    return fluentMethod;
  }

  private static void addJavaDocSeeToComponentMethod(MethodSource<?> fluentMethod,
      TypeElementWrapper component, ExecutableElementWrapper method)
  {
    fluentMethod.getJavaDoc().addTagValue("@see",
        component.getSimpleName() + "#" + method.getSimpleName() + "(" + method.getParameters()
            .stream().map(p -> p.asType().erasure().unwrap().toString()).collect(joining(", "))
            + ")");
  }

  private void createSourceFile(JavaSource<?> javaSource, Element sourceElement)
  {
    try
    {
      boolean writeSuccess = false;
      JavaFileObject file =
          processingEnv.getFiler().createSourceFile(javaSource.getCanonicalName(), sourceElement);
      try (Writer writer = file.openWriter())
      {
        writer.write(javaSource.toString());
        writer.flush();
        writeSuccess = true;
      }
      finally
      {
        if (!writeSuccess)
        {
          file.delete();
        }
      }
    }
    catch (IOException ex)
    {
      printError(
          String.format("Failed to write source file for '%s'.", javaSource.getCanonicalName()), ex,
          sourceElement);
    }
  }

  private void printDebug(String msg)
  {
    printDebug(null, msg);
  }

  private void printDebug(@SuppressWarnings("unused") Element sourceElement, String msg)
  {
    /*
     * sourceElement is ignored currently, because it seems like more noise than actually being
     * useful.
     */
    if (debug)
      processingEnv.getMessager().printMessage(Kind.NOTE, msg, /* sourceElement */ null);
  }

  private void printNote(String msg, Element sourceElement, AnnotationMirror annotation)
  {
    processingEnv.getMessager().printMessage(Kind.NOTE, msg, sourceElement, annotation);
  }

  private void printWarning(String msg, Element sourceElement, AnnotationMirror annotation)
  {
    processingEnv.getMessager().printMessage(Kind.WARNING, msg, sourceElement, annotation);
  }

  private void printError(String msg, Throwable throwable)
  {
    printError(msg, throwable, null);
  }

  private void printError(String msg, Throwable throwable, Element sourceElement)
  {
    printError(msg, throwable, sourceElement, null);
  }

  private void printError(String msg, Throwable throwable, Element sourceElement,
      AnnotationMirror annotation)
  {
    if (throwable == null)
    {
      processingEnv.getMessager().printMessage(Kind.ERROR, msg, sourceElement, annotation);
    }
    else
    {
      String extMsg = String.format("%s See log for full stacktrace.\n -> [%s] %s", msg,
          throwable.getClass().getSimpleName(), throwable.getMessage());
      processingEnv.getMessager().printMessage(Kind.ERROR, extMsg, sourceElement, annotation);
      LOG.log(Level.SEVERE, msg, throwable);
    }
  }


  private static class Output<T extends JavaSource<?> & GenericCapableSource<?, ?> & InterfaceCapableSource<?> & PropertyHolderSource<?> & TypeHolderSource<?>>
  {

    private final T output;


    Output(T output)
    {
      this.output = output;
    }


    T asAny()
    {
      return output;
    }

    boolean isClass()
    {
      return output instanceof JavaClassSource;
    }

    JavaClassSource asClass()
    {
      return (JavaClassSource) output;
    }

    boolean isInterface()
    {
      return output instanceof JavaInterfaceSource;
    }

    @SuppressWarnings("unused")
    JavaInterfaceSource asInterface()
    {
      return (JavaInterfaceSource) output;
    }


    String getCanonicalName()
    {
      return output.getCanonicalName();
    }


    boolean hasConstructor(String... paramTypes)
    {
      return asAny()
          .getMethods().stream().filter(MethodSource::isConstructor).filter(m -> m.getParameters()
              .stream().map(p -> p.getType().getName()).toList().equals(Arrays.asList(paramTypes)))
          .findAny().isPresent();
    }

    boolean hasMethod(String name, String... paramTypes)
    {
      return hasMethod(name, Arrays.asList(paramTypes));
    }

    boolean hasMethod(String name, List<String> paramTypes)
    {
      return asAny().getMethods().stream().filter(m -> !m.isConstructor())
          .filter(m -> m.getName().equals(name)).filter(m -> m.getParameters().stream()
              .map(p -> p.getType().getName()).toList().equals(paramTypes))
          .findAny().isPresent();
    }


    @Override
    public String toString()
    {
      return output.toString();
    }

  }

}
