package de.codecamp.vaadin.fluent.processor;


import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Represents information about all mappings declared on a specific type which is stored alongside
 * the generated classes.
 */
public class FluentMappingSetData
{

  private final Set<String> importedSets = new LinkedHashSet<>();

  private final Map<String, String> componentToFluent = new LinkedHashMap<>();

  private final Map<String, List<String>> componentTypeVars = new LinkedHashMap<>();

  private final Map<String, List<String>> fluentTypeVars = new LinkedHashMap<>();

  private final Map<String, Map<String, String>> typeVarSubstitutions = new LinkedHashMap<>();


  public FluentMappingSetData()
  {
  }


  public Set<String> getImportedSets()
  {
    return importedSets;
  }

  public Map<String, String> getComponentToFluent()
  {
    return componentToFluent;
  }

  public Map<String, List<String>> getComponentTypeVars()
  {
    return componentTypeVars;
  }

  public Map<String, List<String>> getFluentTypeVars()
  {
    return fluentTypeVars;
  }

  public Map<String, Map<String, String>> getTypeVarSubstitutions()
  {
    return typeVarSubstitutions;
  }

}
