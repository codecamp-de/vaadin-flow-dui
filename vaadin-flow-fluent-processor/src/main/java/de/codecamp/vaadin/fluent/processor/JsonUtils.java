package de.codecamp.vaadin.fluent.processor;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParseException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;


/**
 * JSON-related utility methods.
 */
public class JsonUtils
{

  private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();


  private JsonUtils()
  {
    // utility class
  }


  /**
   * Writes the given object as JSON to the given file path.
   *
   * @param source
   *          the object to write
   * @param targetFilePath
   *          the target file path
   * @throws JsonIOException
   *           if an error occurs
   */
  public static void writeToFile(Object source, Path targetFilePath)
  {
    try (Writer out = Files.newBufferedWriter(targetFilePath, StandardCharsets.UTF_8))
    {
      GSON.toJson(source, out);
    }
    catch (IOException ex)
    {
      throw new JsonIOException(ex);
    }
  }

  /**
   * Reads an object as JSON from the given file path.
   *
   * @param <T>
   *          the type of the object
   * @param sourceFilePath
   *          the source file path
   * @param targetType
   *          the type of the object
   * @return the object
   * @throws JsonParseException
   *           if an error occurs
   */
  public static <T> T readFromFile(Path sourceFilePath, Class<T> targetType)
  {
    try (Reader in = Files.newBufferedReader(sourceFilePath, StandardCharsets.UTF_8))
    {
      return GSON.fromJson(in, targetType);
    }
    catch (IOException ex)
    {
      throw new JsonIOException(ex);
    }
  }

  /**
   * Writes the given object as JSON to the given output stream.
   *
   * @param source
   *          the object to write
   * @param output
   *          the output stream
   * @throws JsonIOException
   *           if an error occurs
   */
  public static void writeTo(Object source, OutputStream output)
  {
    try (Writer out = new OutputStreamWriter(output, StandardCharsets.UTF_8))
    {
      GSON.toJson(source, out);
    }
    catch (IOException ex)
    {
      throw new JsonIOException(ex);
    }
  }

  /**
   * Reads an object as JSON from the given input stream.
   *
   * @param <T>
   *          the type of the object
   * @param input
   *          the input stream
   * @param targetType
   *          the type of the object
   * @return the object
   * @throws JsonParseException
   *           if an error occurs
   */
  public static <T> T readFrom(InputStream input, Class<T> targetType)
  {
    try (Reader in = new InputStreamReader(input, StandardCharsets.UTF_8))
    {
      return GSON.fromJson(in, targetType);
    }
    catch (IOException ex)
    {
      throw new JsonIOException(ex);
    }
  }

}
