package de.codecamp.vaadin.fluent.processor;


import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static javax.lang.model.util.ElementFilter.fieldsIn;
import static javax.lang.model.util.ElementFilter.methodsIn;

import java.beans.Introspector;
import java.lang.annotation.Annotation;
import java.lang.annotation.Repeatable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;


public final class LangModelUtils
{

  private static final Set<String> BEAN_PROPERTY_GETTER_PREFIXES = Set.of("get", "is");

  private static final Set<String> IGNORED_BEAN_PROPERTIES = Set.of("class");


  private LangModelUtils()
  {
    // utility class
  }


  public static Optional<TypeElement> getSuperclassType(TypeElement type)
  {
    TypeMirror superclassTypeMirror = type.getSuperclass();
    if (superclassTypeMirror.getKind() == TypeKind.NONE)
      return Optional.empty();
    return Optional.ofNullable((TypeElement) ((DeclaredType) superclassTypeMirror).asElement());
  }

  public static List<TypeElement> getInterfaceTypes(TypeElement type)
  {
    List<TypeElement> result = new ArrayList<>();

    for (TypeMirror iface : type.getInterfaces())
    {
      result.add((TypeElement) ((DeclaredType) iface).asElement());
    }
    return result;
  }


  public static List<TypeElement> getTypeNesting(TypeElement typeElement)
  {
    List<TypeElement> result = new ArrayList<>();

    while (typeElement != null)
    {
      result.add(0, typeElement);

      typeElement = getEnclosingTypeElement(typeElement);
    }
    return result;
  }

  public static TypeElement getEnclosingTypeElement(Element element)
  {
    do
    {
      element = element.getEnclosingElement();
    }
    while (element != null && !element.getKind().isClass() && !element.getKind().isInterface());

    return (TypeElement) element;
  }


  public static PackageElement getPackage(Element element)
  {
    while (element != null && element.getKind() != ElementKind.PACKAGE)
    {
      element = element.getEnclosingElement();
    }
    return (PackageElement) element;
  }

  public static String getPackageName(Element element)
  {
    return getPackage(element).getQualifiedName().toString();
  }

  public static List<VariableElement> getDeclaredFields(TypeElement element)
  {
    return fieldsIn(element.getEnclosedElements());
  }

  public static List<ExecutableElement> getDeclaredMethods(TypeElement element)
  {
    return methodsIn(element.getEnclosedElements());
  }


  public static List<VariableElement> getEnumConstants(TypeElement enumTypeElement)
  {
    if (enumTypeElement.getKind() != ElementKind.ENUM)
      throw new IllegalArgumentException("not an enum: " + enumTypeElement.getQualifiedName());

    return fieldsIn(enumTypeElement.getEnclosedElements()).stream()
        .filter(ve -> ve.getKind() == ElementKind.ENUM_CONSTANT).collect(toList());
  }

  public static Map<String, ExecutableElement> findDeclaredBeanProperties(
      TypeElement beanTypeElement)
  {
    Map<String, ExecutableElement> properties = new HashMap<>();

    for (ExecutableElement methodElement : getDeclaredMethods(beanTypeElement))
    {
      if (!methodElement.getModifiers().contains(Modifier.PUBLIC))
        continue;
      if (methodElement.getModifiers().contains(Modifier.STATIC))
        continue;
      if (methodElement.getReturnType().getKind() == TypeKind.VOID)
        continue;
      if (!methodElement.getParameters().isEmpty())
        continue;

      String methodName = methodElement.getSimpleName().toString();

      String propertyName = null;
      for (String prefix : BEAN_PROPERTY_GETTER_PREFIXES)
      {
        if (methodName.startsWith(prefix))
        {
          propertyName = methodName.substring(prefix.length());
          propertyName = Introspector.decapitalize(propertyName);
          break;
        }
      }
      if (propertyName == null)
        continue;
      if (IGNORED_BEAN_PROPERTIES.contains(propertyName))
        continue;

      properties.put(propertyName, methodElement);
    }

    return properties;
  }


  public static <A extends Annotation> Optional<A> findFirstMetaAnnotation(Element element,
      Class<A> metaAnnotationType)
  {
    List<A> metaAnnotations = findMetaAnnotations(element, metaAnnotationType);
    if (!metaAnnotations.isEmpty())
      return Optional.of(metaAnnotations.get(0));
    else
      return Optional.empty();
  }

  public static <A extends Annotation> List<A> findMetaAnnotations(Element element,
      Class<A> metaAnnotationType)
  {
    List<A> result = new ArrayList<>();

    A annotation = element.getAnnotation(metaAnnotationType);
    if (annotation != null)
      result.add(annotation);

    for (AnnotationMirror am : element.getAnnotationMirrors())
    {
      annotation = am.getAnnotationType().asElement().getAnnotation(metaAnnotationType);

      if (annotation != null)
        result.add(annotation);
    }

    return result;
  }


  public static AnnotationMirror getAnnotationMirror(Element element,
      Class<? extends Annotation> annotationType)
  {
    for (AnnotationMirror am : element.getAnnotationMirrors())
    {
      if (am.getAnnotationType().toString().equals(annotationType.getName()))
      {
        return am;
      }
    }
    return null;
  }

  public static List<AnnotationMirror> getAnnotationMirrors(Element element,
      Class<? extends Annotation> annotationType)
  {
    Class<? extends Annotation> containerAnnotationType = null;
    Repeatable repeatableAt = annotationType.getAnnotation(Repeatable.class);
    if (repeatableAt != null)
    {
      containerAnnotationType = repeatableAt.value();
    }

    List<AnnotationMirror> result = new ArrayList<>();
    for (AnnotationMirror am : element.getAnnotationMirrors())
    {
      if (containerAnnotationType != null
          && am.getAnnotationType().toString().equals(containerAnnotationType.getCanonicalName()))
      {
        result.addAll(getAnnotationValueAsAnnotationMirrors(am, "value"));
      }
      else if (am.getAnnotationType().toString().equals(annotationType.getName()))
      {
        result.add(am);
      }
    }
    return result;
  }


  public static AnnotationValue getAnnotationValue(AnnotationMirror annotationMirror, String key)
  {
    for (Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : annotationMirror
        .getElementValues().entrySet())
    {
      if (entry.getKey().getSimpleName().toString().equals(key))
      {
        AnnotationValue value = entry.getValue();
        if (value == null)
          value = entry.getKey().getDefaultValue();
        return value;
      }
    }
    return null;
  }

  public static <T> T getAnnotationValueAs(AnnotationMirror annotationMirror, String key,
      Class<T> type, T defaultValue)
  {
    AnnotationValue annotationValue = getAnnotationValue(annotationMirror, key);

    Object value;
    if (annotationValue != null)
      value = annotationValue.getValue();
    else
      value = defaultValue;

    return type.cast(value);
  }

  public static <T> List<T> getAnnotationValuesAs(AnnotationMirror annotationMirror, String key,
      Class<T> type, List<T> defaultValue)
  {
    AnnotationValue annotationValue = getAnnotationValue(annotationMirror, key);

    List<T> values = new ArrayList<>();
    if (annotationValue != null)
    {
      @SuppressWarnings("unchecked")
      List<AnnotationValue> annotationValueList =
          (List<AnnotationValue>) annotationValue.getValue();
      for (AnnotationValue annotationValueListElement : annotationValueList)
      {
        values.add(type.cast(annotationValueListElement.getValue()));
      }
    }
    else
    {
      values = defaultValue;
    }

    return values;
  }

  public static <T> List<T> getAnnotationValuesAs(AnnotationMirror annotationMirror, String key,
      Class<T> listElementType)
  {
    List<AnnotationValue> annotationValues = getAnnotationValues(annotationMirror, key);

    if (annotationValues == null)
      return emptyList();

    return annotationValues.stream().map(av -> listElementType.cast(av.getValue()))
        .collect(toList());
  }

  @SuppressWarnings("unchecked")
  public static List<AnnotationValue> getAnnotationValues(AnnotationMirror annotationMirror,
      String key)
  {
    for (Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : annotationMirror
        .getElementValues().entrySet())
    {
      if (entry.getKey().getSimpleName().toString().equals(key))
      {
        AnnotationValue annotationValue = getAnnotationValue(annotationMirror, key);
        return (List<AnnotationValue>) annotationValue.getValue();
      }
    }
    // empty arrays in annotations are not contained among element values
    return emptyList();
  }


  public static TypeElement getAnnotationValueAsType(AnnotationMirror annotationMirror, String key)
  {
    AnnotationValue annotationValue = getAnnotationValue(annotationMirror, key);
    if (annotationValue == null)
      return null;

    DeclaredType typeMirror = (DeclaredType) annotationValue.getValue();
    if (typeMirror == null)
      return null;

    return (TypeElement) typeMirror.asElement();
  }

  public static List<TypeElement> getAnnotationValueAsTypes(AnnotationMirror annotationMirror,
      String key)
  {
    List<AnnotationValue> values = getAnnotationValues(annotationMirror, key);
    if (values == null)
      return emptyList();

    return values.stream().map(av -> (TypeElement) ((DeclaredType) av.getValue()).asElement())
        .collect(toList());
  }

  public static List<AnnotationMirror> getAnnotationValueAsAnnotationMirrors(
      AnnotationMirror annotationMirror, String key)
  {
    List<AnnotationValue> values = getAnnotationValues(annotationMirror, key);
    if (values == null)
      return emptyList();

    return values.stream().map(av -> (AnnotationMirror) av.getValue()).collect(toList());
  }


  public static <A extends TypeElement> Set<A> findComposedAnnotationTypes(
      Set<A> processedAnnotations, Class<? extends Annotation> metaAnnotationType,
      boolean includeMetaAnnotation)
  {
    Set<A> annotations = new HashSet<>();
    for (A annotationElement : processedAnnotations)
    {
      if ((includeMetaAnnotation
          && annotationElement.getQualifiedName().toString().equals(metaAnnotationType.getName()))
          || annotationElement.getAnnotation(metaAnnotationType) != null)
      {
        annotations.add(annotationElement);
      }
    }
    return annotations;
  }

}
