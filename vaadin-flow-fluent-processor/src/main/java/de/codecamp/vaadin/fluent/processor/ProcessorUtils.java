package de.codecamp.vaadin.fluent.processor;


import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.lang.annotation.Annotation;
import java.lang.annotation.Repeatable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.FileObject;
import javax.tools.JavaFileManager.Location;
import javax.tools.StandardLocation;
import org.apache.commons.lang3.StringUtils;


public class ProcessorUtils
{

  private final ProcessingEnvironment processingEnv;


  public ProcessorUtils(ProcessingEnvironment processingEnv)
  {
    this.processingEnv = processingEnv;
  }


  public String getOption(String name)
  {
    return processingEnv.getOptions().get(name);
  }

  public String getOption(String name, String defaultValue)
  {
    String value = getOption(name);
    if (StringUtils.isEmpty(value))
      value = defaultValue;
    return value;
  }

  public Boolean getOptionAsBoolean(String name)
  {
    return Boolean.valueOf(getOption(name));
  }

  public Boolean getOptionAsBoolean(String name, Boolean defaultValue)
  {
    Boolean value = getOptionAsBoolean(name);
    if (value == null)
      value = defaultValue;
    return value;
  }

  public List<String> getOptionAsList(String name)
  {
    String rawValue = processingEnv.getOptions().get(name);
    if (rawValue == null)
      return emptyList();
    return Stream.of(StringUtils.splitPreserveAllTokens(rawValue, ",")).map(String::trim)
        .collect(toList());
  }

  public List<String> getOptionAsList(String name, List<String> defaultValues)
  {
    List<String> value = getOptionAsList(name);
    if (value == null || value.isEmpty())
      value = defaultValues;
    return value;
  }

  public <T extends Enum<T>> T getOptionAsEnum(String name, Class<T> enumType)
  {
    String value = getOption(name);
    if (StringUtils.isEmpty(value))
      return null;
    else
      return Enum.valueOf(enumType, value);
  }

  public <T extends Enum<T>> T getOptionAsEnum(String name, Class<T> enumType, T defaultValue)
  {
    T e = getOptionAsEnum(name, enumType);
    if (e == null)
      e = defaultValue;
    return e;
  }


  public Optional<TypeElement> getGeneratedAnnotation()
  {
    return Optional.ofNullable(
        processingEnv.getElementUtils().getTypeElement("javax.annotation.processing.Generated"));
  }


  /**
   * Finds all elements annotated with the given annotation type or with its container type if it is
   * {@link Repeatable}.
   *
   * @param roundEnv
   *          the current round of processing
   * @param annotationType
   *          the annotation to look for
   * @return all found elements
   */
  public static Set<? extends Element> getElementsAnnotatedWith(RoundEnvironment roundEnv,
      Class<? extends Annotation> annotationType)
  {
    Set<Element> result = new HashSet<>();

    result.addAll(roundEnv.getElementsAnnotatedWith(annotationType));

    Repeatable repeatableAt = annotationType.getAnnotation(Repeatable.class);
    if (repeatableAt != null)
    {
      result.addAll(roundEnv.getElementsAnnotatedWith(repeatableAt.value()));
    }

    return result;
  }


  public Path getSourceOutputPath()
  {
    return getOutputPath(StandardLocation.SOURCE_OUTPUT);
  }

  public Path getClassOutputPath()
  {
    return getOutputPath(StandardLocation.CLASS_OUTPUT);
  }

  private Path getOutputPath(Location location)
  {
    FileObject resource;
    try
    {
      resource =
          processingEnv.getFiler().createResource(location, "", UUID.randomUUID().toString());
      resource.delete();
      return Paths.get(resource.toUri()).getParent();
    }
    catch (IOException ex)
    {
      throw new UncheckedIOException(ex);
    }
  }

}
