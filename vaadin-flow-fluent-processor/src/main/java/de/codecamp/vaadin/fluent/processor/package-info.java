@AnnotationWrapper({GlobalFluentMappingSettings.class, FluentMappingSet.class, FluentMapping.class,
    FluentMethod.class, ThemeGroup.class})
package de.codecamp.vaadin.fluent.processor;


import de.codecamp.vaadin.fluent.annotations.FluentMapping;
import de.codecamp.vaadin.fluent.annotations.FluentMappingSet;
import de.codecamp.vaadin.fluent.annotations.FluentMethod;
import de.codecamp.vaadin.fluent.annotations.GlobalFluentMappingSettings;
import de.codecamp.vaadin.fluent.annotations.ThemeGroup;
import io.toolisticon.aptk.annotationwrapper.api.AnnotationWrapper;
