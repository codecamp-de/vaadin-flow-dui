package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu;
import com.vaadin.flow.component.grid.contextmenu.GridMenuItem;
import com.vaadin.flow.component.grid.contextmenu.GridSubMenu;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.shared.Tooltip;
import com.vaadin.flow.theme.lumo.LumoIcon;
import de.codecamp.vaadin.fluent.shared.FluentTooltip;
import de.codecamp.vaadin.fluent.visandint.FluentContextMenu;
import de.codecamp.vaadin.fluent.visandint.FluentGrid;
import de.codecamp.vaadin.fluent.visandint.FluentGridContextMenu;
import de.codecamp.vaadin.fluent.visandint.FluentGridMenuItem;
import de.codecamp.vaadin.fluent.visandint.FluentGridSubMenu;
import de.codecamp.vaadin.fluent.visandint.FluentIcon;
import de.codecamp.vaadin.fluent.visandint.FluentMenuItem;
import de.codecamp.vaadin.fluent.visandint.FluentSubMenu;


class Fluent
{

  private Fluent()
  {
    // utility class
  }


  /**
   * Returns a fluent wrapper configuring the given {@link Component}.
   * 
   * @param <C>
   *          the component type
   * @param <F>
   *          the fluent type
   * @param component
   *          the component to be configured
   * @return a new fluent wrapper for the given component
   * @see Component
   */
  public static <C extends Component, F extends FluentComponent<C, F>> FluentComponent<C, F> fluent(
      C component)
  {
    return new FluentComponent<>(component);
  }

  /*
   * A simple wrapper function to allow a varargs parameter of components that doesn't have to be at
   * the end of the parameter list.
   */
  public static Component[] c(Component... components)
  {
    return components;
  }


  /**
   * Returns a fluent wrapper configuring the given {@link ContextMenu}.
   * 
   * @param component
   *          the component to be configured
   * @return a new fluent wrapper for the given component
   * @see ContextMenu
   */
  public static FluentContextMenu fluent(ContextMenu component)
  {
    return new FluentContextMenu(component);
  }

  /**
   * Returns a fluent wrapper configuring the given {@link SubMenu}.
   * 
   * @param component
   *          the component to be configured
   * @return a new fluent wrapper for the given component
   * @see SubMenu
   */
  public static FluentSubMenu fluent(SubMenu component)
  {
    return new FluentSubMenu(component);
  }

  /**
   * Returns a fluent wrapper configuring the given {@link MenuItem}.
   * 
   * @param component
   *          the component to be configured
   * @return a new fluent wrapper for the given component
   * @see MenuItem
   */
  public static FluentMenuItem fluent(MenuItem component)
  {
    return new FluentMenuItem(component);
  }

  /**
   * Returns a fluent wrapper configuring the given {@link GridContextMenu}.
   * 
   * @param component
   *          the component to be configured
   * @return a new fluent wrapper for the given component
   * @see GridContextMenu
   */
  public static <ITEM> FluentGridContextMenu<ITEM> fluent(GridContextMenu<ITEM> component)
  {
    return new FluentGridContextMenu<>(component);
  }

  /**
   * Returns a fluent wrapper configuring the given {@link GridSubMenu}.
   * 
   * @param component
   *          the component to be configured
   * @return a new fluent wrapper for the given component
   * @see GridSubMenu
   */
  public static <ITEM> FluentGridSubMenu<ITEM> fluent(GridSubMenu<ITEM> component)
  {
    return new FluentGridSubMenu<>(component);
  }

  /**
   * Returns a fluent wrapper configuring the given {@link GridMenuItem}.
   * 
   * @param component
   *          the component to be configured
   * @return a new fluent wrapper for the given component
   * @see GridMenuItem
   */
  public static <ITEM> FluentGridMenuItem<ITEM> fluent(GridMenuItem<ITEM> component)
  {
    return new FluentGridMenuItem<>(component);
  }

}
