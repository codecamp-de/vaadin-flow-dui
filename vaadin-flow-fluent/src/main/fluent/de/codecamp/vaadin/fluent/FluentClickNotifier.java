package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.ClickNotifier;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyModifier;


interface FluentClickNotifier
{

  /**
   * @return this
   * @see ClickNotifier#addClickListener(com.vaadin.flow.component.ComponentEventListener)
   */
  default F onClick(Runnable listener)
  {
    get().addClickListener(e -> listener.run());
    return (F) this;
  }

}
