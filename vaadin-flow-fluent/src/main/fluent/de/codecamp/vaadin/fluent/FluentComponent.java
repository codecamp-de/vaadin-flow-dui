package de.codecamp.vaadin.fluent;


import static java.util.Objects.requireNonNull;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.ScrollOptions;
import de.codecamp.vaadin.fluent.FluentHasComponentsComponentExtension;
import de.codecamp.vaadin.fluent.FluentHasOrderedComponentsComponentExtension;
import de.codecamp.vaadin.fluent.FluentLayoutConfig;
import de.codecamp.vaadin.fluent.layouts.FluentFlexLayoutComponentExtension;
import de.codecamp.vaadin.fluent.layouts.FluentFormLayoutComponentExtension;
import de.codecamp.vaadin.fluent.layouts.FluentHorizontalLayoutComponentExtension;
import de.codecamp.vaadin.fluent.layouts.FluentScrollerComponentExtension;
import de.codecamp.vaadin.fluent.layouts.FluentSplitLayoutComponentExtension;
import de.codecamp.vaadin.fluent.layouts.FluentVerticalLayoutComponentExtension;
import de.codecamp.vaadin.fluent.visandint.FluentTabsComponentExtension;
import java.util.function.Consumer;


class FluentComponent
  extends
    FluentWrapper<C, F>
  implements
    FluentFlexLayoutComponentExtension<C, F>,
    FluentFormLayoutComponentExtension<C, F>,
    FluentHasComponentsComponentExtension<C, F>,
    FluentHasOrderedComponentsComponentExtension<C, F>,
    FluentHasSingleComponentComponentExtension<C, F>,
    FluentHorizontalLayoutComponentExtension<C, F>,
    FluentScrollerComponentExtension<C, F>,
    FluentSplitLayoutComponentExtension<C, F>,
    FluentTabsComponentExtension<C, F>,
    FluentVerticalLayoutComponentExtension<C, F>
{

  /**
   * Constructs a new instance configuring the given {@link Component}.
   *
   * @param component
   *          the component to be configured
   */
  protected FluentComponent(C component)
  {
    super(requireNonNull(component, "component must not be null"));
  }


  /**
   * @return this
   * @see Component#setVisible(boolean)
   */
  public F hidden()
  {
    return visible(false);
  }


  /**
   * @return this
   * @see Component#scrollIntoView(ScrollOptions)
   */
  public F scrollIntoView(Consumer<FluentScrollOptions> optionsConfigurer)
  {
    if (optionsConfigurer != null)
    {
      ScrollOptions options = new ScrollOptions();
      optionsConfigurer.accept(new FluentScrollOptions(options));
      return scrollIntoView(options);
    }
    else
    {
      return scrollIntoView();
    }
  }


  /**
   * Stores an arbitrary value for the specified key.
   *
   * @param key
   *          the key to associate the value with
   * @param value
   *          the value to set; {@code null} to remove any previous value associated with the key
   * @return this
   * @see ComponentUtil#setData(Component, String, Object)
   * @see ComponentUtil#getData(Component, String)
   */
  public F data(String key, Object value)
  {
    ComponentUtil.setData(get(), key, value);
    return (F) this;
  }

  /**
   * Stores an instance of the specified type.
   *
   * @param <T>
   *          the type of the value
   * @param type
   *          the type of the value used as key to associate the value with
   * @param value
   *          the value to set; {@code null} to remove any previous value associated with the key
   * @return this
   * @see ComponentUtil#setData(Component, Class, Object)
   * @see ComponentUtil#getData(Component, Class)
   */
  public <T> F data(Class<T> type, T value)
  {
    ComponentUtil.setData(get(), type, value);
    return (F) this;
  }


  /**
   * Adds this component to the given container.
   * <p>
   * This method is technically the same as {@link #apply(Consumer)} (so it actually requires a
   * function instead of just the container object), but it conveys the intention in a clearer way.
   * <p>
   * Useful for certain components that don't implement a reusable container interface supported by
   * any of the available {@code add} and {@code replace} methods. Example:
   * {@code button().addTo(container::setContent)}.
   *
   * @param container
   *          consumer that adds/sets the component to the container
   * @return this
   */
  public F addTo(Consumer<C> container)
  {
    return apply(container);
  }


  /**
   * Adds this component to the given container implementing {@link FluentLayoutSupport}, applying
   * the provided layout configurer.
   *
   * @param container
   *          the target container
   * @param layoutConfigurer
   *          the configurer to apply to the component's layout; may be null
   * @param <CONT>
   *          the container type
   * @param <FLC>
   *          the fluent layout config type
   * @return this
   */
  public <CONT extends Component, FLC extends FluentLayoutConfig<CONT, FLC>> F addTo(
      FluentLayoutSupport<FLC> container, Consumer<FLC> layoutConfigurer)
  {
    FLC layoutConfig = container.fluentAdd(get());
    if (layoutConfigurer != null)
      layoutConfigurer.accept(layoutConfig);
    return (F) this;
  }

  /**
   * Adds this component to the given container implementing {@link FluentLayoutSupportIndexed},
   * applying the provided layout configurer.
   *
   * @param container
   *          the target container
   * @param layoutConfigurer
   *          the configurer to apply to the component's layout; may be null
   * @param <CONT>
   *          the container type
   * @param <FLC>
   *          the fluent layout config type
   * @return this
   */
  public <CONT extends Component, FLC extends FluentLayoutConfig<CONT, FLC>> F addTo(
      FluentLayoutSupportIndexed<FLC> container, int index, Consumer<FLC> layoutConfigurer)
  {
    FLC layoutConfig = container.fluentAdd(get(), index);
    if (layoutConfigurer != null)
      layoutConfigurer.accept(layoutConfig);
    return (F) this;
  }

  /**
   * Adds this component to the given container implementing {@link FluentLayoutSupportSlotted} in
   * the specified slot or position.
   *
   * @param <CONT>
   *          the container type
   * @param <FLC>
   *          the fluent layout config type
   * @param <SLOT>
   *          the slot ID type
   * @param container
   *          the target container
   * @param slot
   *          an object identifying the target slot or position in the container; whether this is
   *          mandatory depends on the the component container; may be null depending on the
   *          container
   * @return this
   */
  public <CONT extends Component, FLC extends FluentLayoutConfig<CONT, FLC>, SLOT> F addTo(
      FluentLayoutSupportSlotted<FLC, SLOT> container, SLOT slot)
  {
    return addTo(container, slot, null);
  }

  /**
   * Adds this component to the given container implementing {@link FluentLayoutSupportSlotted} in
   * the specified slot or position, applying the provided layout configurer.
   *
   * @param <CONT>
   *          the container type
   * @param <FLC>
   *          the fluent layout config type
   * @param <SLOT>
   *          the slot ID type
   * @param container
   *          the target container
   * @param slot
   *          an object identifying the target slot or position in the container; may be null
   *          depending on the container
   * @param layoutConfigurer
   *          the configurer to apply to the component's layout; may be null
   * @return this
   */
  public <CONT extends Component, FLC extends FluentLayoutConfig<CONT, FLC>, SLOT> F addTo(
      FluentLayoutSupportSlotted<FLC, SLOT> container, SLOT slot, Consumer<FLC> layoutConfigurer)
  {
    FLC layoutConfig = container.fluentAdd(get(), slot);
    if (layoutConfigurer != null)
      layoutConfigurer.accept(layoutConfig);
    return (F) this;
  }

  public <CONT extends Component, FLC extends FluentLayoutConfig<CONT, FLC>> F addTo(
      FluentLayoutSupportSingle<FLC> container, boolean replace)
  {
    return addTo(container, replace, null);
  }

  public <CONT extends Component, FLC extends FluentLayoutConfig<CONT, FLC>> F addTo(
      FluentLayoutSupportSingle<FLC> container, boolean replace, Consumer<FLC> layoutConfigurer)
  {
    FLC layoutConfig = container.fluentAdd(get(), replace);
    if (layoutConfigurer != null)
      layoutConfigurer.accept(layoutConfig);
    return (F) this;
  }

}
