package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import java.util.Collection;
import java.util.Iterator;


interface FluentHasComponents
{

  /**
   * @return this
   * @see HasComponents#add(Component...)
   */
  default F add(FluentComponent<?, ?> component)
  {
    return add(component.get());
  }

  /**
   * @return this
   * @see HasComponents#add(Component...)
   */
  default F add(FluentComponent<?, ?>... components)
  {
    for (FluentComponent<?, ?> component : components)
    {
      get().add(component.get());
    }
    return (F) this;
  }

  /**
   * @return this
   * @see HasComponents#addComponentAtIndex(int, Component)
   */
  default F addAt(int index, Component... components)
  {
    for (int i = 0; i < components.length; i++)
      get().addComponentAtIndex(index + i, components[i]);
    return (F) this;
  }

  /**
   * @return this
   * @see HasComponents#addComponentAtIndex(int, Component)
   */
  default F addAt(int index, Collection<Component> components)
  {
    int i = 0;
    for (Iterator<Component> it = components.iterator(); it.hasNext(); i++)
      get().addComponentAtIndex(index + i, it.next());
    return (F) this;
  }

}
