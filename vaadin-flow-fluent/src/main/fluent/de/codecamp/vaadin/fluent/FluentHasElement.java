package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.dom.Element;
import java.util.function.Consumer;


interface FluentHasElement
{

  /**
   * @return this
   * @see HasElement#getElement()
   */
  default F applyToElement(Consumer<Element> elementConfigurer)
  {
    elementConfigurer.accept(get().getElement());
    return (F) this;
  }

}
