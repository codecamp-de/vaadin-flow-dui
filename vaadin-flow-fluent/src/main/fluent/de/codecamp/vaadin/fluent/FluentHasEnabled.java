package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.HasEnabled;


interface FluentHasEnabled
{

  /**
   * @return this
   * @see HasEnabled#setEnabled(boolean)
   */
  default F disabled()
  {
    return enabled(false);
  }

}
