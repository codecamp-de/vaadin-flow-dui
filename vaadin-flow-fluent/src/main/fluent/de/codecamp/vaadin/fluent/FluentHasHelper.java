package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.textfield.TextFieldVariant;


interface FluentHasHelper
{

  /**
   * @return this
   */
  default F helperAboveField()
  {
    return helperAboveField(true);
  }

  /**
   * @return this
   */
  default F helperAboveField(boolean helperAboveField)
  {
    /*
     * The "helper-above-field" variant seems to be supported by all components implementing HasHelper. So using
     * the theme constant from any component should to the trick.
     */
    get().getElement().getThemeList().set(TextFieldVariant.LUMO_HELPER_ABOVE_FIELD.getVariantName(),
        helperAboveField);
    return (F) this;
  }

}
