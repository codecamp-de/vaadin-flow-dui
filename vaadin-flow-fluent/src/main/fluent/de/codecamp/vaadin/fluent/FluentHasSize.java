package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.Unit;


interface FluentHasSize
{

  /**
   * @return this
   * @see HasSize#setWidth(String)
   */
  default F widthAuto()
  {
    get().setWidth("auto");
    return (F) this;
  }

  /**
   * @return this
   * @see HasSize#setHeight(String)
   */
  default F heightAuto()
  {
    get().setHeight("auto");
    return (F) this;
  }

  /**
   * @return this
   * @see HasSize#setWidth(String)
   * @see HasSize#setHeight(String)
   */
  default F sizeAuto()
  {
    widthAuto();
    heightAuto();
    return (F) this;
  }


  /**
   * @return this
   * @see HasSize#setWidth(String)
   * @see HasSize#setHeight(String)
   */
  default F size(String width, String height)
  {
    get().setWidth(width);
    get().setHeight(height);
    return (F) this;
  }

  /**
   * @return this
   * @see HasSize#setWidth(String)
   * @see HasSize#setHeight(String)
   */
  default F size(String widthAndHeight)
  {
    return size(widthAndHeight, widthAndHeight);
  }

  /**
   * @return this
   * @see HasSize#setWidth(float, Unit)
   * @see HasSize#setHeight(float, Unit)
   */
  default F size(float width, float height, Unit unit)
  {
    get().setWidth(width, unit);
    get().setHeight(height, unit);
    return (F) this;
  }

  /**
   * @return this
   * @see HasSize#setWidth(float, Unit)
   * @see HasSize#setHeight(float, Unit)
   */
  default F size(float widthAndHeight, Unit unit)
  {
    return size(widthAndHeight, widthAndHeight, unit);
  }

}
