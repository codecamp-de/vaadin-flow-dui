package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.dom.ClassList;
import com.vaadin.flow.dom.Style;
import de.codecamp.vaadin.fluent.dom.FluentClassList;
import de.codecamp.vaadin.fluent.dom.FluentStyle;
import java.util.function.Consumer;


interface FluentHasStyle
{

  /**
   * @return this
   */
  default F classNames(String... classNames)
  {
    get().setClassName(null);
    get().addClassNames(classNames);
    return (F) this;
  }

  /**
   * @return this
   */
  default F addClassNames(String... classNames)
  {
    get().addClassNames(classNames);
    return (F) this;
  }

  /**
   * @return this
   */
  default F removeClassNames(String... classNames)
  {
    get().removeClassNames(classNames);
    return (F) this;
  }


  /**
   * Applies the given classes configurer.
   *
   * @param classesConfigurer
   *          configurer for the classes of the component
   * @return this
   */
  default F applyToClasses(Consumer<ClassList> classesConfigurer)
  {
    classesConfigurer.accept(get().getClassNames());
    return (F) this;
  }

  /**
   * Applies the given classes configurer.
   *
   * @param classesConfigurer
   *          configurer for the classes of the component
   * @return this
   */
  default F classes(Consumer<FluentClassList> classesConfigurer)
  {
    classesConfigurer.accept(new FluentClassList(get().getClassNames()));
    return (F) this;
  }

  /**
   * Applies the given style configurer.
   *
   * @param styleConfigurer
   *          configurer for the styles of the component
   * @return this
   */
  default F applyToStyle(Consumer<Style> styleConfigurer)
  {
    styleConfigurer.accept(get().getStyle());
    return (F) this;
  }

  /**
   * Applies the given style configurer.
   *
   * @param styleConfigurer
   *          configurer for the styles of the component
   * @return this
   */
  default F style(Consumer<FluentStyle> styleConfigurer)
  {
    styleConfigurer.accept(new FluentStyle(get().getStyle()));
    return (F) this;
  }

}
