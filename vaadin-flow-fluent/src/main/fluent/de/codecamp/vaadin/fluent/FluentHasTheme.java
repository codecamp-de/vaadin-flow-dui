package de.codecamp.vaadin.fluent;


import com.vaadin.flow.dom.ThemeList;
import de.codecamp.vaadin.fluent.dom.FluentThemeList;
import java.util.function.Consumer;


interface FluentHasTheme
{

  /**
   * @return this
   */
  default F themeNames(String... themeNames)
  {
    get().setThemeName("");
    get().addThemeNames(themeNames);
    return (F) this;
  }

  /**
   * @return this
   */
  default F addThemeNames(String... themeNames)
  {
    get().addThemeNames(themeNames);
    return (F) this;
  }

  /**
   * @return this
   */
  default F removeThemeNames(String... themeNames)
  {
    get().removeThemeNames(themeNames);
    return (F) this;
  }


  /**
   * Applies the given themes configurer.
   *
   * @return this
   */
  default F applyToThemes(Consumer<ThemeList> themesConfigurer)
  {
    themesConfigurer.accept(get().getThemeNames());
    return (F) this;
  }

  /**
   * Applies the given themes configurer.
   *
   * @return this
   */
  default F themes(Consumer<FluentThemeList> themesConfigurer)
  {
    themesConfigurer.accept(new FluentThemeList(get().getThemeNames()));
    return (F) this;
  }

}
