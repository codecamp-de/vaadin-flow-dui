package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.UI;


interface FluentHasValue
{

  /**
   * Sets the value of this field, but a little later after your UI code has finished. This can be
   * useful to initialize certain fields with default values in a single fluent statement, but have
   * the actual value only set after all components are created and hooked up.
   * <p>
   * This method may only be called from the UI thread.
   *
   * @param value
   *          the new value
   * @return this
   * @see UI#getCurrentView()
   * @see UI#access(com.vaadin.flow.server.Command)
   */
  default F valueDelayed(VALUE value)
  {
    UI ui = UI.getCurrent();
    if (ui == null)
    {
      throw new IllegalStateException(
          "No current UI available. Method may only be called from UI thread.");
    }

    ui.access(() ->
    {
      get().setValue(value);
    });

    return (F) this;
  }

}
