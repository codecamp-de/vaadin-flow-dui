package de.codecamp.vaadin.fluent;


import com.vaadin.flow.data.selection.MultiSelect;
import java.util.Set;


interface FluentMultiSelect
{

  /**
   * @return this
   * @see MultiSelect#select(Object...)
   */
  default F select(ITEM... items)
  {
    get().select(items);
    return (F) this;
  }

  /**
   * @return this
   * @see MultiSelect#deselect(Object...)
   */
  default F deselect(ITEM... items)
  {
    get().deselect(items);
    return (F) this;
  }

  /**
   * @return this
   * @see MultiSelect#select(Iterable)
   */
  default F select(Iterable<ITEM> items)
  {
    get().select(items);
    return (F) this;
  }

  /**
   * @return this
   * @see MultiSelect#deselect(Iterable)
   */
  default F deselect(Iterable<ITEM> items)
  {
    get().deselect(items);
    return (F) this;
  }

  /**
   * @return this
   * @see MultiSelect#deselectAll()
   */
  default F deselectAll()
  {
    get().deselectAll();
    return (F) this;
  }


  /**
   * @return this
   * @see MultiSelect#updateSelection(Set, Set)
   */
  default F updateSelection(Set<ITEM> addedItems, Set<ITEM> removedItems)
  {
    get().updateSelection(addedItems, removedItems);
    return (F) this;
  }

}
