package de.codecamp.vaadin.fluent.dataentry;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.combobox.ComboBoxBase;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu;
import com.vaadin.flow.data.provider.HasLazyDataView;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.function.SerializableSupplier;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.GridVariant;
import de.codecamp.vaadin.fluent.FluentFocusable;
import de.codecamp.vaadin.fluent.FluentHasLazyDataView;
import de.codecamp.vaadin.fluent.FluentSortNotifier;
import de.codecamp.vaadin.fluent.shared.FluentHasThemeVariantGeneric;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import javax.xml.crypto.Data;


class FluentComboBoxBase
{

  /**
   * Lazily initializes the items based on the given supplier when the combo box is focused for the
   * first time.
   * <p>
   * This is similar to what {@link HasLazyDataView} / {@link FluentHasLazyDataView} provides, but
   * does not require support for paging.
   * 
   * @return this
   * @see ComboBoxBase#setItems(java.util.Collection)
   * @see #items(java.util.Collection)
   */
  public F itemsLazy(SerializableSupplier<List<ITEM>> itemSupplier)
  {
    Objects.requireNonNull(itemSupplier, "itemSupplier must not be null");

    onFocus(event ->
    {
      C source = event.getSource();
      VALUE value = source.getValue();

      source.setItems(itemSupplier.get());

      if (value != null)
        source.setValue(value);

      event.unregisterListener();
    });

    /* Setting an empty list allows setValue(...) to be called. */
    items();

    return (F) this;
  }

}
