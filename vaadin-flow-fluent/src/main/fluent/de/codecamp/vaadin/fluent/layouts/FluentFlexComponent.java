package de.codecamp.vaadin.fluent.layouts;


import de.codecamp.vaadin.base.lumo.LumoSpace;


interface FluentFlexComponent
{

  default F spacing(LumoSpace spacing)
  {
    get().getStyle().set("gap", spacing == null ? null : spacing.var());
    return (F) this;
  }

}
