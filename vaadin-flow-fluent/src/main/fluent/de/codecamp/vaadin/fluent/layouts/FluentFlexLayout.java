package de.codecamp.vaadin.fluent.layouts;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import de.codecamp.vaadin.base.lumo.LumoSpace;
import de.codecamp.vaadin.fluent.FluentHasOrderedComponentsContainerExtension;


class FluentFlexLayout
  implements
    FluentHasOrderedComponentsContainerExtension<FlexLayout, FluentFlexLayout, FluentFlexLayoutConfig>
{

  @Override
  public FluentFlexLayoutConfig getLayoutConfigFor(Component... components)
  {
    return new FluentFlexLayoutConfig(get(), components);
  }

}
