package de.codecamp.vaadin.fluent.layouts;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.FormItem;
import com.vaadin.flow.component.html.NativeLabel;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import de.codecamp.vaadin.base.lumo.LumoSpace;
import de.codecamp.vaadin.fluent.FluentHasComponentsContainerExtension;
import io.netty.util.internal.shaded.org.jctools.queues.MessagePassingQueue.Consumer;
import java.util.Objects;


class FluentFormLayout
  implements
    FluentHasComponentsContainerExtension<FormLayout, FluentFormLayout, FluentFormLayoutConfig>
{

  /**
   * @param field
   *          the field
   * @param label
   *          the label
   * @return the new fluent form item
   * @see FormLayout#addFormItem(Component, String)
   */
  public FluentFormItem addFormItem(Component field, String label)
  {
    return new FluentFormItem(get().addFormItem(field, label));
  }
  
  /**
   * @param field
   *          the field
   * @param label
   *          the label
   * @param formItemConfigurer
   *          the configurer to apply to the form item
   * @return this
   * @see FormLayout#addFormItem(Component, String)
   */
  public FluentFormLayout addFormItem(Component field, String label,
      Consumer<FluentFormItem> formItemConfigurer)
  {
    FluentFormItem formItem = addFormItem(field, label);
    if (formItemConfigurer != null)
      formItemConfigurer.accept(formItem);
    return this;
  }

  /**
   * @param field
   *          the field
   * @param label
   *          the label
   * @return the new fluent form item
   * @see FormLayout#addFormItem(Component, Component)
   */
  public FluentFormItem addFormItem(Component field, Component label)
  {
    return new FluentFormItem(get().addFormItem(field, label));
  }
  
  /**
   * @param field
   *          the field
   * @param label
   *          the label
   * @param formItemConfigurer
   *          the configurer to apply to the form item
   * @return this
   * @see FormLayout#addFormItem(Component, Component)
   */
  public FluentFormLayout addFormItem(Component field, Component label,
      Consumer<FluentFormItem> formItemConfigurer)
  {
    FluentFormItem formItem = addFormItem(field, label);
    if (formItemConfigurer != null)
      formItemConfigurer.accept(formItem);
    return this;
  }

  /**
   * @param responsiveStepsConfigurer
   *          the configurer to build the responsive steps
   * @return this
   * @see FormLayout#setResponsiveSteps(java.util.List)
   */
  public FluentFormLayout responsiveSteps(Consumer<FluentResponsiveSteps> responsiveStepsConfigurer)
  {
    Objects.requireNonNull(responsiveStepsConfigurer, "responsiveStepsConfigurer must not be null");
    FluentResponsiveSteps fluentSteps = new FluentResponsiveSteps();
    responsiveStepsConfigurer.accept(fluentSteps);
    return responsiveSteps(fluentSteps.getSteps());
  }

  @Override
  public FluentFormLayoutConfig getLayoutConfigFor(Component... components)
  {
    return new FluentFormLayoutConfig(get(), components);
  }

}
