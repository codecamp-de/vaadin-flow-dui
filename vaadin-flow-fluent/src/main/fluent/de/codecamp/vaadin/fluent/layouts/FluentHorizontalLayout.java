package de.codecamp.vaadin.fluent.layouts;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import de.codecamp.vaadin.base.lumo.LumoSpace;
import de.codecamp.vaadin.fluent.FluentHasOrderedComponentsContainerExtension;


class FluentHorizontalLayout
  implements
    FluentHasOrderedComponentsContainerExtension<HorizontalLayout, FluentHorizontalLayout, FluentHorizontalLayoutConfig>
{

  public FluentHorizontalLayout()
  {
    this(new HorizontalLayout());

    /* remove defaults that only exist in the Java API to be consistent with Web Component */
    get().setSpacing(false);
  }


  @Override
  public FluentHorizontalLayoutConfig getLayoutConfigFor(Component... components)
  {
    return new FluentHorizontalLayoutConfig(get(), components);
  }

}
