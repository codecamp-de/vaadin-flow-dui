package de.codecamp.vaadin.fluent.layouts;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import de.codecamp.vaadin.base.lumo.LumoSpace;
import de.codecamp.vaadin.base.util.ThemableLayoutSpacing;
import de.codecamp.vaadin.fluent.FluentHasOrderedComponentsContainerExtension;


interface FluentThemableLayout
{

  default F spacing(boolean enabled)
  {
    if (enabled)
    {
      return spacing(ThemableLayoutSpacing.M);
    }
    else
    {
      ThemableLayoutSpacing.removeFrom(get());
      return (F) this;
    }
  }

  /**
   * @return this
   * @see ThemableLayoutSpacing
   */
  default F spacing(ThemableLayoutSpacing spacing)
  {
    spacing.applyTo(get());
    return (F) this;
  }

  /**
   * @return this
   * @see ThemableLayoutSpacing#XS
   */
  default F spacingXS()
  {
    return spacing(ThemableLayoutSpacing.XS);
  }

  /**
   * @return this
   * @see ThemableLayoutSpacing#S
   */
  default F spacingS()
  {
    return spacing(ThemableLayoutSpacing.S);
  }

  /**
   * @return this
   * @see ThemableLayoutSpacing#M
   */
  default F spacingM()
  {
    return spacing(ThemableLayoutSpacing.M);
  }

  /**
   * @return this
   * @see ThemableLayoutSpacing#L
   */
  default F spacingL()
  {
    return spacing(ThemableLayoutSpacing.L);
  }

  /**
   * @return this
   * @see ThemableLayoutSpacing#XL
   */
  default F spacingXL()
  {
    return spacing(ThemableLayoutSpacing.XL);
  }

}
