package de.codecamp.vaadin.fluent.layouts;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import de.codecamp.vaadin.base.lumo.LumoSpace;
import de.codecamp.vaadin.fluent.FluentHasOrderedComponentsContainerExtension;


class FluentVerticalLayout
  implements
    FluentHasOrderedComponentsContainerExtension<VerticalLayout, FluentVerticalLayout, FluentVerticalLayoutConfig>
{

  public FluentVerticalLayout()
  {
    super(new VerticalLayout());

    /* remove defaults that only exist in the Java API to be consistent with Web Component */
    get().setWidth(null);
    get().setPadding(false);
    get().setSpacing(false);
  }


  @Override
  public FluentVerticalLayoutConfig getLayoutConfigFor(Component... components)
  {
    return new FluentVerticalLayoutConfig(get(), components);
  }

}
