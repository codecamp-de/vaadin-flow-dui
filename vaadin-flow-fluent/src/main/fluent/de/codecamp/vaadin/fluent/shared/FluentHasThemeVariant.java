package de.codecamp.vaadin.fluent.shared;


import com.vaadin.flow.component.shared.HasThemeVariant;


interface FluentHasThemeVariant
{

  /**
   * Sets the given theme variants. All others will be removed.
   * 
   * @param variants
   *          the theme variants to set
   * @return this
   * @see HasThemeVariant#addThemeVariants(com.vaadin.flow.component.shared.ThemeVariant...)
   * @see HasThemeVariant#removeThemeVariants(com.vaadin.flow.component.shared.ThemeVariant...)
   */
  default F themeVariants(VARIANT... variants)
  {
    removeThemeVariants(FluentInternalUtils.getThemeVariantEnumType(this).getEnumConstants());
    return addThemeVariants(variants);
  }

  /**
   * Adds or removes the given theme variant based on the flag.
   * 
   * @param variant
   *          the theme variant to set
   * @param enabled
   *          whether to add or remove the theme variant
   * @return this
   * @see HasThemeVariant#addThemeVariants(com.vaadin.flow.component.shared.ThemeVariant...)
   * @see HasThemeVariant#removeThemeVariants(com.vaadin.flow.component.shared.ThemeVariant...)
   */
  default F themeVariant(VARIANT variant, boolean enabled)
  {
    if (enabled)
      return addThemeVariants(variant);
    else
      return removeThemeVariants(variant);
  }

}
