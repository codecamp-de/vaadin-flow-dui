package de.codecamp.vaadin.fluent.visandint;


import de.codecamp.vaadin.base.lumo.LumoColor;
import de.codecamp.vaadin.base.lumo.LumoIconSize;


class FluentAbstractIcon
{

  public F size(LumoIconSize size)
  {
    get().setSize(size.var());
    return (F) this;
  }

  public F sizeS()
  {
    return size(LumoIconSize.S);
  }

  public F sizeM()
  {
    return size(LumoIconSize.M);
  }

  public F sizeL()
  {
    return size(LumoIconSize.L);
  }

  public F small()
  {
    return sizeS();
  }

  public F medium()
  {
    return sizeM();
  }

  public F large()
  {
    return sizeL();
  }


  public F color(LumoColor color)
  {
    get().setColor(color.var());
    return (F) this;
  }

}
