package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.icon.IconFactory;


class FluentButtonBase
{

  public F icon(IconFactory icon)
  {
    return icon(icon.create());
  }

}
