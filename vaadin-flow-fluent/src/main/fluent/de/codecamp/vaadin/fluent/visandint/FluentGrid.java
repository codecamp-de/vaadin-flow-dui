package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import de.codecamp.vaadin.fluent.FluentFocusable;
import de.codecamp.vaadin.fluent.FluentSortNotifier;


class FluentGrid
  implements
    FluentFocusable<Grid<ITEM>, FluentGrid<ITEM>>,
    FluentSortNotifier<Grid<ITEM>, FluentGrid<ITEM>, GridSortOrder<ITEM>>
{
}
