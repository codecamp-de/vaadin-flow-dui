package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.GridVariant;
import de.codecamp.vaadin.fluent.FluentFocusable;
import de.codecamp.vaadin.fluent.FluentSortNotifier;
import de.codecamp.vaadin.fluent.shared.FluentHasThemeVariantGeneric;
import java.util.Objects;
import java.util.function.Consumer;


class FluentGridBase
  implements
    FluentHasThemeVariantGeneric<C, F, GridVariant>
{

  /**
   * @return this
   * @see Grid#addColumn(Renderer)
   */
  public F addColumn(Renderer<ITEM> renderer, Consumer<Column<ITEM>> columnConfigurer)
  {
    Objects.requireNonNull(columnConfigurer, "columnConfigurer must not be null");
    columnConfigurer.accept(get().addColumn(renderer));
    return (F) this;
  }

  /**
   * @return this
   * @see Grid#addColumn(ValueProvider)
   */
  public F addColumn(ValueProvider<ITEM, ?> valueProvider, Consumer<Column<ITEM>> columnConfigurer)
  {
    Objects.requireNonNull(columnConfigurer, "columnConfigurer must not be null");
    columnConfigurer.accept(get().addColumn(valueProvider));
    return (F) this;
  }

  /**
   * @return this
   * @see Grid#addComponentColumn(ValueProvider)
   */
  public <V extends Component> F addComponentColumn(ValueProvider<ITEM, V> componentProvider,
      Consumer<Column<ITEM>> columnConfigurer)
  {
    Objects.requireNonNull(columnConfigurer, "columnConfigurer must not be null");
    columnConfigurer.accept(get().addComponentColumn(componentProvider));
    return (F) this;
  }

  /**
   * @return this
   * @see Grid#addContextMenu()
   */
  public F addContextMenu(Consumer<GridContextMenu<ITEM>> contextMenuConfigurer)
  {
    Objects.requireNonNull(contextMenuConfigurer, "contextMenuConfigurer must not be null");
    contextMenuConfigurer.accept(get().addContextMenu());
    return (F) this;
  }

  /**
   * @return this
   * @see Grid#addThemeVariants(GridVariant...)
   */
  @Override
  public F addThemeVariants(GridVariant... variants)
  {
    get().addThemeVariants(variants);
    return (F) this;
  }

  /**
   * @return this
   * @see Grid#removeThemeVariants(GridVariant...)
   */
  @Override
  public F removeThemeVariants(GridVariant... variants)
  {
    get().removeThemeVariants(variants);
    return (F) this;
  }

}
