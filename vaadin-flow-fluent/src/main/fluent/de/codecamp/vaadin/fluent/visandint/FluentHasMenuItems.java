package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.Component;
import java.util.function.Consumer;


interface FluentHasMenuItems
{

  FluentMenuItem addItem(String text);

  default F addItem(String text, Consumer<FluentMenuItem> menuItemConfigurer)
  {
    menuItemConfigurer.accept(addItem(text));
    return (F) this;
  }

  FluentMenuItem addItem(Component component);

  default F addItem(Component component, Consumer<FluentMenuItem> menuItemConfigurer)
  {
    menuItemConfigurer.accept(addItem(component));
    return (F) this;
  }

}
