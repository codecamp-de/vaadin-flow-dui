package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.theme.lumo.LumoIcon;
import de.codecamp.vaadin.base.lumo.LumoColor;
import de.codecamp.vaadin.base.lumo.LumoIconSize;


class FluentIcon
{

  /**
   * Constructs a new instance configuring a new {@link VaadinIcon}.
   */
  public FluentIcon(VaadinIcon icon)
  {
    this(new Icon(icon));
  }

  /**
   * Constructs a new instance configuring a new {@link LumoIcon}.
   */
  public FluentIcon(LumoIcon icon)
  {
    this(icon.create());
  }

}
