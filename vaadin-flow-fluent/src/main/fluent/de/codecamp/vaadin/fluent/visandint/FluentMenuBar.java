package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.Component;
import java.util.function.Consumer;


class FluentMenuBar
{

  @Override
  public FluentMenuItem addItem(String text)
  {
    return new FluentMenuItem(get().addItem(text));
  }

  @Override
  public FluentMenuItem addItem(Component component)
  {
    return new FluentMenuItem(get().addItem(component));
  }

  public FluentMenuItem addItem(String text, String tooltipText)
  {
    return new FluentMenuItem(get().addItem(text, tooltipText));
  }

  public FluentMenuBar addItem(String text, String tooltipText,
      Consumer<FluentMenuItem> menuItemConfigurer)
  {
    menuItemConfigurer.accept(addItem(text, tooltipText));
    return this;
  }

  public FluentMenuItem addItem(Component component, String tooltipText)
  {
    return new FluentMenuItem(get().addItem(component, tooltipText));
  }

  public FluentMenuBar addItem(Component component, String tooltipText,
      Consumer<FluentMenuItem> menuItemConfigurer)
  {
    menuItemConfigurer.accept(addItem(component, tooltipText));
    return this;
  }

}
