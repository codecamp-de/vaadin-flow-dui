package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.notification.Notification;
import java.time.Duration;


class FluentNotification
{

  /**
   * @return this
   * @see Notification#setDuration(int)
   */
  public FluentNotification duration(Duration duration)
  {
    if (duration == null)
      get().setDuration(0);
    else
      get().setDuration(Math.toIntExact(duration.toMillis()));
    return this;
  }

}
