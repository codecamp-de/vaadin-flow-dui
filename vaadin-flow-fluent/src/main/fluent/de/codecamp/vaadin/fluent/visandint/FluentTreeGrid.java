package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.BlurNotifier;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.FocusNotifier;
import com.vaadin.flow.component.Focusable;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyModifier;
import com.vaadin.flow.component.BlurNotifier.BlurEvent;
import com.vaadin.flow.component.FocusNotifier.FocusEvent;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.event.SortEvent;
import com.vaadin.flow.data.event.SortEvent.SortNotifier;
import de.codecamp.vaadin.fluent.FluentFocusable;
import de.codecamp.vaadin.fluent.FluentHasEnabled;
import de.codecamp.vaadin.fluent.FluentSortNotifier;


class FluentTreeGrid
  implements
    // FluentFocusable<TreeGrid<ITEM>, FluentTreeGrid<ITEM>>
    // FluentSortNotifier<TreeGrid<ITEM>, FluentTreeGrid<ITEM>, GridSortOrder<ITEM>>,
    // cannot implement FluentFocusable; at least implement FluentHasEnabled instead
    FluentHasEnabled<TreeGrid<ITEM>, FluentTreeGrid<ITEM>>
{

  /* copied from FluentFocusable */

  /**
   * @return this
   * @see Focusable#setTabIndex(int)
   */
  public FluentTreeGrid tabIndex(int tabIndex)
  {
    get().setTabIndex(tabIndex);
    return this;
  }

  /**
   * @return this
   * @see Focusable#focus()
   */
  public FluentTreeGrid<ITEM> focus()
  {
    get().focus();
    return this;
  }

  /**
   * @return this
   * @see Focusable#blur()
   */
  public FluentTreeGrid<ITEM> blur()
  {
    get().blur();
    return this;
  }

  /**
   * @return this
   * @see Focusable#addFocusShortcut(com.vaadin.flow.component.Key,
   *      com.vaadin.flow.component.KeyModifier[])
   */
  public FluentTreeGrid<ITEM> focusShortcut(Key key, KeyModifier... keyModifiers)
  {
    get().addFocusShortcut(key, keyModifiers);
    return this;
  }

  /* copied from FluentFocusNotifier */

  /**
   * @return this
   * @see FocusNotifier#addFocusListener(com.vaadin.flow.component.ComponentEventListener)
   */
  public FluentTreeGrid<ITEM> onFocus(ComponentEventListener<FocusEvent<Grid<ITEM>>> listener)
  {
    get().addFocusListener(listener);
    return this;
  }

  /* copied from FluentBlurNotifier */
  /**
   * @return this
   * @see BlurNotifier#addBlurListener(com.vaadin.flow.component.ComponentEventListener)
   */
  public FluentTreeGrid<ITEM> onBlur(ComponentEventListener<BlurEvent<Grid<ITEM>>> listener)
  {
    get().addBlurListener(listener);
    return this;
  }

  /* copied from FluentSortNotifier */

  /**
   * @return this
   * @see SortNotifier#addSortListener(com.vaadin.flow.component.ComponentEventListener)
   */
  public FluentTreeGrid<ITEM> onSort(
      ComponentEventListener<SortEvent<Grid<ITEM>, GridSortOrder<ITEM>>> arg0)
  {
    get().addSortListener(arg0);
    return this;
  }

}
