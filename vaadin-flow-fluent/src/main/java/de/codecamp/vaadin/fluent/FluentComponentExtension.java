package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.function.SerializableSupplier;


/**
 * Marker interface for interfaces that extend {@link FluentComponent}, using only default methods.
 * The purpose is to improve code structure and not stuff everything into {@link FluentComponent}.
 *
 * @param <C>
 *          the component type
 * @param <F>
 *          the fluent wrapper type
 */
public interface FluentComponentExtension<C extends Component, F extends FluentComponent<C, F>>
  extends
    SerializableSupplier<C>
{
  // marker interface
}
