package de.codecamp.vaadin.fluent;


import static java.util.Objects.requireNonNull;

import com.vaadin.flow.component.Component;
import java.util.function.Consumer;


/**
 *
 * @param <C>
 *          the component type
 * @param <F>
 *          the fluent type
 * @param <FLC>
 *          the fluent layout config type
 */
@SuppressWarnings("unchecked")
public interface FluentContainerExtension<C extends Component, F extends FluentContainerExtension<C, F, FLC>, FLC extends FluentLayoutConfig<C, FLC>>
{

  /**
   * Returns a fluent layout config for the given components.
   * <p>
   * <em><strong>This is an internal method and not intended to be called directly.</strong></em>
   * You should typically call {@link #layout(Component, Consumer)} or
   * {@link #layout(Component[], Consumer)} instead.
   *
   * @param components
   *          the components to layout
   * @return a fluent layout config for the given components
   * @see #layout(Component, Consumer)
   * @see #layout(Component[], Consumer)
   */
  FLC getLayoutConfigFor(Component... components);

  /**
   * Layouts the given component using the provided configurer.
   *
   * @param component
   *          the component to layout
   * @param layoutConfigurer
   *          the configurer to apply to the component's layout
   * @return this
   */
  default F layout(Component component, Consumer<FLC> layoutConfigurer)
  {
    requireNonNull(layoutConfigurer, "layoutConfigurer must not be null");
    layoutConfigurer.accept(getLayoutConfigFor(component));
    return (F) this;
  }

  /**
   * Layouts the given components using the provided configurer.
   *
   * @param components
   *          the components to layout
   * @param layoutConfigurer
   *          the configurer to apply to the components' layout
   * @return this
   */
  default F layout(Component[] components, Consumer<FLC> layoutConfigurer)
  {
    requireNonNull(layoutConfigurer, "layoutConfigurer must not be null");
    layoutConfigurer.accept(getLayoutConfigFor(components));
    return (F) this;
  }

}
