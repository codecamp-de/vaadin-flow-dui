package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasOrderedComponents;


/**
 * {@link FluentComponentExtension} for {@link HasComponents}.
 *
 * @param <C>
 *          the component type
 * @param <F>
 *          the fluent component type
 * @see HasComponents
 */
@SuppressWarnings("unchecked")
public interface FluentHasComponentsComponentExtension<C extends Component, F extends FluentComponent<C, F>>
  extends
    FluentComponentExtension<C, F>
{

  /**
   * Adds this component to the given {@link HasOrderedComponents} container.
   *
   * @param container
   *          the target container
   * @return this
   * @see HasComponents#add(Component...)
   */
  default F addTo(HasComponents container)
  {
    container.add(get());
    return (F) this;
  }

  /**
   * Adds this component to the given {@link HasOrderedComponents} container at the specified index.
   *
   * @param container
   *          the target container
   * @param index
   *          the index where to insert this component
   * @return this
   * @see HasComponents#addComponentAtIndex(int, Component)
   */
  default F addToAt(HasComponents container, int index)
  {
    container.addComponentAtIndex(index, get());
    return (F) this;
  }

}
