package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import java.util.function.Consumer;


/**
 * The fluent container extension for {@link HasComponents}.
 *
 * @param <C>
 *          the component type
 * @param <F>
 *          the fluent type
 * @param <FLC>
 *          the fluent layout config type
 * @see FluentContainerExtension
 * @see FluentHasComponents
 */
@SuppressWarnings("unchecked")
public interface FluentHasComponentsContainerExtension<C extends Component & HasComponents, F extends FluentHasComponentsContainerExtension<C, F, FLC>, FLC extends FluentLayoutConfig<C, FLC>>
  extends
    FluentContainerExtension<C, F, FLC>,
    FluentHasComponents<C, F>
{

  /**
   * @param component
   *          the component to add
   * @param layoutConfigurer
   *          the configurer to apply to the component's layout; may be null
   * @return this
   * @see HasComponents#add(Component...)
   */
  default F add(Component component, Consumer<FLC> layoutConfigurer)
  {
    get().add(component);
    if (layoutConfigurer != null)
      layout(component, layoutConfigurer);
    return (F) this;
  }

  /**
   * @param components
   *          the components to add
   * @param layoutConfigurer
   *          the configurer to apply to the components' layout; may be null
   * @return this
   * @see HasComponents#add(Component...)
   */
  default F add(Component[] components, Consumer<FLC> layoutConfigurer)
  {
    get().add(components);
    if (layoutConfigurer != null)
      layout(components, layoutConfigurer);
    return (F) this;
  }

  /**
   * @param index
   *          the index where to insert the component
   * @param component
   *          the component to add
   * @param layoutConfigurer
   *          the configurer to apply to the component's layout; may be null
   * @return this
   * @see HasComponents#addComponentAtIndex(int, Component)
   */
  default F addAt(int index, Component component, Consumer<FLC> layoutConfigurer)
  {
    addAt(index, component);
    if (layoutConfigurer != null)
      layout(component, layoutConfigurer);
    return (F) this;
  }

  /**
   * @param index
   *          the index where to add the components
   * @param components
   *          the components to add
   * @param layoutConfigurer
   *          the configurer to apply to the components' layout; may be null
   * @return this
   * @see HasComponents#addComponentAtIndex(int, Component)
   */
  default F addAt(int index, Component[] components, Consumer<FLC> layoutConfigurer)
  {
    addAt(index, components);
    if (layoutConfigurer != null)
      layout(components, layoutConfigurer);
    return (F) this;
  }

}
