package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasOrderedComponents;


/**
 * {@link FluentComponentExtension} for {@link HasOrderedComponents}.
 *
 * @param <C>
 *          the component type
 * @param <F>
 *          the fluent component type
 * @see HasOrderedComponents
 */
@SuppressWarnings("unchecked")
public interface FluentHasOrderedComponentsComponentExtension<C extends Component, F extends FluentComponent<C, F>>
  extends
    FluentComponentExtension<C, F>
{

  /**
   * Replaces the specified old component in the given {@link HasOrderedComponents} container.
   *
   * @param container
   *          the target container
   * @param oldComponent
   *          the old component to be replaced
   * @return this
   * @see HasOrderedComponents#replace(Component, Component)
   */
  default F replace(HasOrderedComponents container, Component oldComponent)
  {
    container.replace(oldComponent, get());
    return (F) this;
  }

}
