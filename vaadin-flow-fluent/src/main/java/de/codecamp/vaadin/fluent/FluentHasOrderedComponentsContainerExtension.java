package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasOrderedComponents;
import java.util.function.Consumer;


/**
 * The fluent container extension for {@link HasOrderedComponents}.
 *
 * @param <C>
 *          the component type
 * @param <F>
 *          the fluent type
 * @param <FLC>
 *          the fluent layout config type
 * @see FluentContainerExtension
 * @see HasOrderedComponents
 */
@SuppressWarnings("unchecked")
public interface FluentHasOrderedComponentsContainerExtension<C extends Component & HasOrderedComponents, F extends FluentHasOrderedComponentsContainerExtension<C, F, FLC>, FLC extends FluentLayoutConfig<C, FLC>>
  extends
    FluentHasOrderedComponents<C, F>,
    FluentHasComponentsContainerExtension<C, F, FLC>
{

  /**
   * @param oldChild
   *          the old child component
   * @param newChild
   *          the new child component
   * @param layoutConfigurer
   *          the configurer to apply to the new component's layout; may be null
   * @return this
   * @see HasOrderedComponents#replace(Component, Component)
   */
  default F replace(Component oldChild, Component newChild, Consumer<FLC> layoutConfigurer)
  {
    replace(oldChild, newChild);
    if (layoutConfigurer != null)
      layout(newChild, layoutConfigurer);
    return (F) this;
  }

}
