package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import de.codecamp.vaadin.base.HasSingleComponent;


/**
 * {@link FluentComponentExtension} for {@link HasSingleComponent}.
 *
 * @param <C>
 *          the component type
 * @param <F>
 *          the fluent component type
 * @see HasComponents
 */
@SuppressWarnings("unchecked")
public interface FluentHasSingleComponentComponentExtension<C extends Component, F extends FluentComponent<C, F>>
  extends
    FluentComponentExtension<C, F>
{

  /**
   * Adds this component as content of the given {@link HasSingleComponent} container. If it already
   * contains a content component, an {@link IllegalStateException} will be thrown.
   *
   * @param container
   *          the target container
   * @return this
   * @throws IllegalStateException
   *           if the container already has a content component set
   * @see HasSingleComponent#setContent(Component)
   */
  default F addTo(HasSingleComponent container)
  {
    return addTo(container, false);
  }

  /**
   * Adds this component as content of the given {@link HasSingleComponent} container, optionally
   * replacing an previous content component. If the {@code replace} parameter is {@code false} and
   * it already contains a content component set an {@link IllegalStateException} will be thrown.
   *
   * @param container
   *          the target container
   * @param replace
   *          whether to replace any previous content component; if {@code false} an exception is
   *          thrown in case of an existing content component
   * @return this
   * @throws IllegalStateException
   *           if {@code replace} is {@code false} and the container already has a content component
   *           set
   * @see HasSingleComponent#setContent(Component)
   */
  default F addTo(HasSingleComponent container, boolean replace)
  {
    if (!replace && container.getContent() != null)
      throw new IllegalStateException("The container already has a content component.");

    container.setContent(get());
    return (F) this;
  }

}
