package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.base.HasSingleComponent;
import de.codecamp.vaadin.fluent.base.FluentHasSingleComponent;
import java.util.function.Consumer;


/**
 * The fluent container extension for {@link HasSingleComponent}.
 *
 * @param <C>
 *          the component type
 * @param <F>
 *          the fluent type
 * @param <FLC>
 *          the fluent layout config type
 * @see FluentContainerExtension
 * @see HasSingleComponent
 */
@SuppressWarnings("unchecked")
public interface FluentHasSingleComponentContainerExtension<C extends Component & HasSingleComponent, F extends FluentHasSingleComponentContainerExtension<C, F, FLC>, FLC extends FluentHasSingleComponentLayoutConfig<C, FLC>>
  extends
    FluentContainerExtension<C, F, FLC>,
    FluentHasSingleComponent<C, F>
{

  /**
   * @param compoment
   *          the component to add
   * @param layoutConfigurer
   *          the configurer to apply to the component's layout; may be null
   * @return this
   * @see HasSingleComponent#setContent(Component)
   */
  default F add(Component compoment, Consumer<FLC> layoutConfigurer)
  {
    get().setContent(compoment);
    if (layoutConfigurer != null)
      layout(compoment, layoutConfigurer);
    return (F) this;
  }

}
