package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.base.HasSingleComponent;
import de.codecamp.vaadin.base.util.SizeUtils;


@SuppressWarnings("unchecked")
public class FluentHasSingleComponentLayoutConfig<CONT extends HasSingleComponent, FLC extends FluentHasSingleComponentLayoutConfig<CONT, FLC>>
  extends
    FluentLayoutConfig<CONT, FLC>
{

  public FluentHasSingleComponentLayoutConfig(CONT container, Component... components)
  {
    super(container, components);
  }


  public FLC widthFull()
  {
    for (Component component : getComponentsAsArray())
      SizeUtils.setWidthFull(component);
    return (FLC) this;
  }

  public FLC heightFull()
  {
    for (Component component : getComponentsAsArray())
      SizeUtils.setHeightFull(component);
    return (FLC) this;
  }

  public FLC sizeFull()
  {
    for (Component component : getComponentsAsArray())
      SizeUtils.setSizeFull(component);
    return (FLC) this;
  }

}
