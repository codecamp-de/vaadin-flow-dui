package de.codecamp.vaadin.fluent;


import static java.util.Objects.requireNonNull;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasElement;
import java.util.List;


/**
 * Base class for layout config classes that allow to further adapt the layout configuration of one
 * or more {@link Component Components} after adding them to a container.
 *
 * @param <CONT>
 *          the type of the container
 * @param <FLC>
 *          the type of this layout config
 */
public abstract class FluentLayoutConfig<CONT extends HasElement, FLC extends FluentLayoutConfig<CONT, FLC>>
{

  /**
   * the container component
   */
  private final CONT container;

  /**
   * the content components being layouted
   */
  private final Component[] components;


  /**
   * Constructs a new instance for the given container and content components.
   *
   * @param container
   *          the container
   * @param components
   *          the content components
   * @throws IllegalArgumentException
   *           if the container is not actually the parent of all content components
   */
  protected FluentLayoutConfig(CONT container, Component... components)
  {
    this(true, container, components);
  }

  /**
   * Constructs a new instance for the given container and content components, optionally verifying
   * that all components actually have the container as parent.
   *
   * @param ensureContainerIsParent
   *          whether to check that the given content components actually have the container as
   *          parent
   * @param container
   *          the container
   * @param components
   *          the content components
   */
  protected FluentLayoutConfig(boolean ensureContainerIsParent, CONT container,
      Component... components)
  {
    this.container = requireNonNull(container, "container must not be null");
    this.components = requireNonNull(components, "components must not be null");

    if (ensureContainerIsParent)
    {
      for (Component component : components)
      {
        if (!component.getElement().getParent().equals(container.getElement()))
        {
          throw new IllegalArgumentException("Component is not a child of the container.");
        }
      }
    }
  }


  /**
   * Returns the container.
   *
   * @return the container
   */
  public CONT getContainer()
  {
    return container;
  }

  /**
   * Returns the components.
   *
   * @return the components
   */
  public List<Component> getComponents()
  {
    return List.of(components);
  }

  /**
   * Returns the components. Do not modify the returned array!
   *
   * @return the components as array
   */
  protected Component[] getComponentsAsArray()
  {
    return components.clone();
  }

}
