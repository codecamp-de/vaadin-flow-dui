package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasOrderedComponents;


/**
 * {@link FluentComponent} implements many {@link FluentComponentExtension container-specific
 * extensions} that allow a component to be added to a container and layouted in a fluent way. But
 * {@link FluentComponent} cannot be extended dynamically. So to support new containers that aren't
 * part of Vaadin Flow, one or more of {@link FluentLayoutSupport},
 * {@link FluentLayoutSupportIndexed}, {@link FluentLayoutSupportSlotted} and
 * {@link FluentLayoutSupportSingle} can be implemented by custom containers.
 * <p>
 * <strong>{@link FluentLayoutSupport} allows adding a component without specifying an index or slot
 * where to insert it. And it's only useful for containers that allow the layouting of the added
 * components to be customized further (i.e. that provide a {@link FluentLayoutConfig}. Otherwise
 * it's enough if the container just implements {@link HasComponents} or
 * {@link HasOrderedComponents}.</strong>
 * <p>
 * If the container for whatever reason cannot implement this interface, any other proxy object
 * would work too. {@link FluentLayoutSupport} does not necessarily have to be a {@link Component},
 * it only needs access to it. But concerning the fluent API, this will be the object you are adding
 * a component to.
 *
 * @param <FLC>
 *          the fluent layout config type; typically a subtype of {@link FluentLayoutConfig} or
 *          {@link Void}
 * @see FluentLayoutSupport
 * @see FluentLayoutSupportIndexed
 * @see FluentLayoutSupportSlotted
 * @see FluentLayoutSupportSingle
 */
public interface FluentLayoutSupport<FLC>
{

  /**
   * Adds the given component to this container and (optionally) returns a specialized
   * {@link FluentLayoutConfig} to further customize the layout of the added component.
   * <p>
   * <em><strong>This method is not intended to be called directly; only by {@link FluentComponent}
   * as part of fluent API calls.</strong></em>
   *
   * @param component
   *          the component to add
   * @return a specialized layout config; may be null if the layout of a component cannot be
   *         customized further
   */
  FLC fluentAdd(Component component);

}
