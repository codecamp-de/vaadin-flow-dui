package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.Component;


/**
 * {@link FluentComponent} implements many {@link FluentComponentExtension container-specific
 * extensions} that allow a component to be added to a container and layouted in a fluent way. But
 * {@link FluentComponent} cannot be extended dynamically. So to support new containers that aren't
 * part of Vaadin Flow, one or more of {@link FluentLayoutSupport},
 * {@link FluentLayoutSupportIndexed}, {@link FluentLayoutSupportSlotted} and
 * {@link FluentLayoutSupportSingle} can be implemented by custom containers.
 * <p>
 * <strong>{@link FluentLayoutSupportSingle} allows setting or replacing the single main content
 * component of a container component, and optionally further customizing the layouting of the added
 * component (via a {@link FluentLayoutConfig} or
 * {@link FluentHasSingleComponentLayoutConfig}).</strong>
 * <p>
 * If the container for whatever reason cannot implement this interface, any other proxy object
 * would work too. {@link FluentLayoutSupport} does not necessarily have to be a {@link Component},
 * it only needs access to it. But concerning the fluent API, this will be the object you are adding
 * a component to.
 *
 * @param <FLC>
 *          the fluent layout config type; typically a subtype of {@link FluentLayoutConfig} or
 *          {@link Void}
 * @see FluentLayoutSupport
 * @see FluentLayoutSupportIndexed
 * @see FluentLayoutSupportSlotted
 * @see FluentLayoutSupportSingle
 */
public interface FluentLayoutSupportSingle<FLC>
{

  /**
   * Adds the given component as the main content component in this container and (optionally)
   * returns a specialized {@link FluentLayoutConfig} to further customize the layout of the added
   * component.
   * <p>
   * <em><strong>This method is not intended to be called directly; only by {@link FluentComponent}
   * as part of fluent API calls.</strong></em>
   *
   * @param component
   *          the component to add
   * @param replace
   *          whether to replace any previous content component; if {@code false} an exception is
   *          thrown in case of an existing content component
   * @return a specialized layout config; may be null if the layout of a component cannot be
   *         customized further
   */
  FLC fluentAdd(Component component, boolean replace);

}
