package de.codecamp.vaadin.fluent;


import com.vaadin.flow.component.ScrollOptions;
import com.vaadin.flow.component.ScrollOptions.Alignment;
import com.vaadin.flow.component.ScrollOptions.Behavior;


/**
 * Fluent wrapper for {@link ScrollOptions}.
 */
public class FluentScrollOptions
  extends
    FluentWrapper<ScrollOptions, FluentScrollOptions>
{

  /**
   * Constructs a new instance configuring the given {@link ScrollOptions}.
   *
   * @param wrapped
   *          the scroll options to be configured
   */
  public FluentScrollOptions(ScrollOptions wrapped)
  {
    super(wrapped);
  }


  /**
   * @param behavior
   *          the scroll behavior
   * @return this
   * @see ScrollOptions#setBehavior(Behavior)
   */
  public FluentScrollOptions behavior(Behavior behavior)
  {
    get().setBehavior(behavior);
    return this;
  }

  /**
   * @return this
   * @see ScrollOptions#setBehavior(Behavior)
   * @see Behavior#AUTO
   */
  public FluentScrollOptions behaviorAuto()
  {
    return behavior(Behavior.AUTO);
  }

  /**
   * @return this
   * @see ScrollOptions#setBehavior(Behavior)
   * @see Behavior#SMOOTH
   */
  public FluentScrollOptions behaviorSmooth()
  {
    return behavior(Behavior.SMOOTH);
  }


  /**
   * @param block
   *          the block / vertical alignment
   * @return this
   * @see ScrollOptions#setBlock(Alignment)
   */
  public FluentScrollOptions block(Alignment block)
  {
    get().setBlock(block);
    return this;
  }

  /**
   * @return this
   * @see ScrollOptions#setBlock(Alignment)
   * @see Alignment#START
   */
  public FluentScrollOptions blockStart()
  {
    return block(Alignment.START);
  }

  /**
   * @return this
   * @see ScrollOptions#setBlock(Alignment)
   * @see Alignment#CENTER
   */
  public FluentScrollOptions blockCenter()
  {
    return block(Alignment.CENTER);
  }

  /**
   * @return this
   * @see ScrollOptions#setBlock(Alignment)
   * @see Alignment#END
   */
  public FluentScrollOptions blockEnd()
  {
    return block(Alignment.END);
  }

  /**
   * @return this
   * @see ScrollOptions#setBlock(Alignment)
   * @see Alignment#NEAREST
   */
  public FluentScrollOptions blockNearest()
  {
    return block(Alignment.NEAREST);
  }


  /**
   * @param inline
   *          the inline / horizontal alignment
   * @return this
   * @see ScrollOptions#setInline(Alignment)
   */
  public FluentScrollOptions inline(Alignment inline)
  {
    get().setInline(inline);
    return this;
  }

  /**
   * @return this
   * @see ScrollOptions#setInline(Alignment)
   * @see Alignment#START
   */
  public FluentScrollOptions inlineStart()
  {
    return inline(Alignment.START);
  }

  /**
   * @return this
   * @see ScrollOptions#setInline(Alignment)
   * @see Alignment#CENTER
   */
  public FluentScrollOptions inlineCenter()
  {
    return inline(Alignment.CENTER);
  }

  /**
   * @return this
   * @see ScrollOptions#setInline(Alignment)
   * @see Alignment#END
   */
  public FluentScrollOptions inlineEnd()
  {
    return inline(Alignment.END);
  }

  /**
   * @return this
   * @see ScrollOptions#setInline(Alignment)
   * @see Alignment#NEAREST
   */
  public FluentScrollOptions inlineNearest()
  {
    return inline(Alignment.NEAREST);
  }

}
