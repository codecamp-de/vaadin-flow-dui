package de.codecamp.vaadin.fluent;


import static java.util.Objects.requireNonNull;

import com.vaadin.flow.function.SerializableSupplier;
import java.util.function.Consumer;


/**
 * Abstract base class for fluent wrappers.
 *
 * @param <W>
 *          the wrapped type
 * @param <F>
 *          the fluent type
 */
@SuppressWarnings("unchecked")
public abstract class FluentWrapper<W, F extends FluentWrapper<W, F>>
  implements
    SerializableSupplier<W>
{

  private final W wrapped;


  /**
   *
   * Constructs a new instance wrapping the given object.
   *
   * @param wrapped
   *          the wrapped object
   */
  protected FluentWrapper(W wrapped)
  {
    this.wrapped = requireNonNull(wrapped, "wrapped must not be null");
  }


  /**
   * Returns the wrapped object.
   *
   * @return the wrapped object
   */
  @Override
  public W get()
  {
    return wrapped;
  }


  /**
   * Applies the given configurer to the wrapped object.
   *
   * @param configurer
   *          the configurer to apply to the object
   * @return this
   */
  public F apply(Consumer<? super W> configurer)
  {
    configurer.accept(get());
    return (F) this;
  }

  /**
   * Applies the given configurers to the wrapped object.
   *
   * @param configurers
   *          the configurers to apply to the object
   * @return this
   */
  @SafeVarargs
  public final F apply(Consumer<? super W>... configurers)
  {
    for (Consumer<? super W> configurer : configurers)
      configurer.accept(get());
    return (F) this;
  }

}
