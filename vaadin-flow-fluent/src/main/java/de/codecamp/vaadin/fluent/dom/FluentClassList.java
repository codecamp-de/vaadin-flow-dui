package de.codecamp.vaadin.fluent.dom;


import com.vaadin.flow.dom.ClassList;
import de.codecamp.vaadin.fluent.FluentWrapper;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;


/**
 * The fluent counterpart of {@link ClassList}.
 */
public class FluentClassList
  extends
    FluentWrapper<ClassList, FluentClassList>
{

  /**
   * Constructs a new instance configuring the given {@link ClassList}.
   *
   * @param wrapped
   *          the class list to be configured
   */
  public FluentClassList(ClassList wrapped)
  {
    super(wrapped);
  }


  /**
   * @param className
   *          the class name to add
   * @return this
   * @see Set#add(Object)
   */
  public FluentClassList add(String className)
  {
    get().add(className);
    return this;
  }

  /**
   * @param className
   *          the class name to toggle
   * @param set
   *          whether to add or remove the class name
   * @return this
   * @see ClassList#set(String, boolean)
   */
  public FluentClassList set(String className, boolean set)
  {
    get().set(className, set);
    return this;
  }

  /**
   * @param classNames
   *          the class names to add
   * @return this
   * @see ClassList#addAll(Collection)
   */
  public FluentClassList addAll(Collection<? extends String> classNames)
  {
    get().addAll(classNames);
    return this;
  }

  /**
   * @param classNames
   *          the class names to add
   * @return this
   * @see ClassList#addAll(Collection)
   */
  public FluentClassList addAll(String... classNames)
  {
    return addAll(Arrays.asList(classNames));
  }

  /**
   * @param classNames
   *          the class names to retain
   * @return this
   * @see ClassList#retainAll(Collection)
   */
  public FluentClassList retainAll(Collection<?> classNames)
  {
    get().retainAll(classNames);
    return this;
  }

  /**
   * @param classNames
   *          the class names to retain
   * @return this
   * @see ClassList#retainAll(Collection)
   */
  public FluentClassList retainAll(String... classNames)
  {
    return removeAll(Arrays.asList(classNames));
  }

  /**
   * @param className
   *          the class name to remove
   * @return this
   * @see ClassList#remove(Object)
   */
  public FluentClassList remove(String className)
  {
    get().remove(className);
    return this;
  }

  /**
   * @param classNames
   *          the class names to remove
   * @return this
   * @see ClassList#removeAll(Collection)
   */
  public FluentClassList removeAll(Collection<?> classNames)
  {
    get().removeAll(classNames);
    return this;
  }

  /**
   * @param classNames
   *          the class names to remove
   * @return this
   * @see ClassList#removeAll(Collection)
   */
  public FluentClassList removeAll(String... classNames)
  {
    return removeAll(Arrays.asList(classNames));
  }

  /**
   * @return this
   * @see ClassList#clear()
   */
  public FluentClassList clear()
  {
    get().clear();
    return this;
  }

}
