package de.codecamp.vaadin.fluent.dom;


import com.vaadin.flow.dom.Style;
import com.vaadin.flow.dom.Style.BoxSizing;
import de.codecamp.vaadin.base.css.CssProperties;
import de.codecamp.vaadin.base.lumo.LumoBorderRadius;
import de.codecamp.vaadin.base.lumo.LumoColor;
import de.codecamp.vaadin.base.lumo.LumoFontSize;
import de.codecamp.vaadin.base.lumo.LumoLineHeight;
import de.codecamp.vaadin.base.lumo.LumoProperty;
import de.codecamp.vaadin.base.lumo.LumoSpace;
import de.codecamp.vaadin.fluent.FluentWrapper;
import java.util.Locale;


/**
 * The fluent counterpart of {@link Style}.
 */
public class FluentStyle
  extends
    FluentWrapper<Style, FluentStyle>
{

  /**
   * Constructs a new instance configuring the given {@link Style}.
   *
   * @param wrapped
   *          the style to be configured
   */
  public FluentStyle(Style wrapped)
  {
    super(wrapped);
  }


  public FluentStyle set(String name, String value)
  {
    get().set(name, value);
    return this;
  }

  public FluentStyle set(String name, LumoProperty lumoProperty)
  {
    get().set(name, lumoProperty.var());
    return this;
  }

  public FluentStyle remove(String name)
  {
    get().remove(name);
    return this;
  }

  public FluentStyle clear()
  {
    get().clear();
    return this;
  }


  public FluentStyle boxSizing(BoxSizing boxSizing)
  {
    return set(CssProperties.boxSizing,
        boxSizing.name().replace("_", "-").toLowerCase(Locale.ENGLISH));
  }

  public FluentStyle boxSizingBorderBox()
  {
    return boxSizing(BoxSizing.BORDER_BOX);
  }

  public FluentStyle boxSizingContentBox()
  {
    return boxSizing(BoxSizing.CONTENT_BOX);
  }


  public FluentStyle outline(String outline)
  {
    return set(CssProperties.outline, outline);
  }


  public FluentStyle margin(LumoSpace margin)
  {
    return set(CssProperties.margin, toString(margin));
  }

  public FluentStyle marginHorizontal(LumoSpace margin)
  {
    return marginLeft(margin).marginRight(margin);
  }

  public FluentStyle marginVertical(LumoSpace margin)
  {
    return marginTop(margin).marginBottom(margin);
  }

  public FluentStyle marginTop(LumoSpace margin)
  {
    return set(CssProperties.marginTop, toString(margin));
  }

  public FluentStyle marginRight(LumoSpace margin)
  {
    return set(CssProperties.marginRight, toString(margin));
  }

  public FluentStyle marginBottom(LumoSpace margin)
  {
    return set(CssProperties.marginBottom, toString(margin));
  }

  public FluentStyle marginLeft(LumoSpace margin)
  {
    return set(CssProperties.marginLeft, toString(margin));
  }


  public FluentStyle border(String border)
  {
    return set(CssProperties.border, border);
  }

  public FluentStyle border(String borderWidth, String borderStyle, LumoColor borderColor)
  {
    return set(CssProperties.border, borderWidth + " " + borderStyle + " " + borderColor.var());
  }

  public FluentStyle borderTop(String border)
  {
    return set(CssProperties.borderTop, border);
  }

  public FluentStyle borderRight(String border)
  {
    return set(CssProperties.borderRight, border);
  }

  public FluentStyle borderBottom(String border)
  {
    return set(CssProperties.borderBottom, border);
  }

  public FluentStyle borderLeft(String border)
  {
    return set(CssProperties.borderLeft, border);
  }

  public FluentStyle borderWidth(String borderWidth)
  {
    return set(CssProperties.borderWidth, borderWidth);
  }

  public FluentStyle borderStyle(String borderStyle)
  {
    return set(CssProperties.borderStyle, borderStyle);
  }

  public FluentStyle borderColor(LumoColor borderColor)
  {
    return set(CssProperties.borderColor, toString(borderColor));
  }

  public FluentStyle borderRadius(LumoBorderRadius borderRadius)
  {
    return set(CssProperties.borderRadius, toString(borderRadius));
  }


  public FluentStyle padding(LumoSpace padding)
  {
    return set(CssProperties.padding, toString(padding));
  }

  public FluentStyle paddingHorizontal(LumoSpace padding)
  {
    return paddingLeft(padding).paddingRight(padding);
  }

  public FluentStyle paddingVertical(LumoSpace padding)
  {
    return paddingTop(padding).paddingBottom(padding);
  }

  public FluentStyle paddingTop(LumoSpace padding)
  {
    return set(CssProperties.paddingTop, toString(padding));
  }

  public FluentStyle paddingRight(LumoSpace padding)
  {
    return set(CssProperties.paddingRight, toString(padding));
  }

  public FluentStyle paddingBottom(LumoSpace padding)
  {
    return set(CssProperties.paddingBottom, toString(padding));
  }

  public FluentStyle paddingLeft(LumoSpace padding)
  {
    return set(CssProperties.paddingLeft, toString(padding));
  }


  public FluentStyle background(String background)
  {
    return set(CssProperties.background, background);
  }

  public FluentStyle backgroundColor(String color)
  {
    return set(CssProperties.backgroundColor, color);
  }

  public FluentStyle backgroundColor(LumoColor color)
  {
    return backgroundColor(toString(color));
  }

  public FluentStyle color(String fontColor)
  {
    return set(CssProperties.color, fontColor);
  }

  public FluentStyle color(LumoColor fontColor)
  {
    return color(toString(fontColor));
  }

  public FluentStyle fontSize(String fontSize)
  {
    return set(CssProperties.fontSize, fontSize);
  }

  public FluentStyle fontSize(LumoFontSize fontSize)
  {
    return set(CssProperties.fontSize, toString(fontSize));
  }

  public FluentStyle fontWeight(String fontWeight)
  {
    return set(CssProperties.fontWeight, fontWeight);
  }

  public FluentStyle lineHeight(String lineHeight)
  {
    return set(CssProperties.lineHeight, lineHeight);
  }

  public FluentStyle lineHeight(LumoLineHeight lineHeight)
  {
    return set(CssProperties.lineHeight, toString(lineHeight));
  }

  public FluentStyle textAlign(String textAlign)
  {
    return set(CssProperties.textAlign, textAlign);
  }

  public FluentStyle textAlignLeft()
  {
    return textAlign("left");
  }

  public FluentStyle textAlignCenter()
  {
    return textAlign("center");
  }

  public FluentStyle textAlignRight()
  {
    return textAlign("right");
  }

  public FluentStyle textAlignJustify()
  {
    return textAlign("justify");
  }

  public FluentStyle textDecoration(String textDecoration)
  {
    return set(CssProperties.textDecoration, textDecoration);
  }


  public FluentStyle cursor(String cursor)
  {
    return set(CssProperties.cursor, cursor);
  }


  private static String toString(LumoProperty property)
  {
    return property == null ? null : property.var();
  }

}
