package de.codecamp.vaadin.fluent.dom;


import com.vaadin.flow.dom.ThemeList;
import de.codecamp.vaadin.fluent.FluentWrapper;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;


/**
 * The fluent counterpart of {@link ThemeList}.
 */
public class FluentThemeList
  extends
    FluentWrapper<ThemeList, FluentThemeList>
{

  /**
   * Constructs a new instance configuring the given {@link ThemeList}.
   *
   * @param wrapped
   *          the theme list to be configured
   */
  public FluentThemeList(ThemeList wrapped)
  {
    super(wrapped);
  }


  /**
   * @param themeName
   *          the theme to add
   * @return this
   * @see Set#add(Object)
   */
  public FluentThemeList add(String themeName)
  {
    get().add(themeName);
    return this;
  }

  /**
   * @param themeName
   *          the theme to toggle
   * @param set
   *          whether to add or remove the theme
   * @return this
   * @see ThemeList#set(String, boolean)
   */
  public FluentThemeList set(String themeName, boolean set)
  {
    get().set(themeName, set);
    return this;
  }

  /**
   * @param themeNames
   *          the themes to add
   * @return this
   * @see ThemeList#addAll(Collection)
   */
  public FluentThemeList addAll(Collection<? extends String> themeNames)
  {
    get().addAll(themeNames);
    return this;
  }

  /**
   * @param themeNames
   *          the themes to add
   * @return this
   * @see ThemeList#addAll(Collection)
   */
  public FluentThemeList addAll(String... themeNames)
  {
    return addAll(Arrays.asList(themeNames));
  }

  /**
   * @param themeNames
   *          the themes to retain
   * @return this
   * @see ThemeList#retainAll(Collection)
   */
  public FluentThemeList retainAll(Collection<?> themeNames)
  {
    get().retainAll(themeNames);
    return this;
  }

  /**
   * @param themeNames
   *          the themes to retain
   * @return this
   * @see ThemeList#retainAll(Collection)
   */
  public FluentThemeList retainAll(String... themeNames)
  {
    return retainAll(Arrays.asList(themeNames));
  }

  /**
   * @param themeName
   *          the theme to remove
   * @return this
   * @see ThemeList#remove(Object)
   */
  public FluentThemeList remove(String themeName)
  {
    get().remove(themeName);
    return this;
  }

  /**
   * @param themeNames
   *          the themes to remove
   * @return this
   * @see ThemeList#removeAll(Collection)
   */
  public FluentThemeList removeAll(Collection<?> themeNames)
  {
    get().removeAll(themeNames);
    return this;
  }

  /**
   * @param themeNames
   *          the themes to remove
   * @return this
   * @see ThemeList#removeAll(Collection)
   */
  public FluentThemeList removeAll(String... themeNames)
  {
    return removeAll(Arrays.asList(themeNames));
  }

  /**
   * @return this
   * @see ThemeList#clear()
   */
  public FluentThemeList clear()
  {
    get().clear();
    return this;
  }

}
