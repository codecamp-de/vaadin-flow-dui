package de.codecamp.vaadin.fluent.layouts;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import de.codecamp.vaadin.base.util.SizeUtils;
import de.codecamp.vaadin.fluent.FluentLayoutConfig;


/**
 * {@link FluentLayoutConfig} for {@link FlexComponent}.
 *
 * @param <C>
 *          the type of the container
 * @param <FLC>
 *          the type of this layout config
 * @see FlexComponent
 */
@SuppressWarnings("unchecked")
public abstract class FluentFlexComponentLayoutConfig<C extends FlexComponent, FLC extends FluentFlexComponentLayoutConfig<C, FLC>>
  extends
    FluentLayoutConfig<C, FLC>
{

  /**
   * Constructs a new instance for the given container and content components.
   *
   * @param container
   *          the container
   * @param components
   *          the content components
   * @throws IllegalArgumentException
   *           if the container is not actually the parent of all content components
   */
  protected FluentFlexComponentLayoutConfig(C container, Component... components)
  {
    super(container, components);
  }


  /**
   * @param alignment
   *          alignment for the layouted components
   * @return this
   * @see FlexComponent#setAlignSelf(Alignment, com.vaadin.flow.component.HasElement...)
   */
  public FLC align(Alignment alignment)
  {
    for (Component child : getComponentsAsArray())
      getContainer().setAlignSelf(alignment, child);
    return (FLC) this;
  }

  /**
   * @return this
   * @see FlexComponent#setAlignSelf(Alignment, com.vaadin.flow.component.HasElement...)
   * @see Alignment#AUTO
   */
  public FLC alignAuto()
  {
    return align(Alignment.AUTO);
  }

  /**
   * @return this
   * @see FlexComponent#setAlignSelf(Alignment, com.vaadin.flow.component.HasElement...)
   * @see Alignment#START
   */
  public FLC alignStart()
  {
    return align(Alignment.START);
  }

  /**
   * @return this
   * @see FlexComponent#setAlignSelf(Alignment, com.vaadin.flow.component.HasElement...)
   * @see Alignment#END
   */
  public FLC alignEnd()
  {
    return align(Alignment.END);
  }

  /**
   * @return this
   * @see FlexComponent#setAlignSelf(Alignment, com.vaadin.flow.component.HasElement...)
   * @see Alignment#CENTER
   */
  public FLC alignCenter()
  {
    return align(Alignment.CENTER);
  }

  /**
   * @return this
   * @see FlexComponent#setAlignSelf(Alignment, com.vaadin.flow.component.HasElement...)
   * @see Alignment#BASELINE
   */
  public FLC alignBaseline()
  {
    return align(Alignment.BASELINE);
  }

  /**
   * @return this
   * @see FlexComponent#setAlignSelf(Alignment, com.vaadin.flow.component.HasElement...)
   * @see Alignment#STRETCH
   */
  public FLC alignStretch()
  {
    return align(Alignment.STRETCH);
  }


  /**
   * @return this
   * @see SizeUtils#setWidthFullAdaptive(FlexComponent, Component[], boolean, double)
   */
  public FLC widthFull()
  {
    return widthFull(1);
  }

  /**
   * @param weight
   *          the weight
   * @return this
   * @see SizeUtils#setWidthFullAdaptive(FlexComponent, Component[], boolean, double)
   */
  public FLC widthFull(double weight)
  {
    SizeUtils.setWidthFullAdaptive(getContainer(), getComponentsAsArray(), isHorizontal(), weight);
    return (FLC) this;
  }

  /**
   * @return this
   * @see SizeUtils#setHeightFullAdaptive(FlexComponent, Component[], boolean, double)
   */
  public FLC heightFull()
  {
    return heightFull(1);
  }

  /**
   * @param weight
   *          the weight
   * @return this
   * @see SizeUtils#setHeightFullAdaptive(FlexComponent, Component[], boolean, double)
   */
  public FLC heightFull(double weight)
  {
    SizeUtils.setHeightFullAdaptive(getContainer(), getComponentsAsArray(), isHorizontal(), weight);
    return (FLC) this;
  }

  /**
   * @return this
   * @see SizeUtils#setWidthFullAdaptive(FlexComponent, Component[], boolean, double)
   * @see SizeUtils#setHeightFullAdaptive(FlexComponent, Component[], boolean, double)
   */
  public FLC sizeFull()
  {
    return sizeFull(1);
  }

  /**
   * @param weight
   *          the weight
   * @return this
   * @see SizeUtils#setWidthFullAdaptive(FlexComponent, Component[], boolean, double)
   * @see SizeUtils#setHeightFullAdaptive(FlexComponent, Component[], boolean, double)
   */
  public FLC sizeFull(double weight)
  {
    widthFull(weight);
    heightFull(weight);
    return (FLC) this;
  }


  /**
   * @return this
   * @see FlexComponent#expand(Component...)
   */
  public FLC expand()
  {
    getContainer().expand(getComponentsAsArray());
    return (FLC) this;
  }

  /**
   * @return this
   * @see FlexComponent#setFlexGrow(double, com.vaadin.flow.component.HasElement...)
   */
  public FLC grow()
  {
    return grow(1);
  }

  /**
   * @param weight
   *          the weight
   * @return this
   * @see FlexComponent#setFlexGrow(double, com.vaadin.flow.component.HasElement...)
   */
  public FLC grow(double weight)
  {
    getContainer().setFlexGrow(weight, getComponentsAsArray());
    return (FLC) this;
  }


  /**
   * Returns whether the layout is horizontal.
   *
   * @return whether the layout is horizontal; vertical otherwise
   */
  protected abstract boolean isHorizontal();

}
