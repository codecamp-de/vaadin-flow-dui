package de.codecamp.vaadin.fluent.layouts;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import de.codecamp.vaadin.fluent.FluentComponent;
import de.codecamp.vaadin.fluent.FluentComponentExtension;
import java.util.function.Consumer;


/**
 * {@link FluentComponentExtension} for {@link FlexLayout}.
 *
 * @param <C>
 *          the component type
 * @param <F>
 *          the fluent component type
 * @see FlexLayout
 */
@SuppressWarnings("unchecked")
public interface FluentFlexLayoutComponentExtension<C extends Component, F extends FluentComponent<C, F>>
  extends
    FluentComponentExtension<C, F>
{

  /**
   * Adds this component to the given {@link FlexLayout}, applying the provided layout configurer.
   *
   * @param container
   *          the target container
   * @param layoutConfigurer
   *          the layout configurer; may be null
   * @return this
   * @see FlexLayout#add(Component...)
   */
  default F addTo(FlexLayout container, Consumer<FluentFlexLayoutConfig> layoutConfigurer)
  {
    container.add(get());
    if (layoutConfigurer != null)
      layoutConfigurer.accept(new FluentFlexLayoutConfig(container, get()));
    return (F) this;
  }

  /**
   * Adds this component to the given {@link FlexLayout} at the specified index, applying the
   * provided layout configurer.
   *
   * @param container
   *          the target container
   * @param index
   *          the index where to insert this component
   * @param layoutConfigurer
   *          the layout configurer; may be null
   * @return this
   * @see FlexLayout#addComponentAtIndex(int, Component)
   */
  default F addToAt(FlexLayout container, int index,
      Consumer<FluentFlexLayoutConfig> layoutConfigurer)
  {
    container.addComponentAtIndex(index, get());
    if (layoutConfigurer != null)
      layoutConfigurer.accept(new FluentFlexLayoutConfig(container, get()));
    return (F) this;
  }

  /**
   * Replaces the specified old component in the given {@link FlexLayout} with this component,
   * applying the provided layout configurer.
   *
   * @param container
   *          the target container
   * @param oldComponent
   *          the old component to be replaced
   * @param layoutConfigurer
   *          the layout configurer; may be null
   * @return this
   * @see FlexLayout#replace(Component, Component)
   */
  default F replace(FlexLayout container, Component oldComponent,
      Consumer<FluentFlexLayoutConfig> layoutConfigurer)
  {
    container.replace(oldComponent, get());
    if (layoutConfigurer != null)
      layoutConfigurer.accept(new FluentFlexLayoutConfig(container, get()));
    return (F) this;
  }

}
