package de.codecamp.vaadin.fluent.layouts;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.FlexLayout.FlexDirection;
import de.codecamp.vaadin.fluent.FluentLayoutConfig;


/**
 * {@link FluentLayoutConfig} for {@link FlexLayout}.
 */
public class FluentFlexLayoutConfig
  extends
    FluentFlexComponentLayoutConfig<FlexLayout, FluentFlexLayoutConfig>
{

  /**
   * Constructs a new instance for the given container and content components.
   *
   * @param container
   *          the container
   * @param components
   *          the content components
   * @throws IllegalArgumentException
   *           if the container is not actually the parent of all content components
   */
  public FluentFlexLayoutConfig(FlexLayout container, Component... components)
  {
    super(container, components);
  }


  @Override
  protected boolean isHorizontal()
  {
    FlexDirection flexDirection = getContainer().getFlexDirection();
    switch (flexDirection)
    {
      case ROW:
      case ROW_REVERSE:
        return true;

      case COLUMN:
      case COLUMN_REVERSE:
        return false;

      default:
        throw new IllegalStateException("Flex direction not recognized: " + flexDirection);
    }
  }


  /**
   * Sets the flex shrink property.
   *
   * @param weight
   *          the weight
   * @return this
   * @see FlexLayout#setFlexShrink(double, com.vaadin.flow.component.HasElement...)
   */
  public FluentFlexLayoutConfig shrink(double weight)
  {
    getContainer().setFlexShrink(weight, getComponentsAsArray());
    return this;
  }

  /**
   * Sets the flex basis property .
   *
   * @param width
   *          the width
   * @return this
   * @see FlexLayout#setFlexBasis(String, com.vaadin.flow.component.HasElement...)
   */
  public FluentFlexLayoutConfig basis(String width)
  {
    getContainer().setFlexBasis(width, getComponentsAsArray());
    return this;
  }

  /**
   * Sets the order property.
   *
   * @param order
   *          the order
   * @return this
   * @see FlexLayout#setOrder(int, com.vaadin.flow.component.HasElement)
   */
  public FluentFlexLayoutConfig order(int order)
  {
    for (Component child : getComponentsAsArray())
      getContainer().setOrder(order, child);
    return this;
  }

}
