package de.codecamp.vaadin.fluent.layouts;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.formlayout.FormLayout;
import de.codecamp.vaadin.fluent.FluentComponent;
import de.codecamp.vaadin.fluent.FluentComponentExtension;
import java.util.function.Consumer;


/**
 * {@link FluentComponentExtension} for {@link FormLayout}.
 *
 * @param <C>
 *          the component type
 * @param <F>
 *          the fluent component type
 * @see FormLayout
 */
@SuppressWarnings("unchecked")
public interface FluentFormLayoutComponentExtension<C extends Component, F extends FluentComponent<C, F>>
  extends
    FluentComponentExtension<C, F>
{

  /**
   * Adds this component to the given {@link FormLayout}, applying the provided layout configurer.
   *
   * @param container
   *          the target container
   * @param layoutConfigurer
   *          the layout configurer; may be null
   * @return this
   * @see FormLayout#add(Component...)
   */
  default F addTo(FormLayout container, Consumer<FluentFormLayoutConfig> layoutConfigurer)
  {
    container.add(get());
    if (layoutConfigurer != null)
      layoutConfigurer.accept(new FluentFormLayoutConfig(container, get()));
    return (F) this;
  }

  /**
   * Adds this component to the given {@link FormLayout} at the specified index, applying the
   * provided layout configurer.
   *
   * @param container
   *          the target container
   * @param index
   *          the index where to insert this component
   * @param layoutConfigurer
   *          the layout configurer; may be null
   * @return this
   * @see FormLayout#addComponentAtIndex(int, Component)
   */
  default F addToAt(FormLayout container, int index,
      Consumer<FluentFormLayoutConfig> layoutConfigurer)
  {
    container.addComponentAtIndex(index, get());
    if (layoutConfigurer != null)
      layoutConfigurer.accept(new FluentFormLayoutConfig(container, get()));
    return (F) this;
  }

}
