package de.codecamp.vaadin.fluent.layouts;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.formlayout.FormLayout;
import de.codecamp.vaadin.fluent.FluentLayoutConfig;


/**
 * {@link FluentLayoutConfig} for {@link FormLayout}.
 *
 * @see FormLayout
 */
public class FluentFormLayoutConfig
  extends
    FluentLayoutConfig<FormLayout, FluentFormLayoutConfig>
{

  /**
   * Constructs a new instance for the given container and content components.
   *
   * @param container
   *          the container
   * @param components
   *          the content components
   * @throws IllegalArgumentException
   *           if the container is not actually the parent of all content components
   */
  protected FluentFormLayoutConfig(FormLayout container, Component... components)
  {
    super(container, components);
  }


  /**
   * @param colspan
   *          the column span for the layouted components
   * @return this
   * @see FormLayout#setColspan(Component, int)
   */
  public FluentFormLayoutConfig colspan(int colspan)
  {
    for (Component child : getComponentsAsArray())
      getContainer().setColspan(child, colspan);
    return this;
  }

}
