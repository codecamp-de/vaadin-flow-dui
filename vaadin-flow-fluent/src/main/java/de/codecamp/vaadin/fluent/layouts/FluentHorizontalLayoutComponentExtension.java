package de.codecamp.vaadin.fluent.layouts;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import de.codecamp.vaadin.fluent.FluentComponent;
import de.codecamp.vaadin.fluent.FluentComponentExtension;
import java.util.function.Consumer;


/**
 * {@link FluentComponentExtension} for {@link HorizontalLayout}.
 *
 * @param <C>
 *          the component type
 * @param <F>
 *          the fluent component type
 * @see HorizontalLayout
 */
@SuppressWarnings("unchecked")
public interface FluentHorizontalLayoutComponentExtension<C extends Component, F extends FluentComponent<C, F>>
  extends
    FluentComponentExtension<C, F>
{

  /**
   * Adds this component to the given {@link HorizontalLayout}, applying the provided layout
   * configurer, applying the provided layout configurer.
   *
   * @param container
   *          the target container
   * @param layoutConfigurer
   *          the layout configurer; may be null
   * @return this
   * @see HorizontalLayout#add(Component...)
   */
  default F addTo(HorizontalLayout container,
      Consumer<FluentHorizontalLayoutConfig> layoutConfigurer)
  {
    container.add(get());
    if (layoutConfigurer != null)
      layoutConfigurer.accept(new FluentHorizontalLayoutConfig(container, get()));
    return (F) this;
  }

  /**
   * Adds this component to the given {@link HorizontalLayout} at the specified index, applying the
   * provided layout configurer, applying the provided layout configurer.
   *
   * @param container
   *          the target container
   * @param index
   *          the index where to insert this component
   * @param layoutConfigurer
   *          the layout configurer; may be null
   * @return this
   * @see HorizontalLayout#addComponentAtIndex(int, Component)
   */
  default F addToAt(HorizontalLayout container, int index,
      Consumer<FluentHorizontalLayoutConfig> layoutConfigurer)
  {
    container.addComponentAtIndex(index, get());
    if (layoutConfigurer != null)
      layoutConfigurer.accept(new FluentHorizontalLayoutConfig(container, get()));
    return (F) this;
  }

  /**
   * Replaces the specified old component in the given {@link HorizontalLayout} with this component,
   * applying the provided layout configurer, applying the provided layout configurer.
   *
   * @param container
   *          the target container
   * @param oldComponent
   *          the old component to be replaced
   * @param layoutConfigurer
   *          the layout configurer; may be null
   * @return this
   * @see HorizontalLayout#replace(Component, Component)
   */
  default F replace(HorizontalLayout container, Component oldComponent,
      Consumer<FluentHorizontalLayoutConfig> layoutConfigurer)
  {
    container.replace(oldComponent, get());
    if (layoutConfigurer != null)
      layoutConfigurer.accept(new FluentHorizontalLayoutConfig(container, get()));
    return (F) this;
  }

}
