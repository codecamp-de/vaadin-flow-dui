package de.codecamp.vaadin.fluent.layouts;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import de.codecamp.vaadin.fluent.FluentLayoutConfig;


/**
 * {@link FluentLayoutConfig} for {@link HorizontalLayout}.
 */
public class FluentHorizontalLayoutConfig
  extends
    FluentFlexComponentLayoutConfig<HorizontalLayout, FluentHorizontalLayoutConfig>
{

  /**
   * Constructs a new instance for the given container and content components.
   *
   * @param container
   *          the container
   * @param components
   *          the content components
   * @throws IllegalArgumentException
   *           if the container is not actually the parent of all content components
   */
  public FluentHorizontalLayoutConfig(HorizontalLayout container, Component... components)
  {
    super(container, components);
  }


  @Override
  protected boolean isHorizontal()
  {
    return true;
  }

}
