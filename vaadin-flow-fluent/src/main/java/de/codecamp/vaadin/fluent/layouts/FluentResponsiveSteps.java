package de.codecamp.vaadin.fluent.layouts;


import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep;
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep.LabelsPosition;
import java.util.ArrayList;
import java.util.List;


public class FluentResponsiveSteps
{

  private final List<ResponsiveStep> steps = new ArrayList<>();


  public FluentResponsiveSteps()
  {
  }


  public FluentResponsiveSteps step(String minWidth, int column)
  {
    steps.add(new ResponsiveStep(minWidth, column));
    return this;
  }

  public FluentResponsiveSteps step(String minWidth, int column, LabelsPosition labelsPosition)
  {
    steps.add(new ResponsiveStep(minWidth, column, labelsPosition));
    return this;
  }

  public List<ResponsiveStep> getSteps()
  {
    return steps;
  }

}
