package de.codecamp.vaadin.fluent.layouts;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.Scroller;
import de.codecamp.vaadin.fluent.FluentComponent;
import de.codecamp.vaadin.fluent.FluentComponentExtension;
import java.util.function.Consumer;


/**
 * {@link FluentComponentExtension} for {@link Scroller}.
 *
 * @param <C>
 *          the component type
 * @param <F>
 *          the fluent component type
 * @see Scroller
 */
@SuppressWarnings("unchecked")
public interface FluentScrollerComponentExtension<C extends Component, F extends FluentComponent<C, F>>
  extends
    FluentComponentExtension<C, F>
{

  /**
   * Adds this component to the given {@link Scroller}.
   * <p>
   * Since {@link Scroller} only allows a single content component, an {@link IllegalStateException}
   * will be thrown when it already has one assigned. Use {@link #addTo(Scroller, boolean)} instead
   * to optionally replace the content component.
   *
   * @param container
   *          the target container
   * @return this
   * @throws IllegalStateException
   *           if the {@link Scroller} already has a content component assigned
   * @see #addTo(Scroller, boolean)
   */
  default F addTo(Scroller container)
  {
    return addTo(container, false, null);
  }

  /**
   * Adds this component to the given {@link Scroller}, applying the provided layout configurer.
   * <p>
   * Since {@link Scroller} only allows a single content component, an {@link IllegalStateException}
   * will be thrown when it already has one assigned. Use
   * {@link #addTo(Scroller, boolean, Consumer)} instead to optionally replace the content
   * component.
   *
   * @param container
   *          the target container
   * @param layoutConfigurer
   *          the layout configurer; may be null
   * @return this
   * @throws IllegalStateException
   *           if the {@link Scroller} already has a content component assigned
   */
  default F addTo(Scroller container, Consumer<FluentScrollerLayoutConfig> layoutConfigurer)
  {
    return addTo(container, false, layoutConfigurer);
  }

  /**
   * Adds this component to the given {@link Scroller}.
   * <p>
   * Since {@link Scroller} only allows a single content component, the {@code replace} parameter
   * allows to control whether the current content component should be replaced. If the parameter is
   * {@code false} and a content component is assigned, an {@link IllegalStateException} will be
   * thrown.
   *
   * @param container
   *          the target container
   * @param replace
   *          whether the current content component should be replaced; otherwise an exception may
   *          be thrown
   * @return this
   * @throws IllegalStateException
   *           if the {@code replace} parameter is {@code false} and {@link Scroller} already has a
   *           content component assigned
   */
  default F addTo(Scroller container, boolean replace)
  {
    return addTo(container, replace, null);
  }

  /**
   * Adds this component to the given {@link Scroller}, applying the provided layout configurer.
   *
   * @param container
   *          the target container
   * @param replace
   *          whether the current content component should be replaced; otherwise an exception may
   *          be thrown
   * @param layoutConfigurer
   *          the layout configurer; may be null
   * @return this
   * @throws IllegalStateException
   *           if the {@code replace} parameter is {@code false} and {@link Scroller} already has a
   *           content component assigned
   */
  default F addTo(Scroller container, boolean replace,
      Consumer<FluentScrollerLayoutConfig> layoutConfigurer)
  {
    if (!replace && container.getContent() != null)
      throw new IllegalStateException("The Scroller already has a content component.");

    container.setContent(get());
    if (layoutConfigurer != null)
      layoutConfigurer.accept(new FluentScrollerLayoutConfig(container, get()));
    return (F) this;
  }

}
