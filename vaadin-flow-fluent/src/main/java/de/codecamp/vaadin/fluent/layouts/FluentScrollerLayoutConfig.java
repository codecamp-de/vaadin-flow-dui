package de.codecamp.vaadin.fluent.layouts;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.Scroller;
import de.codecamp.vaadin.base.util.SizeUtils;
import de.codecamp.vaadin.fluent.FluentLayoutConfig;


/**
 * {@link FluentLayoutConfig} for {@link Scroller}.
 */
public class FluentScrollerLayoutConfig
  extends
    FluentLayoutConfig<Scroller, FluentScrollerLayoutConfig>
{

  /**
   * Constructs a new instance for the given container and content components.
   *
   * @param container
   *          the container
   * @param components
   *          the content components
   * @throws IllegalArgumentException
   *           if the container is not actually the parent of all content components
   */
  public FluentScrollerLayoutConfig(Scroller container, Component... components)
  {
    super(container, components);
  }


  /**
   * Sets the width to the full width of the container.
   *
   * @return this
   */
  public FluentScrollerLayoutConfig widthFull()
  {
    for (Component child : getComponentsAsArray())
      SizeUtils.setWidthFull(child);
    return this;
  }

  /**
   * Sets the height to the full height of the container.
   *
   * @return this
   */
  public FluentScrollerLayoutConfig heightFull()
  {
    for (Component child : getComponentsAsArray())
      SizeUtils.setHeightFull(child);
    return this;
  }

  /**
   * Sets the width and height to the full and height width of the container.
   *
   * @return this
   */
  public FluentScrollerLayoutConfig sizeFull()
  {
    for (Component child : getComponentsAsArray())
      SizeUtils.setSizeFull(child);
    return this;
  }

}
