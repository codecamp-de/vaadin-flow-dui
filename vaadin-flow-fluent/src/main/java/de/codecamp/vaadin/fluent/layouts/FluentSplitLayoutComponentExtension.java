package de.codecamp.vaadin.fluent.layouts;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import de.codecamp.vaadin.fluent.FluentComponent;
import de.codecamp.vaadin.fluent.FluentComponentExtension;


/**
 * {@link FluentComponentExtension} for {@link SplitLayout}.
 *
 * @param <C>
 *          the component type
 * @param <F>
 *          the fluent component type
 * @see SplitLayout
 */
@SuppressWarnings("unchecked")
public interface FluentSplitLayoutComponentExtension<C extends Component, F extends FluentComponent<C, F>>
  extends
    FluentComponentExtension<C, F>
{

  /**
   * Adds this component to the primary slot of the given {@link SplitLayout}.
   *
   * @param container
   *          the target container
   * @return this
   * @see SplitLayout#addToPrimary(Component...)
   */
  default F addToPrimary(SplitLayout container)
  {
    container.addToPrimary(get());
    return (F) this;
  }

  /**
   * Adds this component to the secondary slot of the given {@link SplitLayout}.
   *
   * @param container
   *          the target container
   * @return this
   * @see SplitLayout#addToSecondary(Component...)
   */
  default F addToSecondary(SplitLayout container)
  {
    container.addToSecondary(get());
    return (F) this;
  }

}
