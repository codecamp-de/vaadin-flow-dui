package de.codecamp.vaadin.fluent.layouts;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import de.codecamp.vaadin.fluent.FluentComponent;
import de.codecamp.vaadin.fluent.FluentComponentExtension;
import java.util.function.Consumer;


/**
 * {@link FluentComponentExtension} for {@link VerticalLayout}.
 *
 * @param <C>
 *          the component type
 * @param <F>
 *          the fluent component type
 * @see VerticalLayout
 */
@SuppressWarnings("unchecked")
public interface FluentVerticalLayoutComponentExtension<C extends Component, F extends FluentComponent<C, F>>
  extends
    FluentComponentExtension<C, F>
{

  /**
   * Adds this component to the given {@link VerticalLayout}.
   *
   * @param container
   *          the target container
   * @param layoutConfigurer
   *          the layout configurer; may be null
   * @return this
   * @see VerticalLayout#add(Component...)
   */
  default F addTo(VerticalLayout container, Consumer<FluentVerticalLayoutConfig> layoutConfigurer)
  {
    container.add(get());
    if (layoutConfigurer != null)
      layoutConfigurer.accept(new FluentVerticalLayoutConfig(container, get()));
    return (F) this;
  }

  /**
   * Adds this component to the given {@link VerticalLayout} at the specified index.
   *
   * @param container
   *          the target container
   * @param index
   *          the index where to insert this component
   * @param layoutConfigurer
   *          the layout configurer; may be null
   * @return this
   * @see VerticalLayout#addComponentAtIndex(int, Component)
   */
  default F addToAt(VerticalLayout container, int index,
      Consumer<FluentVerticalLayoutConfig> layoutConfigurer)
  {
    container.addComponentAtIndex(index, get());
    if (layoutConfigurer != null)
      layoutConfigurer.accept(new FluentVerticalLayoutConfig(container, get()));
    return (F) this;
  }

  /**
   * Replaces the specified old component in the given {@link VerticalLayout} with this component.
   *
   * @param container
   *          the target container
   * @param oldComponent
   *          the old component to be replaced
   * @param layoutConfigurer
   *          the layout configurer; may be null
   * @return this
   * @see VerticalLayout#replace(Component, Component)
   */
  default F replace(VerticalLayout container, Component oldComponent,
      Consumer<FluentVerticalLayoutConfig> layoutConfigurer)
  {
    container.replace(oldComponent, get());
    if (layoutConfigurer != null)
      layoutConfigurer.accept(new FluentVerticalLayoutConfig(container, get()));
    return (F) this;
  }

}
