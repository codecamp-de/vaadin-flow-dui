package de.codecamp.vaadin.fluent.layouts;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import de.codecamp.vaadin.fluent.FluentLayoutConfig;


/**
 * {@link FluentLayoutConfig} for {@link VerticalLayout}.
 */
public class FluentVerticalLayoutConfig
  extends
    FluentFlexComponentLayoutConfig<VerticalLayout, FluentVerticalLayoutConfig>
{

  /**
   * Constructs a new instance for the given container and content components.
   *
   * @param container
   *          the container
   * @param components
   *          the content components
   * @throws IllegalArgumentException
   *           if the container is not actually the parent of all content components
   */
  public FluentVerticalLayoutConfig(VerticalLayout container, Component... components)
  {
    super(container, components);
  }


  @Override
  protected boolean isHorizontal()
  {
    return false;
  }

}
