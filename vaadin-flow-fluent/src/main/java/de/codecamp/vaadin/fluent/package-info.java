@FluentMappingSet( //
    basePackage = "de.codecamp.vaadin.fluent",
    staticFactoryClass = "Fluent")
@FluentMapping( //
    componentType = Component.class,
    mapped = {@FluentMethod(name = "scrollIntoView")})
@FluentMapping( //
    componentType = AbstractField.class,
    typeVars = {TYPEVAR_COMPONENT, TYPEVAR_VALUE})
// TODO doesn't work yet
//@FluentMapping( //
//    componentType = AbstractCompositeField.class,
//    typeVars = {"X", TYPEVAR_COMPONENT, TYPEVAR_VALUE})
@FluentMapping( //
    componentType = AbstractSinglePropertyField.class,
    typeVars = {TYPEVAR_COMPONENT, TYPEVAR_VALUE})
@FluentMapping( //
    componentType = AttachNotifier.class)
@FluentMapping( //
    componentType = BlurNotifier.class,
    typeVars = {TYPEVAR_COMPONENT})
@FluentMapping( //
    componentType = ClickNotifier.class,
    typeVars = {TYPEVAR_COMPONENT},
    mapped = {@FluentMethod(name = "addClickShortcut", fluent = "clickShortcut")})
@FluentMapping( //
    componentType = Composite.class,
    typeVars = {"X"})
@FluentMapping( //
    componentType = CompositionNotifier.class)
@FluentMapping( //
    componentType = DetachNotifier.class)
@FluentMapping( //
    componentType = Focusable.class,
    typeVars = TYPEVAR_COMPONENT,
    mapped = { //
        @FluentMethod(name = "focus"), //
        @FluentMethod(name = "blur"),
        @FluentMethod(name = "addFocusShortcut", fluent = "focusShortcut")})
@FluentMapping( //
    componentType = HasAriaLabel.class)
@FluentMapping( //
    componentType = FocusNotifier.class,
    typeVars = {TYPEVAR_COMPONENT})
@FluentMapping( //
    componentType = HasComponents.class,
    mapped = { //
        @FluentMethod(name = "add"), //
        @FluentMethod(name = "remove"), //
        @FluentMethod(name = "removeAll")})
@FluentMapping( //
    componentType = HasDataProvider.class,
    typeVars = {TYPEVAR_ITEM})
@FluentMapping( //
    componentType = HasDataGenerators.class,
    typeVars = {TYPEVAR_ITEM})
@FluentMapping( //
    componentType = HasDataView.class,
    typeVars = {TYPEVAR_ITEM, TYPEVAR_FILTER, TYPEVAR_DATAVIEW})
@FluentMapping( //
    componentType = HasElement.class)
@FluentMapping( //
    componentType = HasEnabled.class)
@FluentMapping( //
    componentType = HasHelper.class,
    mapped = { //
        @FluentMethod(name = "setHelperText", fluent = "helper", withTranslation = true),
        @FluentMethod(name = "setHelperComponent", fluent = "helper")})
@FluentMapping( //
    componentType = HasHierarchicalDataProvider.class,
    typeVars = {TYPEVAR_ITEM},
    mapped = {@FluentMethod(name = "setItems")})
@FluentMapping( //
    componentType = HasItemComponents.class,
    typeVars = {TYPEVAR_ITEM},
    mapped = { //
        @FluentMethod(name = "addComponents", fluent = "addAfter"),
        @FluentMethod(name = "prependComponents", fluent = "addBefore")})
@FluentMapping( //
    componentType = HasItems.class,
    typeVars = {TYPEVAR_ITEM})
@FluentMapping( //
    componentType = HasLabel.class)
@FluentMapping( //
    componentType = HasLazyDataView.class,
    typeVars = {TYPEVAR_ITEM, TYPEVAR_FILTER, TYPEVAR_DATAVIEW},
    mapped = {@FluentMethod(name = "setItems")})
@FluentMapping( //
    componentType = HasListDataView.class,
    typeVars = {TYPEVAR_ITEM, TYPEVAR_DATAVIEW})
@FluentMapping( //
    componentType = HasOrderedComponents.class,
    mapped = {@FluentMethod(name = "replace")})
@FluentMapping( //
    componentType = HasPlaceholder.class)
@FluentMapping( //
    componentType = HasSize.class)
@FluentMapping( //
    componentType = HasStyle.class)
@FluentMapping( //
    componentType = HasText.class,
    mapped = { //
        @FluentMethod(name = "setText", withTranslation = true),
        @FluentMethod(name = "setWhiteSpace", expandEnum = false)})
@FluentMapping( //
    componentType = HasTheme.class)
@FluentMapping( //
    componentType = HasValidation.class)
@FluentMapping(
    componentType = HasValidator.class, //
    typeVars = {TYPEVAR_VALUE})
@FluentMapping( //
    componentType = HasValue.class,
    typeVars = {TYPEVAR_EVENT, TYPEVAR_VALUE},
    mapped = { //
        @FluentMethod(name = "clear")})
@FluentMapping( //
    componentType = HasValueAndElement.class,
    typeVars = {TYPEVAR_EVENT, TYPEVAR_VALUE})
@FluentMapping( //
    componentType = HasValueChangeMode.class)
@FluentMapping( //
    componentType = InputNotifier.class)
@FluentMapping( //
    componentType = KeyNotifier.class)
@FluentMapping( //
    componentType = MultiSelect.class,
    typeVars = {TYPEVAR_COMPONENT, TYPEVAR_ITEM})
@FluentMapping( //
    componentType = SingleSelect.class,
    typeVars = {TYPEVAR_COMPONENT, TYPEVAR_ITEM},
    innerTypeVarMapping = {TYPEVAR_VALUE + ":" + TYPEVAR_ITEM})
@FluentMapping( //
    componentType = SortNotifier.class,
    typeVars = {TYPEVAR_COMPONENT, TYPEVAR_SORT})
/*
 * Shared
 */
@FluentMapping( //
    componentType = HasAllowedCharPattern.class,
    targetPackage = "shared")
@FluentMapping( //
    componentType = HasAutoOpen.class,
    targetPackage = "shared")
@FluentMapping( //
    componentType = HasClearButton.class,
    targetPackage = "shared")
@FluentMapping( //
    componentType = HasClientValidation.class,
    targetPackage = "shared")
@FluentMapping( //
    componentType = HasOverlayClassName.class, //
    targetPackage = "shared")
@FluentMapping( //
    componentType = HasPrefix.class, //
    targetPackage = "shared",
    mapped = {@FluentMethod(name = "setPrefixComponent", fluent = "prefix")})
@FluentMapping( //
    componentType = HasPrefixAndSuffix.class, //
    targetPackage = "shared")
@FluentMapping( //
    componentType = HasSuffix.class, //
    targetPackage = "shared",
    mapped = {@FluentMethod(name = "setSuffixComponent", fluent = "suffix")})
@FluentMapping( //
    componentType = HasThemeVariant.class,
    targetPackage = "shared",
    typeVars = {TYPEVAR_VARIANT},
    mapped = { //
        @FluentMethod(name = "addThemeVariants"), //
        @FluentMethod(name = "removeThemeVariants")})
@FluentMapping( //
    componentType = HasTooltip.class, //
    targetPackage = "shared",
    mapped = {@FluentMethod(name = "setTooltipText", fluent = "tooltip")})
@FluentMapping( //
    componentType = HasValidationProperties.class, //
    targetPackage = "shared")
@FluentMapping( //
    componentType = InputField.class, //
    targetPackage = "shared",
    typeVars = {TYPEVAR_EVENT, TYPEVAR_VALUE})
@FluentMapping(
    componentType = Tooltip.class,
    targetPackage = "shared",
    autoStaticFactories = false,
    staticFactories = {
        "FluentTooltip tooltipFor(Component component) { return new FluentTooltip(Tooltip.forComponent(component)); }"})
/*
 * Data Entry
 */
@FluentMapping( //
    componentType = AbstractNumberField.class,
    targetPackage = "dataentry",
    typeVars = {TYPEVAR_COMPONENT, TYPEVAR_VALUE})
@FluentMapping( //
    componentType = BigDecimalField.class,
    targetPackage = "dataentry",
    i18nUtils = BigDecimalFieldI18nUtils.class,
    themes = { //
        @ThemeGroup(variant = {"LUMO_SMALL", ""}, fluent = {"medium"}),
        @ThemeGroup(
            variant = {"", "LUMO_ALIGN_CENTER", "LUMO_ALIGN_RIGHT"},
            fluent = {"alignLeft"}),
        @ThemeGroup(variant = "LUMO_HELPER_ABOVE_FIELD"), //
    })
@FluentMapping( //
    componentType = Checkbox.class,
    targetPackage = "dataentry",
    i18nUtils = CheckboxI18nUtils.class,
    mapped = {@FluentMethod(name = "setLabelComponent", fluent = "label")})
@FluentMapping( //
    componentType = CheckboxGroup.class,
    targetPackage = "dataentry",
    typeVars = {TYPEVAR_ITEM},
    i18nUtils = CheckboxGroupI18nUtils.class,
    mapped = {@FluentMethod(name = "setRenderer", fluent = "itemRenderer")},
    themes = { //
        @ThemeGroup(variant = "LUMO_VERTICAL"), //
        @ThemeGroup(variant = "LUMO_HELPER_ABOVE_FIELD"), //
    })
@FluentMapping( //
    componentType = ComboBox.class,
    targetPackage = "dataentry",
    typeVars = {TYPEVAR_ITEM},
    i18nUtils = ComboBoxI18nUtils.class,
    innerTypeVarMapping = {TYPEVAR_VALUE + ":" + TYPEVAR_ITEM},
    themes = { //
        @ThemeGroup(variant = {"LUMO_SMALL", ""}, fluent = {"medium"}),
        @ThemeGroup(variant = {"LUMO_ALIGN_LEFT", "LUMO_ALIGN_CENTER", "LUMO_ALIGN_RIGHT"}),
        @ThemeGroup(variant = "LUMO_HELPER_ABOVE_FIELD"), //
    })
@FluentMapping( //
    componentType = ComboBoxBase.class,
    targetPackage = "dataentry",
    typeVars = {TYPEVAR_COMPONENT, TYPEVAR_ITEM, TYPEVAR_VALUE},
    mapped = { //
        @FluentMethod(name = "setClassNameGenerator", fluent = "itemClassNameGenerator"),
        @FluentMethod(name = "setRenderer", fluent = "itemRenderer"),
        @FluentMethod(name = "setItemsWithFilterConverter", fluent = "items")})
@FluentMapping( //
    componentType = DatePicker.class,
    targetPackage = "dataentry",
    i18nUtils = DatePickerI18nUtils.class,
    mapped = {@FluentMethod(name = "setName", skip = true)},
    themes = { //
        @ThemeGroup(variant = {"LUMO_SMALL", ""}, fluent = {"medium"}),
        @ThemeGroup(variant = {"LUMO_ALIGN_LEFT", "LUMO_ALIGN_CENTER", "LUMO_ALIGN_RIGHT"}),
        @ThemeGroup(variant = "LUMO_HELPER_ABOVE_FIELD"), //
    })
@FluentMapping( //
    componentType = DateTimePicker.class,
    targetPackage = "dataentry",
    i18nUtils = DateTimePickerI18nUtils.class,
    themes = { //
        @ThemeGroup(variant = {"LUMO_SMALL", ""}, fluent = {"medium"}),
        @ThemeGroup(variant = {"LUMO_ALIGN_LEFT", "LUMO_ALIGN_CENTER", "LUMO_ALIGN_RIGHT"}),
        @ThemeGroup(variant = "LUMO_HELPER_ABOVE_FIELD"), //
    })
@FluentMapping( //
    componentType = EmailField.class,
    targetPackage = "dataentry",
    i18nUtils = EmailFieldI18nUtils.class,
    themes = { //
        @ThemeGroup(variant = {"LUMO_SMALL", ""}, fluent = {"medium"}),
        @ThemeGroup(
            variant = {"", "LUMO_ALIGN_CENTER", "LUMO_ALIGN_RIGHT"},
            fluent = {"alignLeft"}),
        @ThemeGroup(variant = "LUMO_HELPER_ABOVE_FIELD"), //
    })
@FluentMapping( //
    componentType = HasAutocapitalize.class, //
    targetPackage = "dataentry")
@FluentMapping( //
    componentType = HasAutocomplete.class, //
    targetPackage = "dataentry")
@FluentMapping( //
    componentType = HasAutocorrect.class, //
    targetPackage = "dataentry")
@FluentMapping( //
    componentType = IntegerField.class,
    targetPackage = "dataentry",
    i18nUtils = IntegerFieldI18nUtils.class,
    themes = { //
        @ThemeGroup(variant = {"LUMO_SMALL", ""}, fluent = {"medium"}),
        @ThemeGroup(
            variant = {"", "LUMO_ALIGN_CENTER", "LUMO_ALIGN_RIGHT"},
            fluent = {"alignLeft"}),
        @ThemeGroup(variant = "LUMO_HELPER_ABOVE_FIELD"), //
    })
@FluentMapping( //
    componentType = ListBox.class,
    targetPackage = "dataentry",
    typeVars = {TYPEVAR_ITEM},
    innerTypeVarMapping = {TYPEVAR_VALUE + ":" + TYPEVAR_ITEM})
@FluentMapping( //
    componentType = ListBoxBase.class,
    targetPackage = "dataentry",
    typeVars = {TYPEVAR_COMPONENT, TYPEVAR_ITEM, TYPEVAR_VALUE},
    mapped = {@FluentMethod(name = "setRenderer", fluent = "itemRenderer")})
@FluentMapping( //
    componentType = MessageInput.class,
    targetPackage = "dataentry",
    i18nUtils = MessageInputI18nUtils.class)
@FluentMapping( //
    componentType = MultiSelectComboBox.class,
    targetPackage = "dataentry",
    typeVars = {TYPEVAR_ITEM},
    i18nUtils = MultiSelectComboBoxI18nUtils.class,
    themes = { //
        @ThemeGroup(variant = {"LUMO_SMALL", ""}, fluent = {"medium"}),
        @ThemeGroup(variant = {"LUMO_ALIGN_LEFT", "LUMO_ALIGN_CENTER", "LUMO_ALIGN_RIGHT"}),
        @ThemeGroup(variant = "LUMO_HELPER_ABOVE_FIELD"), //
    })
@FluentMapping( //
    componentType = MultiSelectListBox.class,
    targetPackage = "dataentry",
    typeVars = {TYPEVAR_ITEM})
@FluentMapping( //
    componentType = NumberField.class,
    targetPackage = "dataentry",
    i18nUtils = NumberFieldI18nUtils.class,
    themes = { //
        @ThemeGroup(variant = {"LUMO_SMALL", ""}, fluent = {"medium"}),
        @ThemeGroup(
            variant = {"", "LUMO_ALIGN_CENTER", "LUMO_ALIGN_RIGHT"},
            fluent = {"alignLeft"}),
        @ThemeGroup(variant = "LUMO_HELPER_ABOVE_FIELD"), //
    })
@FluentMapping( //
    componentType = PasswordField.class,
    targetPackage = "dataentry",
    i18nUtils = PasswordFieldI18nUtils.class,
    themes = { //
        @ThemeGroup(variant = {"LUMO_SMALL", ""}, fluent = {"medium"}),
        @ThemeGroup(
            variant = {"", "LUMO_ALIGN_CENTER", "LUMO_ALIGN_RIGHT"},
            fluent = {"alignLeft"}),
        @ThemeGroup(variant = "LUMO_HELPER_ABOVE_FIELD"), //
    })
@FluentMapping( //
    componentType = RadioButtonGroup.class,
    targetPackage = "dataentry",
    typeVars = {TYPEVAR_ITEM},
    innerTypeVarMapping = {TYPEVAR_VALUE + ":" + TYPEVAR_ITEM},
    i18nUtils = RadioButtonGroupI18nUtils.class,
    mapped = {@FluentMethod(name = "setRenderer", fluent = "itemRenderer")},
    themes = { //
        @ThemeGroup(variant = "LUMO_VERTICAL"), //
        @ThemeGroup(variant = "LUMO_HELPER_ABOVE_FIELD"), //
    })
@FluentMapping( //
    componentType = Select.class,
    targetPackage = "dataentry",
    typeVars = {TYPEVAR_ITEM},
    innerTypeVarMapping = {TYPEVAR_VALUE + ":" + TYPEVAR_ITEM},
    i18nUtils = SelectI18nUtils.class,
    mapped = { //
        @FluentMethod(name = "setRenderer", fluent = "itemRenderer"),
        @FluentMethod(name = "setTextRenderer", fluent = "itemTextRenderer")},
    themes = { //
        @ThemeGroup(variant = {"LUMO_SMALL", ""}, fluent = {"medium"}),
        @ThemeGroup(variant = {"LUMO_ALIGN_LEFT", "LUMO_ALIGN_CENTER", "LUMO_ALIGN_RIGHT"}),
        @ThemeGroup(variant = "LUMO_HELPER_ABOVE_FIELD"), //
    })
@FluentMapping( //
    componentType = TextArea.class,
    targetPackage = "dataentry",
    i18nUtils = TextAreaI18nUtils.class,
    themes = { //
        @ThemeGroup(variant = {"LUMO_SMALL", ""}, fluent = {"medium"}),
        @ThemeGroup(
            variant = {"", "LUMO_ALIGN_CENTER", "LUMO_ALIGN_RIGHT"},
            fluent = {"alignLeft"}),
        @ThemeGroup(variant = "LUMO_HELPER_ABOVE_FIELD"), //
    })
@FluentMapping( //
    componentType = TextField.class,
    targetPackage = "dataentry",
    i18nUtils = TextFieldI18nUtils.class,
    themes = { //
        @ThemeGroup(variant = {"LUMO_SMALL", ""}, fluent = {"medium"}),
        @ThemeGroup(
            variant = {"", "LUMO_ALIGN_CENTER", "LUMO_ALIGN_RIGHT"},
            fluent = {"alignLeft"}),
        @ThemeGroup(variant = "LUMO_HELPER_ABOVE_FIELD"), //
    })
@FluentMapping( //
    componentType = TextFieldBase.class,
    targetPackage = "dataentry",
    typeVars = {TYPEVAR_COMPONENT, TYPEVAR_VALUE})
@FluentMapping( //
    componentType = TimePicker.class,
    targetPackage = "dataentry",
    i18nUtils = TimePickerI18nUtils.class,
    themes = { //
        @ThemeGroup(variant = {"LUMO_SMALL", ""}, fluent = {"medium"}),
        @ThemeGroup(variant = {"LUMO_ALIGN_LEFT", "LUMO_ALIGN_CENTER", "LUMO_ALIGN_RIGHT"}),
        @ThemeGroup(variant = "LUMO_HELPER_ABOVE_FIELD"), //
    })
@FluentMapping( //
    componentType = Upload.class,
    targetPackage = "dataentry",
    i18nUtils = UploadI18nUtils.class)
/*
 * Layouts
 */
@FluentMapping( //
    componentType = AppLayout.class,
    targetPackage = "layouts",
    i18nUtils = AppLayoutI18nUtils.class)
@FluentMapping( //
    componentType = FlexComponent.class,
    targetPackage = "layouts",
    mapped = { //
        @FluentMethod(name = "setJustifyContentMode", fluent = "justifyContent"),
        @FluentMethod(name = "setAlignSelf", skip = true),
        @FluentMethod(name = "setFlexGrow", skip = true),
        @FluentMethod(name = "setFlexShrink", skip = true)})
@FluentMapping( //
    componentType = FlexLayout.class,
    targetPackage = "layouts",
    mapped = { //
        @FluentMethod(name = "setFlexBasis", skip = true),
        @FluentMethod(name = "setOrder", skip = true)})
@FluentMapping( //
    componentType = FormItem.class,
    targetPackage = "layouts")
@FluentMapping( //
    componentType = FormLayout.class,
    targetPackage = "layouts",
    mapped = {@FluentMethod(name = "setColspan", skip = true)})
@FluentMapping( //
    componentType = HorizontalLayout.class,
    targetPackage = "layouts",
    mapped = {
        @FluentMethod(
            name = "setDefaultVerticalComponentAlignment",
            fluent = {"defaultVerticalComponentAlignment", "defaultItemAlignment"}),
        @FluentMethod(name = "setVerticalComponentAlignment", skip = true),
        @FluentMethod(name = "setAlignSelf", skip = true)})
@FluentMapping( //
    componentType = Scroller.class,
    targetPackage = "layouts",
    mapped = { //
        @FluentMethod(name = "scrollToTop"), //
        @FluentMethod(name = "scrollToBottom")},
    themes = {@ThemeGroup(variant = "LUMO_OVERFLOW_INDICATORS"), //
    })
@FluentMapping( //
    componentType = SplitLayout.class,
    targetPackage = "layouts",
    themes = {@ThemeGroup(variant = {"LUMO_MINIMAL", "LUMO_SMALL", ""}, fluent = {"medium"}), //
    })
@FluentMapping( //
    componentType = ThemableLayout.class,
    targetPackage = "layouts")
@FluentMapping( //
    componentType = VerticalLayout.class,
    targetPackage = "layouts",
    mapped = {
        @FluentMethod(
            name = "setDefaultHorizontalComponentAlignment",
            fluent = {"defaultHorizontalComponentAlignment", "defaultItemAlignment"}),
        @FluentMethod(name = "setHorizontalComponentAlignment", skip = true),
        @FluentMethod(name = "setAlignSelf", skip = true)})
/*
 * Visualization & Interaction
 */
@FluentMapping( //
    componentType = AbstractIcon.class,
    targetPackage = "visandint",
    typeVars = {TYPEVAR_COMPONENT})
@FluentMapping( //
    componentType = Accordion.class,
    targetPackage = "visandint")
@FluentMapping( //
    componentType = AccordionPanel.class,
    targetPackage = "visandint")
@FluentMapping( //
    componentType = Avatar.class,
    targetPackage = "visandint",
    i18nUtils = AvatarI18nUtils.class,
    mapped = {@FluentMethod(name = "setImageResource", fluent = "image")},
    themes = { //
        @ThemeGroup(
            variant = {"LUMO_XSMALL", "LUMO_SMALL", "", "LUMO_LARGE", "LUMO_XLARGE"},
            fluent = {"medium"}), //
    })
@FluentMapping( //
    componentType = AvatarGroup.class,
    targetPackage = "visandint",
    i18nUtils = AvatarGroupI18nUtils.class,
    themes = { //
        @ThemeGroup(
            variant = {"LUMO_XSMALL", "LUMO_SMALL", "", "LUMO_LARGE", "LUMO_XLARGE"},
            fluent = {"medium"}), //
    })
@FluentMapping( //
    componentType = Button.class,
    targetPackage = "visandint",
    createBase = true,
    mapped = { //
        @FluentMethod(name = "click", fluent = "click"),
        @FluentMethod(name = "clickInClient", fluent = "clickInClient")},
    themes = { //
        @ThemeGroup(variant = "LUMO_ICON"),
        @ThemeGroup(variant = {"LUMO_SMALL", "", "LUMO_LARGE"}, fluent = {"medium"}),
        @ThemeGroup(
            variant = {"LUMO_PRIMARY", "", "LUMO_TERTIARY", "LUMO_TERTIARY_INLINE"},
            fluent = {"secondary"}),
        @ThemeGroup(
            variant = {"", "LUMO_SUCCESS", "LUMO_WARNING", "LUMO_ERROR", "LUMO_CONTRAST"},
            fluent = {"standard"}), //
    })
@FluentMapping( //
    componentType = Details.class,
    targetPackage = "visandint",
    createBase = true,
    mapped = {@FluentMethod(name = "setSummaryText", fluent = "summary", withTranslation = true)},
    themes = { //
        @ThemeGroup(variant = "FILLED"), //
        @ThemeGroup(variant = "REVERSE"), @ThemeGroup(variant = {"SMALL", ""}, fluent = {"medium"}), //
    })
@FluentMapping( //
    componentType = Dialog.class,
    targetPackage = "visandint",
    createBase = true,
    themes = { //
        @ThemeGroup(variant = "LUMO_NO_PADDING"), //
    })
@FluentMapping( //
    componentType = FontIcon.class,
    targetPackage = "visandint")
@FluentMapping( //
    componentType = Grid.class,
    targetPackage = "visandint",
    typeVars = {TYPEVAR_ITEM},
    createBase = true,
    ignoredComponentInterfaces = {Focusable.class, SortNotifier.class},
    mapped = { //
        @FluentMethod(name = "configureBeanType", fluent = "beanType"),
        @FluentMethod(name = "setSelectionDragDetails", skip = true),
        @FluentMethod(name = "setEmptyStateText", fluent = "emptyState"),
        @FluentMethod(name = "setEmptyStateComponent", fluent = "emptyState")},
    themeVariantType = GridVariant.class,
    themes = { //
        @ThemeGroup(variant = "LUMO_NO_BORDER"), //
        @ThemeGroup(variant = "LUMO_NO_ROW_BORDERS"), //
        @ThemeGroup(variant = "LUMO_COLUMN_BORDERS"), //
        @ThemeGroup(variant = "LUMO_ROW_STRIPES"), //
        @ThemeGroup(variant = "LUMO_COMPACT"), //
        @ThemeGroup(variant = "LUMO_WRAP_CELL_CONTENT"), //
    },
    staticFactories = {
        "<ITEM> FluentGrid<ITEM> grid(Class<ITEM> itemType) { return grid(itemType, false); }",
        "<ITEM> FluentGrid<ITEM> grid(Class<ITEM> itemType, boolean autoCreateColumns) { return new FluentGrid<ITEM>(new Grid<>(itemType, autoCreateColumns)); }"})
// HasGridMenuItems is not public
//@FluentMapping( //
//    componentType = HasGridMenuItems.class,
//    targetPackage = FLUENT_VISANDINT_PACKAGE,
//    typeVars = {TYPEVAR_ITEM})
@FluentMapping( //
    componentType = HasMenuItems.class,
    targetPackage = "visandint")
@FluentMapping( //
    componentType = Icon.class,
    targetPackage = "visandint",
    mapped = {@FluentMethod(name = "setIcon", expandEnum = false)},
    staticFactories = { //
        "FluentIcon icon(VaadinIcon icon) { return new FluentIcon(icon); }",
        "FluentIcon icon(LumoIcon icon) { return new FluentIcon(icon); }"})
@FluentMapping( //
    componentType = MenuBar.class,
    targetPackage = "visandint",
    i18nUtils = MenuBarI18nUtils.class,
    mapped = {@FluentMethod(name = "setTooltipText", skip = true)},
    themeVariantType = MenuBarVariant.class,
    themes = { //
        @ThemeGroup(variant = "LUMO_ICON"), //
        @ThemeGroup(variant = "LUMO_END_ALIGNED"),
        @ThemeGroup(variant = {"LUMO_SMALL", "", "LUMO_LARGE"}, fluent = {"medium"}),
        @ThemeGroup(variant = "LUMO_DROPDOWN_INDICATORS"),
        @ThemeGroup(
            variant = {"LUMO_PRIMARY", "", "LUMO_TERTIARY", "LUMO_TERTIARY_INLINE"},
            fluent = {"secondary"}),
        @ThemeGroup(variant = {"", "LUMO_CONTRAST"}, fluent = {"standard"}), //
    })
@FluentMapping( //
    componentType = MessageList.class,
    targetPackage = "visandint")
@FluentMapping( //
    componentType = Notification.class,
    targetPackage = "visandint",
    mapped = { //
        @FluentMethod(name = "text", withTranslation = true), //
        @FluentMethod(name = "open"), //
        @FluentMethod(name = "close")},
    themes = { //
        @ThemeGroup(
            variant = {"", "LUMO_PRIMARY", "LUMO_SUCCESS", "LUMO_WARNING", "LUMO_ERROR",
                "LUMO_CONTRAST"},
            fluent = {"standard"}), //
    })
@FluentMapping( //
    componentType = Popover.class,
    targetPackage = "visandint",
    mapped = { //
        @FluentMethod(name = "setFor", fluent = "target"), //
        @FluentMethod(name = "open"), //
        @FluentMethod(name = "close")},
    themes = { //
        @ThemeGroup(variant = "ARROW"), //
        @ThemeGroup(variant = "LUMO_NO_PADDING"), //
    })
@FluentMapping( //
    componentType = ProgressBar.class,
    targetPackage = "visandint",
    themes = { //
        @ThemeGroup(
            variant = {"", "LUMO_SUCCESS", "LUMO_ERROR", "LUMO_CONTRAST"},
            fluent = {"standard"}), //
    })
@FluentMapping( //
    componentType = SideNav.class,
    targetPackage = "visandint",
    i18nUtils = SideNavI18nUtils.class)
@FluentMapping( //
    componentType = SideNavItem.class,
    targetPackage = "visandint")
@FluentMapping( //
    componentType = SvgIcon.class,
    targetPackage = "visandint")
@FluentMapping( //
    componentType = Tab.class,
    targetPackage = "visandint",
    themes = { //
        @ThemeGroup(variant = "LUMO_ICON_ON_TOP", fluent = "iconOnTop"), //
    })
@FluentMapping( //
    componentType = Tabs.class,
    targetPackage = "visandint",
    themes = { //
        @ThemeGroup(variant = "LUMO_ICON_ON_TOP"), //
        @ThemeGroup(variant = "LUMO_CENTERED"), //
        @ThemeGroup(variant = "LUMO_HIDE_SCROLL_BUTTONS"),
        @ThemeGroup(variant = "LUMO_EQUAL_WIDTH_TABS"),
        @ThemeGroup(variant = {"LUMO_MINIMAL", "LUMO_SMALL", ""}, fluent = {"medium"}), //
    })
@FluentMapping( //
    componentType = TabSheet.class,
    targetPackage = "visandint",
    mapped = { //
        @FluentMethod(name = "add"), //
        @FluentMethod(name = "remove")},
    themes = { //
        @ThemeGroup(variant = "LUMO_TABS_ICON_ON_TOP"), //
        @ThemeGroup(variant = "LUMO_TABS_CENTERED"),
        @ThemeGroup(variant = "LUMO_TABS_HIDE_SCROLL_BUTTONS"),
        @ThemeGroup(variant = "LUMO_TABS_EQUAL_WIDTH_TABS"), //
        @ThemeGroup(variant = "LUMO_BORDERED"), //
        @ThemeGroup(variant = "LUMO_NO_PADDING"),
        @ThemeGroup(variant = {"LUMO_TABS_MINIMAL", "LUMO_TABS_SMALL", ""}, fluent = {"medium"}), //
    })
@FluentMapping( //
    componentType = TreeGrid.class,
    targetPackage = "visandint",
    typeVars = {TYPEVAR_ITEM},
    staticFactories = {
        "<ITEM> FluentTreeGrid<ITEM> treeGrid(Class<ITEM> itemType) { return treeGrid(itemType, false); }",
        "<ITEM> FluentTreeGrid<ITEM> treeGrid(Class<ITEM> itemType, boolean autoCreateColumns) { return new FluentTreeGrid<ITEM>(new TreeGrid<>(itemType, autoCreateColumns)); }"})
@FluentMapping( //
    componentType = VirtualList.class,
    targetPackage = "visandint",
    typeVars = {TYPEVAR_ITEM},
    mapped = { //
        @FluentMethod(name = "setRenderer", fluent = "itemRenderer"), //
        @FluentMethod(name = "scrollToIndex"), //
        @FluentMethod(name = "scrollToStart"), //
        @FluentMethod(name = "scrollToEnd")})
/*
 * HTML
 */
@FluentMapping( //
    componentType = HtmlComponent.class,
    createBase = true,
    staticFactories = {
        "FluentHtmlComponent htmlComponent(String tagName) { return new FluentHtmlComponent(tagName); }"})
@FluentMapping( //
    componentType = HtmlContainer.class,
    createBase = true,
    staticFactories = {
        "FluentHtmlContainer htmlContainer(String tagName) { return new FluentHtmlContainer(tagName); }"})
@FluentMapping( //
    componentType = Anchor.class,
    targetPackage = "html")
@FluentMapping( //
    componentType = Div.class,
    targetPackage = "html")
@FluentMapping( //
    componentType = FieldSet.class,
    targetPackage = "html")
@FluentMapping( //
    componentType = H1.class,
    targetPackage = "html")
@FluentMapping( //
    componentType = H2.class,
    targetPackage = "html")
@FluentMapping( //
    componentType = H3.class,
    targetPackage = "html")
@FluentMapping( //
    componentType = H4.class,
    targetPackage = "html")
@FluentMapping( //
    componentType = H5.class,
    targetPackage = "html")
@FluentMapping( //
    componentType = H6.class,
    targetPackage = "html")
@FluentMapping( //
    componentType = Hr.class,
    targetPackage = "html")
@FluentMapping( //
    componentType = Image.class,
    targetPackage = "html")
@FluentMapping( //
    componentType = Legend.class,
    targetPackage = "html")
@FluentMapping( //
    componentType = Span.class,
    targetPackage = "html")
/*
 * Misc
 */
@FluentMapping( //
    componentType = RouterLink.class,
    mapped = { //
        @FluentMethod(name = "setRoute", params = {Router.class, Class.class}, skip = true),
        @FluentMethod(
            name = "setRoute",
            params = {Router.class, Class.class, Object.class},
            skip = true),
        @FluentMethod(
            name = "setRoute",
            params = {Router.class, Class.class, RouteParameters.class},
            skip = true)})
/*
 * CC Base
 */
@FluentMapping( //
    componentType = de.codecamp.vaadin.base.AbstractCompositeField.class,
    targetPackage = "base")
@FluentMapping( //
    componentType = de.codecamp.vaadin.base.Composite.class,
    targetPackage = "base")
@FluentMapping( //
    componentType = HasSingleComponent.class,
    targetPackage = "base")
package de.codecamp.vaadin.fluent;


import static de.codecamp.vaadin.fluent.annotations.FluentMapping.TYPEVAR_COMPONENT;
import static de.codecamp.vaadin.fluent.annotations.FluentMapping.TYPEVAR_DATAVIEW;
import static de.codecamp.vaadin.fluent.annotations.FluentMapping.TYPEVAR_EVENT;
import static de.codecamp.vaadin.fluent.annotations.FluentMapping.TYPEVAR_FILTER;
import static de.codecamp.vaadin.fluent.annotations.FluentMapping.TYPEVAR_ITEM;
import static de.codecamp.vaadin.fluent.annotations.FluentMapping.TYPEVAR_SORT;
import static de.codecamp.vaadin.fluent.annotations.FluentMapping.TYPEVAR_VALUE;
import static de.codecamp.vaadin.fluent.annotations.FluentMapping.TYPEVAR_VARIANT;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.AbstractSinglePropertyField;
import com.vaadin.flow.component.AttachNotifier;
import com.vaadin.flow.component.BlurNotifier;
import com.vaadin.flow.component.ClickNotifier;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.CompositionNotifier;
import com.vaadin.flow.component.DetachNotifier;
import com.vaadin.flow.component.FocusNotifier;
import com.vaadin.flow.component.Focusable;
import com.vaadin.flow.component.HasAriaLabel;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.HasEnabled;
import com.vaadin.flow.component.HasHelper;
import com.vaadin.flow.component.HasLabel;
import com.vaadin.flow.component.HasOrderedComponents;
import com.vaadin.flow.component.HasPlaceholder;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.HasText;
import com.vaadin.flow.component.HasTheme;
import com.vaadin.flow.component.HasValidation;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.HasValueAndElement;
import com.vaadin.flow.component.HtmlComponent;
import com.vaadin.flow.component.HtmlContainer;
import com.vaadin.flow.component.InputNotifier;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.accordion.AccordionPanel;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.avatar.AvatarGroup;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.checkbox.CheckboxGroup;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.combobox.ComboBoxBase;
import com.vaadin.flow.component.combobox.MultiSelectComboBox;
import com.vaadin.flow.component.contextmenu.HasMenuItems;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.FormItem;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.FieldSet;
import com.vaadin.flow.component.html.FieldSet.Legend;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.html.H6;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.AbstractIcon;
import com.vaadin.flow.component.icon.FontIcon;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.SvgIcon;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.listbox.ListBoxBase;
import com.vaadin.flow.component.listbox.MultiSelectListBox;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.menubar.MenuBarVariant;
import com.vaadin.flow.component.messages.MessageInput;
import com.vaadin.flow.component.messages.MessageList;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.orderedlayout.ThemableLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.popover.Popover;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.shared.HasAllowedCharPattern;
import com.vaadin.flow.component.shared.HasAutoOpen;
import com.vaadin.flow.component.shared.HasClearButton;
import com.vaadin.flow.component.shared.HasClientValidation;
import com.vaadin.flow.component.shared.HasOverlayClassName;
import com.vaadin.flow.component.shared.HasPrefix;
import com.vaadin.flow.component.shared.HasSuffix;
import com.vaadin.flow.component.shared.HasThemeVariant;
import com.vaadin.flow.component.shared.HasTooltip;
import com.vaadin.flow.component.shared.HasValidationProperties;
import com.vaadin.flow.component.shared.InputField;
import com.vaadin.flow.component.shared.Tooltip;
import com.vaadin.flow.component.sidenav.SideNav;
import com.vaadin.flow.component.sidenav.SideNavItem;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.TabSheet;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.textfield.AbstractNumberField;
import com.vaadin.flow.component.textfield.BigDecimalField;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.HasAutocapitalize;
import com.vaadin.flow.component.textfield.HasAutocomplete;
import com.vaadin.flow.component.textfield.HasAutocorrect;
import com.vaadin.flow.component.textfield.HasPrefixAndSuffix;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldBase;
import com.vaadin.flow.component.timepicker.TimePicker;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.virtuallist.VirtualList;
import com.vaadin.flow.data.binder.HasDataProvider;
import com.vaadin.flow.data.binder.HasItemComponents;
import com.vaadin.flow.data.binder.HasItems;
import com.vaadin.flow.data.binder.HasValidator;
import com.vaadin.flow.data.event.SortEvent.SortNotifier;
import com.vaadin.flow.data.provider.HasDataGenerators;
import com.vaadin.flow.data.provider.HasDataView;
import com.vaadin.flow.data.provider.HasLazyDataView;
import com.vaadin.flow.data.provider.HasListDataView;
import com.vaadin.flow.data.provider.hierarchy.HasHierarchicalDataProvider;
import com.vaadin.flow.data.selection.MultiSelect;
import com.vaadin.flow.data.selection.SingleSelect;
import com.vaadin.flow.data.value.HasValueChangeMode;
import com.vaadin.flow.router.RouteParameters;
import com.vaadin.flow.router.Router;
import com.vaadin.flow.router.RouterLink;
import de.codecamp.vaadin.base.HasSingleComponent;
import de.codecamp.vaadin.base.i18n.AppLayoutI18nUtils;
import de.codecamp.vaadin.base.i18n.AvatarGroupI18nUtils;
import de.codecamp.vaadin.base.i18n.AvatarI18nUtils;
import de.codecamp.vaadin.base.i18n.BigDecimalFieldI18nUtils;
import de.codecamp.vaadin.base.i18n.CheckboxGroupI18nUtils;
import de.codecamp.vaadin.base.i18n.CheckboxI18nUtils;
import de.codecamp.vaadin.base.i18n.ComboBoxI18nUtils;
import de.codecamp.vaadin.base.i18n.DatePickerI18nUtils;
import de.codecamp.vaadin.base.i18n.DateTimePickerI18nUtils;
import de.codecamp.vaadin.base.i18n.EmailFieldI18nUtils;
import de.codecamp.vaadin.base.i18n.IntegerFieldI18nUtils;
import de.codecamp.vaadin.base.i18n.MenuBarI18nUtils;
import de.codecamp.vaadin.base.i18n.MessageInputI18nUtils;
import de.codecamp.vaadin.base.i18n.MultiSelectComboBoxI18nUtils;
import de.codecamp.vaadin.base.i18n.NumberFieldI18nUtils;
import de.codecamp.vaadin.base.i18n.PasswordFieldI18nUtils;
import de.codecamp.vaadin.base.i18n.RadioButtonGroupI18nUtils;
import de.codecamp.vaadin.base.i18n.SelectI18nUtils;
import de.codecamp.vaadin.base.i18n.SideNavI18nUtils;
import de.codecamp.vaadin.base.i18n.TextAreaI18nUtils;
import de.codecamp.vaadin.base.i18n.TextFieldI18nUtils;
import de.codecamp.vaadin.base.i18n.TimePickerI18nUtils;
import de.codecamp.vaadin.base.i18n.UploadI18nUtils;
import de.codecamp.vaadin.fluent.annotations.FluentMapping;
import de.codecamp.vaadin.fluent.annotations.FluentMappingSet;
import de.codecamp.vaadin.fluent.annotations.FluentMethod;
import de.codecamp.vaadin.fluent.annotations.ThemeGroup;
