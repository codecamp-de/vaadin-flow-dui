@FluentMappingSet( //
    basePackage = "de.codecamp.vaadin.fluent.pro",
    staticFactoryClass = "FluentPro")
@FluentMapping( //
    componentType = Board.class)
@FluentMapping( //
    componentType = Chart.class)
@FluentMapping( //
    componentType = CookieConsent.class)
@FluentMapping( //
    componentType = Crud.class,
    typeVars = {TYPEVAR_ITEM})
@FluentMapping( //
    componentType = GridPro.class,
    typeVars = {TYPEVAR_ITEM})
@FluentMapping( //
    componentType = RichTextEditor.class,
    themes = { //
        @ThemeGroup(variant = "LUMO_NO_BORDER"), //
        @ThemeGroup(variant = "LUMO_COMPACT"), //
    })
package de.codecamp.vaadin.fluent.pro;


import static de.codecamp.vaadin.fluent.annotations.FluentMapping.TYPEVAR_ITEM;

import com.vaadin.flow.component.board.Board;
import com.vaadin.flow.component.charts.Chart;
import com.vaadin.flow.component.cookieconsent.CookieConsent;
import com.vaadin.flow.component.crud.Crud;
import com.vaadin.flow.component.gridpro.GridPro;
import com.vaadin.flow.component.richtexteditor.RichTextEditor;
import de.codecamp.vaadin.fluent.annotations.FluentMapping;
import de.codecamp.vaadin.fluent.annotations.FluentMappingSet;
import de.codecamp.vaadin.fluent.annotations.ThemeGroup;
