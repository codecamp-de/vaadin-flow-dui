package de.codecamp.vaadin.fluent.shared;


import com.vaadin.flow.component.HasTheme;


@SuppressWarnings("unchecked")
public interface FluentHasThemeVariantGeneric<C extends HasTheme, F extends FluentHasThemeVariantGeneric<C, F, VARIANT>, VARIANT extends Enum<VARIANT>>
{

  default F themeVariants(VARIANT... variants)
  {
    removeThemeVariants(
        FluentInternalUtils.getThemeVariantEnumTypeGeneric(this).getEnumConstants());
    return addThemeVariants(variants);
  }

  default F themeVariant(VARIANT variant, boolean enabled)
  {
    if (enabled)
      return addThemeVariants(variant);
    else
      return removeThemeVariants(variant);
  }

  F addThemeVariants(VARIANT... variants);

  F removeThemeVariants(VARIANT... variants);

}
