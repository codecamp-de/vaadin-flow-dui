package de.codecamp.vaadin.fluent.shared;


import com.vaadin.flow.component.shared.ThemeVariant;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


final class FluentInternalUtils
{

  private static final ConcurrentMap<Class<?>, Class<? extends Enum<?>>> THEME_VARIANT_ENUMS =
      new ConcurrentHashMap<>();


  private FluentInternalUtils()
  {
    // utility class
  }


  @SuppressWarnings("unchecked")
  static <VARIANT extends ThemeVariant, F extends FluentHasThemeVariant<?, ?, VARIANT>> Class<VARIANT> getThemeVariantEnumType(
      F fluent)
  {
    return (Class<VARIANT>) THEME_VARIANT_ENUMS.computeIfAbsent(fluent.getClass(), f ->
    {
      Type variantType = null;
      hierarchyLoop: while (f != null)
      {
        for (Type type : f.getGenericInterfaces())
        {
          if (type instanceof ParameterizedType)
          {
            ParameterizedType ptype = (ParameterizedType) type;
            if (ptype.getRawType() == FluentHasThemeVariant.class)
            {
              variantType = ptype.getActualTypeArguments()[2];
              break hierarchyLoop;
            }
          }
        }
        f = f.getSuperclass();
      }

      if (variantType == null || !Enum.class.isAssignableFrom((Class<?>) variantType))
        throw new IllegalStateException("Failed to determine type of theme variant enum.");
      return (Class<? extends Enum<?>>) variantType;
    });
  }

  @SuppressWarnings("unchecked")
  static <VARIANT extends Enum<VARIANT>, F extends FluentHasThemeVariantGeneric<?, ?, VARIANT>> Class<VARIANT> getThemeVariantEnumTypeGeneric(
      F fluent)
  {
    return (Class<VARIANT>) THEME_VARIANT_ENUMS.computeIfAbsent(fluent.getClass(), f ->
    {
      Type variantType = null;
      hierarchyLoop: while (f != null)
      {
        for (Type type : f.getGenericInterfaces())
        {
          if (type instanceof ParameterizedType)
          {
            ParameterizedType ptype = (ParameterizedType) type;
            if (ptype.getRawType() == FluentHasThemeVariantGeneric.class)
            {
              variantType = ptype.getActualTypeArguments()[2];
              break hierarchyLoop;
            }
          }
        }
        f = f.getSuperclass();
      }

      if (variantType == null || !Enum.class.isAssignableFrom((Class<?>) variantType))
        throw new IllegalStateException("Failed to determine type of theme variant enum.");
      return (Class<? extends Enum<?>>) variantType;
    });
  }

}
