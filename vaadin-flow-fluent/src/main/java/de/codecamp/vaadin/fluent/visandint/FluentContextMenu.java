package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import de.codecamp.vaadin.fluent.shared.FluentHasOverlayClassName;


/**
 * The fluent counterpart of {@link ContextMenu}.
 */
public class FluentContextMenu
  extends
    FluentContextMenuBase<ContextMenu, FluentContextMenu, MenuItem, FluentMenuItem, SubMenu, FluentSubMenu>
  implements
    FluentHasMenuItems<ContextMenu, FluentContextMenu>,
    FluentHasOverlayClassName<ContextMenu, FluentContextMenu>
{

  /**
   * Constructs a new instance configuring a new {@link ContextMenu}.
   */
  public FluentContextMenu()
  {
    super(new ContextMenu());
  }

  /**
   * Constructs a new instance configuring the given {@link ContextMenu}.
   *
   * @param component
   *          the component to be configured
   */
  public FluentContextMenu(ContextMenu component)
  {
    super(component);
  }


  @Override
  public FluentMenuItem addItem(String text)
  {
    return new FluentMenuItem(get().addItem(text));
  }

  @Override
  public FluentMenuItem addItem(Component component)
  {
    return new FluentMenuItem(get().addItem(component));
  }

}
