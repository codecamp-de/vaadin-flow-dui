package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.contextmenu.ContextMenuBase;
import com.vaadin.flow.component.contextmenu.ContextMenuBase.OpenedChangeEvent;
import com.vaadin.flow.component.contextmenu.MenuItemBase;
import com.vaadin.flow.component.contextmenu.SubMenuBase;
import de.codecamp.vaadin.fluent.FluentComponent;
import de.codecamp.vaadin.fluent.FluentHasComponents;
import de.codecamp.vaadin.fluent.FluentHasStyle;
import java.util.function.Consumer;


/**
 * The fluent counterpart of {@link ContextMenuBase}.
 *
 * @param <CM>
 *          the context menu type
 * @param <FCM>
 *          the fluent context menu type
 * @param <MI>
 *          the menu item type
 * @param <FMI>
 *          the fluent menu item type
 * @param <SM>
 *          the sub menu type
 * @param <FSM>
 *          the fluent sub menu type
 */
@SuppressWarnings("unchecked")
public abstract class FluentContextMenuBase<CM extends ContextMenuBase<CM, MI, SM>, FCM extends FluentContextMenuBase<CM, FCM, MI, FMI, SM, FSM>, MI extends MenuItemBase<CM, MI, SM>, FMI extends FluentMenuItemBase<MI, FMI, CM, FCM, SM, FSM>, SM extends SubMenuBase<CM, MI, SM>, FSM extends FluentSubMenuBase<SM, FSM, MI, FMI, CM, FCM>>
  extends
    FluentComponent<CM, FCM>
  implements
    FluentHasComponents<CM, FCM>,
    FluentHasStyle<CM, FCM>
{

  /**
   * Constructs a new instance configuring the given {@link FluentContextMenuBase}.
   *
   * @param component
   *          the context menu to be configured
   */
  protected FluentContextMenuBase(CM component)
  {
    super(component);
  }


  /**
   * @param target
   *          the target component
   * @return this
   * @see ContextMenuBase#setTarget(Component)
   */
  public FCM target(Component target)
  {
    get().setTarget(target);
    return (FCM) this;
  }


  /**
   * @param text
   *          the text content for the created menu item
   * @return the new fluent menu item
   * @see ContextMenuBase#addItem(String)
   */
  public abstract FMI addItem(String text);

  /**
   * @param component
   *          the component to add to the created menu item
   * @return the new fluent menu item
   * @see ContextMenuBase#addItem(Component)
   */
  public abstract FMI addItem(Component component);

  /**
   * @param text
   *          the text content for the created menu item
   * @param menuItemConfigurer
   *          the configurer to apply to the menu item
   * @return this
   * @see ContextMenuBase#addItem(String)
   */
  public FCM addItem(String text, Consumer<FMI> menuItemConfigurer)
  {
    FMI item = addItem(text);
    if (menuItemConfigurer != null)
      menuItemConfigurer.accept(item);
    return (FCM) this;
  }

  /**
   * @param component
   *          the component to add to the created menu item
   * @param menuItemConfigurer
   *          the configurer to apply to the menu item
   * @return this
   * @see ContextMenuBase#addItem(Component)
   */
  public FCM addItem(Component component, Consumer<FMI> menuItemConfigurer)
  {
    FMI item = addItem(component);
    if (menuItemConfigurer != null)
      menuItemConfigurer.accept(item);
    return (FCM) this;
  }


  /**
   * @return this
   * @see ContextMenuBase#setOpenOnClick(boolean)
   */
  public FCM openOnClick()
  {
    return openOnClick(true);
  }

  /**
   * @param openOnClick
   *          whether to open on click
   * @return this
   * @see ContextMenuBase#setOpenOnClick(boolean)
   */
  public FCM openOnClick(boolean openOnClick)
  {
    get().setOpenOnClick(openOnClick);
    return (FCM) this;
  }

  /**
   * @return this
   * @see ContextMenuBase#close()
   */
  public FCM close()
  {
    get().close();
    return (FCM) this;
  }

  /**
   * @param listener
   *          the listener
   * @return this
   * @see ContextMenuBase#addOpenedChangeListener(ComponentEventListener)
   */
  public FCM onOpenedChange(ComponentEventListener<OpenedChangeEvent<CM>> listener)
  {
    get().addOpenedChangeListener(listener);
    return (FCM) this;
  }

}
