package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu;
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu.GridContextMenuOpenedEvent;
import com.vaadin.flow.component.grid.contextmenu.GridMenuItem;
import com.vaadin.flow.component.grid.contextmenu.GridSubMenu;
import com.vaadin.flow.function.SerializablePredicate;


/**
 * The fluent counterpart of {@link GridContextMenu}.
 *
 * @param <ITEM>
 *          the item type
 */
public class FluentGridContextMenu<ITEM>
  extends
    FluentContextMenuBase<GridContextMenu<ITEM>, FluentGridContextMenu<ITEM>, GridMenuItem<ITEM>, FluentGridMenuItem<ITEM>, GridSubMenu<ITEM>, FluentGridSubMenu<ITEM>>
  implements
    FluentHasGridMenuItems<GridContextMenu<ITEM>, FluentGridContextMenu<ITEM>, ITEM>
{

  /**
   * Constructs a new instance configuring a new {@link GridContextMenu}.
   */
  public FluentGridContextMenu()
  {
    super(new GridContextMenu<>());
  }

  /**
   * Constructs a new instance configuring the given {@link GridContextMenu}.
   *
   * @param component
   *          the component to be configured
   */
  public FluentGridContextMenu(GridContextMenu<ITEM> component)
  {
    super(component);
  }


  @Override
  public FluentGridMenuItem<ITEM> addItem(String text)
  {
    return new FluentGridMenuItem<>(get().addItem(text));
  }

  @Override
  public FluentGridMenuItem<ITEM> addItem(Component component)
  {
    return new FluentGridMenuItem<>(get().addItem(component));
  }

  /**
   * @param dynamicContentHandler
   *          the dynamic content handler
   * @return this
   * @see GridContextMenu#setDynamicContentHandler(SerializablePredicate)
   */
  public FluentGridContextMenu<ITEM> dynamicContentHandler(
      SerializablePredicate<ITEM> dynamicContentHandler)
  {
    get().setDynamicContentHandler(dynamicContentHandler);
    return this;
  }


  /**
   * @param listener
   *          the listener
   * @return this
   * @see GridContextMenu#addGridContextMenuOpenedListener(ComponentEventListener)
   */
  public FluentGridContextMenu<ITEM> onGridContextMenuOpened(
      ComponentEventListener<GridContextMenuOpenedEvent<ITEM>> listener)
  {
    get().addGridContextMenuOpenedListener(listener);
    return this;
  }

}
