package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu;
import com.vaadin.flow.component.grid.contextmenu.GridMenuItem;
import com.vaadin.flow.component.grid.contextmenu.GridSubMenu;


/**
 * The fluent counterpart of {@link GridMenuItem}.
 *
 * @param <ITEM>
 *          the item type
 */
public class FluentGridMenuItem<ITEM>
  extends
    FluentMenuItemBase<GridMenuItem<ITEM>, FluentGridMenuItem<ITEM>, GridContextMenu<ITEM>, FluentGridContextMenu<ITEM>, GridSubMenu<ITEM>, FluentGridSubMenu<ITEM>>
{

  /**
   * Constructs a new instance configuring the given {@link GridMenuItem}.
   *
   * @param component
   *          the component to be configured
   */
  public FluentGridMenuItem(GridMenuItem<ITEM> component)
  {
    super(component);
  }


  @Override
  public FluentGridSubMenu<ITEM> subMenu()
  {
    return new FluentGridSubMenu<>(get().getSubMenu());
  }


  /**
   * @param listener
   *          the listener
   * @return this
   * @see GridMenuItem#addMenuItemClickListener(ComponentEventListener)
   */
  public FluentGridMenuItem<ITEM> onMenuItemClick(
      ComponentEventListener<GridContextMenu.GridContextMenuItemClickEvent<ITEM>> listener)
  {
    get().addMenuItemClickListener(listener);
    return this;
  }

}
