package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu;
import com.vaadin.flow.component.grid.contextmenu.GridMenuItem;
import com.vaadin.flow.component.grid.contextmenu.GridSubMenu;


/**
 * The fluent counterpart of {@link GridSubMenu}.
 *
 * @param <ITEM>
 *          the item type
 */
public class FluentGridSubMenu<ITEM>
  extends
    FluentSubMenuBase<GridSubMenu<ITEM>, FluentGridSubMenu<ITEM>, GridMenuItem<ITEM>, FluentGridMenuItem<ITEM>, GridContextMenu<ITEM>, FluentGridContextMenu<ITEM>>
  implements
    FluentHasGridMenuItems<GridSubMenu<ITEM>, FluentGridSubMenu<ITEM>, ITEM>
{

  /**
   * Constructs a new instance configuring the given {@link GridSubMenu}.
   *
   * @param subMenu
   *          the sub menu to be configured
   */
  public FluentGridSubMenu(GridSubMenu<ITEM> subMenu)
  {
    super(subMenu);
  }


  @Override
  public FluentGridMenuItem<ITEM> addItem(String text)
  {
    return new FluentGridMenuItem<>(get().addItem(text));
  }

  @Override
  public FluentGridMenuItem<ITEM> addItem(Component component)
  {
    return new FluentGridMenuItem<>(get().addItem(component));
  }

}
