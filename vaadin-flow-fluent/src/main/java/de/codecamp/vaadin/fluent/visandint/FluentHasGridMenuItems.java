package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu;
import java.util.function.Consumer;


/**
 * The fluent counterpart of {@link com.vaadin.flow.component.grid.contextmenu.HasGridMenuItems}.
 *
 * @param <C>
 *          the component type
 * @param <F>
 *          the fluent type
 * @param <ITEM>
 *          the item type
 * @see com.vaadin.flow.component.grid.contextmenu.HasGridMenuItems
 */
@SuppressWarnings("unchecked")
// C should extend HasGridMenuItems<ITEM> but it is package protected
public interface FluentHasGridMenuItems<C, F extends FluentHasGridMenuItems<C, F, ITEM>, ITEM>
{

  /**
   * @param text
   *          the text to add
   * @return the new menu item
   * @see GridContextMenu#addItem(String)
   */
  FluentGridMenuItem<ITEM> addItem(String text);

  /**
   * @param text
   *          the text to add
   * @param menuItemConfigurer
   *          the configurer to apply to the menu item
   * @return this
   * @see com.vaadin.flow.component.grid.contextmenu.HasGridMenuItems#addItem(String,
   *      com.vaadin.flow.component.ComponentEventListener)
   */
  default F addItem(String text, Consumer<FluentGridMenuItem<ITEM>> menuItemConfigurer)
  {
    menuItemConfigurer.accept(addItem(text));
    return (F) this;
  }

  /**
   * @param component
   *          the component to add
   * @return the new menu item
   * @see GridContextMenu#add(Component...)
   */
  FluentGridMenuItem<ITEM> addItem(Component component);

  /**
   * @param component
   *          the component to add
   * @param menuItemConfigurer
   *          the configurer to apply to the menu item
   * @return this
   * @see com.vaadin.flow.component.grid.contextmenu.HasGridMenuItems#addItem(Component,
   *      com.vaadin.flow.component.ComponentEventListener)
   */
  default F addItem(Component component, Consumer<FluentGridMenuItem<ITEM>> menuItemConfigurer)
  {
    menuItemConfigurer.accept(addItem(component));
    return (F) this;
  }

}
