package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import de.codecamp.vaadin.fluent.FluentHasElement;
import de.codecamp.vaadin.fluent.shared.FluentHasThemeVariantGeneric;


@SuppressWarnings("unchecked")
public interface FluentHasGridVariant<C extends Grid<?>, F extends FluentHasGridVariant<C, F>>
  extends
    FluentHasThemeVariantGeneric<C, F, GridVariant>,
    FluentHasElement<C, F>
{

  @Override
  default F addThemeVariants(GridVariant... variants)
  {
    get().addThemeVariants(variants);
    return (F) this;
  }

  @Override
  default F removeThemeVariants(GridVariant... variants)
  {
    get().removeThemeVariants(variants);
    return (F) this;
  }

  default F border()
  {
    return border(true);
  }

  default F border(boolean border)
  {
    return themeVariant(GridVariant.LUMO_NO_BORDER, !border);
  }

  default F rowBorders()
  {
    return rowBorders(true);
  }

  default F rowBorders(boolean rowBorders)
  {
    return themeVariant(GridVariant.LUMO_NO_ROW_BORDERS, !rowBorders);
  }

  default F columnBorders()
  {
    return columnBorders(true);
  }

  default F columnBorders(boolean columnBorders)
  {
    return themeVariant(GridVariant.LUMO_COLUMN_BORDERS, columnBorders);
  }

  default F rowStripes()
  {
    return rowStripes(true);
  }

  default F rowStripes(boolean rowStripes)
  {
    return themeVariant(GridVariant.LUMO_ROW_STRIPES, rowStripes);
  }

  default F compact()
  {
    return compact(true);
  }

  default F compact(boolean compact)
  {
    return themeVariant(GridVariant.LUMO_COMPACT, compact);
  }

  default F wrapCellContent()
  {
    return wrapCellContent(true);
  }

  default F wrapCellContent(boolean wrapCellContent)
  {
    return themeVariant(GridVariant.LUMO_WRAP_CELL_CONTENT, wrapCellContent);
  }

}
