package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import de.codecamp.vaadin.fluent.FluentClickNotifier;


/**
 * The fluent counterpart of {@link MenuItem}.
 */
public class FluentMenuItem
  extends
    FluentMenuItemBase<MenuItem, FluentMenuItem, ContextMenu, FluentContextMenu, SubMenu, FluentSubMenu>
  implements
    FluentClickNotifier<MenuItem, FluentMenuItem>
{

  /**
   * Constructs a new instance configuring the given {@link MenuItem}.
   *
   * @param component
   *          the component to be configured
   */
  public FluentMenuItem(MenuItem component)
  {
    super(component);
  }


  @Override
  public FluentSubMenu subMenu()
  {
    return new FluentSubMenu(get().getSubMenu());
  }

}
