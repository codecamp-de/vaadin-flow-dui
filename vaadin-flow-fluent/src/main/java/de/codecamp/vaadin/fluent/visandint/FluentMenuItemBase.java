package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.contextmenu.ContextMenuBase;
import com.vaadin.flow.component.contextmenu.MenuItemBase;
import com.vaadin.flow.component.contextmenu.SubMenuBase;
import de.codecamp.vaadin.fluent.FluentComponent;
import de.codecamp.vaadin.fluent.FluentHasAriaLabel;
import de.codecamp.vaadin.fluent.FluentHasComponents;
import de.codecamp.vaadin.fluent.FluentHasEnabled;
import de.codecamp.vaadin.fluent.FluentHasText;
import java.util.function.Consumer;


/**
 * The fluent counterpart of {@link MenuItemBase}.
 *
 * @param <MI>
 *          the menu item type
 * @param <FMI>
 *          the fluent menu item type
 * @param <CM>
 *          the context menu type
 * @param <FCM>
 *          the fluent context menu type
 * @param <SM>
 *          the sub menu type
 * @param <FSM>
 *          the fluent sub menu type
 */
@SuppressWarnings("unchecked")
public abstract class FluentMenuItemBase<MI extends MenuItemBase<CM, MI, SM>, FMI extends FluentMenuItemBase<MI, FMI, CM, FCM, SM, FSM>, CM extends ContextMenuBase<CM, MI, SM>, FCM extends FluentContextMenuBase<CM, FCM, MI, FMI, SM, FSM>, SM extends SubMenuBase<CM, MI, SM>, FSM extends FluentSubMenuBase<SM, FSM, MI, FMI, CM, FCM>>
  extends
    FluentComponent<MI, FMI>
  implements
    FluentHasAriaLabel<MI, FMI>,
    FluentHasComponents<MI, FMI>,
    FluentHasEnabled<MI, FMI>,
    FluentHasText<MI, FMI>
{

  /**
   * Constructs a new instance configuring the given {@link MenuItemBase}.
   *
   * @param component
   *          the menu item to be configured
   */
  protected FluentMenuItemBase(MI component)
  {
    super(component);
  }


  /**
   * @return the fluent sub menu
   */
  public abstract FSM subMenu();

  /**
   * @param subMenuConfigurer
   *          the configurer to apply to the sub menu
   * @return this
   */
  public FMI subMenu(Consumer<FSM> subMenuConfigurer)
  {
    subMenuConfigurer.accept(subMenu());
    return (FMI) this;
  }


  /**
   * @return this
   * @see MenuItemBase#setCheckable(boolean)
   */
  public FMI checkable()
  {
    return checkable(true);
  }

  /**
   * @param checkable
   *          whether the menu item should be checkable
   * @return this
   * @see MenuItemBase#setCheckable(boolean)
   */
  public FMI checkable(boolean checkable)
  {
    get().setCheckable(checkable);
    return (FMI) this;
  }

  /**
   * @return this
   * @see MenuItemBase#setChecked(boolean)
   */
  public FMI checked()
  {
    return checked(true);
  }

  /**
   * @param checked
   *          whether the menu item should be checked
   * @return this
   * @see MenuItemBase#setChecked(boolean)
   */
  public FMI checked(boolean checked)
  {
    get().setChecked(checked);
    return (FMI) this;
  }

  /**
   * @return this
   * @see MenuItemBase#setKeepOpen(boolean)
   */
  public FMI keepOpen()
  {
    return keepOpen(true);
  }

  /**
   * @param keepOpen
   *          whether to keep the menu item open
   * @return this
   * @see MenuItemBase#setKeepOpen(boolean)
   */
  public FMI keepOpen(boolean keepOpen)
  {
    get().setKeepOpen(keepOpen);
    return (FMI) this;
  }


  /**
   * @param themeNames
   *          the themes to add
   * @return this
   * @see MenuItemBase#addThemeNames(String...)
   */
  public FMI addThemeNames(String... themeNames)
  {
    get().addThemeNames(themeNames);
    return (FMI) this;
  }

  /**
   * @param themeNames
   *          the themes to remove
   * @return this
   * @see MenuItemBase#removeThemeNames(String...)
   */
  public FMI removeThemeNames(String... themeNames)
  {
    get().removeThemeNames(themeNames);
    return (FMI) this;
  }

}
