package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;


/**
 * The fluent counterpart of {@link SubMenu}.
 */
public class FluentSubMenu
  extends
    FluentSubMenuBase<SubMenu, FluentSubMenu, MenuItem, FluentMenuItem, ContextMenu, FluentContextMenu>
  implements
    FluentHasMenuItems<SubMenu, FluentSubMenu>
{

  /**
   * Constructs a new instance configuring the given {@link SubMenu}.
   *
   * @param subMenu
   *          the sub menu to be configured
   */
  public FluentSubMenu(SubMenu subMenu)
  {
    super(subMenu);
  }


  @Override
  public FluentMenuItem addItem(String text)
  {
    return new FluentMenuItem(get().addItem(text));
  }

  @Override
  public FluentMenuItem addItem(Component component)
  {
    return new FluentMenuItem(get().addItem(component));
  }

  /**
   * @return this
   * @see SubMenu#addSeparator()
   */
  public FluentSubMenu addSeparator()
  {
    get().addSeparator();
    return this;
  }

}
