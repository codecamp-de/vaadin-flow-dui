package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.contextmenu.ContextMenuBase;
import com.vaadin.flow.component.contextmenu.MenuItemBase;
import com.vaadin.flow.component.contextmenu.SubMenuBase;
import de.codecamp.vaadin.fluent.FluentWrapper;
import java.util.function.Consumer;


/**
 * The fluent counterpart of {@link SubMenuBase}.
 *
 * @param <SM>
 *          the sub menu type
 * @param <FSM>
 *          the fluent sub menu type
 * @param <MI>
 *          the menu item type
 * @param <FMI>
 *          the fluent menu item type
 * @param <CM>
 *          the context menu type
 * @param <FCM>
 *          the fluent context menu
 */
@SuppressWarnings("unchecked")
public abstract class FluentSubMenuBase<SM extends SubMenuBase<CM, MI, SM>, FSM extends FluentSubMenuBase<SM, FSM, MI, FMI, CM, FCM>, MI extends MenuItemBase<CM, MI, SM>, FMI extends FluentMenuItemBase<MI, FMI, CM, FCM, SM, FSM>, CM extends ContextMenuBase<CM, MI, SM>, FCM extends FluentContextMenuBase<CM, FCM, MI, FMI, SM, FSM>>
  extends
    FluentWrapper<SM, FSM>
{

  /**
   * Constructs a new instance configuring the given {@link SubMenuBase}.
   *
   * @param wrapped
   *          the sub menu to be configured
   */
  protected FluentSubMenuBase(SM wrapped)
  {
    super(wrapped);
  }


  /**
   * @param text
   *          the text content for the created menu item
   * @return the new fluent menu item
   * @see SubMenuBase#addItem(String)
   */
  public abstract FMI addItem(String text);

  /**
   * @param component
   *          the component to add to the created menu item
   * @return the new fluent menu item
   * @see SubMenuBase#addItem(Component)
   */
  public abstract FMI addItem(Component component);

  /**
   * @param text
   *          the text content for the created menu item
   * @param menuItemConfigurer
   *          the configurer to apply to the menu item
   * @return this
   * @see SubMenuBase#addItem(String)
   */
  public FSM addItem(String text, Consumer<FMI> menuItemConfigurer)
  {
    FMI item = addItem(text);
    if (menuItemConfigurer != null)
      menuItemConfigurer.accept(item);
    return (FSM) this;
  }

  /**
   * @param component
   *          the component to add to the created menu item
   * @param menuItemConfigurer
   *          the configurer to apply to the menu item
   * @return this
   * @see SubMenuBase#addItem(Component)
   */
  public FSM addItem(Component component, Consumer<FMI> menuItemConfigurer)
  {
    FMI item = addItem(component);
    if (menuItemConfigurer != null)
      menuItemConfigurer.accept(item);
    return (FSM) this;
  }


  /**
   * @param components
   *          the components to add
   * @return this
   * @see SubMenuBase#add(Component...)
   */
  public FSM add(Component... components)
  {
    get().add(components);
    return (FSM) this;
  }

  /**
   * @param index
   *          the index where to insert the components
   * @param components
   *          the components to add
   * @return this
   * @see SubMenuBase#addComponentAtIndex(int, Component)
   */
  public FSM addAt(int index, Component... components)
  {
    for (int i = 0; i < components.length; i++)
      get().addComponentAtIndex(index + i, components[i]);
    return (FSM) this;
  }

  /**
   * @param components
   *          the components to remove
   * @return this
   * @see SubMenuBase#remove(Component...)
   */
  public FSM remove(Component... components)
  {
    get().remove(components);
    return (FSM) this;
  }

  /**
   * @return this
   * @see SubMenuBase#removeAll()
   */
  public FSM removeAll()
  {
    get().removeAll();
    return (FSM) this;
  }

}
