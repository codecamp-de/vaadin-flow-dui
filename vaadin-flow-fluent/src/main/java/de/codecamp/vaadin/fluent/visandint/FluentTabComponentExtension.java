package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.TabSheet;
import com.vaadin.flow.component.tabs.Tabs;
import de.codecamp.vaadin.fluent.FluentComponent;
import de.codecamp.vaadin.fluent.FluentComponentExtension;


/**
 * {@link FluentComponentExtension} for {@link Tabs} and {@link TabSheet}. Only implemented by
 * {@link FluentTab}.
 *
 * @param <C>
 *          the component type
 * @param <F>
 *          the fluent component type
 * @see Tabs
 * @see TabSheet
 */
@SuppressWarnings("unchecked")
public interface FluentTabComponentExtension<C extends Tab, F extends FluentComponent<C, F>>
  extends
    FluentComponentExtension<C, F>
{

  /**
   * Adds this tab to the given {@link Tabs}.
   *
   * @param container
   *          the container
   * @return this
   * @see Tabs#add(Tab...)
   */
  default F addToAsTab(Tabs container)
  {
    container.add(get());
    return (F) this;
  }

  /**
   * Adds this tab to the given {@link Tabs} at the specified index.
   *
   * @param container
   *          the container
   * @param index
   *          the index where to insert the tab
   * @return this
   * @see Tabs#addTabAtIndex(int, Tab)
   */
  default F addToAsTabAt(Tabs container, int index)
  {
    container.addTabAtIndex(index, get());
    return (F) this;
  }

  /**
   * Replaces the specified old tab component in the given {@link Tabs} with this tab.
   *
   * @param container
   *          the container
   * @param oldTab
   *          the old tab to be replaced
   * @return this
   * @see Tabs#replace(Tab, Tab)
   */
  default F replace(Tabs container, Tab oldTab)
  {
    container.replace(oldTab, get());
    return (F) this;
  }


  /**
   * Adds this component as tab to the given {@link TabSheet} with the given component as content.
   *
   * @param container
   *          the container
   * @param content
   *          the content of the tab
   * @return this
   * @see TabSheet#add(Tab, Component)
   */
  default F addToAsTab(TabSheet container, Component content)
  {
    container.add(get(), content);
    return (F) this;
  }

  /**
   * Adds this component as tab to the given {@link TabSheet} at the specified index with the given
   * component as content.
   *
   * @param container
   *          the container
   * @param index
   *          the index where to insert the tab
   * @param content
   *          the content of the tab
   * @return this
   * @see TabSheet#add(Tab, Component, int)
   */
  default F addToAsTabAt(TabSheet container, int index, Component content)
  {
    container.add(get(), content, index);
    return (F) this;
  }

}
