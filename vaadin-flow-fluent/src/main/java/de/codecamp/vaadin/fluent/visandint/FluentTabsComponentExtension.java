package de.codecamp.vaadin.fluent.visandint;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.TabSheet;
import de.codecamp.vaadin.fluent.FluentComponent;
import de.codecamp.vaadin.fluent.FluentComponentExtension;


@SuppressWarnings("unchecked")
public interface FluentTabsComponentExtension<C extends Component, F extends FluentComponent<C, F>>
  extends
    FluentComponentExtension<C, F>
{

  /**
   * Adds this component as tab content to the given {@link TabSheet} container with the given
   * {@link Tab} component.
   *
   * @param container
   *          the container
   * @param tab
   *          the tab component for this content component
   * @return this
   * @see TabSheet#add(Tab, Component)
   */
  default F addTo(TabSheet container, Tab tab)
  {
    container.add(tab, get());
    return (F) this;
  }

  /**
   * Adds this component as tab content to the given {@link TabSheet} container at the specified
   * index with the given {@link Tab} component.
   *
   * @param container
   *          the container
   * @param index
   *          the index where to insert the tab
   * @param tab
   *          the tab component for this content component
   * @return this
   * @see TabSheet#add(Tab, Component, int)
   */
  default F addToAt(TabSheet container, int index, Tab tab)
  {
    container.add(tab, get(), index);
    return (F) this;
  }

}
